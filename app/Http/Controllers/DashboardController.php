<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Models\TaxElejarDetailCalendar;
use App\Models\TaxElejarDetailCurrent;
use App\Models\MngAnnouncement;
use App\Models\TaxAttachment;
use Illuminate\Http\Request;
use App\Models\MngQuestion;
use App\Models\UserSetting;
use App\Models\MngService;
use App\Models\TaxBrosure;
use App\Data\Repo\ApiRepo;
use App\Models\WebauthnKey;
use App\Models\TaxProfile;
use App\Models\MngMobile;
use App\Models\TaxElejar;
use App\Models\TaxCp500;
use App\Models\TaxEspc;
use App\Models\comlist;
use App\Models\MngHelp;
use App\Models\MngApp;
use App\User;
use Jenssegers\Agent\Agent;
use DateTimeZone;
use Redirect;
use hash_hmac;
use DateTime;
use Curl;
use Auth;
use PDF;


class DashboardController extends BaseController
{
   public function __construct(ApiRepo $repos)
   {
      
        $this->repos = $repos;  
    
   }

    public function faql(){

         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         // $listfaq = MngQuestion::where('status','1')
         //            ->with('answer')
         //            ->get();

      $listfaq=MngQuestion::with(['answer' => function($author) {

              $author->where('status', 1);

      }])->get();
                   
         return view('dashboard.faq',compact('listfaq','user')); 

      
    }
    function faqs(){

     

         // $listfaq = MngQuestion::where('status','1')->with('answer')->get();

         $listfaq=MngQuestion::with(['answer' => function($author) {

                $author->where('status', 1);

        }])->get();
         
         $idtype = '';
         return view('dashboard.faqx',compact('listfaq','idtype')); 
    }

     function brochure(){
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
       return view('dashboard.brochure',compact('user')); 
    }

    function contact(){
     
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
       
        return view('dashboard.contact',compact('user')); 
      
    }

    function contacts(){

        return view('dashboard.contactS'); 
    }

    function surat(){


      $locale = \App::getLocale();
      $surat = $this->repos->api_cp500surat();

      if($locale == 'en')
      {

        return view('cp500s.bi',compact('surat')); 

      }else{

         return view('cp500s.bm',compact('surat')); 

      }
    }




    function brochures(){
       
       
       return view('dashboard.brochures'); 
    }
    function helps()
    {

          $list = MngHelp::where('status','=','1')->get();
        return view('dashboard.helps',compact('list')); 
  
    }
    function help()
    {

     
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        $list = MngHelp::where('status','=','1')->get();
        return view('dashboard.help',compact('list','user')); 
      
    }

    function announcement($ids){
       

       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();
       $list = MngAnnouncement::where('status','1')->orderBy('created_at','DESC')->get();
       return view('dashboard.announcement',compact('list','user','ids')); 
    }

    function mobileannouncement($ids){
       

       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();
       $announcement = MngAnnouncement::where('status','1')->get();
       return view('mobile.announcement',compact('announcement','user','ids')); 
    }

    function mobileviewannouncement($ids){
       

       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();
       $data = MngAnnouncement::where('id','=',$ids)->first();
       return view('mobile.readannouncement',compact('data','user')); 
    }

    function announcementread($ids){
       

       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();
       $data = MngAnnouncement::where('id','=',$ids)->first();
       return view('announcement.read',compact('data','user')); 
    }

    function apps(){

       if(Auth::check())
        {
           $users= auth()->user();
           $id = $users->id;
           $user= User::where('id',$id)->first();
           $list = MngService::where('status','=',1)->where('acl', 'like', "%{$user->access}%")->orderBy('order','ASC')->get();

           $checkfeatured = UserSetting::where('fk_users','=',$id)->where('key','=','featured')->first();

           if($checkfeatured)
           {
                if($checkfeatured->value)
                {
                    $array = json_decode($checkfeatured->value);
                    $appfav = MngService::whereIn('id',$array)->get();
                    $appfavcount = MngService::whereIn('id',$array)->count();

                    // dd($appfavcount);
                }else{

                    $appfavcount = 0;
                    $appfav = [];
                    // dd($appfavcount);
                }
            }else
            {
                $appfavcount = 0;
                $appfav = [];
            }


           return view('dashboard.apps',compact('list','user','appfav','appfavcount')); 

        }else
        {   
           $list = MngService::where('status','=',1)->get();
           $error = "You need to be logged in";
           return view('auth.login',compact('error','list'));
        }

      
    }

    function taxstatus(){

      if(Auth::check())
      {
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         $surat = $this->repos->api_cp500surat();
         // dd($surat);
         $comlist = $this->repos->api_comlist();
         $apidata = $this->repos->api_refundcom();
         $elejar = $this->repos->api_elejar();
         $cp500 = $this->repos->api_cp500();
         $cp204data = $this->repos->api_cp204();
         $spcdata = $this->repos->api_spccom();

         
         $spcdata = TaxEspc::where('fk_users','=',$id)->count();
         if($spcdata > 0)
         {
             $spcdata = TaxEspc::where('fk_users','=',$id)->orderBy('TkhLoad')->get();

             foreach ($spcdata as $key => $value) 
             {

                   if($user->language == 'en')
                    {
                        
                        if($value->statbi)
                        {
                          $stats =  $value->statbi;

                        }else{

                           $stats =  $value->stat;
                        }
                    }else{

                         $stats =  $value->stat;
                    }
                  
                  $datastaff[$value->taxp_itrefno][$value->empl_ref_no][$value->name][]=[

                      'stat' => $stats,                      
                      'date' => $value->TkhLoad,
                      'Amt' => $value->Amt,
                  ];
             }

             $spc = [

                  'emp' =>$datastaff,
             ];

         }else{

            $spc =[];
         }

         // dd($spc);

         
         $checkprofile = TaxProfile::where('fk_users','=',$user->id)->first();       
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->get();
         $lejarcom = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','SYARIKAT')->get();
         $cp500data = TaxCp500::where('fk_users','=',$id)->whereNotNull('refid')->first();          
         $list = MngService::where('status','1')->get();
         $comlist = comlist::where('fk_users','=',$user->id)->get();

         $check1 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                   ->whereNull('lejar_type')
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check2 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                   ->where('lejar_type','=','INDIVIDU')
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $check3 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                   ->whereNull('lejar_type')
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check4 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                   ->where('lejar_type','=','INDIVIDU')
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $checksal = $check1+$check2;
         $checkpro = $check3+$check4;

         return view('dashboard.taxstatus',compact('list','spc','user','lejar','cp500data','checkprofile','apidata','cp204data','lejarcom','comlist','checksal','checkpro','surat')); 

      }else
      {   
          \Session::put('locale', 'ms');
          $list = MngService::where('status','=',1)->get();
          return view('auth.login',compact('list'));
      }
    }

    function mobilespc()
    {

      if(Auth::check())
      {
          $users= auth()->user();
          $id = $users->id;
          $user= User::where('id',$id)->first();
          $spcdata = $this->repos->api_spclist();
          $spcdata = TaxEspc::where('fk_users','=',$id)->count();
          if($spcdata > 0)
          {
              $spcdata = TaxEspc::where('fk_users','=',$id)->orderBy('TkhLoad')->get();
              foreach ($spcdata as $key => $value) 
              {
                    
                  $datastaff[$value->taxp_itrefno][$value->empl_ref_no][$value->name][]=[
                      'stat' => $value->stat,
                      'date' => $value->TkhLoad,
                  ];
              }

              $spc = [

                  'emp' =>$datastaff,
              ];
          }else{

              $spc =[];
          }

           return view('mobile.spc',compact('spc')); 

      }else{   

            return redirect('mobile');
      }
  }

    function mobilecp500()
    {
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         $cp500 = $this->repos->api_cp500();
         $cp500data = TaxCp500::where('fk_users','=',$id)->first();          

         return view('mobile.cp500',compact('cp500data')); 
    }

    function mobilecp204()
    {
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         $cp204data = $this->repos->api_cp204();  

         return view('mobile.cp204',compact('cp204data')); 
    }

    function mobilerefund()
    {

       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();

       $apidata = $this->repos->api_refundcom();


       return view('mobile.refund',compact('apidata','user')); 
    }

    

    // mobile 
    function taxstatusmobile(){

      if(Auth::check())
      {

         return view('mobile.taxstatus'); 

      }else
      {   
          \Session::put('locale', 'ms');
          return view('auth.login');
      }
    }

    function profilemobile()
    {

      if(Auth::check())
        {
             $users= auth()->user();
             $id = $users->id;
             $user= User::where('id',$id)->first();
             
             $comlist = $this->repos->api_comlist();

             $dataprofile = $this->repos->api_profile_page();
             $dataprofiles = $this->repos->api_profile_add();
             $comlist = comlist::where('fk_users','=',$user->id)->get();
             $comlistcount = comlist::where('fk_users','=',$user->id)->count();
             $profile = TaxProfile::where('fk_users','=',$id)->first();
             $webau = WebauthnKey::where('user_id','=',$id)->first();

       return view('mobile.mobileprofile',compact('user','profile','comlist','comlistcount','dataprofile','webau'));



        }else
        {   
            \Session::put('locale', 'ms');
            $agent = new Agent();
            $device = $agent->device();
            $mobile = MngMobile::where('status','=',1)->get();
            return view('auth.mobilelogin',compact('device','mobile'));
        }
    }

    function appmobile()
    {
      $agent = new Agent();
      $device = 'PC';
      if( $agent->isiOS() ){
           $device = 'IOS';
      }

      if( $agent->isAndroidOS() ){

           $device = 'ANDROID';
      }

      if(Auth::check())
      {
          $user = auth()->user();


                   
          $mobile = MngMobile::where('status','=',1)->get();
          return view('mobile.mobileapp',compact('user','mobile','device'));

      }else
      {   
          \Session::put('locale', 'ms');
          return redirect('mobile');
      }
    }

    function mobileapps($ids){

       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();
       $list = MngService::where('status','1')->orderBy('order','ASC')->get();
       return view('mobile.apps',compact('list','user','ids')); 
    }

    

    function delweb($id){
       $webau = WebauthnKey::where('id','=',$id)->delete();

       return redirect::to('mobile/profile');
    }
     
}
