<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\Models\TaxElejarDetailCalendar;
use App\Models\TaxElejarDetailCurrent;
use App\Models\MngAnnouncement;
use App\Models\TaxElejarDetail;
use App\Models\MngQuestion;
use App\Models\TaxProfile;
use App\Data\Repo\ApiRepo;
use App\Models\MngService;
use App\Models\TaxElejar;
use App\Models\MngMobile;
use App\Models\TaxEspc;
use App\Models\Comlist;
use App\Models\MngApp;
use App\User;
use DateTimeZone;
use hash_hmac;
use DateTime;
use Curl;
use Auth;
use DB;

class LejarController extends BaseController
{
    public function __construct(ApiRepo $repos)
    {
      
        $this->repos = $repos;  
    
    }

    function index()
    {
         
         $users= auth()->user();
         $id = $users->id;
         $comlist = comlist::where('fk_users','=',$id)->get();
         $user= User::where('id',$id)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->get();
         $lejarcom = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','SYARIKAT')->get();

         $check1 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                   ->whereNull('lejar_type')
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check2 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                   ->where('lejar_type','=','INDIVIDU')
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $check3 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                   ->whereNull('lejar_type')
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check4 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                   ->where('lejar_type','=','INDIVIDU')
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $checksal = $check1+$check2;
         $checkpro = $check3+$check4;

         $referer = $_SERVER['HTTP_REFERER'];
          if(strpos($referer, 'mobile') !== false){

            return view('mobile.lejar.load',compact('lejar','user','comlist')); 

        } else{

           return view('lejar.index',compact('lejar','lejarcom','user','comlist','checkpro','checksal')); 

        }

         
    }


    function penutup($type,$ltype)
    {
         $referer = $_SERVER['HTTP_REFERER'];
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
         $lejar = TaxElejarDetail::where('fk_users','=',$id)->where('income_type',$type)->where('lejar_type','=',$typel)->get();
         $profile = TaxProfile::where('fk_users','=',$id)->first();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         $calendar = DB::SELECT("select Tahun from tax_elejar_detail_calendar where fk_users = '$id' and income_type = '$type' and lejar_type is null group by TAHUN");

         $calendarcurrent = DB::SELECT("select Tahun from tax_elejar_detail_current where fk_users = '$id' and income_type = '$type' and lejar_type = 'INDIVIDU' group by TAHUN");

         if(strpos($referer, 'mobile') !== false){

             return view('mobile.lejar.penutup',compact('lejar','profile','user','calendar','typelejar','typel','ltype','calendarcurrent')); 

        } else{

            return view('lejar.penutup',compact('lejar','profile','user','calendar','typelejar','typel','ltype','calendarcurrent')); 

        }

        
    }

    function current($year,$type,$ltype)
    {

         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCurrent::where('fk_users','=',$id)->where('TAHUN','=',$year)->where('lejar_type','=',$typel)->where('income_type',$type)->get();
         $profile = TaxProfile::where('fk_users','=',$id)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();



         $referer = $_SERVER['HTTP_REFERER'];
         if(strpos($referer, 'mobile') !== false){

           return view('mobile.lejar.current',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel')); 

        } else{

          return view('lejar.current',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel')); 

        }
         
    }

    
    function calendar($year,$type)
    {
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCalendar::where('fk_users','=',$id)->where('TAHUN','=',$year)->where('income_type',$type)->whereNull('lejar_type')->get();
         $profile = TaxProfile::where('fk_users','=',$id)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->where('income_type','=',$type)->first();
          $typel = 'INDIVIDU';
          $ltype = 1;


         $referer = $_SERVER['HTTP_REFERER'];
         if(strpos($referer, 'mobile') !== false){

           return view('mobile.lejar.calendar',compact('profile','user','calendar','year','lejar','typelejar','typel','ltype')); 

        } else{

          return view('lejar.calendar',compact('profile','user','calendar','year','lejar','typelejar')); 

        }

         
    }

    function comdata($id,$type)
    {
        // dd($type);
         $elejarcom = $this->repos->api_elejarcomlist($id);
         $profile = comlist::where('id','=',$id)->first();
         $users= $profile->fk_users;
         $user= User::where('id',$users)->first();
         $lejar = TaxElejarDetail::where('fk_lkp_tcl','=',$id)->where('income_type','=',$type)->get();
         $ltype = 2;
         $typel = 'SYARIKAT';
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         $lid = $id;
         $calendar = DB::SELECT("select Tahun from tax_elejar_detail_current where fk_lkp_tcl = '$id' and income_type = '$type' group by TAHUN");
         $calendar2 = DB::SELECT("select Tahun from tax_elejar_detail_calendar where fk_lkp_tcl = '$id' and income_type = '$type' group by TAHUN");

         $check1 = TaxElejarDetailCalendar::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check2 = TaxElejarDetailCurrent::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $check3 = TaxElejarDetailCalendar::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check4 = TaxElejarDetailCurrent::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $checksal = $check1+$check2;
         $checkpro = $check3+$check4;

         return view('lejar.penutupcom',compact('profile','lejar','ltype','typel','typelejar','calendar','type','lid','calendar2','checksal','checkpro')); 
    }

    function currentcom($year,$type,$ltype,$lid)
    {

         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCurrent::where('fk_lkp_tcl','=',$lid)->where('TAHUN','=',$year)->where('lejar_type','=',$typel)->where('income_type',$type)->whereNotin('TRANSACTION_CODE',['071','072','192'])->get();
         $profile = comlist::where('id','=',$lid)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         return view('lejar.currentcom',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel','lid','type')); 
    }

    function calendarcom($year,$type,$ltype,$lid)
    {

         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
         $users= auth()->user();
         $id = $users->id;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCalendar::where('fk_lkp_tcl','=',$lid)->where('TAHUN','=',$year)->where('lejar_type','=',$typel)->where('income_type',$type)->whereNotin('TRANSACTION_CODE',['071','072','192'])->get();
         $profile = comlist::where('id','=',$lid)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         return view('lejar.calendarcom',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel','lid','type')); 
    }

    

    

    function mobileindex()
    {
        
            $users= auth()->user();
            $id = $users->id;
            $user= User::where('id',$id)->first();
            $comlist = $this->repos->api_comlist();
            $elejar = $this->repos->api_elejar();
            $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->get();
            $comlist = comlist::where('fk_users','=',$user->id)->get();

            return view('mobile.lejar.index',compact('user','lejar','comlist')); 
       
    }

    function resit($file,$rujukan,$resit,$type,$ltype)
    {
         $resit = $this->repos->api_resit($file,$rujukan,$resit,$type);
         if($resit[0])
         {

            if($resit[0]->SEQ_NO == '0')
            {
                return 0;
            }else
            {
                $data = $resit[0];
                if($type == 1)
                {
                     // dd($data);
                    return view('lejar.resit',compact('data','file','rujukan','ltype')); 
                }else{

                    // dd($data);

                    return view('lejar.resitcom',compact('data','file','rujukan','ltype')); 
                }
                
            }
        }else{

            return 0;

        }
         // $profile = comlist::where('id','=',$id)->first();
         // $users= $profile->fk_users;
         // $user= User::where('id',$users)->first();
         // $lejar = TaxElejarDetail::where('fk_lkp_tcl','=',$id)->get();
         // $ltype = 2;
         // $typel = 'SYARIKAT';
         // $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         // $lid = $id;
         // $calendar = DB::SELECT("select Tahun from tax_elejar_detail_current where fk_lkp_tcl = '$id' and income_type = '$type' group by TAHUN");
         // return view('lejar.penutupcom',compact('profile','lejar','ltype','typel','typelejar','calendar','type','lid')); 
    }



     
}
