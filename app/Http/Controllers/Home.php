<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use App\Models\MngAnnouncement;
use App\Models\UserSetting;
use App\Data\Repo\ApiRepo;
use App\Models\MngService;
use App\Models\TaxProfile;
use App\Models\MngMobile;
use App\Models\MngBanner;
use App\Models\TaxInbox;
use App\Models\MngApp;
use App\User;
use DateTimeZone;
use hash_hmac;
use DateTime;
use Session;
use Config;
use Curl;
use Auth;
use App;
use LaravelWebauthn\Facades\Webauthn;
use LaravelWebauthn\Models\WebauthnKey;
use Webauthn\PublicKeyCredentialCreationOptions;
use Webauthn\PublicKeyCredentialRequestOptions;

class Home extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct(ApiRepo $repos)
    {
        $this->repos = $repos;
    }


    public function __invoke(Request $request)
    {
        $now = date('Y-m-d');
        

          if(env('DB_CONNECTION') == 'mysql')
          {

              $banner = MngBanner::where('status','=',1)
               ->whereRaw('start_date <= "'.$now.'" AND end_date >= "'.$now.'"')
               ->orderBy('order','ASC')->get();
          }else{


                try{

                    $banner = MngBanner::where('status','=',1)
                           ->whereRaw('start_date <= GETDATE() AND end_date >= GETDATE()')
                           ->orderBy('order','ASC')->get();

                }catch(\Illuminate\Database\QueryException $ex){ 

                     $banner = MngBanner::where('status','=',1)
               ->whereRaw('start_date <= "'.$now.'" AND end_date >= "'.$now.'"')
               ->orderBy('order','ASC')->get();
                }

          }
        
        if(Auth::check())
        {
            $user = auth()->user();

            $data = [];

            // $profile = TaxProfile::where('fk_users','=',$user->id)->first();
            $startc = strtotime(date('h:m:s')) - strtotime('TODAY');
                $data = $this->repos->api_individu();
            $endc = strtotime(date('h:m:s')) - strtotime('TODAY');
            $total = $endc-$startc;
            $debug['individu'] = 'log API(https://mytax.hasil.gov.my/MyTaxApi/api/individu) < '.$total.' second';

            $startc = strtotime(date('h:m:s')) - strtotime('TODAY');
                $dataprofile = $this->repos->api_inbox();
            $endc = strtotime(date('h:m:s')) - strtotime('TODAY');
            $total = $endc-$startc;
            $debug['inboxcount'] = 'log API(https://mytax.hasil.gov.my/MyTaxApi/api/Mailbox) < '.$total.' second';

            $startc = strtotime(date('h:m:s')) - strtotime('TODAY');
                $dataprofiles = $this->repos->api_profile();
            $endc = strtotime(date('h:m:s')) - strtotime('TODAY');
            $total = $endc-$startc;
            $debug['dataprofile'] = 'log API(SSOService.svc/user/getprofile) < '.$total.' second';


            $startc = strtotime(date('h:m:s')) - strtotime('TODAY');
                $apidata = $this->repos->api_refundindc($data);
            $endc = strtotime(date('h:m:s')) - strtotime('TODAY');
            $total = $endc-$startc;
            $debug['datarefund'] = 'log API(https://mytax.hasil.gov.my/MyTaxApi/api/RefundStatus) < '.$total.' second';




            $refunds = '0.00';
         // dd($apidata);
            if($apidata)
            {
                foreach ($apidata as $key => $value) 
                {                
                    foreach($value as $key =>$refund)
                    {
                        if(($refund->seq == 4) AND (strpos($refund->Keteranganbm, 'Selesai') !== false))
                        {

                            $refunds = $refund->amt;
                        }
                    }
                }
            }

                 
            $profile = TaxProfile::where('fk_users','=',$user->id)->first();
            $checkfeatured = UserSetting::where('fk_users','=',$user->id)->where('key','=','featured')->first();

            if($checkfeatured)
            {
                $array = json_decode($checkfeatured->value);
                if($array){
                    $list = MngService::whereIn('id',$array)->get();    
                }else{

                    $list = MngService::where('status','=',1)->where('acl', 'like', "%{$user->access}%")->limit(4)->get();
                }
                        
            }else
            {
                $list = MngService::where('status','=',1)->where('acl', 'like', "%{$user->access}%")->limit(4)->get();
            }

            $inbox = TaxInbox::where('NoId','=',$user->reference_id)->count();

            if($user->language)
            {
                \Session::put('locale', $user->language);
                \App::setLocale($user->language);
            } 

            $username = base64_encode($user->reference_id); 
            $pass = $user->password; 
            $type = $user->reference_type;

            


            $startc = strtotime(date('h:m:s')) - strtotime('TODAY');
                $graph = $this->repos->api_graph();
            $endc = strtotime(date('h:m:s')) - strtotime('TODAY');
            $total = $endc-$startc;
            $debug['graph'] = 'log API(https://mytax.hasil.gov.my/MyTaxAPI/api/LejarCukaiDonut) < '.$total.' second';

            // dd($graph);

            // dd($debug);

            $agent = new Agent();
            $browser = $agent->browser();

            $dataebe = $this->repos->api_ebe();
            $dataebelogin = $this->repos->api_ebelogin();

            $dataebe2 = $this->repos->api_ebe2();
            $dataebelogin2 = $this->repos->api_ebelogin2();

            $url=env('API_DOMAIN')."/SSOService.svc/user/login?username=".$username."&password=".$pass."&IdType=".$type."&nocp=undefined";       
            return view('dashboard.homepage',compact('dataebelogin','dataebelogin2','user','profile','list','inbox','url','graph','dataebe','dataebe2','refunds','browser'));

        }else
        {   
            $list = MngService::where('status','=',1)->limit(4)->get();
            if (\Session::has('locale')) {
            $locale = \Session::get('locale', Config::get('app.locale'));
            } else {
                $locale = 'ms';

                if ($locale !== 'en') {
                    $locale = 'ms';
                }
            }

            \App::setLocale($locale);
            \Session::put('locale', $locale);

            $idtype= 0;

            if($request->error){

                $error = $request->error;

                return view('auth.login',compact('list','banner','idtype','error'));

            }else{

                return view('auth.login',compact('list','banner','idtype'));
            }
            
           
            
        }
    }

    public function mobile(Request $request)
    {

        if(Auth::check())
        {
            
            $data = [];

            $user = auth()->user();

            // $profile = TaxProfile::where('fk_users','=',$user->id)->first();
            $data = $this->repos->api_individu();
            $dataprofile = $this->repos->api_inbox();
            $dataprofiles = $this->repos->api_profile();
            
            
            $apidata = $this->repos->api_refundindc($data);
            $refunds = '0.00';
         // dd($apidata);
            if($apidata)
            {
                foreach ($apidata as $key => $value) 
                {                
                    foreach($value as $key =>$refund)
                    {
                        if(($refund->seq == 4) AND (strpos($refund->Keteranganbm, 'Selesai') !== false))
                        {

                            $refunds = $refund->amt;
                        }
                    }
                }
            }


            $checkfeatured = UserSetting::where('fk_users','=',$user->id)->where('key','=','featured')->first();

            if($checkfeatured)
            {
                $array = json_decode($checkfeatured->value);
                if($array){
                    $list = MngService::whereIn('id',$array)->get();    
                }else{

                    $list = MngService::where('status','=',1)->where('acl', 'like', "%{$user->access}%")->limit(4)->get();
                }
                        
            }else
            {
                $list = MngService::where('status','=',1)->where('acl', 'like', "%{$user->access}%")->limit(4)->get();
            }
         
            $now = date('Y-m-d');
            if(env('DB_CONNECTION') == 'mysql')
            {

                $announcement = MngAnnouncement::where('status','=',1)
                         ->whereRaw('"'.$now.'" >= start_date AND "'.$now.'" <= end_date')
                         ->orderBy('created_at','DESC')->limit('5')->get();
            }else{

                $announcement = MngAnnouncement::where('status','=',1)
                             ->whereRaw('GETDATE() >= start_date AND GETDATE() <= end_date')
                             ->orderBy('created_at','DESC')
                             ->limit(5)
                             ->get();

            }

            $dataebe = $this->repos->api_ebe();
            $dataebelogin = $this->repos->api_ebelogin();

            $dataebe2 = $this->repos->api_ebe2();
            $dataebelogin2 = $this->repos->api_ebelogin2();
         
            $profile = TaxProfile::where('fk_users','=',$user->id)->first();
            return view('mobile.homepage',compact('dataebelogin2','dataebe2','dataebelogin','dataebe','user','profile','list','announcement','refunds'));

        }else
        {   
            \Session::put('locale', 'ms');
            \App::setLocale('ms');
            $agent = new Agent();
              $device = 'PC';
              if( $agent->isiOS() ){
                   $device = 'IOS';
              }

              if( $agent->isAndroidOS() ){

                   $device = 'ANDROID';
              }
            $mobile = MngMobile::where('status','=',1)->get();
            return view('auth.mobilelogin',compact('device','mobile'));
        }
    }
    
    public function changeLocale(request $request)
    {


       
        \Session::put('locale', $request->lang);

        if(Auth::check())
        {
            $user = auth()->user();
            $user->language = $request->lang;
            $user->update();
        }

        $pre = \URL::previous();
        $contains = Str::contains($pre, 'log');
        if($contains)
        {
           return redirect('/');
        }

        return redirect()->back();
    }

    public function mobilelogout()
    {
        Auth::logout();

        return redirect('mobile');
    }

    public function checkdevice()
    {
        $agent = new Agent();
        $device = 'PC';
        
        if( $agent->isiOS() ){
           $device = 'IOS';
        }

        if( $agent->isAndroidOS() ){

           $device = 'ANDROID';
        }

        if($device == 'PC')
        {
            return redirect('');
        }else{

            return redirect('mobile');
        }
    }

    public function webauth($id)
    {

        $user = User::where('id',$id)->first();
        $publicKey = Webauthn::getAuthenticateData($user);

        // dd($publicKey);
        return view('auth.webauth',compact('publicKey','id'));
    }


}
