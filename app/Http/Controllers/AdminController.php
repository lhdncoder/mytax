<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Models\MngLkpQuestionFeedback;
use App\Models\MngLkpAnswerPicFeedback;
use App\Models\Aclpermission;
use App\Models\TaxAttachment;
use App\Data\Repo\AdminRepo;
use App\Models\Mngfeedback;
use App\Models\TaxProfile;
use App\Models\TaxBrosure;
use App\Models\ApiReport;
use App\Models\MngBanner;
use App\Models\Hits;
use App\User;
use Redirect;
use Storage;
use PDF;
use Curl;
use File;
use Auth;
use DB;

class AdminController extends BaseController
{


    public function __construct(AdminRepo $repos)
    {
        
      $this->repos = $repos;  
      
    }


    public function userman(Request $request)
    {
        
        $role = Aclpermission::get();
        return view('admin.userman',compact('role'));
          
    }

    public function saverole(Request $request)
    {
        
        $role = new Aclpermission;
        $role->name = $request->name;
        $role->save();

        return redirect()->back()->withSuccess('add successfully');
          
    }

    public function deleterole($id)
    {
        
        $role = Aclpermission::where('id','=',$id)->delete();
        return redirect()->back()->withSuccess('remove successfully');
          
    }
    public function announcement()
    {
        
       return view('admin.announcement');
          
    }



    public function inbox($id)
    {
        
       $user= User::where('id',$id)->first();
       $profile = TaxProfile::where('fk_users','=',$id)->first();

       return view('dashboard.inbox',compact('user','profile'));
          
    }
    public function listfaq(Request $request)
    {
        $listfaq = $this->repos->listfaq($request);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        
        return view('admin.faq.listfaq',compact('listfaq','user')); 

          
    }
    public function addfaq()
    {
        
       return view('admin.faq.addfaq');
          
    }
     public function editfaq(Request $request)
    {
    
       $dataques=$this->repos->dataques($request->id);
       $dataanswer=$this->repos->dataanswer($request->id);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();

       return view('admin.faq.editfaq',compact('dataques','dataanswer','user'));
          
    }
    public function svfaq(Request $request)
    {
        
       $data = $this->repos->svfaq($request);
       return redirect::to('admin/editfaq/'.$data)->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function svefaq(Request $request)
    {
        
       $data = $this->repos->svefaq($request);
       return redirect::to('admin/listfaq')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function svansw(Request $request)
    {
       
       $data = $this->repos->svansw($request);
       return redirect::to('admin/editfaq/'.$request->get('idquest'))->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function sveansw(Request $request)
    {
       
        $data = $this->repos->sveansw($request);
       return redirect::to('admin/editfaq/'.$request->get('idquestedit'))->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function listhelp(Request $request)
    {
        $listhelp = $this->repos->listhelp($request);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        
        return view('admin.help.listhelp',compact('listhelp','user')); 

          
    }
    public function addhelp()
    {
        
       return view('admin.help.addhelp');
          
    }
    public function svhelp(Request $request)
    {
      
       $data = $this->repos->svhelp($request);
       return redirect::to('admin/listhelp')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function edithelp(Request $request)
    {
    
       $datahelp=$this->repos->datahelp($request->id);
       return view('admin.help.edithelp',compact('datahelp'));
          
    }
    public function svehelp(Request $request)
    {
        
       $data = $this->repos->svehelp($request);
       return redirect::to('admin/listhelp')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function listmobile(Request $request)
    {
        $listmobile = $this->repos->listmobile($request);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        
        return view('admin.mobile.listmobile',compact('listmobile','user')); 

          
    }
    public function addmobile()
    {
        
       return view('admin.mobile.addmobile');
          
    }
    public function svmobile(Request $request)
    {
        
       $data = $this->repos->svmobile($request);
       return redirect::to('admin/listmobile/')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function editmobile(Request $request)
    {
    
       $datamobile=$this->repos->datamobile($request->id);
       return view('admin.mobile.editmobile',compact('datamobile'));
          
    }
    public function svemobile(Request $request)
    {
        
       $data = $this->repos->svemobile($request);
       return redirect::to('admin/listmobile')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function listannounce(Request $request)
    {
        $listannounce = $this->repos->listannounce($request);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        
        return view('admin.announcement.listannounce',compact('listannounce','user')); 

          
    }
    public function addannounce()
    {
        
       return view('admin.announcement.addannounce');
          
    }
    public function svannounce(Request $request)
    {    
      //   $validator = Validator::make($request->all(), [
      //       'announce' => 'required',
           
      //       // 'cc'=>'required',
      //       // 'kelas_tuntutan'=>'required',
            
      //   ],
      //   [
      //     'announce.required' => 'Jenis Kenderaan adalah wajib !',
         
      //     // 'cc.required'=>'Kuasa (c.c) wajib !',
      //     // 'kelas_tuntutan.required'=>'Kelas Tuntutan wajib !'
        
      // ]);

      //   if($validator->fails()) {
      //      return back()->withErrors($validator)->withInput($request->all());
      //   }else{

       $data = $this->repos->svannounce($request);
       return redirect::to('admin/listannounce/')->withSuccess(__('ID Tuntutan telah dijana'));

     // }
          
    }
    public function editannounce(Request $request)
    {
        $dataannounce=$this->repos->dataannounce($request->id);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
       return view('admin.announcement.editannounce',compact('dataannounce','user'));
          
    }
    public function sveannounce(Request $request)
    {
        
       $data = $this->repos->sveannounce($request);
       return redirect::to('admin/listannounce')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function listapp(Request $request)
    {
        $listapp = $this->repos->listapp($request);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        
        return view('admin.application.listapp',compact('listapp','user')); 

          
    }
    public function addapp()
    { 
       return view('admin.application.addapp');
          
    }
    public function svapp(Request $request)
    {
        
       $data = $this->repos->svapp($request);
       return redirect::to('admin/listapp')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function editapp(Request $request)
    {  
       $dataapp=$this->repos->dataapp($request->id);

       return view('admin.application.editapp',compact('dataapp'));
          
    }
    public function sveapp(Request $request)
    {
        
       $data = $this->repos->sveapp($request);
       return redirect::to('admin/listapp')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function feedback()
    {       
       // $datafeedback=MngLkpAnswerPicFeedback::with('questfeedback')
       //              ->orderby('fk_lkp_question_feedback','ASC')
       //              ->get()
       //              ->groupBy('fk_lkp_question_feedback');

                     // $datafeedback=MngLkpAnswerPicFeedback::whereHas('questfeedback', function ($query) {
                     //                  return $query->where('status', '=', 1);
                     //              })->with('questfeedback')->orderby('fk_lkp_question_feedback','ASC')->get()->groupBy('fk_lkp_question_feedback');

 
      $datafeedback=MngLkpAnswerPicFeedback::with(['questfeedback' => function($author) {

          $author->where('status', 1);

      }])->where('status',1)->orderby('fk_lkp_question_feedback','ASC')->get()->groupBy('fk_lkp_question_feedback');
                    


       $arr = array();


          foreach ($datafeedback as $key => $value) {


            $arr[] = [
                'id' => $value[0]->questfeedback['id'],
                'quest_en' => $value[0]->questfeedback['quest_en'],
                'quest_bm' => $value[0]->questfeedback['quest_bm'],
                'status'   => $value[0]->questfeedback['status'],
                'detail_pic' => $value,
                'detail_answ' => $value,


            ];

            
        }
// dd($arr);
      
       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();

       Auth::logout();

       
       return view('feedback.form',compact('datafeedback','user','arr'));
          
    }
    public function svfeedback(Request $request)
    {

       $data = $this->repos->svfeedback($request);

       //redirect to logout

       return redirect('/');

       //return redirect('/feedback/form');

      // return redirect('/')->with(Auth::logout());
          
    }
    public function listservice(Request $request)
    {
        $listservice = $this->repos->listservice($request);
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        
        return view('admin.service.listservice',compact('listservice','user')); 

          
    }
     public function addservice()
    { 
       return view('admin.service.addservice');
          
    }
    public function svservice(Request $request)
    {
       
       $data = $this->repos->svservice($request);
       return redirect::to('admin/listservice')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function editservice(Request $request)
    {  
       $dataservice=$this->repos->dataservice($request->id);

       $check = json_decode($dataservice->acl);
       if($check == null){
          $check = [];
       }
       // dd($check);


       return view('admin.service.editservice',compact('dataservice','check'));
          
    }   
    public function sveservice(Request $request)
    {
        
       $data = $this->repos->sveservice($request);
       return redirect::to('admin/listservice')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function reportapp(request $request)
    { 

      $users= auth()->user();
      $id = $users->id;
      $user= User::where('id',$id)->first();
      $data = [];
      $starts = date('d-m-Y');
      $ends = date('d-m-Y');
       if($request->start_date){

          $starts = $request->start_date;
          $ends = $request->end_date;
          $start =  date("Y-m-d", strtotime($request->start_date));
          $end =  date("Y-m-d", strtotime($request->end_date));
          $data = Hits::where('date','>=',$start)
                      ->where('date','<=',$end)
                      ->selectRaw('sum(hits) as sum,fk_lkp_mng_service')
                      ->groupBy('fk_lkp_mng_service')->orderBy('sum','DESC')->get();
       }
       return view('admin.report.application',compact('data','starts','ends','user'));
          
    }
    public function reportfeedback(request $request)
    { 

    

        if(count($request->all())==0){
           $datefrom=date('d-m-Y');
           $dateto=date('d-m-Y');

        }else{


          $datefrom=$request->start_date;
          $dateto=$request->end_date;

        }
        
        $type=0;





       $datafeedback=MngLkpAnswerPicFeedback::with(['questfeedback' => function($author) {

                  $author->where('status', 1);

        }])->where('status',1)->orderby('fk_lkp_question_feedback','ASC')
           ->get()
           ->groupBy('fk_lkp_question_feedback');
                    


       $arr = array();


          foreach ($datafeedback as $key => $value) {


            $arr[] = [
                'id' => $value[0]->questfeedback['id'],
                'quest_en' => $value[0]->questfeedback['quest_en'],
                'quest_bm' => $value[0]->questfeedback['quest_bm'],
                'status'   => $value[0]->questfeedback['status'],
                'detail_answ' => $value


            ];

            
        }




       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();

       return view('admin.report.feedback',compact('arr','datefrom','dateto','user'));
          
    }
    public function reportcomment(request $request)
    { 

    

        if(count($request->all())==0){
           $datefrom=date('d-m-Y');
           $dateto=date('d-m-Y');

        }else{


          $datefrom=$request->start_date;
          $dateto=$request->end_date;

        }
        
        $type=0;


       $data=Mngfeedback::where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                          ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                          ->whereNotNull('comment')
                          ->paginate(20);


       return view('admin.report.usercomment',compact('data','datefrom','dateto'));
          
    }
    public function listbrosure()
    { 
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        $list = TaxBrosure::with('files')->get();
        return view('admin.brosure.list',compact('list','user'));
          
    }

    public function addbrosure()
    { 
       return view('admin.brosure.add');
    }

    public function delattach($aid,$bid)
    { 
        $att = TaxAttachment::where('id','=',$aid)->delete();
        $attall = TaxAttachment::where('fk_tax_brosure','=',$bid)->get();
       return view('admin.brosure.attach',compact('attall'));
    }

    public function savebrosure(Request $request)
    { 

       $new = new TaxBrosure;
       $new->name_bm = $request->name_bm;
       $new->name_en = $request->name_en;
       $new->status = $request->status;
       $new->save();

       $id = $new->id;


       if($request->lampiran){

            foreach ($request->lampiran as $key => $value) 
            {
                $files=$value;
                $folder='lampiran';
                $mainapp = $id;

                if (!file_exists(public_path().'/storage/lampiran/')) {
                    
                         mkdir(public_path()."/storage/".$folder);
                }
                if (!file_exists(public_path().'/storage/lampiran/'.$mainapp)) {
                    
                          mkdir(public_path()."/storage/lampiran/".$mainapp);  
                }
               

                $path = public_path()."/storage/lampiran/".$mainapp;

                $shortpath = "/storage/lampiran/".$mainapp."/";
                $movepath = "/public/lampiran/".$mainapp."/";
                     // $photo->move($path, $photo->getClientOriginalName());
                $filename= $files->getClientOriginalName();

                $extension = $files->getClientOriginalExtension();
                    
                $size= $files->getClientSize();

                $label_bm = $request->lampiran_name_bm[$key];
                $label_en = $request->lampiran_name_en[$key];
                $paths =  $files->storeAs($movepath,$filename);

                $svdt = new TaxAttachment;
                $svdt->fk_tax_brosure = $id;
                $svdt->date = date('Y-m-d');
                $svdt->label_bm = $label_bm;
                $svdt->label_en = $label_en;
                $svdt->dir = $path;  //full path from var
                $svdt->full_path = $shortpath; //short path from storage/medical/
                $svdt->file_name = $filename;
                $svdt->file_size = $size;
                $svdt->status = 1;
                $svdt->save();
            }


        }

        return redirect::to('admin/brosure')->withSuccess(__('ID Tuntutan telah dijana'));

    }

    public function updatebrosure(Request $request)
    { 

        // dd($request);

       $new = TaxBrosure::where('id','=',$request->id)->first();
       $new->name_bm = $request->name_bm;
       $new->name_en = $request->name_en;
       $new->status = $request->status;
       $new->update();

       $id = $request->id;

       if($request->lampiran){

            foreach ($request->lampiran as $key => $value) 
            {
                $files=$value;
                $folder='lampiran';
                $mainapp = $id;

                if (!file_exists(public_path().'/storage/lampiran/')) {
                    
                         mkdir(public_path()."/storage/".$folder);
                }
                if (!file_exists(public_path().'/storage/lampiran/'.$mainapp)) {
                    
                          mkdir(public_path()."/storage/lampiran/".$mainapp);  
                }
               

                $path = public_path()."/storage/lampiran/".$mainapp;

                $shortpath = "/storage/lampiran/".$mainapp."/";
                $movepath = "/public/lampiran/".$mainapp."/";
                     // $photo->move($path, $photo->getClientOriginalName());
                $filename= $files->getClientOriginalName();

                $extension = $files->getClientOriginalExtension();
                    
                $size= $files->getClientSize();

                $label_bm = $request->lampiran_name_bm[$key];
                $label_en = $request->lampiran_name_en[$key];
                $paths =  $files->storeAs($movepath,$filename);

                $svdt = new TaxAttachment;
                $svdt->fk_tax_brosure = $id;
                $svdt->date = date('Y-m-d');
                $svdt->label_bm = $label_bm;
                $svdt->label_en = $label_en;
                $svdt->dir = $path;  //full path from var
                $svdt->full_path = $shortpath; //short path from storage/medical/
                $svdt->file_name = $filename;
                $svdt->file_size = $size;
                $svdt->status = 1;
                $svdt->save();
            }


        }

        return redirect::to('admin/brosure')->withSuccess(__('ID Tuntutan telah dijana'));

    }
    public function listfeedback()
    { 
       
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        $listfeedback = $this->repos->listfeedback();
        
        return view('admin.feedback.listfeedback',compact('listfeedback','user')); 
          
    }
    public function addquestfeedback()
    {
        
       return view('admin.feedback.addquestfeedback');
          
    }
    public function reportapi()
    {
      $data = ApiReport::get();
      return view('admin.report.apireport',compact('data'));
          
    }
    public function svquestfeedback(Request $request)
    {
       $data = $this->repos->svquestfeedback($request);
       return redirect::to('admin/feedback/');
          
    }
    public function editquestfeedback(Request $request)
    {
    
       $dataquesfeedback=$this->repos->dataquesfeedback($request->id);
       $dataanswerfeedback=$this->repos->dataanswerfeedback($request->id);
       $picfeedbcak=$this->repos->picfeedbcak($request->id);

       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();

       return view('admin.feedback.editquestfeedback',compact('dataquesfeedback','dataanswerfeedback','user','picfeedbcak'));
          
    }
    public function svequestfeedback(Request $request)
    {
        
       $data = $this->repos->svequestfeedback($request);
       return redirect::to('admin/feedback')->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function svanswrfeedback(Request $request)
    {
       
       $data = $this->repos->svanswrfeedback($request);
       return redirect::to('admin/editquestfeedback/'.$request->get('idquest'))->withSuccess(__('ID Tuntutan telah dijana'));
          
    }
    public function sveanswrfeedback(Request $request)
    {

       
       $data = $this->repos->sveanswrfeedback($request);
       return redirect::to('admin/editquestfeedback/'.$request->get('idquestedit'))->withSuccess(__('ID Tuntutan telah dijana'));
          
    }

    public function banner()
    {       
      $listbanner = MngBanner::get();
      $users= auth()->user();
      $id = $users->id;
      $user= User::where('id',$id)->first();
      $now = date('Y-m-d');
      if(env('DB_CONNECTION') == 'mysql')
      {

          $banner = MngBanner::where('status','=',1)
           ->whereRaw('start_date <= "'.$now.'" AND end_date >= "'.$now.'"')
           ->orderBy('order','ASC')->get();
      }else{

           $banner = MngBanner::where('status','=',1)
                       ->whereRaw('start_date <= GETDATE() AND end_date >= GETDATE()')
                       ->orderBy('order','ASC')->get();

      }
      return view('admin.banner.list',compact('listbanner','user','banner'));          
    }

    public function addbanner()
    {       

      return view('admin.banner.add');          
    }

    public function savebanner(Request $request)
    {
        
        $newbanner = new MngBanner;
        $newbanner->title_bm = $request->title_bm;
        $newbanner->title_en = $request->title_en;
        $newbanner->start_date = date('Y-m-d', strtotime($request->get('start_date')));
        $newbanner->end_date = date('Y-m-d', strtotime($request->get('end_date')));
        $newbanner->order = $request->order;
        $newbanner->status = $request->status;

        if($request->img)
        {
              
                $files=$request->img;
                $folder='banner';

                if (!file_exists(public_path().'/storage/banner/')) {
                    
                         mkdir(public_path()."/storage/".$folder);
                }
               

                $path = public_path()."/storage/banner/";

                $shortpath = "/storage/banner/";
                $movepath = "/public/banner/";

                $filename= $files->getClientOriginalName();
                $paths =  $files->storeAs($movepath,$filename);

                $newbanner->banner_img=$filename;

          }
        
        $newbanner->save();
         return redirect::to('admin/banner/')->withSuccess(__('Banner berjaya ditambah'));

                 
    }

    public function editbanner($id)
    {       
      $banner = MngBanner::where('id',$id)->first();
      return view('admin.banner.edit',compact('banner'));          
    }


    public function updatebanner(Request $request)
    {
        
        $newbanner = MngBanner::where('id',$request->bid)->first();
        $newbanner->title_bm = $request->title_bm;
        $newbanner->title_en = $request->title_en;
        $newbanner->start_date = date('Y-m-d', strtotime($request->get('start_date')));
        $newbanner->end_date = date('Y-m-d', strtotime($request->get('end_date')));
        $newbanner->order = $request->order;
        $newbanner->status = $request->status;

        if($request->img)
        {
              
                $files=$request->img;
                $folder='banner';

                if (!file_exists(public_path().'/storage/banner/')) {
                    
                         mkdir(public_path()."/storage/".$folder);
                }
               

                $path = public_path()."/storage/banner/";

                $shortpath = "/storage/banner/";
                $movepath = "/public/banner/";

                $filename= $files->getClientOriginalName();
                $paths =  $files->storeAs($movepath,$filename);

                $newbanner->banner_img=$filename;

          }
        
        $newbanner->update();
         return redirect::to('admin/banner/'.$request->bid)->withSuccess(__('Banner berjaya ditambah'));

                 
    }
    public function exportfeedback($type,$datefrom,$dateto)
    {

      // return $this->repos->exportfeedback($request,$type,$datefrom,$dateto,$this);

      if($type=='2'){//excel
        $collectionparam = collect([$type,$datefrom,$dateto]);

        return Excel::download(new ExportFeedback($collectionparam), 'Maklum_Balas_Pelanggan.xlsx');

      }else{//pdf



        $datafeedback=MngLkpAnswerPicFeedback::with(['questfeedback' => function($author) {

                  $author->where('status', 1);

              }])->where('status',1)->orderby('fk_lkp_question_feedback','ASC')
                 ->get()
                 ->groupBy('fk_lkp_question_feedback');
                    


          $arr = array();


          foreach ($datafeedback as $key => $value) {


            $arr[] = [
                'id' => $value[0]->questfeedback['id'],
                'quest_en' => $value[0]->questfeedback['quest_en'],
                'quest_bm' => $value[0]->questfeedback['quest_bm'],
                'status'   => $value[0]->questfeedback['status'],
                'detail_answ' => $value,


            ];

            
        }


       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();



       $data = [
          'datefrom' => $datefrom,
          'dateto' => $dateto,
          'arr'=>$arr,
          'user'=>$user
            ];
        
        $pdf = PDF::loadView('admin.feedback.pdffeedback', $data);  

        return $pdf->download('Maklum_Balas_Pelanggan.pdf');

      }
        


    }
    public function exportkomen($type,$datefrom,$dateto)
    {

      // return $this->repos->exportfeedback($request,$type,$datefrom,$dateto,$this);

      if($type=='2'){//excel
        $collectionparam = collect([$type,$datefrom,$dateto]);

        return Excel::download(new ExportComment($collectionparam), 'Senarai Komen Maklum Balas.xlsx');

      }else{//pdf



         $data=Mngfeedback::where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                          ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                          ->whereNotNull('comment')
                          ->paginate(20);

          $data = [
          'datefrom' => $datefrom,
          'dateto' => $dateto,
          'data'=>$data
            ];

        $pdf = PDF::loadView('admin.feedback.pdfcomment', $data);  

        return $pdf->download('Senarai_Komen_Maklum_Balas_Pelanggan.pdf');

      }
        


    }






}
