<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\Models\TaxTemplateLookup;
use App\Data\Repo\TemplateRepo;
use App\Models\LkpTemplate;
use App\Models\TaxTemplate;
use App\Models\UserSetting;
use App\Data\Repo\ApiRepo;
use App\Models\MngService;
use App\Models\TaxProfile;
use App\Models\MngMobile;
use App\Models\TaxInbox;
use App\Models\TaxCp500;
use App\Models\MngApp;
use App\User;
use DateTimeZone;
use hash_hmac;
use Redirect;
use DateTime;
use Auth;
use Curl;

class MailController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function __construct(TemplateRepo $repos,ApiRepo $apirepo)
   {
      
        $this->repos = $repos;  
        $this->apirepo = $apirepo;  
    
   }

    public function inbox()
    {
               
        $dataprofile = $this->apirepo->api_inbox();
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        $data = TaxInbox::where('NoId','=',$user->reference_id)->whereNotIn('Sumber',['CP500','CP500G'])->where('Sumber','!=','CP500G')->orderBy('TarikhNotis','DESC')->get();
        $datacp500 = TaxInbox::where('NoId','=',$user->reference_id)->whereIn('Sumber',['CP500','CP500G'])->orderBy('TarikhNotis','DESC')->get();
        $inboxcount = TaxInbox::where('NoId','=',$user->reference_id)->where('Unread','=','true')->count();
        return view('dashboard.inbox',compact('data','inboxcount','datacp500'));

       
           
    }

    public function templatetype()
    {
        $data = LkpTemplate::get();

        return view('template.type',compact('data'));
        
    }

    public function template()
    {
        $data = TaxTemplate::with('type')->get();
        $datalookup = TaxTemplateLookup::get();

        return view('template.index',compact('data','datalookup'));
        
    }

    public function addtemplate()
    {
        $lkptemplate = LkpTemplate::get();
        $datalookup = TaxTemplateLookup::get();

        return view('template.add',compact('lkptemplate','datalookup'));
        
    }

    public function addtemplatetype()
    {
        return view('template.addtype');
    }


    public function edittemplate($id)
    {
        $data = TaxTemplate::where('id','=',$id)->first();
        $lkptemplate = LkpTemplate::get();
         $datalookup = TaxTemplateLookup::get();

        return view('template.edit',compact('data','lkptemplate','datalookup'));
        
    }

    public function edittemplatetype($id)
    {
        $lkptemplate = LkpTemplate::where('id','=',$id)->first();

        return view('template.edittype',compact('lkptemplate'));
        
    }



    public function savetemplate(Request $request)
    {

        // dd($request->detail);
        $findold = TaxTemplate::where('fk_lkp_template','=',$request->lookup)->get();
        foreach ($findold as $key =>$ids) {

            $findolddata = TaxTemplate::where('id','=',$ids->id)->first();
            $findolddata->status = 0;
            $findolddata->update();
            # code...
        }


        $newtemp = new TaxTemplate;
        $newtemp->fk_lkp_template = $request->lookup;
        if($request->detail_en){

            $newtemp->detail_en = $request->detail_en;

        }else{
            
            $newtemp->detail_en = $request->detail;
        }
        $newtemp->detail = $request->detail;
        $newtemp->status = 1;
        $newtemp->save();

         return redirect::to('/mail/template');

        
    }

    public function savetemplatetype(Request $request)
    {

        // dd($request->detail);
        
        $newtemp = new LkpTemplate;
        $newtemp->type = $request->type;
        $newtemp->status = 1;
        $newtemp->save();

         return redirect::to('/mail/templatetype');

        
    }

    public function updatetemplate(Request $request)
    {

        // dd($request);
        $findold = TaxTemplate::where('fk_lkp_template','=',$request->lookup)->get();
        foreach ($findold as $key =>$ids) {

            if($request->status == 1)
            {

                $findolddata = TaxTemplate::where('id','=',$ids->id)->first();
                $findolddata->status = 0;
                $findolddata->update();
            }
        }


        $newtemp = TaxTemplate::where('id','=',$request->id)->first();
        $newtemp->fk_lkp_template = $request->lookup;
        if($request->detail_en){

            $newtemp->detail_en = $request->detail_en;

        }else{

            $newtemp->detail_en = $request->detail;
        }
        $newtemp->detail = $request->detail;
        $newtemp->status = $request->status;
        $newtemp->update();

         return redirect::to('/mail/template');

        
    }

    public function updatetemplatetype(Request $request)
    {

        // dd($request);
        
        $newtemp = LkpTemplate::where('id','=',$request->id)->first();
        $newtemp->type = $request->type;
        $newtemp->status = $request->status;
        $newtemp->update();

         return redirect::to('/mail/templatetype');

        
    }

    public function read($id)
    {

       $data = TaxInbox::where('id','=',$id)->first();
       $data->Unread = 'false';
       $data->update();
       $type = $data->Sumber;
       $body = $this->repos->template($id);
       return view('inbox.read',compact('body','data','type','id'));
        
    }

    public function cptable($id)
    {
        $inboxdata = TaxInbox::where('id','=',$id)->first(); 
        $cp500data = TaxCp500::where('refid','=',$inboxdata->RefId)->first(); 
        return view('inbox.cptable',compact('cp500data'));
        
    }

    public function inboxmobile()
    {

        $dataprofile = $this->apirepo->api_inbox();
        $users= auth()->user();
        $id = $users->id;
        $user= User::where('id',$id)->first();
        $data = TaxInbox::where('NoId','=',$user->reference_id)->whereNotIn('Sumber',['CP500','CP500G'])->where('Sumber','!=','CP500G')->orderBy('TarikhNotis','DESC')->get();
        $datacp500 = TaxInbox::where('NoId','=',$user->reference_id)->whereIn('Sumber',['CP500','CP500G'])->orderBy('TarikhNotis','DESC')->get();
        $inboxcount = TaxInbox::where('NoId','=',$user->reference_id)->count();
        return view('mobile.inbox',compact('data','inboxcount','datacp500','user'));


    }

    public function mobileread($id,$types)
    {

       $data = TaxInbox::where('id','=',$id)->first();
       $data->Unread = 'false';
       $data->update();
       $type = $data->Sumber;
       $body = $this->repos->template($id);
       return view('mobile.readinbox',compact('body','data','type','id','types'));
        
    }

    


}
