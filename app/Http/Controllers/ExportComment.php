<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use App\Models\MngLkpAnswerPicFeedback;
use App\Models\MngLkpQuestionFeedback;
use App\Models\Mngfeedback;
use App\User;
use Auth;

class ExportComment implements FromView
{

	protected $collectionparam;

 public function __construct(Collection $collectionparam) {

            $this->type = $collectionparam[0];
            $this->datefrom = $collectionparam[1];
            $this->dateto = $collectionparam[2];
        }


// public function view(): View
// {
//     return view('admin.feedback.excelfeedback', [
//         'params' =>  $this->collectionparam,
//     ]);
//  }

 public function view(): View
{
    

  

			    $data=Mngfeedback::where('date_feedback','>=', array( date("Y-m-d", strtotime($this->datefrom))))   
                          ->where('date_feedback','<', array( date("Y-m-d", strtotime($this->dateto))))
                          ->whereNotNull('comment')
                          ->paginate(20);






            return view('admin.feedback.excelcomment', [
                'type' =>   $this->type,
                'datefrom' =>   $this->datefrom,
                'dateto' =>   $this->dateto,
                'data'=>$data

            ]);
 }
}