<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\Models\TaxElejarDetailCalendar;
use App\Models\TaxPcbDetailCalendar;
use App\Models\TaxPcbDetailTahun;
use App\Models\TaxElejarDetail;
use App\Models\MngAnnouncement;
use App\Models\MngQuestion;
use App\Models\TaxProfile;
use App\Models\MngService;
use App\Data\Repo\ApiRepo;
use App\Models\TaxElejar;
use App\Models\MngMobile;
use App\Models\TaxEspc;
use App\Models\MngApp;
use App\User;
use DateTimeZone;
use hash_hmac;
use DateTime;
use Auth;
use Curl;
use DB;

class PcbController extends BaseController
{
    public function __construct(ApiRepo $repos)
    {
      
        $this->repos = $repos;  
    
    }


    public function detail($type)
    {
        $users= auth()->user();
        $year = date('Y');
        $lastyear = $year-1;
        $selectedyear = $year;
        $id = $users->id;
        $user= User::where('id',$id)->first();

        if($type == 'calendar')
        {
            $calendar = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$year)->get();
            $total = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$year)->orderBy('id', 'desc')->first();
            return view('pcb.calendarindex',compact('calendar','year','lastyear','selectedyear','total')); 

        }else
        {
            $calendar = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$year)->get();
            $total = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$year)->orderBy('id', 'desc')->first();
            return view('pcb.yearindex',compact('calendar','year','lastyear','selectedyear','total')); 

        }
    }

    public function year($selectedyear)
    {
        $users= auth()->user();
        $year = date('Y');
        $lastyear = $year-1;
        $id = $users->id;
        $user= User::where('id',$id)->first();
        $calendar = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$selectedyear)->get();
        $total = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$selectedyear)->orderBy('id', 'desc')->first();
        return view('pcb.yearindex',compact('calendar','year','lastyear','selectedyear','total')); 

    }

    public function calendar($selectedyear)
    {
        $users= auth()->user();
        $year = date('Y');
        $lastyear = $year-1;
        $id = $users->id;
        $user= User::where('id',$id)->first();
        $calendar = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$selectedyear)->get();
        $total = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$selectedyear)->orderBy('id', 'desc')->first();
        return view('pcb.calendarindex',compact('calendar','year','lastyear','selectedyear','total')); 
    
    }

    public function mobilepcb()
    {
        $users= auth()->user();
        $year = date('Y');
        $lastyear = $year-1;
        $id = $users->id;
        $elejar = $this->repos->api_elejar();

        $yearlycurrent = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$year)->get();
        $yearlylast = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$lastyear)->get();
        $totalyearlycurrent = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$year)->orderBy('id', 'desc')->first();
        $totalyearlylast = TaxPcbDetailTahun::where('fk_users','=',$id)->where('Tahun',$lastyear)->orderBy('id', 'desc')->first();

        $calendarcurrent = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$year)->get();
        $calendarlast = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$lastyear)->get();
        $totalcalcurrent = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$year)->orderBy('id', 'desc')->first();
        $totalcallast = TaxPcbDetailCalendar::where('fk_users','=',$id)->where('Tahun',$lastyear)->orderBy('id', 'desc')->first();

        return view('mobile.pcb.index',compact('year','lastyear','yearlycurrent','yearlylast','totalyearlycurrent','totalyearlylast','calendarcurrent','calendarlast','totalcalcurrent','totalcallast')); 
    
    }





     
}
