<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\Models\TaxProfile;
use App\Data\Repo\ApiRepo;
use App\Models\MngService;
use App\Models\MngMobile;
use App\Models\MngBanner;
use App\Models\UserToken;
use App\Models\TaxInbox;
use App\Models\comlist;
use App\User;
use Redirect;
use Auth;
use File;
use Curl;

class LoginController extends BaseController
{

    public function __construct(ApiRepo $repos)
    {
      
        $this->repos = $repos;  
    
    }


    public function ssologin(request $request)
    {

        $token = $request->token;
        $tryuser = User::where('reference_id','=',$token)->first();
        if($tryuser)
        {
            auth()->loginUsingId($tryuser->id);
            $dataprofile = $this->repos->api_profile();
            return 1;
        }

    }

    public function sso(request $request)
    {

        $token = $request->token;
        $tryuser = User::where('reference_id','=',$token)->first();
        if($tryuser)
        {
            auth()->loginUsingId($tryuser->id);
            $dataprofile = $this->repos->api_profile();
            return redirect(url('/'));
        }
       
        $token = $request->token;
        $response = Curl::to(env('API_TOKEN'))
                         ->withHeaders( array( 'Cookie: SSOCookies='.$token) )
                         ->returnResponseObject()
                         ->get();

        $result = json_decode($response->content);        

        $access = $result->RequestTokenMyTaxResult->Access;

        if($access > 0)
        {
            $IdNo   = $result->RequestTokenMyTaxResult->IdNo;
            $IdType = $result->RequestTokenMyTaxResult->IdType;

            $user = User::where('reference_id','=',$IdNo)->first();
            if($user)
            {
                
                $user->token = $result->RequestTokenMyTaxResult->Token;
                // $user->password = $request->idenpass;
                $user->update();

                
            }
                  
                // $userlogin = User::where('reference_id','=',$IdNo)->first();
                auth()->loginUsingId($user->id);
                // $dataprofile = $this->repos->api_profile();

                return 1;


                // return redirect(url('/'));

         
        }else
        {
            $data = $this->createnewauth($token);
            return $data;
            // return redirect(url('https://ezlatihan.hasil.gov.my/CI/'));
        }


    }

    public function createnewauth($token)
    {
        $tryuser = User::where('token','=',$token)->first();
        $username = base64_encode($tryuser->reference_id); 
        $password = $tryuser->password; 
        $idtype = $tryuser->reference_type;
        $callback = '';
        $nocp ='';


        $response = Curl::to(env('API_DOMAIN').'/SvcSSOMyTax/SSOMyTax.svc/user/Login')->withData( 
                            array
                            ( 
                                'callback' => $callback,
                                'username' => $username,
                                'password' => $password,
                                'IdType'   => $idtype,
                                'nocp'     => $nocp
                            ) 
                        )
                        ->returnResponseObject()
                        ->withResponseHeaders()
                        ->get();

            if($response->status == 0)
            {
                $error = $response->error;
                return view('auth.login',compact('error'));

            }else{

                auth()->loginUsingId($tryuser->id);
                return 1;
            }

    }

    public function log(request $request)
    {
        $now = date('Y-m-d');
        

        if(env('DB_CONNECTION') == 'mysql')
      {

          $banner = MngBanner::where('status','=',1)
           ->whereRaw('start_date <= "'.$now.'" AND end_date >= "'.$now.'"')
           ->orderBy('order','ASC')->get();
      }else{

           $banner = MngBanner::where('status','=',1)
                       ->whereRaw('start_date <= GETDATE() AND end_date >= GETDATE()')
                       ->orderBy('order','ASC')->get();

      }
        
        if($request->slog == 1)
        {
            $uname = $request->idenno;
            $username = base64_encode($request->idenno); 
            $idtype = $request->idtype;
            $stage = 2;
            $list = MngService::where('status','=',1)->limit(4)->get();

            $response = Curl::to(env('API_DOMAIN').'/svcsso/SSOService.svc/user/getphrase')->withData( 
                            array
                            ( 
                                'idno' => $username,
                                'idtype' => $idtype
                            ) 
                        )
                        ->returnResponseObject()
                        ->withResponseHeaders()
                        ->get();

            // dd($response);
            if($response->status == 0)
                {
                    $error = $response->error;
                    $list = MngService::where('status','=',1)->get();
                    return view('auth.loginpage',compact('error','list','banner','idtype'));

                }

            $phrase = json_decode($response->content);
            return view('auth.loginpage',compact('phrase','idtype','uname','stage','list','banner'));

        }else
        {

            $username = base64_encode($request->idenno); 
            $password = base64_encode($request->idenpass); 
            $idtype = $request->idtype;
            $callback = '';
            $nocp ='';
            $response = Curl::to(env('API_DOMAIN').'/svcsso/SSOService.svc/user/login')->withData( 
                                array
                                ( 
                                    'callback' => $callback,
                                    'username' => $username,
                                    'password' => $password,
                                    'IdType'   => $idtype,
                                    'nocp'     => $nocp
                                ) 
                            )
                            ->returnResponseObject()
                            ->withResponseHeaders()
                            ->get();

                            // dd($response);

                if($response->status == 0)
                {
                    $error = $response->error;
                    $list = MngService::where('status','=',1)->get();
                    return view('auth.loginpage',compact('error','list','banner','idtype'));

                }

                $trimresult = substr($response->content,2);
                $result = json_decode(substr($trimresult, 0, -2));

                // dd($result);

                if($result->LoginResult->Status == 'Success')
                {
                    $user = User::where('reference_id','=',$request->idenno)->first();
                    if($user)
                    {
                        $user->password = $password;
                        $user->access = $result->LoginResult->Access;
                        $user->token = $result->LoginResult->Token;
                        // $user->password = $request->idenpass;
                        $user->update();

                        
                    }else
                    {

                        $users = new User;
                        $users->name = str_replace("+"," ",$result->LoginResult->FullName);
                        $users->reference_id = $request->idenno;
                        $users->access = $result->LoginResult->Access;
                        $users->reference_type = $idtype;
                        $users->password = $password;
                        $users->token = $result->LoginResult->Token;
                        $users->save();

                       

                    }

                      
                    $userlogin = User::where('reference_id','=',$request->idenno)->first();
                    auth()->loginUsingId($userlogin->id);
                    $dataprofile = $this->repos->api_profile();


                    return redirect(url('/'));


                }else
                {
                    $list = MngService::where('status','=',1)->get();
                    $error = $result->LoginResult->Error;
                    return view('auth.loginpage',compact('error','list','banner','idtype'));
                }
        }


    }

    
    public function loginmodal(request $request)
    {
        $now = date('Y-m-d');
        

       if(env('DB_CONNECTION') == 'mysql')
      {

          $banner = MngBanner::where('status','=',1)
           ->whereRaw('start_date <= "'.$now.'" AND end_date >= "'.$now.'"')
           ->orderBy('order','ASC')->get();
      }else{

           try{

                $banner = MngBanner::where('status','=',1)
                       ->whereRaw('start_date <= GETDATE() AND end_date >= GETDATE()')
                       ->orderBy('order','ASC')->get();

            }catch(\Illuminate\Database\QueryException $ex){ 

                 $banner = MngBanner::where('status','=',1)
           ->whereRaw('start_date <= "'.$now.'" AND end_date >= "'.$now.'"')
           ->orderBy('order','ASC')->get();
            }

      }

        if(isset($request->idenno))
        {
            if($request->slog == 1)
            {
                $uname = $request->idenno;
                $username = base64_encode($request->idenno); 
                $idtype = $request->idtype;
                $stage = 2;
                $list = MngService::where('status','=',1)->limit(4)->get();

                $response = Curl::to(env('API_DOMAIN').'/SSOService.svc/user/getphrase')->withData( 
                                array
                                ( 
                                    'idno' => $username,
                                    'idtype' => $idtype
                                ) 
                            )
                            ->returnResponseObject()
                            ->withResponseHeaders()
                            ->get();
                if($response->status == 0)
                {
                    $error = $response->error;
                    $list = MngService::where('status','=',1)->get();
                    // return redirect_to('auth.login',compact('error','list','banner','idtype'));
                    // return redirect::to('/')->withError(__('ID Tuntutan telah dijana'));

                    return response()->json(['success'=>true,'result'=>$request,'url'=> route('home', ['error' => $error ])]);

                }

                 if($response->status == 503)
                {
                    $error = 'HTTP Error 503. The Login service is unavailable.';
                    $list = MngService::where('status','=',1)->get();
                    // return redirect_to('auth.login',compact('error','list','banner','idtype'));
                    // return redirect::to('/')->withError(__('ID Tuntutan telah dijana'));

                    return response()->json(['success'=>true,'result'=>$request,'url'=> route('home', ['error' => $error ])]);

                }

                $phrase = json_decode($response->content);
                return view('auth.loginmodal',compact('phrase','idtype','uname','stage','list','banner'));

            }else
            {

                $username = base64_encode($request->idenno); 
                $password = base64_encode($request->idenpass); 
                $idtype = $request->idtype;
                $callback = '';
                $nocp ='';
                $response = Curl::to(env('API_DOMAIN').'/SSOService.svc/user/login')->withData( 
                                    array
                                    ( 
                                        'callback' => $callback,
                                        'username' => $username,
                                        'password' => $password,
                                        'IdType'   => $idtype,
                                        'nocp'     => $nocp
                                    ) 
                                )
                                ->returnResponseObject()
                                ->withResponseHeaders()
                                ->get();

                                // dd($response);

                    if($response->status == 0)
                    {
                        $error = $response->error;
                        $list = MngService::where('status','=',1)->get();
                        return view('auth.login',compact('error','list','banner','idtype'));

                    }

                    $trimresult = substr($response->content,2);
                    $result = json_decode(substr($trimresult, 0, -2));

                    // dd($result);

                    if($result->LoginResult->Status == 'Success')
                    {
                        $user = User::where('reference_id','=',$request->idenno)->first();
                        if($user)
                        {
                            $user->password = $password;
                            $user->access = $result->LoginResult->Access;
                            $user->token = $result->LoginResult->Token;
                            // $user->password = $request->idenpass;
                            $user->update();

                            
                        }else
                        {

                            $users = new User;
                            $users->name = str_replace("+"," ",$result->LoginResult->FullName);
                            $users->reference_id = $request->idenno;
                            $users->access = $result->LoginResult->Access;
                            $users->reference_type = $idtype;
                            $users->password = $password;
                            $users->token = $result->LoginResult->Token;
                            $users->save();

                           

                        }

                          
                        $userlogin = User::where('reference_id','=',$request->idenno)->first();
                        auth()->loginUsingId($userlogin->id);
                        $dataprofile = $this->repos->api_profile();


                        return redirect(url('/'));


                    }else
                    {
                        $list = MngService::where('status','=',1)->limit(4)->get();
                        $error = $result->LoginResult->Error;
                        return view('auth.login',compact('error','list','banner','idtype'));
                    }
            }
        }else
        {
            $idtype = 0;
            return view('auth.loginmodal',compact('banner','idtype'));
        }
        
        


    }

    public function login($idtype)
    {
        $now = date('Y-m-d');


        if(env('DB_CONNECTION') == 'mysql')
      {

          $banner = MngBanner::where('status','=',1)
           ->whereRaw('start_date <= "'.$now.'" AND end_date >= "'.$now.'"')
           ->orderBy('order','ASC')->get();
      }else{

           $banner = MngBanner::where('status','=',1)
                       ->whereRaw('start_date <= GETDATE() AND end_date >= GETDATE()')
                       ->orderBy('order','ASC')->get();

      }
        return view('auth.loginpage',compact('idtype','banner'));
    }

    public function logout()
    {

        $username = '1586180505883';//base64_encode($request->idenno); 
        $password = '';//base64_encode($request->idenpass); 
        $idtype = '1';//$request->idtype;
        $callback = 'jQuery1609246550949812533_1586182209083&Type=1&_=1586182218960';
        $nocp ='';

        $response = Curl::to(env('API_DOMAIN').'/SSOMyTax.svc/user/Logout')->withData( 
            array
            ( 
                'callback' => $callback,
                // '_' => $username,
                // 'password' => $password,
                // 'Type'   => $idtype,
                // 'nocp'     => $nocp

            ) 
        )
        ->returnResponseObject()
        ->withResponseHeaders()
        ->get();

            // if($response->status == 0)
            // {
            //     $error = $response->error;
            //     return view('auth.login',compact('error'));

            // }

            // $trimresult = substr($response->content,2);
            // $result = json_decode(substr($trimresult, 0, -2));

            Auth::logout();

        return redirect('home/checkdevice');

            // if($result->LoginResult->Access > 0)
            // {
            //     $user = User::where('reference_id','=',$request->idenno)->first();
            //     if($user)
            //     {
            //         $user->password = Hash::make($request->idenpass);
            //         $user->access = $result->LoginResult->Access;
            //         // $user->password = $request->idenpass;
            //         $user->update();

            //     }else
            //     {

            //         $users = new User;
            //         $users->name = str_replace("+"," ",$result->LoginResult->FullName);
            //         $users->reference_id = $request->idenno;
            //         $users->access = $result->LoginResult->Access;
            //         $users->reference_type = $idtype;
            //         $users->password = Hash::make($request->idenpass);
            //         $users->save();

            //     }

                  
            //     $userlogin = User::where('reference_id','=',$request->idenno)->first();
            //     auth()->loginUsingId($userlogin->id);
            //     $dataprofile = $this->repos->api_profile();


            //     return redirect(url('/'));


            // }else
            // {
            //     $error = $result->LoginResult->Error;
            //     return view('auth.login',compact('error'));
            // }

            // dd($result->LoginResult->Access);

    }

    public function mobilelog(request $request)
    {

       // dd($request);
        if($request->slog == 1)
        {
            $uname = $request->idenno;
            $username = base64_encode($request->idenno); 
            $idtype = $request->idtype;
            $stage = 2;

            $response = Curl::to(env('API_DOMAIN').'/SSOService.svc/user/getphrase')->withData( 
                            array
                            ( 
                                'idno' => $username,
                                'idtype' => $idtype
                            ) 
                        )
                        ->returnResponseObject()
                        ->withResponseHeaders()
                        ->get();


            if($response->status == 0)
            {
               $error = $response->error;
                $agent = new Agent();
                $device = $agent->device();
                $mobile = MngMobile::where('status','=',1)->get();
                return view('auth.mobilelogin',compact('error','device','mobile'));

            }

             if($response->status == 503)
            {
                $error = 'HTTP Error 503. The Login service is unavailable.';
                    $agent = new Agent();
                    $device = $agent->device();
                    $mobile = MngMobile::where('status','=',1)->get();
                    return view('auth.mobilelogin',compact('error','device','mobile'));


            }

            $phrase = json_decode($response->content);
            $agent = new Agent();
            $device = $agent->device();
            $mobile = MngMobile::where('status','=',1)->get();
            return view('auth.mobilelogin',compact('phrase','idtype','uname','stage','device','mobile'));

        }else
        {


            $username = base64_encode($request->idenno); 
            $password = base64_encode($request->idenpass); 
            $idtype = $request->idtype;
            $callback = '';
            $nocp ='';

            $response = Curl::to(env('API_DOMAIN').'/SSOService.svc/user/Login')->withData( 
                                array
                                ( 
                                    'callback' => $callback,
                                    'username' => $username,
                                    'password' => $password,
                                    'IdType'   => $idtype,
                                    'nocp'     => $nocp
                                ) 
                            )
                            ->returnResponseObject()
                            ->withResponseHeaders()
                            ->get();

                if($response->status == 0)
                {
                    $error = $response->error;
                    $agent = new Agent();
                    $device = $agent->device();
                    $mobile = MngMobile::where('status','=',1)->get();
                    return view('auth.mobilelogin',compact('error','device','mobile'));

                }

                $trimresult = substr($response->content,2);
                $result = json_decode(substr($trimresult, 0, -2));

                if($result->LoginResult->Status == 'Success')
                {
                    $user = User::where('reference_id','=',$request->idenno)->first();
                    if($user)
                    {
                        $user->password = $password;
                        $user->access = $result->LoginResult->Access;
                        $user->token = $result->LoginResult->Token;
                        // $user->password = $request->idenpass;
                        $user->update();

                        
                    }else
                    {

                        $users = new User;
                        $users->name = str_replace("+"," ",$result->LoginResult->FullName);
                        $users->reference_id = $request->idenno;
                        $users->access = $result->LoginResult->Access;
                        $users->reference_type = $idtype;
                        $users->password = $password;
                        $users->token = $result->LoginResult->Token;
                        $users->save();

                       

                    }

                      
                    $userlogin = User::where('reference_id','=',$request->idenno)->first();
                    auth()->loginUsingId($userlogin->id);
                    $dataprofile = $this->repos->api_profile();


                    return redirect(url('/mobile'));


                }else
                {
                    $error = $result->LoginResult->Error;
                    $agent = new Agent();
                    $device = $agent->device();
                    $mobile = MngMobile::where('status','=',1)->get();
                    return view('auth.mobilelogin',compact('error','device','mobile'));
                }
        }

           
    }

    public function logweb($id)
    {
        $userlogin = User::where('id','=',$id)->first();
        auth()->loginUsingId($userlogin->id);
        $dataprofile = $this->repos->api_profile();
        return redirect(url('/mobile'));
    }



    


    public function profile()
    {
    
       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();
       
       $comlist = $this->repos->api_comlist();

       $dataprofile = $this->repos->api_profile_page();
       $dataprofiles = $this->repos->api_profile_add();
       $comlist = comlist::where('fk_users','=',$user->id)->get();
       $comlistcount = comlist::where('fk_users','=',$user->id)->count();
       $profile = TaxProfile::where('fk_users','=',$id)->first();
       return view('dashboard.profile',compact('user','profile','comlist','comlistcount','dataprofile'));
          
    }
}
