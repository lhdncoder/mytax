<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use App\Models\TaxElejarDetailCalendar;
use App\Models\TaxElejarDetailCurrent;
use App\Models\MngAnnouncement;
use App\Models\TaxElejarDetail;
use App\Data\Repo\SearchRepo;
use App\Models\UserSetting;
use App\Models\TaxProfile;
use App\Models\MngService;
use App\Models\MngMobile;
use App\Models\MngBanner;
use App\Models\TaxElejar;
use App\Models\TaxInbox;
use App\Models\Comlist;
use App\Models\MngApp;
use App\User;
use DateTimeZone;
use hash_hmac;
use DateTime;
use Session;
use Config;
use Cookie;
use Auth;
use Curl;
use App;
use DB;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct(SearchRepo $repos)
    {
        $this->repos = $repos;
        
    }

    public function index()
    {
         return view('search.index');

    }


    public function iden(Request $request)
    {
        $data = $this->repos->api_profile($request);
        // dd($data);
        if($data == '1')
        {
            return view('search.nodata');

        }else{

            $elejar = $this->repos->api_elejar($request);
            // dd($elejar);

            if($elejar == '0')
            {
                return view('search.nodata');

            }else{

                $user = User::where('reference_id','=',$request->idenno)->first();
                $id = $user->id;


                $comlist = $this->repos->api_comlist($request);

                $dataprofile = $this->repos->api_profile_page($request);
                $dataprofiles = $this->repos->api_profile_add($request);
                $comlist = comlist::where('fk_users','=',$user->id)->get();
                $comlistcount = comlist::where('fk_users','=',$user->id)->count();
                $profile = TaxProfile::where('fk_users','=',$id)->first();

                $comlist = comlist::where('fk_users','=',$id)->get();
                $user= User::where('id',$id)->first();
                $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->get();
                $lejarcom = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','SYARIKAT')->get();
                $check1 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                           ->whereNull('lejar_type')
                           ->where('income_type','=','SALARY')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();
                $check2 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                           ->where('lejar_type','=','INDIVIDU')
                           ->where('income_type','=','SALARY')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();

                $check3 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                           ->whereNull('lejar_type')
                           ->where('income_type','=','PROPERTIES')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();
                $check4 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                           ->where('lejar_type','=','INDIVIDU')
                           ->where('income_type','=','PROPERTIES')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();

                $checksal = $check1+$check2;
                $checkpro = $check3+$check4;

                app('session')->put('suid', $id);

                // dd($dataprofile);

                return view('search.lejar',compact('lejar','lejarcom','checkpro','checksal','user','profile','comlist','comlistcount','dataprofile')); 

            }

        }

    }



    public function iden2(Request $request)
    {

        $search = $this->repos->api_profile_pagesearch($request);

        // dd($search);

        if($search == [])
        {
            return view('search.nodata');
        }else{

            if($search->NEW_IC_NO)
            {
                $request = new \Illuminate\Http\Request();
                $request['idenno']=$search->NEW_IC_NO; 
                $request['idtype']=1; 
            }else
            {

                return view('search.nodata');
            }

           

        }

        $data = $this->repos->api_profile($request);
        // dd($data);
        if($data == '1')
        {
            return view('search.nodata');

        }else{

            $elejar = $this->repos->api_elejar($request);
            // dd($elejar);

            if($elejar == '0')
            {
                return view('search.nodata');

            }else{

                $user = User::where('reference_id','=',$request->idenno)->first();
                $id = $user->id;


                $comlist = $this->repos->api_comlist($request);

                $dataprofile = $this->repos->api_profile_page($request);
                $dataprofiles = $this->repos->api_profile_add($request);
                $comlist = comlist::where('fk_users','=',$user->id)->get();
                $comlistcount = comlist::where('fk_users','=',$user->id)->count();
                $profile = TaxProfile::where('fk_users','=',$id)->first();

                $comlist = comlist::where('fk_users','=',$id)->get();
                $user= User::where('id',$id)->first();
                $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->get();
                $lejarcom = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','SYARIKAT')->get();
                $check1 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                           ->whereNull('lejar_type')
                           ->where('income_type','=','SALARY')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();
                $check2 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                           ->where('lejar_type','=','INDIVIDU')
                           ->where('income_type','=','SALARY')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();

                $check3 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                           ->whereNull('lejar_type')
                           ->where('income_type','=','PROPERTIES')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();
                $check4 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                           ->where('lejar_type','=','INDIVIDU')
                           ->where('income_type','=','PROPERTIES')
                           ->whereIn('TRANSACTION_CODE',['071','072','192'])
                           ->count();

                $checksal = $check1+$check2;
                $checkpro = $check3+$check4;

                app('session')->put('suid', $id);

                // dd($dataprofile);

                return view('search.lejar',compact('lejar','lejarcom','checkpro','checksal','user','profile','comlist','comlistcount','dataprofile')); 

            }

        }

    }


    function indexlejar()
    {
         $uids = app('session')->get('suid'); 
         $id = $uids;
         $user= User::where('id',$id)->first();
         $comlist = comlist::where('fk_users','=',$id)->get();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->get();
         $lejarcom = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','SYARIKAT')->get();
         $dataprofile = $this->repos->api_profile_pageindex($user);

         $check1 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                   ->whereNull('lejar_type')
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check2 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                   ->where('lejar_type','=','INDIVIDU')
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $check3 = TaxElejarDetailCalendar::where('fk_users','=',$id)
                   ->whereNull('lejar_type')
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check4 = TaxElejarDetailCurrent::where('fk_users','=',$id)
                   ->where('lejar_type','=','INDIVIDU')
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $checksal = $check1+$check2;
         $checkpro = $check3+$check4;

       

           return view('search.lejarindex',compact('lejar','lejarcom','user','comlist','checkpro','checksal','dataprofile')); 

        
    }


    function penutup($type,$ltype)
    {
         $referer = $_SERVER['HTTP_REFERER'];

         $uids = app('session')->get('suid'); 
         $id = $uids;
         $user= User::where('id',$id)->first();
         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
         $lejar = TaxElejarDetail::where('fk_users','=',$id)->where('income_type',$type)->where('lejar_type','=',$typel)->get();
         $profile = TaxProfile::where('fk_users','=',$id)->first();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         $calendar = DB::SELECT("select Tahun from tax_elejar_detail_calendar where fk_users = '$id' and income_type = '$type' and lejar_type is null group by TAHUN");

         if(strpos($referer, 'mobile') !== false){

             return view('mobile.lejar.penutup',compact('lejar','profile','user','calendar','typelejar','typel','ltype')); 

        } else{

            return view('lejar.penutup',compact('lejar','profile','user','calendar','typelejar','typel','ltype')); 

        }

        
    }

    function current($year,$type,$ltype)
    {

         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
         $uids = app('session')->get('suid'); 
         $id = $uids;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCurrent::where('fk_users','=',$id)->where('TAHUN','=',$year)->where('lejar_type','=',$typel)->where('income_type',$type)->get();
         $profile = TaxProfile::where('fk_users','=',$id)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();



         $referer = $_SERVER['HTTP_REFERER'];
         if(strpos($referer, 'mobile') !== false){

           return view('mobile.lejar.current',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel')); 

        } else{

          return view('lejar.current',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel')); 

        }
         
    }

    
    function calendar($year,$type)
    {
         $uids = app('session')->get('suid'); 
         $id = $uids;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCalendar::where('fk_users','=',$id)->where('TAHUN','=',$year)->where('income_type',$type)->whereNull('lejar_type')->get();
         $profile = TaxProfile::where('fk_users','=',$id)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=','INDIVIDU')->where('income_type','=',$type)->first();
          $typel = 'INDIVIDU';
          $ltype = 1;


         $referer = $_SERVER['HTTP_REFERER'];
         if(strpos($referer, 'mobile') !== false){

           return view('mobile.lejar.calendar',compact('profile','user','calendar','year','lejar','typelejar','typel','ltype')); 

        } else{

          return view('lejar.calendar',compact('profile','user','calendar','year','lejar','typelejar')); 

        }

         
    }

    function comdata($id,$type)
    {
        // dd($type);
         $elejarcom = $this->repos->api_elejarcomlist($id);
         $profile = comlist::where('id','=',$id)->first();
         $users= $profile->fk_users;
         $user= User::where('id',$users)->first();
         $lejar = TaxElejarDetail::where('fk_lkp_tcl','=',$id)->where('income_type','=',$type)->get();
         $ltype = 2;
         $typel = 'SYARIKAT';
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         $lid = $id;
         $calendar = DB::SELECT("select Tahun from tax_elejar_detail_current where fk_lkp_tcl = '$id' and income_type = '$type' group by TAHUN");
         $calendar2 = DB::SELECT("select Tahun from tax_elejar_detail_calendar where fk_lkp_tcl = '$id' and income_type = '$type' group by TAHUN");

         $check1 = TaxElejarDetailCalendar::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check2 = TaxElejarDetailCurrent::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','SALARY')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $check3 = TaxElejarDetailCalendar::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();
         $check4 = TaxElejarDetailCurrent::where('fk_lkp_tcl','=',$id)
                   ->where('income_type','=','PROPERTIES')
                   ->whereIn('TRANSACTION_CODE',['071','072','192'])
                   ->count();

         $checksal = $check1+$check2;
         $checkpro = $check3+$check4;

         return view('lejar.penutupcom',compact('profile','lejar','ltype','typel','typelejar','calendar','type','lid','calendar2','checksal','checkpro')); 
    }

    function currentcom($year,$type,$ltype,$lid)
    {

         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
          $uids = app('session')->get('suid'); 
         $id = $uids;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCurrent::where('fk_lkp_tcl','=',$lid)->where('TAHUN','=',$year)->where('lejar_type','=',$typel)->where('income_type',$type)->whereNotin('TRANSACTION_CODE',['071','072','192'])->get();
         $profile = comlist::where('id','=',$lid)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         return view('lejar.currentcom',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel','lid','type')); 
    }

    function calendarcom($year,$type,$ltype,$lid)
    {

         if($ltype == 1){

            $typel = 'INDIVIDU';
         }else
         {
            $typel = 'SYARIKAT';
         }
         $uids = app('session')->get('suid'); 
         $id = $uids;
         $user= User::where('id',$id)->first();
         $calendar = TaxElejarDetailCalendar::where('fk_lkp_tcl','=',$lid)->where('TAHUN','=',$year)->where('lejar_type','=',$typel)->where('income_type',$type)->whereNotin('TRANSACTION_CODE',['071','072','192'])->get();
         $profile = comlist::where('id','=',$lid)->first();
         $lejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->get();
         $typelejar = TaxElejar::where('fk_users','=',$id)->where('lejar_type','=',$typel)->where('income_type','=',$type)->first();
         return view('lejar.calendarcom',compact('profile','user','calendar','year','lejar','typelejar','ltype','typel','lid','type')); 
    }

}
