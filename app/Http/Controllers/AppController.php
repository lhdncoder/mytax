<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\TaxElejarDetailCalendar;
use App\Models\TaxElejarDetailCurrent;
use App\Models\TaxElejarDetail;
use App\Models\MngAnnouncement;
use App\Models\MngQuestion;
use App\Data\Repo\ApiRepo;
use App\Models\TaxProfile;
use App\Models\UserSetting;
use App\Models\MngService;
use App\Models\TaxElejar;
use App\Models\TaxEspc;
use App\Models\MngApp;
use App\Models\Hits;
use App\User;
use DateTimeZone;
use hash_hmac;
use DateTime;
use Curl;
use Auth;
use DB;

class AppController extends BaseController
{
    public function __construct(ApiRepo $repos)
    {
      
        $this->repos = $repos;  
    
    }

    function view($id)
    {

      
        $users= auth()->user();
        $ids = $users->id;
        $user= User::where('id',$ids)->first();
        $app = MngService::where('id','=',$id)->first();
        $date = (date('Y-m-d'));
        // $time = (date('H:i:s'));

        $count = Hits::where('fk_lkp_mng_service','=',$id)
                     ->where('date','=',$date)
                     ->where('hits','>',0)
                     ->first();

        if($count){

            $count->hits = $count->hits+1;
            $count->update();
        }else{

            $hit = new Hits;
            $hit->fk_lkp_mng_service = $id;
            $hit->date = $date;
            $hit->hits = 1;
            $hit->save();
        }

        $checkfeatured = UserSetting::where('fk_users','=',$user->id)->where('key','=','featured')->first();

        if($checkfeatured)
        {

            if($checkfeatured->value)
            {
                $array = json_decode($checkfeatured->value);
                $remkey = array_search($id,$array);

                 if(is_numeric($remkey))
                 {
                   $remkey = true;

                 }else{

                    $remkey = false;
                 }

               



                $appfav = MngService::whereIn('id',$array)->get();
                $appfavcount = MngService::whereIn('id',$array)->count();

                // dd($appfavcount);
            }else{

                $appfavcount = 0;
                $appfav = [];
                $remkey = false;
                // dd($appfavcount);
            }
        }else
        {
            $appfavcount = 0;
            $appfav = [];
            $remkey = false;
        }

        return view('app.view',compact('app','user','appfavcount','appfav','remkey')); 
        
    }

    function views($id)
    {

        $list = MngService::where('status','=',1)->get();
        $app = MngService::where('id','=',$id)->first();
        $date = (date('Y-m-d'));
            // $time = (date('H:i:s'));
        $count = Hits::where('fk_lkp_mng_service','=',$id)
                         ->where('date','=',$date)
                         ->where('hits','>',0)
                         ->first();

            if($count){

                $count->hits = $count->hits+1;
                $count->update();
            }else{

                $hit = new Hits;
                $hit->fk_lkp_mng_service = $id;
                $hit->date = $date;
                $hit->hits = 1;
                $hit->save();
            }
        return view('dashboard.views',compact('app','list')); 
      
    }

    function removefav($id)
    {
         $users= auth()->user();
        $ids = $users->id;
        $user= User::where('id',$ids)->first();


        $checkfeatured = UserSetting::where('fk_users','=',$user->id)->where('key','=','featured')->first();

        if($checkfeatured->value)
        {
            $array = json_decode($checkfeatured->value);

            $remkey = array_search($id,$array,true);
            unset($array["$remkey"]);

            // dd($array);

            $appfav = MngService::whereIn('id',$array)->get();
            $appfavcount = MngService::whereIn('id',$array)->count();

            if($array == []){
                $checkfeatured->value = null;
            }else{

                $checkfeatured->value = json_encode($array);
            }
            
            $checkfeatured->update();

            // dd($appfavcount);
        }else{

            $appfavcount = 0;
            $appfav = [];
            // dd($appfavcount);
        }

       return view('app.updatefav',compact('appfav','appfavcount','user')); 
      
    }

    function addfav($id)
    {
        $users= auth()->user();
        $ids = $users->id;
        $user= User::where('id',$ids)->first();


        $checkfeatured = UserSetting::where('fk_users','=',$user->id)->where('key','=','featured')->first();

        if($checkfeatured)
        {
            if($checkfeatured->value)
            {
                $array = json_decode($checkfeatured->value);
                $array[] = $id;
                $checkfeatured->value = json_encode($array);
                $checkfeatured->update();

                return 1;

                // dd($appfavcount);
            }else
            {
                $array[] = $id;
                $checkfeatured->value = json_encode($array);
                $checkfeatured->update();

                return 1;

            }


        }else{

                $array[] = $id;

                $checkfeatured = new UserSetting;
                $checkfeatured->fk_users = $user->id;
                $checkfeatured->key = 'featured';
                $checkfeatured->value = json_encode($array);
                $checkfeatured->save();

                return 1;
                
        }

      
    }

    function mobileview($id)
    {
        $users= auth()->user();
        $ids = $users->id;
        $user= User::where('id',$ids)->first();
        $app = MngService::where('id','=',$id)->first();
        $date = (date('Y-m-d'));
            // $time = (date('H:i:s'));
        $count = Hits::where('fk_lkp_mng_service','=',$id)
                         ->where('date','=',$date)
                         ->where('hits','>',0)
                         ->first();

            if($count){

                $count->hits = $count->hits+1;
                $count->update();
            }else{

                $hit = new Hits;
                $hit->fk_lkp_mng_service = $id;
                $hit->date = $date;
                $hit->hits = 1;
                $hit->save();
            }
        return view('mobile.appview',compact('app','user')); 
    }


    
     
}
