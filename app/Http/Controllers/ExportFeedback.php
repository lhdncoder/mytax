<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use App\Models\MngLkpQuestionFeedback;
use App\Models\MngLkpAnswerPicFeedback;
use App\User;
use Auth;

class ExportFeedback implements FromView
{

	protected $collectionparam;

 public function __construct(Collection $collectionparam) {

            $this->type = $collectionparam[0];
            $this->datefrom = $collectionparam[1];
            $this->dateto = $collectionparam[2];
        }


// public function view(): View
// {
//     return view('admin.feedback.excelfeedback', [
//         'params' =>  $this->collectionparam,
//     ]);
//  }

 public function view(): View
{
    

  

				$datafeedback=MngLkpAnswerPicFeedback::with(['questfeedback' => function($author) {

                  $author->where('status', 1);

			        }])->where('status',1)->orderby('fk_lkp_question_feedback','ASC')
			           ->get()
			           ->groupBy('fk_lkp_question_feedback');
                    


       		$arr = array();


          foreach ($datafeedback as $key => $value) {


            $arr[] = [
                'id' => $value[0]->questfeedback['id'],
                'quest_en' => $value[0]->questfeedback['quest_en'],
                'quest_bm' => $value[0]->questfeedback['quest_bm'],
                'status'   => $value[0]->questfeedback['status'],
                'detail_answ' => $value,


            ];

            
        }


       $users= auth()->user();
       $id = $users->id;
       $user= User::where('id',$id)->first();






    return view('admin.feedback.excelfeedback', [
        'type' =>   $this->type,
        'datefrom' =>   $this->datefrom,
        'dateto' =>   $this->dateto,
        'arr'=>$arr,
        'user'=>$user

    ]);
 }
}