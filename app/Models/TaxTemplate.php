<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaxTemplate extends Model

{

    protected $table = 'tax_template';

    public function type()
    {
        return $this->hasOne('App\Models\LkpTemplate','id','fk_lkp_template');
    }

}