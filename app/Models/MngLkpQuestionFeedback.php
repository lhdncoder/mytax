<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MngLkpQuestionFeedback extends Model
{

   protected $table = 'mng_lkp_question_feedback';


   public function answerfeedback()
    {
        return $this->hasMany('App\Models\MngLkpAnswerPicFeedback','fk_lkp_question_feedback','id');
    }
      



}