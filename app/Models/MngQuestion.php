<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MngQuestion extends Model

{

   protected $table = 'mng_question';



	 public function answer()
	{
		return $this->hasMany('App\Models\MngAnswer','fk_question','id');
	}

}