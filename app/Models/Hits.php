<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hits extends Model

{

   //

   protected $table = 'tax_hitscount';

   public function names()
    {
        return $this->hasOne('App\Models\MngService','id','fk_lkp_mng_service');
    }


}