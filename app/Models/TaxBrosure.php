<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaxBrosure extends Model

{

    protected $table = 'tax_brosure';

    public function files()
    {
        return $this->hasMany('App\Models\TaxAttachment','fk_tax_brosure','id');
    }

}