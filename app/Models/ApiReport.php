<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiReport extends Model

{
   protected $table = 'tax_api_report';
}