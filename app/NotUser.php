<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'push_subscriptions';


    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    // public function desc()
    // {
    //     return $this->hasOne('Portal\Ppj\Model\Urole','user_id','id');  
    // }


}
