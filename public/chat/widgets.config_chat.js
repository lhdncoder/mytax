//var t_routing_category="";
var lhdn_lang;

if(lang==1) {lhdn_lang="ms"} else  {lhdn_lang="en"; }

if (!window._genesys) {

  window._genesys = {};
}
if (!window._gt) {
  window._gt = [];
}
var _genesys = {
                 debug: true
                 };
window._genesys.widgets = {
  main: {
    debug: true,
    theme: "light",
      lang: lhdn_lang,
    i18n: "/chat/widgets.language_"+lhdn_lang+".json",
    customStylesheetID: "genesys_widgets_custom",
    plugins: []
  },
  webchat: {
   dataURL: "https://mobile.hasil.gov.my/genesys/2/chat/CE18_Digital_Chat/",
    apikey: '',
    userData: {},
    emojis: true,
    cometD: {
      enabled: false
    },
    autoInvite: {
      enabled: false,
      timeToInviteSeconds: 5,
      inviteTimeoutSeconds: 30
    },
    chatButton: {
      enabled: true,
      openDelay: 1000,
      effectDuration: 300,
      hideDuringInvite: true
    },
    uploadsEnabled: true,

    form: {
      wrapper: '<table></table>',

      inputs: [


        {
          id: "cx_webchat_form_firstname",
          name: "firstname",
          maxlength: "100",
          placeholder: "@i18n:webchat.ChatFormPlaceholderFirstName",
          label: "@i18n:webchat.ChatFormFirstName",
          validate: function(event, form, input, label, $, CXBus, Common) {
            if (input != undefined) {
              return input[0].value == '' ? false : true;
            }
          }
        }, {
          id: "cx_webchat_form_lastname",
          name: "lastname",
          type: "hidden",
	  value: "-"
        }, {
          id: "cx_webchat_form_phone",
          name: "phone",
          maxlength: "12",
          placeholder: "@i18n:webchat.ChatFormPlaceholderPhone",
          label: "@i18n:webchat.ChatFormPhone",
          validate: function(event, form, input, label, $, CXBus, Common) {
            if (input != undefined) {
              t_value=$("#cx_webchat_form_phone").val();
            if (isNaN(t_value))
                {
                console.log("phone has non numeric entry");
                return false;
                }
            else {
            if(t_value.length<8) {
               console.log("cx_webchat_form_phone <8");
               return false;
              }
              else {
                 console.log("cx_webchat_form_phone >7");
              return true;
              }
            }

              //return input[0].value == '' ? false : true;
            }
          }
        }, {
          id: "cx_webchat_form_email",
          name: "email",
          maxlength: "100",
          placeholder: "@i18n:webchat.ChatFormPlaceholderEmail",
          label: "@i18n:webchat.ChatFormEmail",
          validate: function(event, form, input, label, $, CXBus, Common) {
            if (input != undefined) {
              return input[0].value == '' ? false : true;
            }
          }
        }, {
          id: "cx_webchat_form_idtype",
          name: "idtype",
          maxlength: "100",
          placeholder: "@i18n:webchat.ChatFormPlaceholderIdType",
          label: "@i18n:webchat.ChatFormIdType",
          type: "select",
          options: [],
          validate: function(event, form, input, label, $, CXBus, Common) {
            if (input != undefined) {
              return input[0].value == '-1' ? false : true;
            }
          }
        }, {
          id: "cx_webchat_form_idnumber",
          name: "idnumber",
          maxlength: "12",
          placeholder: "@i18n:webchat.ChatFormPlaceholderIdNumber",
          label: "@i18n:webchat.ChatFormIdNumber",
          validate: function(event, form, input, label, $, CXBus, Common) {
            if (input != undefined) {
                t_id_type=$("#cx_webchat_form_idtype").val();
                t_value=$("#cx_webchat_form_idnumber").val();

                if(t_id_type=="1") {
                //ic
                console.log("ic validation");
                        if (isNaN(t_value))
                        {
                        console.log("ic has non numeric entry");
                        return false;
                        }
                        else {
                                if(t_value.length!=12) {
                                console.log("ic entered is not valid");
                                return false;
                                }
                                else {
                                        console.log("ic is validated successfully");
                                        return true;
                                }
                        }
                }
                else if (t_id_type=="2") {
                        console.log("no polis validation");
                //polis
                }
                else if (t_id_type=="3") {
                console.log("no army validation");
                //army
                }
                else if (t_id_type=="4"){
                console.log("passport validation");
                var letterNumber = /^[0-9a-zA-Z]+$/;
                if(t_value.match(letterNumber))
                {
                console.log("passport no is ok");
                return true;
                }
                else
                {
                console.log("passport no is not ok");
                return false;
                }
                //paspot
                }
                else if (t_id_type=="-1") {
                console.log("error due to unselected id type");
                return false;
                }
              //return input[0].value == '' ? false : true;
            }
          }
        }, {
          id: "cx_webchat_form_user_category",
          type: "select",
          name: "user_category",
          maxlength: "100",
          placeholder: "@i18n:webchat.ChatFormPlaceholderCategoryType",
          label: "@i18n:webchat.ChatFormCategory",
          //options: [{text:'Pertanyaan Percukaian', value:'1'},{text:'Permohonan', value: '1'} ,{text:'Pertanyaan EzHasil', value: '1'} ,{text:'Aduan', value: '1'}]
          options: [],
          validate: function(event, form, input, label, $, CXBus, Common) {
            if (input != undefined) {
              //	console.log("event:"+JSON.stringify(event)+" category:"+input[0].value);
              if (event.type == "change") {
                populatesubCategory(input[0].value);

                var main_category = $("#cx_webchat_form_user_category option:selected").text();
                console.log("main category:"+main_category);
                $("#cx_webchat_form__category").val(main_category);

              }
              return input[0].value == '-1' ? false : true;
            }

          //  return true;
          }


        },

        {
          id: "cx_webchat_form_user_subcategory",
          name: "user_subcategory",
          maxlength: "200",
          type: "select",
          label: "@i18n:webchat.ChatFormSubcategory",
          //options: [{text:'Pertanyaan Percukaian', value:'1'},{text:'Permohonan', value: '1'} ,{text:'Pertanyaan EzHasil', value: '1'} ,{text:'Aduan', value: '1'}]
          options: [],
          validate: function(event, form, input, label, $, CXBus, Common) {
            if (input != undefined) {
              if (event.type == "change") {
                var common_category = $("#cx_webchat_form_user_subcategory option:selected").val();
                console.log("common_category:"+common_category);
                $("#cx_webchat_form__common_category").val(common_category);

                var subcategory = $("#cx_webchat_form_user_subcategory option:selected").text();
                console.log("subcategory:"+subcategory);
                $("#cx_webchat_form_subcategory").val(subcategory);
              }

              return input[0].value == '-1' ? false : true;
            }
          }
        },
       
       	{
					id: "cx_webchat_form_subject",
					name: "subject",
					maxlength: "100",
					placeholder: "@i18n:webchat.ChatFormPlaceholderSubject",
					label: "@i18n:webchat.ChatFormSubject",
					
	},  
        {
          id: "cx_webchat_form_taxagent",
          name: "istaxagent",
          type: "checkbox",
          maxlength: "100",
          placeholder: "@i18n:webchat.ChatFormPlaceholderTaxAgent",
          label: "@i18n:webchat.ChatFormTaxAgent"
        },
				{
					id: "cx_webchat_form__category",
					name: "main_category",
					type: "hidden"
				}
        ,
				{
					id: "cx_webchat_form__common_category",
					name: "RoutingCategory",
					type: "hidden"
				}
        ,
        {
          id: "cx_webchat_form_subcategory",
          name: "subcategory",
          type: "hidden"
        }
      ]
    }

  },



  knowledgecenter: {
    host: "https://gkc.hasil.gov.my/gks-server/api",
    tenantId: "1",
    knowledgebases: lhdn_kb,
    apiVersion: "v2",
    lang: lhdn_lang,
    media: "all",
    maxSearchResults: 100,
    apiClientId: "gwidget",
    apiClientMediaType: "selfservice",
    deflection: {
      enabled: welcome_gkc,
      agentTranscript: "readable",
      workspace: {
        enabled: true
      },
      reporting: {
        enabled: true
      }
    },
    search: {
      SearchButton: {
        enabled: true
      }
    }
  },
  gwe: {
    httpEndpoint: "https://engage.hasil.gov.my",
    //dslResource: "https://engage.hasil.gov.my/server/resources/dsl/domain-model.xml",
    debug: true 
  },
  cobrowse: {
    disableWebSockets: true,
    src: "https://cobrowse.hasil.gov.my/cobrowse/js/gcb.min.js",
    url: "https://cobrowse.hasil.gov.my/cobrowse"
  }


};

var log_prefix = "LHDN widget-plugin: ";

if (!window._genesys.widgets.extensions)
  window._genesys.widgets.extensions = {};
  window._genesys.widgets.extensions.LHDN = function($, CXBus, CXCommon) {
  var oPlugin = CXBus.registerPlugin("LHDN");

  $('#btnWebChatPop').click(function(e) {
    console.log(log_prefix + 'WebChat popup');
    oPlugin.command("WebChat.open").done(function(e) {

      // Handle success return state
      //var lang = '<?php echo "test ".$_SESSION[$lgv]; ?>';
      //alert(lang);


    }).fail(function(e) {

      // Handle failure return state
    });
  });


  oPlugin.subscribe('WebChat.opened', function(e) {
    console.log(log_prefix + 'WebChat opened');

  //  populateChatFromCookies();
  });
};

function populateChatFromCookies() {
  $.getJSON("user.login.json", function(data, status) {
    if (status == 'success') {
      populateChat(data);
    }
  })
}

function sendSignInFromCookies() {
  $.getJSON("user.login.json", function(data, status) {
    if (status == 'success') {
      sendSignIn(data);
    }
  })
}

function sendSignOutFromCookies() {
  $.getJSON("user.login.json", function(data, status) {
    if (status == 'success') {
      sendSignOut(data);
    }
  })
}

function sendUserInfoFromCookies() {
  $.getJSON("user.login.json", function(data, status) {
    if (status == 'success') {
      sendUserInfo(data);
    }
  })
}

function sendPasswordFailureFromCookies(pname) {
  $.getJSON("user.login.json", function(data, status) {
    if (status == 'success') {
      data.PageName = pname;
      sendPasswordFailure(data);
    }
  })
}

function sendPaymentFailureFromCookies(pname) {
  $.getJSON("user.login.json", function(data, status) {
    if (status == 'success') {
      data.PageName = pname;
      sendPaymentFailure(data);
    }
  })
}

function sendUnabletoProceedFromCookies(pname) {

  	var options = {
  		eventName: 'UnabletoProceed',
  		pageName: i.PageName,
  		Language: i.Language
  	};
  	_gt.push(['event', 'UnabletoProceed', {
  				data: options
  			}
  		])
}

function sendFilingErrorFromCookies(pname) {
  $.getJSON("user.login.json", function(data, status) {
    if (status == 'success') {
      data.PageName = pname;
      sendFilingError(data);
    }
  })
}



function sendSignIn() {

	var options = {
		TaxFileNumber: "x",
		MobilePhone: "x",
		userID: "x"
	};
	_gt.push(['event', 'SignIn', {
				data: options
			}
		]);
}

function sendSignOut() {

	var options = {
		CustomerID: "x",
		MobilePhone: "x"
	};
	_gt.push(['event', 'SignOut', {
				data: options
			}
		])
}

function sendUserInfo() {

	var options = {
		userID: i.UserID
	};
	_gt.push(['event', 'UserInfo', {
				data: options
			}
		])
}

function sendPasswordFailure() {

  var options = {
    eventName: 'PasswordFailure',
    pageName: i.PageName,
    CustomerSegment: i.CustomerSegment,
    Attempts: i.Attempts,
    Language: i.Language
  };
  _gt.push(['event', 'PasswordFailure', {
    data: options
  }])
}

function sendPaymentFailure() {

  var options = {
    eventName: 'PaymentFailure',
    pageName: i.PageName,
    Language: i.Language
  };
  _gt.push(['event', 'PaymentFailure', {
    data: options
  }])
}

function sendUnabletoProceed() {

  var options = {
    eventName: 'UnabletoProceed',
    pageName: i.PageName,
    Language: i.Language
  };
  _gt.push(['event', 'UnabletoProceed', {
    data: options
  }])
}

function sendFilingError() {

  var options = {
    eventName: 'FilingError',
    pageName: i.PageName,
    TaxAmount: i.TaxAmount,
    Language: i.Language
  };
  _gt.push(['event', 'FilingError', {
    data: options
  }])
}

function sendFilingError() {

  var options = {
    eventName: 'FilingError',
    pageName: i.PageName,
    TaxAmount: i.TaxAmount,
    Language: i.Language
  };
  _gt.push(['event', 'FilingError', {
    data: options
  }])
}

function sendEmployerRecordNotFoundError() {

  var options = {
    eventName: 'EmployerRecordNotFound',
    pageName: "",
    TaxAmount: "",
    Language: ""
  };
  _gt.push(['event', 'EmployerRecordNotFound', {
    data: options
  }])
}

function sendTaxPayerRecordNotFoundError() {

  var options = {
    eventName: 'TaxPayerRecordNotFound',
    pageName: "",
    TaxAmount: "",
    Language: ""
  };
  _gt.push(['event', 'TaxPayerRecordNotFound', {
    data: options
  }])
}


(function(o) {
  var f = function() {
      var d = o.location;
      o.aTags = o.aTags || [];
      for (var i = 0; i < o.aTags.length; i++) {
        var oTag = o.aTags[i];
        var fs = d.getElementsByTagName(oTag.type)[0],
          e;
        if (d.getElementById(oTag.id))
          return;
        e = d.createElement(oTag.type);
        e.id = oTag.id;
        if (oTag.type == "script") {
          e.src = oTag.path;
        } else {
          e.type = 'text/css';
          e.rel = 'stylesheet';
          e.href = oTag.path;
        }
        if (fs) {
          fs.parentNode.insertBefore(e, fs);
        } else {
          d.head.appendChild(e);
        }
      }
    },
    ol = window.onload;
  if (o.onload) {
    typeof window.onload != "function" ? window.onload = f : window.onload = function() {
      ol();
      f();
    }
  } else
    f();
})({
  location: document,
  onload: false,
  aTags: [{
    type: "script",
    id: "genesys-cx-widget-script",
    path: "/chat/widgets.min.js"
  }, {
    type: "link",
    id: "genesys-cx-widget-styles",
    path: "/chat/widgets.min.css"
  }]
});

window._genesys.widgets.extensions.LHDN = function($, CXBus, CXCommon) {

  var oMyPlugin = CXBus.registerPlugin('MyPlugin');
  oMyPlugin.subscribe('WebChat.opened', function(e) {
    populateIdType();
    populateCategory();
    populateChat();
  });

  var oMyPlugin2 = CXBus.registerPlugin('MyPlugin2');
  oMyPlugin2.subscribe('SideBar.ready', function(e) {
    please_invite(); //popup chat invitation
  });
}

function populateIdType() {
  console.log("populateIdType called");
  var $select = $('#cx_webchat_form_idtype');
  $select.empty();
  if (lhdn_lang == "en") {
    $('<option>').val("-1").text("Please select one").appendTo($select);
    $('<option>').val("1").text("New Identification No").appendTo($select);
    $('<option>').val("2").text("Police").appendTo($select);
    $('<option>').val("3").text("Army No").appendTo($select);
    $('<option>').val("4").text("Passport No").appendTo($select);
  } else {
    $('<option>').val("-1").text("Sila pilih").appendTo($select);
    $('<option>').val("1").text("No Kad Pengenalan Baru").appendTo($select);
    $('<option>').val("2").text("No Polis").appendTo($select);
    $('<option>').val("3").text("No Tentera").appendTo($select);
    $('<option>').val("4").text("No Pasport").appendTo($select);
  }
}

function populateCategory() {
  console.log("populateCategory called");

  var param = {
    "lang": lhdn_lang
  };

  var $select = $('#cx_webchat_form_user_category');
  $select.empty();
  if (lhdn_lang == "en") {
    $('<option>').val("-1").text("Please select one").appendTo($select);
  } else {
    $('<option>').val("-1").text("Sila pilih").appendTo($select);

  }

var t_cat_id="";
var url = "https://byrhasil.hasil.gov.my/chat/get_category.php";

  jQuery.ajax({
    url: url,
    async: true,
    type: 'POST',
    dataType: 'json',
    data: param,
    beforeSend: function() {
      console.log("data:" + JSON.stringify(param));
    },
    success: function(data) {
		  console.log("url:"+url+" data:" + JSON.stringify(data));
      $.each(data.cat, function(ndx, data) {
        t_cat_id=data[0];
        if(t_cat_id=="3") {
            $('<option selected>').val(data[0]).text(data[1]).appendTo($select);
        }
        else {
            //$('<option>').val(data[0]).text(data[1]).appendTo($select);
            // add line below by wmr 18/4/2019
                if (welcome_gkc=="true"){
                    //reactive
                    $('<option>').val(data[0]).text(data[1]).appendTo($select);
            }else{
                    //proactive
                    $('<option disabled>').val(data[0]).text(data[1]).appendTo($select);
                }

        }

         console.log("category:"+data[1]);
      });

    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log("populateCategory ERROR:" + textStatus + " errorThrown:" + errorThrown);
    },
    done: function() {}
  });

}


function populatesubCategory(cat_id) {
  console.log("populatesubCategory called");


  var $select = $('#cx_webchat_form_user_subcategory');
  $select.empty();
	if (lhdn_lang == "en") {
		$('<option>').val("-1").text("Please select one").appendTo($select);
	} else {
		$('<option>').val("-1").text("Sila pilih").appendTo($select);

	}
  var param = {
    "lang": lhdn_lang,
    "cat_id": cat_id
  };

var t_subcat_id="";
var url="https://byrhasil.hasil.gov.my/chat/get_subcategory.php";

  jQuery.ajax({
    url: url,
    async: true,
    type: 'POST',
    dataType: 'json',
    data: param,
    beforeSend: function() {
      //console.log("data:" + JSON.stringify(param));
    },
    success: function(data) {
      console.log("url:"+url+" data:" + JSON.stringify(data));
      $.each(data.sub_cat, function(ndx, data) {

t_subcat_id=data[0];
if(t_subcat_id=="11") {
    $('<option selected>').val(data[2]).text(data[1]).appendTo($select);
    $("#cx_webchat_form__common_category").val(data[2]);
    console.log("1auto common_category:"+$("#cx_webchat_form__common_category").val());

    var main_category = $("#cx_webchat_form_user_category option:selected").text();
    console.log("1auto category:"+main_category);
    $("#cx_webchat_form__category").val(main_category);

    var subcategory = $("#cx_webchat_form_user_subcategory option:selected").text();
    console.log("1auto subcategory:"+subcategory);
    $("#cx_webchat_form_subcategory").val(subcategory);

}
else {
    //$('<option>').val(data[2]).text(data[1]).appendTo($select);
    //add below line by wmr 18/4/2019
    if (welcome_gkc=="true"){
      //reactive
    $('<option>').val(data[2]).text(data[1]).appendTo($select);
    }else{
     //proactive
    $('<option disabled>').val(data[2]).text(data[1]).appendTo($select);
        }
   
}

        //  console.log("subcategory:"+data[0]);
      });

    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log("populatesubCategory ERROR:" + textStatus + " errorThrown:" + errorThrown);
    },
    done: function() {}
  });

}

function populateChat() {
  console.log("populateChat data2");
  if(is_populate_chat) {
    $("#cx_webchat_form_firstname").val($("#nama").val());
   // $("#cx_webchat_form_email").val($("#email").val());
    $("#cx_webchat_form_lastname").val("-");
    $('#cx_webchat_form_idtype > option').eq(jenisid).attr('selected','selected');
    $("#cx_webchat_form_idnumber").val(idterima);
    populatesubCategory("3");
  }
	else
	{
  populatesubCategory("3");
	}
}

function please_invite(){
  console.log("please_invite event PaymentFailure");
  
  if(eventID == "200")
  {
    console.log("dont invite");
    console.log(eventID);
    console.log(welcome_gkc);
    return;
  }else{}

  if(!is_populate_chat)
  {
    console.log("dont invite");
    return;
  }
  else {
    console.log("invite");
    console.log(eventID);
    console.log(welcome_gkc);
  }
console.log("t_routing:"+t_routing_category);

      var options = {
        eventName: 'PaymentFailure',
        pageName: "detailtp",
        GCTI_LanguageCode: lhdn_lang,
        //RoutingCategory : 'webengage_ezHASiL_eFiling',
        RoutingCategory : t_routing_category,
        CustomerSegment : 'DEFAULT'
      };
         console.log(t_routing_category);
      _gt.push(['event', 'PaymentFailure', {
        data: options
      }]);
}
