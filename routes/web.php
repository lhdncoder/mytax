<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'namespace'  => '\App\Http\Controllers',
        'prefix'     => '',
        'middleware' => ['web','auth'],
    ],
    function () {
    	Route::get('mobile/lejar', '\App\Http\Controllers\LejarController@mobileindex')->name('mobile.lejar');
    	Route::get('user/profile', '\App\Http\Controllers\LoginController@profile')->name('user.profile');

    	Route::any('admin/userman', '\App\Http\Controllers\AdminController@userman')->name('admin.userman');
    	Route::post('admin/saverole', '\App\Http\Controllers\AdminController@saverole')->name('admin.saverole');
    	Route::get('admin/deleterole/{id}', '\App\Http\Controllers\AdminController@deleterole')->name('admin.deleterole');

    	//inbox section
    	Route::get('user/inbox', '\App\Http\Controllers\MailController@inbox')->name('user.inbox');
		Route::get('mail/read/{id}', '\App\Http\Controllers\MailController@read')->name('mail.read');

		//risalah
		Route::get('dashboard/brochure', '\App\Http\Controllers\DashboardController@brochure')->name('dashboard.brochure');

		//Announcement section
		Route::get('dashboard/announcement/{id}', '\App\Http\Controllers\DashboardController@announcement')->name('dashboard.announcement');
		Route::get('dashboard/announcementread/{id}', '\App\Http\Controllers\DashboardController@announcementread')->name('dashboard.announcementread');

		Route::get('app/view/{id}', '\App\Http\Controllers\AppController@view')->name('app.view');
		Route::get('dashboard/apps', '\App\Http\Controllers\DashboardController@apps')->name('dashboard.apps');

		//admin/pengurusan
		Route::get('admin/announcement', '\App\Http\Controllers\AdminController@announcement')->name('admin.announcement');
		Route::get('admin/listfaq', '\App\Http\Controllers\AdminController@listfaq')->name('admin.listfaq');
		Route::get('admin/addfaq', '\App\Http\Controllers\AdminController@addfaq')->name('admin.addfaq');
		Route::post('admin/svfaq', '\App\Http\Controllers\AdminController@svfaq')->name('admin.svfaq');
		Route::get('admin/editfaq/{id}', '\App\Http\Controllers\AdminController@editfaq')->name('admin.editfaq');
		Route::post('admin/svefaq', '\App\Http\Controllers\AdminController@svefaq')->name('admin.svefaq');
		Route::post('admin/svansw', '\App\Http\Controllers\AdminController@svansw')->name('admin.svansw');
		Route::post('admin/sveansw', '\App\Http\Controllers\AdminController@sveansw')->name('admin.sveansw');
		Route::get('admin/listhelp', '\App\Http\Controllers\AdminController@listhelp')->name('admin.listhelp');
		Route::get('admin/addhelp', '\App\Http\Controllers\AdminController@addhelp')->name('admin.addhelp');
		Route::post('admin/svhelp', '\App\Http\Controllers\AdminController@svhelp')->name('admin.svhelp');
		Route::get('admin/edithelp/{id}', '\App\Http\Controllers\AdminController@edithelp')->name('admin.edithelp');
		Route::post('admin/svehelp', '\App\Http\Controllers\AdminController@svehelp')->name('admin.svehelp');
		Route::get('admin/listmobile', '\App\Http\Controllers\AdminController@listmobile')->name('admin.listmobile');
		Route::get('admin/addmobile', '\App\Http\Controllers\AdminController@addmobile')->name('admin.addmobile');
		Route::post('admin/svmobile', '\App\Http\Controllers\AdminController@svmobile')->name('admin.svmobile');
		Route::get('admin/editmobile/{id}', '\App\Http\Controllers\AdminController@editmobile')->name('admin.editmobile');
		Route::post('admin/svemobile', '\App\Http\Controllers\AdminController@svemobile')->name('admin.svemobile');
		Route::get('admin/listannounce', '\App\Http\Controllers\AdminController@listannounce')->name('admin.listannounce');
		Route::get('admin/addannounce', '\App\Http\Controllers\AdminController@addannounce')->name('admin.addannounce');
		Route::post('admin/svannounce', '\App\Http\Controllers\AdminController@svannounce')->name('admin.svannounce');
		Route::get('admin/editannounce/{id}', '\App\Http\Controllers\AdminController@editannounce')->name('admin.editannounce');
		Route::post('admin/sveannounce', '\App\Http\Controllers\AdminController@sveannounce')->name('admin.sveannounce');
		Route::get('admin/listapp', '\App\Http\Controllers\AdminController@listapp')->name('admin.listapp');
		Route::get('admin/addapp', '\App\Http\Controllers\AdminController@addapp')->name('admin.addapp');
		Route::post('admin/svapp', '\App\Http\Controllers\AdminController@svapp')->name('admin.svapp');
		Route::get('admin/editapp/{id}', '\App\Http\Controllers\AdminController@editapp')->name('admin.editapp');
		Route::post('admin/sveapp', '\App\Http\Controllers\AdminController@sveapp')->name('admin.sveapp');
		Route::get('admin/listservice', '\App\Http\Controllers\AdminController@listservice')->name('admin.listservice');
		Route::get('admin/addservice', '\App\Http\Controllers\AdminController@addservice')->name('admin.addservice');
		Route::get('admin/editservice/{id}', '\App\Http\Controllers\AdminController@editservice')->name('admin.editservice');
		Route::post('admin/svservice', '\App\Http\Controllers\AdminController@svservice')->name('admin.svservice');
		Route::post('admin/sveservice', '\App\Http\Controllers\AdminController@sveservice')->name('admin.sveservice');

		Route::get('admin/banner', '\App\Http\Controllers\AdminController@banner')->name('admin.banner');
		Route::get('admin/banner/{id}', '\App\Http\Controllers\AdminController@editbanner')->name('admin.editbanner');
		Route::get('admin/addbanner/', '\App\Http\Controllers\AdminController@addbanner')->name('admin.addbanner');
		Route::post('admin/updatebanner/', '\App\Http\Controllers\AdminController@updatebanner')->name('admin.updatebanner');
		Route::post('admin/savebanner/', '\App\Http\Controllers\AdminController@savebanner')->name('admin.savebanner');


		Route::get('admin/brosure', '\App\Http\Controllers\AdminController@listbrosure')->name('admin.listbrosure');
		Route::get('admin/brosure/files/{id}', '\App\Http\Controllers\AdminController@listfile')->name('admin.listfile');
		Route::get('admin/delattach/{aid}/{id}', '\App\Http\Controllers\AdminController@delattach')->name('admin.delattach');

		Route::get('admin/addbrosure', '\App\Http\Controllers\AdminController@addbrosure')->name('admin.addbrosure');
		Route::post('admin/savebrosure', '\App\Http\Controllers\AdminController@savebrosure')->name('admin.savebrosure');
		Route::post('admin/updatebrosure', '\App\Http\Controllers\AdminController@updatebrosure')->name('admin.updatebrosure');

		Route::get('admin/feedback', '\App\Http\Controllers\AdminController@listfeedback')->name('admin.listfeedback');
		Route::get('admin/addquestfeedback', '\App\Http\Controllers\AdminController@addquestfeedback')->name('admin.addquestfeedback');
		Route::post('admin/svquestfeedback', '\App\Http\Controllers\AdminController@svquestfeedback')->name('admin.svquestfeedback');
		Route::get('admin/editquestfeedback/{id}', '\App\Http\Controllers\AdminController@editquestfeedback')->name('admin.editquestfeedback');
		Route::post('admin/svequestfeedback', '\App\Http\Controllers\AdminController@svequestfeedback')->name('admin.svequestfeedback');
		Route::post('admin/svanswrfeedback', '\App\Http\Controllers\AdminController@svanswrfeedback')->name('admin.svanswrfeedback');
		Route::post('admin/sveanswrfeedback', '\App\Http\Controllers\AdminController@sveanswrfeedback')->name('admin.sveanswrfeedback');

		Route::get('feedback/form', '\App\Http\Controllers\AdminController@feedback')->name('feedback.form');
		Route::post('feedback/svfeedback', '\App\Http\Controllers\AdminController@svfeedback')->name('feedback.svfeedback');

		Route::any('admin/reportapp', '\App\Http\Controllers\AdminController@reportapp')->name('admin.reportapp');
		Route::get('admin/reportapi', '\App\Http\Controllers\AdminController@reportapi')->name('admin.reportapi');

		Route::any('admin/reportfeedback', '\App\Http\Controllers\AdminController@reportfeedback')->name('admin.reportfeedback');
		Route::get('admin/exportfeedback/{type}/{datefrom}/{dateto}', '\App\Http\Controllers\AdminController@exportfeedback')->name('admin.exportfeedback');

		Route::any('admin/reportcomment', '\App\Http\Controllers\AdminController@reportcomment')->name('admin.reportcomment');
		Route::get('admin/exportkomen/{type}/{datefrom}/{dateto}', '\App\Http\Controllers\AdminController@exportkomen')->name('admin.exportkomen');
		
		// Route::get('admin/reportapp', '\App\Http\Controllers\AdminController@reportapp')->name('admin.reportapp');

		//taxstatus section
		Route::get('dashboard/taxstatus', '\App\Http\Controllers\DashboardController@taxstatus')->name('dashboard.taxstatus');
		Route::get('lejar/penutup/{type}/{lejar}', '\App\Http\Controllers\LejarController@penutup')->name('lejar.penutup');
		Route::get('lejar/index', '\App\Http\Controllers\LejarController@index')->name('lejar.index');
		Route::get('lejar/calendar/{year}/{type}', '\App\Http\Controllers\LejarController@calendar')->name('lejar.calendar');
		Route::get('lejar/current/{year}/{type}/{ltype}', '\App\Http\Controllers\LejarController@current')->name('lejar.current');
		Route::get('lejar/comdata/{id}/{type}', '\App\Http\Controllers\LejarController@comdata')->name('lejar.comdata');
		Route::get('lejar/currentcom/{year}/{type}/{ltype}/{lid}', '\App\Http\Controllers\LejarController@currentcom')->name('lejar.currentcom');
		Route::get('lejar/calendarcom/{year}/{type}/{ltype}/{lid}', '\App\Http\Controllers\LejarController@calendarcom')->name('lejar.calendarcom');

		Route::get('lejar/resit/{type}/{ref}/{resit}/{types}/{ltype}', '\App\Http\Controllers\LejarController@resit')->name('lejar.resit');
		//pcb
		Route::get('pcb/detail/{type}', '\App\Http\Controllers\PcbController@detail')->name('pcb.detail');
		Route::get('pcb/year/{year}', '\App\Http\Controllers\PcbController@year')->name('pcb.year');
		Route::get('pcb/calendar/{year}', '\App\Http\Controllers\PcbController@calendar')->name('pcb.calendar');


		Route::get('cp500/surat', '\App\Http\Controllers\DashboardController@surat')->name('cp500.surat');

		//template management
		Route::get('mail/template', '\App\Http\Controllers\MailController@template')->name('mail.template');
		Route::get('mail/addtemplate', '\App\Http\Controllers\MailController@addtemplate')->name('mail.addtemplate');
		Route::post('mail/savetemplate', '\App\Http\Controllers\MailController@savetemplate')->name('mail.savetemplate');
		Route::get('mail/cptable/{id}', '\App\Http\Controllers\MailController@cptable')->name('mail.cptable');

		Route::get('mail/edittemplate/{id}', '\App\Http\Controllers\MailController@edittemplate')->name('mail.edittemplate');
		Route::post('mail/updatetemplate', '\App\Http\Controllers\MailController@updatetemplate')->name('mail.updatetemplate');

		//templatetype
		Route::get('mail/templatetype', '\App\Http\Controllers\MailController@templatetype')->name('mail.templatetype');
		Route::get('mail/addtemplatetype', '\App\Http\Controllers\MailController@addtemplatetype')->name('mail.addtemplatetype');
		Route::get('mail/edittemplatetype/{id}', '\App\Http\Controllers\MailController@edittemplatetype')->name('mail.edittemplatetype');
		Route::post('mail/savetemplatetype', '\App\Http\Controllers\MailController@savetemplatetype')->name('mail.savetemplatetype');
		Route::post('mail/updatetemplatetype', '\App\Http\Controllers\MailController@updatetemplatetype')->name('mail.updatetemplatetype');

				//mobile
		
		Route::get('mobile/taxstatus', '\App\Http\Controllers\DashboardController@taxstatusmobile')->name('mobile.taxstatusmobile');
		Route::get('mobile/profile', '\App\Http\Controllers\DashboardController@profilemobile')->name('mobile.profile');
		Route::get('mobile/app', '\App\Http\Controllers\DashboardController@appmobile')->name('mobile.app');
		Route::get('mobile/inbox', '\App\Http\Controllers\MailController@inboxmobile')->name('mobile.inbox');
		Route::get('mail/mobileread/{id}/{type}', '\App\Http\Controllers\MailController@mobileread')->name('mail.mobileread');

		
		Route::get('mobile/apps/{id}', '\App\Http\Controllers\DashboardController@mobileapps')->name('mobile.apps');
		Route::get('mobile/ann/{id}', '\App\Http\Controllers\DashboardController@mobileannouncement')->name('mobile.announcement');
		Route::get('mobile/del/{id}', '\App\Http\Controllers\DashboardController@delweb')->name('mobile.delweb');
		Route::get('app/mobileview/{id}', '\App\Http\Controllers\AppController@mobileview')->name('app.mobileview');
		Route::get('app/removefav/{id}', '\App\Http\Controllers\AppController@removefav')->name('app.removefav');
		Route::get('app/addfav/{id}', '\App\Http\Controllers\AppController@addfav')->name('app.addfav');
		Route::get('announcement/mobileview/{id}', '\App\Http\Controllers\DashboardController@mobileviewannouncement')->name('announcement.mobileview');

		

		Route::get('mobile/refund', '\App\Http\Controllers\DashboardController@mobilerefund')->name('mobile.refund');
		Route::get('mobile/pcb', '\App\Http\Controllers\PcbController@mobilepcb')->name('mobile.pcb');
		Route::get('mobile/spc', '\App\Http\Controllers\DashboardController@mobilespc')->name('mobile.spc');
		Route::get('mobile/cp500', '\App\Http\Controllers\DashboardController@mobilecp500')->name('mobile.cp500');
		Route::get('mobile/cp204', '\App\Http\Controllers\DashboardController@mobilecp204')->name('mobile.cp204');

		Route::get('dashboard/contact', '\App\Http\Controllers\DashboardController@contact')->name('dashboard.contact');
		Route::get('dashboard/help', '\App\Http\Controllers\DashboardController@help')->name('dashboard.help');
		Route::get('dashboard/faql', '\App\Http\Controllers\DashboardController@faql')->name('dashboard.faql');

		//searching
		Route::get('search/index', '\App\Http\Controllers\SearchController@index')->name('search.index');
		Route::post('search/iden', '\App\Http\Controllers\SearchController@iden')->name('search.iden');
		Route::post('search/iden2', '\App\Http\Controllers\SearchController@iden2')->name('search.iden2');

		Route::get('search/penutup/{type}/{lejar}', '\App\Http\Controllers\SearchController@penutup')->name('search.penutup');
		Route::get('search/indexlejar', '\App\Http\Controllers\SearchController@indexlejar')->name('search.indexlejar');
		Route::get('search/calendar/{year}/{type}', '\App\Http\Controllers\SearchController@calendar')->name('search.calendar');
		Route::get('search/current/{year}/{type}/{ltype}', '\App\Http\Controllers\SearchController@current')->name('search.current');
		Route::get('search/comdata/{id}/{type}', '\App\Http\Controllers\SearchController@comdata')->name('search.comdata');
		Route::get('search/currentcom/{year}/{type}/{ltype}/{lid}', '\App\Http\Controllers\SearchController@currentcom')->name('search.currentcom');
		Route::get('search/calendarcom/{year}/{type}/{ltype}/{lid}', '\App\Http\Controllers\SearchController@calendarcom')->name('search.calendarcom');
		Route::get('search/resit/{type}/{ref}/{resit}/{types}/{ltype}', '\App\Http\Controllers\SearchController@resit')->name('search.resit');






    }
);


Route::group(
    [
        'namespace'  => '\App\Http\Controllers',
        'prefix'     => '',
        'middleware' => ['web'],
    ],
    function () {

    	Route::get('/', Home::class)->name('home');
    	Route::get('home/checkdevice', '\App\Http\Controllers\Home@checkdevice')->name('home.device');
    	Route::get('home/{lang}', '\App\Http\Controllers\Home@changeLocale')->name('home.lang');
    	Route::get('/mobile', '\App\Http\Controllers\Home@mobile')->name('mobile');
    	Route::get('/mobile/webauth/{id}', '\App\Http\Controllers\Home@webauth')->name('mobile.webauth');
    	Route::get('/mobile/logwebauth/{id}', '\App\Http\Controllers\LoginController@logweb')->name('usermobile.logweb');
    	Route::get('/mobile/logout', '\App\Http\Controllers\Home@mobilelogout')->name('mobile.logout');

    	Route::get('sso', '\App\Http\Controllers\LoginController@sso')->name('user.sso');
    	Route::get('ssologin', '\App\Http\Controllers\LoginController@ssologin')->name('user.ssologin');
    	Route::get('logout', '\App\Http\Controllers\LoginController@logout')->name('user.logout');
    	Route::get('user/login/{id}', '\App\Http\Controllers\LoginController@login')->name('user.login');
    	Route::post('usermobile/log', '\App\Http\Controllers\LoginController@mobilelog')->name('usermobile.log');
		Route::post('user/log', '\App\Http\Controllers\LoginController@log')->name('user.log');
		Route::post('user/loginmodal', '\App\Http\Controllers\LoginController@loginmodal')->name('user.loginmodal');

		Route::get('dashboard/faqs', '\App\Http\Controllers\DashboardController@faqs')->name('dashboard.faqs');
		Route::get('dashboard/brochures', '\App\Http\Controllers\DashboardController@brochures')->name('dashboard.brochures');
		Route::get('dashboard/helps', '\App\Http\Controllers\DashboardController@helps')->name('dashboard.helps');
		Route::get('dashboard/contacts', '\App\Http\Controllers\DashboardController@contacts')->name('dashboard.contacts');

		Route::get('app/views/{id}', '\App\Http\Controllers\AppController@views')->name('app.views');

       }
);


