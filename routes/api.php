<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/save-subscription/{id}',function($id, Request $request){
  $user = \App\User::findOrFail($id);
  $subs = \App\NotUser::where('subscribable_id','=',$id)->count();

  if($subs >= 1)
  {
      $user->updatePushSubscription($request->input('endpoint'), $request->input('keys.p256dh'), $request->input('keys.auth'));
      $user->notify(new \App\Notifications\GenericNotification("Selamat Datang Ke MyTax", "Selamat Datang Ke MyTax. {$user->name}, Anda akan dimaklumkan jika ada sebarang notifikasi. ",'https://dev-cukai.3fresources.com'));

        return response()->json(['success' => true]);


  }else{

      $user->updatePushSubscription($request->input('endpoint'), $request->input('keys.p256dh'), $request->input('keys.auth'));   
      $user->notify(new \App\Notifications\GenericNotification("Selamat Datang Ke MyTax", "Selamat Datang Ke MyTax. {$user->name}, Anda akan dimaklumkan jika ada sebarang notifikasi. ",'https://dev-cukai.3fresources.com'));

        return response()->json(['success' => true]);

    }
});






Route::any('/send-notification/{id}', function($id, Request $request){
  $user = \App\User::findOrFail($id);

  try {
      
      $user->notify(new \App\Notifications\GenericNotification($request->title, $request->body,$request->url));
      return response()->json(['success' => true]);

  } catch (Exception $e) {

      return response()->json(['error' => $e]);
  }
 
});