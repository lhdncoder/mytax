<?php 

if($typel == 'INDIVIDU')
{
    $color = '#4680FF';
}else{
    $color = 'orange';
}

?>

    <div class="card-body table-border-style" style="padding-right: unset; font-size: 12px !important">
        <button type="button" class="btn btn-info has-ripple btn-sm" onClick="printdiv('div_print');" style="float:right;margin-left: 10px;"><i class="mr-2 feather icon-printer"></i>@lang('homepage.printlabel')<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>

        <button type="button" class="btn btn-success has-ripple btn-sm" onclick="javascript:loadpenutup({{$ltype}},'{{$typelejar->income_type}}');" style="margin-left: 10px;float:right"><i class="feather mr-2 icon-info"></i>@lang('homepage.backlabel')<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>

        <button type="button" style="float: right;" onclick="javascript:loadlejar();" class="btn btn-warning has-ripple  btn-sm"><i class="feather mr-2 icon-file-text"></i>@lang('homepage.sumlabel')<span class="ripple ripple-animate" style="float:right;height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -32.825px; left: 1.375px;"></span></button>

        <br>
        <br>
        <br>
        <th colspan="9" style="vertical-align: middle;text-align:left;border: unset;text-transform:unset;color:grey"><h6>@lang('lejar.title-current') : @if($typelejar->description == "Cukai Pendapatan") @lang("lejar.table-income") @else @lang("lejar.table-ckht") @endif ({{$typel}})<br><br><span class="">@lang('lejar.title-current-year-1') @if($typelejar->description == "Cukai Pendapatan") @lang("lejar.table-income") @else @lang("lejar.table-ckht") @endif @lang('lejar.title-current-year-2') {{$year}} @if($year == date('Y')) @lang('lejar.title-current-current') {{date('d/m/Y')}}) @endif</span>  

                        

                        

                        </h6> </th>
        <div class="table-responsive shadow" style="border-radius: 8px;margin-bottom:30px">
            <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                <thead>
                    <tr>
                        <!-- <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col0')</th> -->
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col1')</th>
                         <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-calendar-col2')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col3')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col4')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col5')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col6')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col7')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col8')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-current-col9')</th>
                    </tr>


                </thead>
                <tbody>
                    @forelse($calendar as $dat =>$list)
                    <?php 

                        $datep = new DateTime($list->POSTED_DATE);
                        $datet = new DateTime($list->TRANSACTION_DATE);

                         
                        
                    ?>
                        <tr>
                            <!-- <td>{{$datep->format('d/m/Y')}}</td> -->
                            <td>{{$datet->format('d/m/Y')}}</td>
                            <td>{{$list->TRANSACTION_CODE}}</td>
                            <td style="text-align: left">{{$list->Keterangan}}</td>
                            <td style="text-align: left">
                                @if($list->RECEIPT_NO == '0')

                                @else
                                   <!--  <a href="javascript:loadresit('SG','3157123090','20080893318','1','{{$typelejar->description}}');">SAMPLE</a> <br> -->

                                     @if((strtoupper($list->JnsTransaksi) == 'BAYARAN KREDIT') AND ($list->TRANSACTION_CODE == '084') AND ($list->BRANCH_CODE !== 'SY-') AND ($list->BRANCH_CODE !== 'SY'))

                                        <a href="javascript:loadresit('{{$user->doc_type}}','{{$user->tax_no}}','{{$list->RECEIPT_NO}}','1','{{$typelejar->description}}');">{{$list->BRANCH_CODE.$list->RECEIPT_NO}}</a>

                                    @else

                                        {{$list->BRANCH_CODE.$list->RECEIPT_NO}}
                                        
                                    @endif

                                @endif
                            </td>
                            @if($list->Keterangan == 'Baki Permulaan')
                                <td></td>
                                <td></td>
                            @else
                                <td>{{$list->ASSESSMENT_YEAR}}</td>
                                <td>{{$list->ASSESSMENT_NO}}</td>
                            @endif
                            <td style="text-align: right">{{$list->TggnCukai}}</td>
                            <td style="text-align: right">{{$list->BayaranCukai}}</td>
                            <td style="text-align: right">{{number_format($list->BakiCukai,2,'.',',')}}</td>
                        </tr>
                       
                    @empty
                    <tr>    
                        <td colspan="9"><h6>@lang('lejar.table-record')</h6></td>
                    </tr>
                    @endforelse
                    <tr>    
                        <td colspan="6" style="text-align: right"><h6>@lang('lejar.table-penutup-total')</h6></td>
                        <td style="text-align: right">{{number_format($typelejar->BakiCukai,2,'.',',')}}</td>
                        <td style="text-align: right">{{number_format($typelejar->BakiLejar,2,'.',',')}}</td>
                       <td style="text-align: right"></td>
                    </tr>
                
                   
                </tbody>
            
            </table>
            
        </div>
        <div class="align-middle m-b-25">

             @if($year == date('Y')) 
            <table style="float:right;text-align: left;" class="table-bordered">
                    <tbody><tr style="background-color: #D6E8FF">
                        <td colspan="2" style="text-align: center; font-weight: bold; height: 30px;" class="titleSmall">@lang('lejar.table-current-col10')<sup>4</sup></td>
                    </tr>
                     @foreach($lejar as $key => $data)
                            <tr>
                                <td><span style="padding:10px !important">@lang('lejar.table-current-col11-o')</td>
                                <td>
                                    <span style="padding:5px !important">{{number_format($data->BakiCukai,2,'.',',')}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span style="padding:10px !important">@lang('lejar.table-current-col12')</span></td>
                                <td>
                                    <span style="padding:5px !important">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span style="padding:10px !important">@lang('lejar.table-current-col13')</span></td>
                                <td>
                                    <span style="padding:5px !important">{{number_format($data->BakiLejar,2,'.',',')}}</span>
                                </td>
                            </tr>
                    @endforeach
                    
                </tbody>
            </table>
            @endif
            <div class="d-inline-block">
                    <b>@lang('lejar.note'): </b><br>
                    @lang('lejar.note-calendar-1')
                    @lang('lejar.note-calendar-2')
                    @lang('lejar.note-calendar-3')
                     @if($year == date('Y')) 
                       @lang('lejar.note-calendar-4')
                       @lang('lejar.note-calendar-5')
                       @lang('lejar.note-calendar-6')
                     @endif


                </div>
        </div>

    </div>


    
    <div class="card-body table-border-style" id='div_print' style="height:100vh;font-size: 11px !important;background-color: white !important;display:none ">
        <link rel="icon" href="{{asset('themes/ablepro/assets/images/favicon.ico')}}" type="image/x-icon">
        <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/plugins/dataTables.bootstrap4.min.css')}}">
        <br>
        <div class="align-middle m-b-25">
            <img src="{{asset('themes/ablepro/assets/images/logoprint.jpg')}}" alt="user image" class="align-top m-r-10" width="80px">
            <div class="d-inline-block"><b>
                NAMA:   {{$user->name}}<br>
                NO. PENGENALAN: {{$user->reference_id}}<br>
                NO. CUKAI PENDAPATAN: {{$user->doc_type}}{{$user->tax_no}}<br>
                JENIS LEJAR: {{$typelejar->description}}<br>
                </b>
            </div>
        </div>
        <b><span class="">Lejar {{$typelejar->description}} bagi tahun taksiran {{$year}} (Kedudukan sehingga {{date('d/m/Y')}})</span></b>
        <br>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-xs text-center" id="tableprint">
                <thead  style="">
                    <tr>
                        <!-- <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Tarikh<p style="font-size: unset !important">Kemaskini</p></th> -->
                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Tarikh<p style="font-size: unset !important">Transaksi</p></th>
                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Kod</th>
                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Keterangan<p style="font-size: unset !important">Transaksi</p></th>

                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Rujukan/<p style="font-size: unset !important">No. Resit</p></th>

                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Tahun<p style="font-size: unset !important">Taksiran</p></th>

                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Bulan/<p style="font-size: unset !important">Bil Ansuran</p></th>
                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Taksiran & <p style="font-size: unset !important">Lain-lain<sup>1</sup></p></th>
                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Bayaran & <p style="font-size: unset !important">Lain-lain<sup>2</sup></p></th>
                        <th style="font-size: unset !important;vertical-align: middle;text-transform:unset;background: {{$color}};color:white">Baki (RM)<sup>3</sup></th>
                    </tr>


                </thead>
                <tbody>
                    @forelse($calendar as $dat =>$list)
                    <?php 

                         $datep = new DateTime($list->POSTED_DATE);
                         $datet = new DateTime($list->TRANSACTION_DATE);

                         if($list->RECEIPT_NO == '0')
                         {
                            $resit = '';
                         }else{
                            $resit = $list->BRANCH_CODE.$list->RECEIPT_NO;
                         }

                        
                    ?>
                         <tr>
                             <!-- <td>{{$datep->format('d/m/Y')}}</td> -->
                             <td>{{$datet->format('d/m/Y')}}</td>
                             <td>{{$list->TRANSACTION_CODE}}</td>
                             <td style="text-align: left">{{$list->Keterangan}}</td>
                             <td style="text-align: left">{{$resit}}</td>
                             @if($list->Keterangan == 'Baki Permulaan')
                             <td></td>
                             <td></td>
                             @else
                             <td>{{$list->ASSESSMENT_YEAR}}</td>
                             <td>{{$list->ASSESSMENT_NO}}</td>
                             @endif
                             <td style="text-align: right">{{$list->TggnCukai}}</td>
                             <td style="text-align: right">{{$list->BayaranCukai}}</td>
                             <td style="text-align: right">{{number_format($list->BakiCukai,2,'.',',')}}</td>
                         </tr>
                       
                    @empty
                    <tr>    
                        <td colspan="9"><h6>Tiada Rekod</h6></td>
                    </tr>
                    @endforelse
                    <tr>    
                        <td colspan="6" style="text-align: right"><b>Jumlah</b></td>
                        <td style="text-align: right">{{number_format($typelejar->BakiCukai,2,'.',',')}}</td>
                        <td style="text-align: right">{{number_format($typelejar->BakiLejar,2,'.',',')}}</td>
                       <td style="text-align: right"></td>
                    </tr>
                
                   
                </tbody>
            
            </table>

        </div>

         @if($year == date('Y')) 
            <table style="float:right;text-align: left;font-size: 10px" class="table-bordered">
                    <tbody><tr style="background-color: #D6E8FF">
                        <td colspan="2" style="text-align: center; font-weight: bold; height: 30px;" class="titleSmall">Ringkasan Baki<sup>4</sup></td>
                    </tr>
                     @foreach($lejar as $key => $data)
                            <tr>
                                <td><span style="padding:10px !important">Baki Cukai<sup>5</sup> Tertunggak</span></td>
                                <td>
                                    <span style="padding:5px !important">{{number_format($data->BakiCukai,2,'.',',')}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span style="padding:10px !important">Bayaran Belum Boleh Guna<sup>6</sup></span></td>
                                <td>
                                    <span style="padding:5px !important">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td><span style="padding:10px !important">Baki Lejar</span></td>
                                <td>
                                    <span style="padding:5px !important">{{number_format($data->BakiLejar,2,'.',',')}}</span>
                                </td>
                            </tr>
                    @endforeach
                    
                </tbody>
            </table>
            @endif
        <div class="align-middle m-b-25">
            <img src="{{asset('themes/ablepro/assets/images/elejarc.jpg')}}" alt="user image" style="float:right;margin-top:-15px" class="align-top" width="100px">
            <div class="d-inline-block">
                <b>Nota: </b><br>
                <b>1. Taksiran & lain-lain </b>=  Cukai dibangkitkan/kenaikan cukai/bayaran balik dan lain-lain<br>
                <b>2. Bayaran & Lain-lain </b>=  Transaksi yang mengurangkan <b>Taksiran & Lain-Lain<sup>1</sup> </b>. Contoh bayaran PCB, bayaran ansuran dan pelarasan/pengurangan cukai.<br>
                <b>3. Baki </b> = Perbezaan antara <b>Taksiran & Lain-Lain<sup>1</sup></b> dan <b>Bayaran & Lain-lain<sup>2</sup>.</b><br>
                 @if($year == date('Y')) 
                        <b>4. Ringkasan Baki </b> = Butiran Terperinci kedudukan <b>Baki<sup>3</sup></b> yang terdiri daripada <b>Belum Boleh Guna<sup>5</sup></b> dan/atau <b>Baki Cukai<sup>6</sup></b><br>
                        <b>5. Baki cukai </b> = Jumlah Tunggakan Cukai/-Lebihan Bayaran Cukai, selepas mengambilkira <b>Taksiran & lain-Lain<sup>1</sup></b> dan <b>Bayaran Cukai<sup>2</sup></b> bagi tahun taksiran yang sama.<br>
                        <b>6. Bayaran Belum Boleh Guna </b> = Bayaran yang telah dibuat oleh pembayar cukai seperti bayaran PCB atau bayaran ansuran.<br>Bayaran ini akan ditolak dengan baki cukai apabila taksiran dibangkitkan/disifatkan.
                     @endif
            </div>
        </div>


        <div class="bottom-align-text fixed-bottom" style="font-size:8px">
            Penafian: Walaupun segala penelitian telah diambil dalam penyediaan maklumat dalam penyata ini, penyata ini bertujuan untuk rujukan sahaja dan<br>
            maklumat dari penyata tidak akan menjelaskan apa - apa tuntutan LHDNM yang difailkan di Mahkamah.
        </div>
    </div>


    <div class="card-body table-border-style" id='div_print_resit' style="width:800px;height:100vh;background-color: white !important;display:none ">
    </div>