<?php 

if($typel == 'INDIVIDU')
{
    $color = '#4680FF';
}else{
    $color = '#4680FF';
}

$rowcount = 0;

// $checksal = 2;
if(($profile->No_Rujukan == '0487835807') OR ($profile->No_Rujukan == '2424451502'))
{
    $checksal = 2;
}

?>


        <div class="col-sm-12 col-md-12">
            <div class="table-responsive">
                <table class="table table-borderless table-xs">
                  
                    <tbody>
                        <tr>
                            <td width="10%">@lang('lejar.com-penutup-label1')</td>
                            <td  width="40%">: {{$profile->Nama_Syarikat}}</td>
                            <td  width="10%">@lang('lejar.com-penutup-label4')</td>
                            <td  width="40%">: {{$profile->IT_Assm_Branch}}</td>
                        </tr>
                        <tr>
                            <td>@lang('lejar.com-penutup-label2')</td>
                            <td>: {{$profile->Jenis_File}} {{$profile->No_Rujukan}}</td>
                            <td>@lang('lejar.com-penutup-label5')</td>
                            <td>: {{$profile->IT_Collection_Branch}}</td>
                        </tr>
                        <tr>
                            <td>@lang('lejar.com-penutup-label3')</td>
                            <td>: @if($type == "SALARY") @lang("lejar.table-income") @else @lang("lejar.table-ckht") @endif </td>
                             <td>@lang('lejar.com-penutup-label6')</td>
                            <td>: {{$profile->Bank_CD}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>@lang('lejar.com-penutup-label7')</td>
                            <td>: {{$profile->Bank_Acct_No}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
   <br>
   <br>
   <br>
   <div class="row">

   <div class="col-md-12">
 <h6 class="p-l-30">
   @if($type == 'PROPERTIES')
    @lang('lejar.title-calview') : <span class="m-r-20"></span>
    @if($checksal > 0 OR $checkpro > 0)
    @else
       @forelse($calendar2 as $key => $year)

             <button type="button" onclick="javascript:loadlejarcalendarcom({{$ltype}},{{$year->Tahun}},'{{$type}}',{{$lid}});" class="btn btn-info has-ripple btn-sm"><i class="feather mr-2 icon-calendar"></i>{{$year->Tahun}}<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
       @empty
          @lang('lejar.table-record')
       @endforelse
   @endif
   @endif


      <button type="button" class="btn btn-success has-ripple btn-sm" onclick="javascript:loadlejar();" style="float:right">
          <i class="feather mr-2 icon-info"></i>@lang('homepage.backlabel')
          <span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span>
      </button>
   </h6>
   </div>
   </div>
   <br>
    <div class="card-body table-border-style" style="padding-right: unset;">
        <div class="table-responsive shadow" style="border-radius: 8px;margin-bottom:30px">
            <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                <thead>
                    <tr>
                        <th colspan="4" style="vertical-align: middle;text-align:left;border: unset;text-transform:unset;color:grey"><h6>@lang('lejar.title-sum-current') {{date('d/m/Y')}})</h6></th>
                        <th colspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-penutup-col1')<sup>4</sup></th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-penutup-col2')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-penutup-col3')<sup>1</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-penutup-col4')<sup>2</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white">@lang('lejar.table-penutup-col5')<sup>3</sup> (RM)</th>
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-penutup-col6')<sup>5</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-penutup-col7')<sup>6</sup></p></th>
                    </tr>



                </thead>
                <tbody>

                    @if($checksal > 0 OR $checkpro > 0)
                        <tr>
                            <td colspan="6" style="color:red">@lang('homepage.lejarerror')</td>
                        <tr>
                        <?php 

                            $rowcount = 1;
                        ?>
                    @else
                        @forelse($lejar as $dat =>$list)
                            <?php $data = 0;$data1 =0; ?>
                             <tr>
                                <td>
                                @forelse($calendar as $key => $year)
                                        @if($year->Tahun == $list->ASSESSMENT_YEAR)
                                            <a href="javascript:loadlejarcurrentcom({{$ltype}},{{$year->Tahun}},'{{$type}}',{{$lid}});">{{$list->ASSESSMENT_YEAR}}</a>
                                            <?php $data1 = 1; ?>
                                        @else
                                            <?php $data = 1; ?>
                                        @endif
                                @empty
                                  <?php $data = 1; ?>
                                @endforelse
                                @if(($data == 1) && ($data1 == 0)) {{$list->ASSESSMENT_YEAR}} @endif
                                 </td>  
                                 <td style="text-align: right">{{number_format($list->JumTggnCukai,2,'.',',')}}</td>
                                 <td style="text-align: right">{{number_format($list->JumBayaranCukai,2,'.',',')}}</td>
                                 <td style="text-align: right">{{number_format($list->JumBersih,2,'.',',')}}</td>
                                 <td style="text-align: right">{{number_format($list->ByrnBelumBolehGuna,2,'.',',')}}</td>
                                 <td style="text-align: right">{{number_format($list->BakiCukaiSemasa,2,'.',',')}}</td>

                             </tr>
                           
                        @empty
                            <tr>
                                <td colspan="6">@lang('lejar.table-record')</td>
                            <tr>
                            <?php 

                            $rowcount = 1;
                        ?>

                        @endforelse
                    @endif
                    @if($rowcount == '0')
                        <tr>
                            <td>@lang('lejar.table-penutup-total')</td>  
                             <td style="text-align: right"></td>
                             <td style="text-align: right"></td>
                             
                             @if($type == 'SALARY') 
                                <td style="text-align: right">{{number_format($profile->BakiLejar,2,'.',',')}}</td>
                                <td style="text-align: right">{{number_format($profile->ByrnBelumBolehGuna,2,'.',',')}}</td>
                                <td style="text-align: right">{{number_format($profile->BakiCukai,2,'.',',')}}</td>
                             @else
                                <td style="text-align: right">{{number_format($profile->BakiLejarCkht,2,'.',',')}}</td>
                                <td style="text-align: right">{{number_format($profile->ByrnBelumBolehGunaCkht,2,'.',',')}}</td>
                                <td style="text-align: right">{{number_format($profile->BakiCukaiCkht,2,'.',',')}}</td>
                             @endif

                             
                        <tr>
                    @endif

                
                   
                </tbody>
            </table>
        </div>
       <div style="font-size:11px">
        <b>@lang('lejar.note'):</b><br>
          @lang('lejar.note-penutup-1')
          @lang('lejar.note-penutup-2')
          @lang('lejar.note-penutup-3')
          @lang('lejar.note-penutup-4')
          @lang('lejar.note-penutup-5')
          @lang('lejar.note-penutup-6')
        </div>
    </div>