<div class="row">
            <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/style.css')}}">
	<div class="col-12 text-center table-responsive">
	    <table class="table table-borderless table-xs " style="line-height: 1" id="tableprint">
	        <tbody>
	            <tr>
	                <th class="text-left" colspan="2"><h6>RESIT RASMI</h6></th>
	                <th rowspan="5" style="vertical-align: middle;"><img src="{{asset('themes/ablepro/assets/images/headerresit.jpg')}}" alt="user image" width="80%"></th>
	                <th colspan="2"></th>
	                <th>CP 6A - PIN. 1/2012 </th>
	            </tr>

	            <tr>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">NO. SIRI :</th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">{{$data->UPDATE_USERID}}{{date('dmY', strtotime($data->DT))}}{{$data->seq_receipt}}</th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">NO. SLIP BANK</th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">: {{$data->BRCH_CD}} -{{$data->BANK_PAY_IN_SLIP}}</th>
	            </tr>
	            <tr>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">JENIS RESIT :</th>
	                <th class="text-left text-uppercase" style="font-weight: ;font-size: 12px;">{{$ltype}}</th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">NO. RESIT</th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">: {{$data->BRCH_CD}} -{{(int)$data->RECEIPT_NO}}</th>
	            </tr>
	             <tr>
	                <th class="text-left" ></th>
	                <th class="text-left" ></th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">TARIKH RESIT</th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">: {{date('d/m/Y', strtotime($data->RECEIPT_DT))}}</th>
	            </tr>
	            <tr>
	                <th class="text-left" ></th>
	                <th class="text-left" ></th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">TARIKH TERIMA</th>
	                <th class="text-left" style="font-weight: ;font-size: 12px;">: {{date('d/m/Y', strtotime($data->PYMT_RECV_DT))}}</th>
	            </tr>
	             <!-- <tr>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">NO. SIRI :</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">{{$data->UPDATE_USERID}}{{date('dmY', strtotime($data->DT))}}{{$data->seq_receipt}}</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">NO. RESIT :</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">{{(int)$data->RECEIPT_DT}}</th>
	            </tr>
	            <tr>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">JENIS RESIT :</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;"></th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">TARIKH RESIT :</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">{{date('d/m/Y', strtotime($data->PYMT_RECV_DT))}}</th>
	            </tr>
	            <tr>
	                <th class="text-left" ></th>
	                <th class="text-left" ></th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">TARIKH TERIMA :</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">{{date('d/m/Y', strtotime($data->RECEIPT_DT))}}</th>
	            </tr> -->
	        </tbody>
	    </table>
	    <br>
	    <table class="table table-borderless table-xs" style="border:2px solid black;line-height: 1.2;margin-bottom: unset;" id="tableprint2">
	        <tbody>
	            <tr>
	                <th class="text-left" colspan="4">DITERIMA DARIPADA :<br>
	               </th>

	            </tr>

	            <tr>
	                <th rowspan="7" class="text-left" style="width:50%;white-space: unset;">
	                {{$data->NAME}}<br>
	                	<?php 

	                		$add1 = $data->ADDR_LINE_1.$data->ADDR_LINE_2.$data->ADDR_LINE_3.$data->ADDR_LINE_2.$data->CITY_KAUNTER.$data->POSCODE_KAUNTER.$data->STATE_KAUNTER;
	                		$add2 = $data->LINE_1.$data->LINE_2.$data->LINE_3.$data->LINE_2.$data->POSTCODE_BANK.$data->CITY_BANK.$data->STATE_BANK;
	                		if($add1){

	                			$add = $data->ADDR_LINE_1.' '.$data->ADDR_LINE_2.' '.$data->ADDR_LINE_3.' '.$data->ADDR_LINE_2.'<br>'.$data->CITY_KAUNTER.'<br>'.$data->POSCODE_KAUNTER.','.$data->STATE_KAUNTER;
	                		}else if($add2)
	                		{
	                			$add = $data->LINE_1.' '.$data->LINE_2.' '.$data->LINE_3.' '.$data->LINE_2.'<br>'.$data->POSTCODE_BANK.' '.$data->CITY_BANK.'<br>'.$data->STATE_BANK;
	                		}else{

	                			$add = '';
	                		}


	                	?>
	                	<span><?php echo $add ?></span>

	                </th>
	                <?php 

	                	if($data->NEW_IC_NO)
	                	{
	                		$ic = $data->NEW_IC_NO;

	                	}else if($data->OLD_IC_NO)
	                	{
	                		$ic = $data->OLD_IC_NO;

	                	}else if($data->POLICE_ARMY_NO)
	                	{
	                		$ic = $data->POLICE_ARMY_NO;

	                	}else{

	                		$ic = '';
	                	}

	                ?>
	                <th class="text-left" >NO. PENGENALAN</th>
	                <th class="text-left"  style="font-weight: lighter;font-size: 12px;">: {{$ic}}</th>
	                <th class="text-left" ></th>
	                
	            </tr>
	            <tr>
	                 <th class="text-left" >NO. RUJUKAN</th>
	                <th class="text-left"  style="font-weight: lighter;font-size: 12px;">: {{$file}} {{$rujukan}}</th>
	                <th class="text-left" ></th>
	            </tr>
	           <tr>
	                <th class="text-left">JUMLAH</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">: RM {{number_format($data->amt,2,'.',',')}}</th>
	                <th class="text-left"> </th>
	            </tr>
	            <tr>
	                <th class="text-left" >CARA BAYARAN</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">: {{$data->DESCPAYMODE}}</th>
	                <th class="text-left"> </th>
	            </tr>
	            <tr>
	                <th class="text-left" >NO. INSTRUMEN</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">:</th>
	                <th class="text-right" rowspan="3"><img src="{{asset('themes/ablepro/assets/images/elejarc.jpg')}}" alt="user image" width="90px"></th>
	            </tr>
	            <tr>
	                <th class="text-left" >BULAN / ANSURAN</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">: @if($data->INSTAL_DEDUCT_MTH == '99')  @else {{$data->INSTAL_DEDUCT_MTH}} @endif</th>
	            </tr>
	            <tr>
	                <th class="text-left">TAHUN</th>
	                <th class="text-left" style="font-weight: lighter;font-size: 12px;">: {{$data->Tahun}}  <br><br> </th>
	            </tr>
	        </tbody>

	    </table>
	    <span style="font-size: 11px;float:left">NO. KELULUSAN : KK/BPKS/10/600-2/1/2(13)</span>
	    <br>


	     <span style="width:70%;display:inline-block;font-size: 11px;line-height: 1">TERIMA KASIH KERANA MENYUMBANG UNUTK PEMBANGUNAN NEGARA<br> KETUA PENGARAH HASIL DALAM NEGERI<br> TELEFON KUALA LUMPUR : 03-62091000 KUCHING: 082-243211 KOTA KINABALU: 088-328400<br><h6 style="font-size: 14px">RESIT INI SAH SETELAH CEK DIPERAKUI OLEH BANK</h6><h6 style="font-size: 14px;font-weight: lighter;margin-bottom: 0.1em;">INI ADALAH CETAKAN KOMPUTER DAN TANDATANGAN TIDAK DIPERLUKAN
		</h6></span>
		<br>
		<span style="font-size: 10px;display:inline-block;font-style: italic;line-height: 0.9">Penafian: Walaupun segala penelitian telah diambil dalam penyediaan maklumat dalam resit ini, resit ini bertujuan untuk rujukan sahaja dan maklumat dari resit tidak akan menjelaskan apa - apa tuntutan LHDNM yang difailkan di Mahkamah.</span>
	</div>
</div>