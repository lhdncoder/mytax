<!--    <br>

   <button type="button" style="float: right;" onclick="javascript:loadlejar();" class="btn btn-warning has-ripple"><i class="feather mr-2 icon-file-text"></i>Ringkasan<span class="ripple ripple-animate" style="float:right;height: 113.25px; width: 113.25px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -32.825px; left: 1.375px;"></span></button> -->
<?php 

if($typel == 'INDIVIDU')
{
    $color = '#4680FF';
}else{
    $color = 'orange';
}

?>


   <br>
   <div class="row">

   <div class="col-md-12"><h6 class="p-l-30">
   @lang('lejar.title-penutup') : @if($typelejar->description == 'Cukai Pendapatan') @lang('lejar.table-income') ({{$typel}}) @else @lang('lejar.table-ckht') ({{$typel}}) @endif <br>

   @if($typel == 'INDIVIDU')
   @lang('lejar.title-calview') : <span class="m-r-20"></span>
   @forelse($calendar as $key => $year)

         <button type="button" onclick="javascript:loadlejarcalendar({{$year->Tahun}},'{{$typelejar->income_type}}');" class="btn btn-info has-ripple btn-sm"><i class="feather mr-2 icon-calendar"></i>{{$year->Tahun}}<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
   @empty
      Tiada Rekod
   @endforelse
   @endif

<button type="button" class="btn btn-success has-ripple btn-sm" onclick="javascript:loadlejar();" style="float:right"><i class="feather mr-2 icon-info"></i>@lang('homepage.backlabel')<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
   </h6>
   </div>
   </div>
   <br>
    <div class="card-body table-border-style" style="padding-right: unset;">
        <div class="table-responsive shadow" style="border-radius: 8px;margin-bottom:30px">
            <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                <thead>
                    <tr>
                        <th colspan="4" style="vertical-align: middle;text-align:left;border: unset;text-transform:unset;color:grey"><h6>@lang('lejar.title-sum-current') {{date('d/m/Y')}})</h6></th>
                        <th colspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-penutup-col1')<sup>4</sup></th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;padding-bottom:unset">@lang('lejar.table-penutup-col2')</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;padding-bottom:unset">@lang('lejar.table-penutup-col3')<sup>1</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;padding-bottom:unset">@lang('lejar.table-penutup-col4')<sup>2</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;padding-bottom:unset">@lang('lejar.table-penutup-col5')<sup>3</sup> (RM)</th>
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white;padding-bottom:unset">@lang('lejar.table-penutup-col6')<sup>5</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white;padding-bottom:unset">@lang('lejar.table-penutup-col7')<sup>6</sup></p></th>
                    </tr>


                </thead>
                <tbody>
                    @forelse($lejar as $dat =>$list)
                        <?php $data = 0;$data1 =0; ?>
                         <tr>
                            <td>
                            @forelse($calendarcurrent as $key => $year)
                                    @if($year->Tahun == $list->ASSESSMENT_YEAR)
                                        <a href="javascript:loadlejarcurrent({{$ltype}},{{$year->Tahun}},'{{$typelejar->income_type}}');">{{$list->ASSESSMENT_YEAR}}</a>
                                        <?php $data1 = 1; ?>
                                    @else
                                        <?php $data = 1; ?>
                                    @endif
                            @empty
                               <?php $data = 1; ?>
                            @endforelse
                            @if(($data == 1) && ($data1 == 0)) {{$list->ASSESSMENT_YEAR}} @endif
                             </td>  
                             <td style="text-align: right">{{number_format($list->JumTggnCukai,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->JumBayaranCukai,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->JumBersih,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->ByrnBelumBolehGuna,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->BakiCukaiSemasa,2,'.',',')}}</td>

                         </tr>
                       
                    @empty
                        <tr>
                            <td colspan="6">@lang('lejar.table-record')</td>
                        <tr>

                    @endforelse
                        <tr>
                            <td>@lang('lejar.table-penutup-total')</td>  
                             <td style="text-align: right"></td>
                             <td style="text-align: right"></td>
                             <td style="text-align: right">{{number_format($typelejar->BakiLejar,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($typelejar->ByrnBelumBolehGuna,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($typelejar->BakiCukai,2,'.',',')}}</td>
                        <tr>

                
                   
                </tbody>
            </table>
        </div>
        <div style="font-size:11px">
        <b>@lang('lejar.note'):</b><br>
          @lang('lejar.note-penutup-1')
          @lang('lejar.note-penutup-2')
          @lang('lejar.note-penutup-3')
          @lang('lejar.note-penutup-4')
          @lang('lejar.note-penutup-5')
          @lang('lejar.note-penutup-6')
        </div>
    </div>