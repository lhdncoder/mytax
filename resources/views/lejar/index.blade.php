<br>
<p class="mb-0"><h5>@lang('lejar.title-index')</h5></p>
<br>
<?php $row=0; ?>
@if(!$lejar->isEmpty())

@if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 4) OR ($user->access == 5) OR ($user->access == 7))
 <div class="card-body table-border-style">
    <div class="table-responsive shadow" style="border-radius: 8px">
        <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
            <thead>
                <tr>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bil')</th>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-type') (@if($user->language == 'ms') Individu @else Individual @endif)</th>
                    <th colspan="3" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-sum') (RM) </th>
                </tr>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bal')<sup>1</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-unused')<sup>2</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-lej')<sup>3</sup></th>
                <tr>
            </thead>

            <tbody>
            @foreach($lejar as $key => $data)
                @if($data->income_type == 'SALARY')
                    @if($checksal > 0)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>@lang('lejar.table-income')</td>
                             <td colspan="3" style="color:red">@lang('homepage.lejarerror')</td>
                        </tr>
                    @else
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>@lang('lejar.table-income')</td>
                        <td style="text-align: right">{{number_format($data->BakiCukai,2,'.',',')}}</td>
                        <td style="text-align: right">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</td>
                        <td style="text-align: right"><a href="javascript:loadpenutup('1','SALARY');">{{number_format($data->BakiLejar,2,'.',',')}}</a></td>
                    </tr>
                    @endif

                @else

                    @if(($data->BakiCukai == '0') && ($data->ByrnBelumBolehGuna == '0') && ($data->BakiLejar == '0'))
                     <tr>
                        <td>{{$key+1}}</td>
                        <td>@lang('lejar.table-ckht')</td>
                        <td colspan="3">@lang('lejar.table-record')</td>
                    </tr>
                    @else
                        @if($checkpro > 0)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>@lang('lejar.table-ckht')</td>
                                <td colspan="3" style="color:red">@lang('homepage.lejarerror')</td>
                            </tr>
                        @else
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>@lang('lejar.table-ckht')</td>
                            <td style="text-align: right">{{number_format($data->BakiCukai,2,'.',',')}}</td>
                            <td style="text-align: right">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</td>
                             <td style="text-align: right"><a href="javascript:loadpenutup('1','PROPERTIES');">{{number_format($data->BakiLejar,2,'.',',')}}</a></td>
                        </tr>
                        @endif
                    @endif


                @endif
                
            @endforeach
               
            </tbody>
        </table>
    </div>
</div>
@endif
@endif
<br>
@if(($user->access == 2) OR ($user->access == 3) OR ($user->access == 6) OR ($user->access == 7))
<div class="card-body table-border-style">
    <div class="table-responsive shadow" style="border-radius: 8px">
        <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
            <thead>
                
                <tr>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bil')</th>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-type') (@if($user->language == 'ms') Syarikat @else Company @endif)</th>
                    <th colspan="3" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-sum') (RM) </th>
                </tr>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bal')<sup>1</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-unused')<sup>2</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-lej')<sup>3</sup></th>
                <tr>
            </thead>
            <tbody>
                @if(count($comlist) > 0 )
                    <tr>
                        <td>1</td>
                        <td>@lang('lejar.table-income')</td>
                        <td colspan="3">
                            <span style="cursor:pointer;text-decoration: underline" data-toggle="collapse" data-target=".pro-wrk-edit" aria-expanded="false" aria-controls="pro-wrk-edit-1 pro-wrk-edit-2"><b>@lang('lejar.table-com')</b></span>
                        </td>
                    </tr>
                    <tr class="pro-wrk-edit collapse" id="pro-wrk-edit-2">
                        <td colspan="5" style="padding:20px !important">
                        <button type="button" class="btn btn-primary btn-sm rounded m-0 float-right" data-toggle="collapse" data-target=".pro-wrk-edit" aria-expanded="false" aria-controls="pro-wrk-edit-1 pro-wrk-edit-2">
                            <i class="feather icon-x-circle"></i>
                        </button>
                            <div class="table-responsive"   style="background-color: white;">
                                 <table id="comle" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($comlist as $keycom => $com)
                                            <tr>
                                                <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                <td>{{$com->Jenis_File}}</td>
                                                <td><a href="javascript:loadcom({{$com->id}},'SALARY');">{{$com->No_Rujukan}}</a> </td>
                                                <td>{{$com->No_Roc}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>@lang('lejar.table-ckht')</td>
                        <td colspan="3">
                            <span style="cursor:pointer;text-decoration: underline" data-toggle="collapse" data-target=".pro-wrk-editc" aria-expanded="false" aria-controls="pro-wrk-edit-1c pro-wrk-edit-2c"><b>@lang('lejar.table-com')</b></span>
                        </td>
                    </tr>
                    <tr class="pro-wrk-editc collapse" id="pro-wrk-edit-2c">
                        <td colspan="5" style="padding:20px !important">
                        <button type="button" class="btn btn-primary btn-sm rounded m-0 float-right" data-toggle="collapse" data-target=".pro-wrk-editc" aria-expanded="false" aria-controls="pro-wrk-edit-1c pro-wrk-edit-2c">
                            <i class="feather icon-x-circle"></i>
                        </button>
                            <div class="table-responsive"   style="background-color: white;">
                                 <table id="comck" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($comlist as $keycom => $com)
                                            <tr>
                                                <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                <td>{{$com->Jenis_File}}</td>
                                                <td><a href="javascript:loadcom({{$com->id}},'PROPERTIES');">{{$com->No_Rujukan}}</a> </td>
                                                <td>{{$com->No_Roc}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    


                @else
                <tr>
                    <td>1</td>
                    <td>@lang('lejar.table-income')</td>
                    <td colspan="3">@lang('lejar.table-nocom')</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>@lang('lejar.table-ckht')</td>
                    <td colspan="3">@lang('lejar.table-nocom')</td>
                </tr>
                @endif
               
            </tbody>
        </table>
    </div>
</div>
@endif

<div style="font-size:12px;margin-left:20px">
    <b>@lang('lejar.note'):</b><br>
    @lang('lejar.note-1')<br>
    @lang('lejar.note-2')<br>
    @lang('lejar.note-3')
</div>
