@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">My Inbox</h5>
            
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
                <div class="card email-card">
                   
                    <div class="card-body">
                        <div class="mail-body">
                            <div class="row">
                                <!-- [ inbox-left section ] start -->
                                <div class="col-xl-2 col-md-3">
                                    <ul class="mb-2 nav nav-tab flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left active" href="/user/inbox"  aria-controls="v-pills-home" aria-selected="false">
                                                <span><i class="feather icon-inbox"></i> Kembali</span>
                                                <span class="float-right"></span>
                                            </a>
                                        </li>
                                        @if(($data->Sumber == 'CP500') OR ($data->Sumber == 'CP500G'))
                                        <li class="nav-item mail-section">
                                        <a class="nav-link text-left active" href="javascript:printdiv('div_print');"  aria-controls="v-pills-home" aria-selected="false">
                                                <span><i class="feather icon-printer"></i> @lang('homepage.printlabel')</span>
                                                <span class="float-right"></span>
                                            </a>
                                            
                                        </li>
                                        @endif

                                               
                                        <!-- <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-starred-tab" data-toggle="pill" href="#v-pills-starred" role="tab">
                                                <span><i class="feather icon-star-on"></i> Starred</span>
                                            </a>
                                        </li>
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-draft" role="tab">
                                                <span><i class="feather icon-file-text"></i> Drafts</span>
                                            </a>
                                        </li>
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-mail" role="tab">
                                                <span><i class="feather icon-navigation"></i> Sent Mail</span>
                                            </a>
                                        </li>
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-Trash-tab" data-toggle="pill" href="#v-pills-Trash" role="tab">
                                                <span><i class="feather icon-trash-2"></i> Trash</span>
                                            </a>
                                        </li> -->
                                    </ul>
                                </div>
                                <div class="col-xl-10 col-md-9 inbox-right" id='div_print'>
                                        <?php 
                                            $date = new DateTime($data->TarikhNotis);
                                        ?>
                                    <link rel="icon" href="{{asset('themes/ablepro/assets/images/favicon.ico')}}" type="image/x-icon">
                                    <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/style.css')}}">
                                    <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/plugins/dataTables.bootstrap4.min.css')}}">
                                                                   
                                   <div class="card">
                                        <div class="card-header">
                                            <h6 class="d-inline-block m-0">{{$data->Subjek}}</h6>
                                            <p class="float-right m-0"><strong>{{$date->format('d-m-Y, h:i a')}}</strong></p>
                                        </div>
                                        <div class="card-body">
                                            
                                            <div class="m-b-20 m-l-50 p-l-10 email-contant">
                                                <div class="photo-contant">
                                                    <div>
                                                        <div class="email-content">
                                                            <?php echo $body?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="card-body border-top pro-det-edit collapse " id="pro-det-edit-2">
           
        </div>
    </div>
</div>

@endsection
@push('script')
<script>

$(document).ready(function() {


    var temp = '<?php echo $type ?>';

    if(temp == 'CP500'){

       $.ajax({

            type: "GET", 
            url: "{{ URL::to('mail/cptable')}}"+"/"+<?php echo $id ?>,
                   
            beforeSend: function () 
            {
                  
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $('#home').html(data);
                  // document.getElementById("nod").value = data;
                 
            }


        });
    }

});
   
</script>

<script type="text/javascript">
function printdiv(printpage)
{


document.getElementById(printpage).style.width = "100%";
var headstr = "<html><head><title></title></head><body style='background-color: white;font-size:10px;'>";
var footstr = "</body>";
var newstr = document.all.item(printpage).innerHTML;

var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
winPrint.document.write(headstr+newstr+footstr);
winPrint.document.close();
winPrint.focus();

setTimeout(function()
{
  winPrint.print();
  winPrint.close(); 

}, 1000);
// winPrint.print();



}


</script>

@endpush