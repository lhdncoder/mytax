<div class="table-responsive">
    <table class="table table-bordered table-xs text-center">
        <thead>
           
            <tr>
                <th style="vertical-align: middle;text-transform:unset;background: darkmagenta;color:white">Bil. Ansuran</th>
                <th style="vertical-align: middle;text-transform:unset;background: darkmagenta;color:white">Tarikh Perlu Bayar</th>
                <th style="vertical-align: middle;text-transform:unset;background: darkmagenta;color:white">Amaun Ansuran (RM)</th>

                
            </tr>


        </thead>
        <tbody>

            <?php 
                $total1=0;

                $dateskim = new DateTime($cp500data->SKIM_DATE);
                $startdate = new DateTime($cp500data->BASIS_START_DATE);
                $enddate = new DateTime($cp500data->BASIS_END_DATE);

                if($cp500data->BIL_PERLU_BYR > 1)
                {
                    for ($x = 1; $x <= $cp500data->BIL_PERLU_BYR-1; $x++) 
                    {

                        $total1 += $cp500data->MIN_DUE_AMOUNT;

            ?>


                            <tr>
                                <td>{{$x}}</td>
                                <td>{{$startdate->format('d/m/Y')}}</td>
                                <td>RM {{number_format($cp500data->MIN_DUE_AMOUNT,2,'.',',')}}</td>
                            </tr>

            <?php   
                        $startdate->modify('+2 months');
                    }  
            ?>

                    <tr>
                        <td>{{$cp500data->BIL_PERLU_BYR}}</td>
                        <td>{{$enddate->format('d/m/Y')}}</td>
                        <td>RM {{number_format($cp500data->MAX_DUE_AMOUNT,2,'.',',')}}</td>
                    </tr>
                    <!-- <tr>
                        <td colspan="2" style="text-align: left;background: darkmagenta;color:white">JUMLAH</td>
                        <td>RM {{number_format(($cp500data->MAX_DUE_AMOUNT + $total1),2,'.',',')}}</td>
                    </tr> -->

            <?php
                }else
                {


                }

            ?>
           
        
           
        </tbody>
    
    </table>

</div>