<div class="card-body" style="margin:30px">
    <div class="mail-body">
        <div class="row">
            <!-- [ inbox-left section ] start -->
            <div class="col-xl-1 col-md-2">
                <ul class="mb-2 nav nav-tab flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <li class="nav-item mail-section">
                        <a class="nav-link text-left active" href="javascript:loadall()"  aria-controls="v-pills-home" aria-selected="false">
                            <span><i class="feather icon-inbox"></i>  @lang('homepage.back-button')</span>
                            <span class="float-right"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xl-11 col-md-10 inbox-right">                    
               <div class="card">
                    <div class="card-header">
                        <h6 class="d-inline-block m-0">
                        @if($user->language == 'en')
                         	<?php echo $data->announcement_en ?>
                         @else
							<?php echo $data->announcement_bm ?>
						 @endif
                        </h6>
                        <p class="float-right m-0"><strong>{{date("d/m/Y", strtotime($data->start_date))}}</strong></p>
                    </div>
                    <div class="card-body" style="margin:20px">
                        <div class="email-content">
                             @if($user->language == 'en')
	                         	<?php echo $data->body_en ?>
	                         @else
								<?php echo $data->body_bm ?>
							 @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>