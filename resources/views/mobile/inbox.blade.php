@extends('ui::ablepro.mobile-content')

@section('content')
<style type="text/css">

.nav-pills>li>a:hover {
  background-color: #FABC0B !important
  color:white !important;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:#009EC5;
    color:white;
    font-weight:550;

    }

 </style>
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/inbox.png')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;background-color: white">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='{{URL::previous()}}' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">@lang('homepage.mobile-inbox')</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
    <div class="bt-wizard  fixed-top" style="position: sticky;">
        <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">
                    <li class="nav-item"><a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab1" class="nav-link active" data-toggle="tab">@lang('inbox.info')</a></li>
                    <li class="nav-item"><a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab2" class="nav-link" data-toggle="tab">@lang('inbox.memo')</a></li>
                    <li class="nav-item"><a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab3" class="nav-link" data-toggle="tab">@lang('inbox.letter')</a></li>
                    <li class="nav-item"><a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab4" class="nav-link" data-toggle="tab">@lang('inbox.noti')</a></li>
                </ul>
    </div>
    <div class="tab-content text-left " style="font-size: 12px !important;height:65vh;   width:100%;">
                    <div class="tab-pane active show" id="b-w-tab1" >
                        <div id="tab1data">
                            @forelse($data as $key => $value)
                                <?php 
                                    if($value->Unread == 'false')
                                    {
                                        $read = '';

                                    }else{
                                        $read = '<span class="badge badge-info" style="float:right">unread</span>';
                                    }
                                ?>

                                  <div class="shadow-sm p-2 bg-white rounded" onclick="javascript:loaddata(1,'{{$value->id}}');" style="cursor: pointer;" ><?php echo $read ?>{{$value->Subjek}}<br><small class="text-muted"><cite>{{date('d/m/Y, h:i a', strtotime($value->TarikhNotis))}}</cite></small></div>
                                  <br>
                                
                            @empty
                                 <form class="text-center">
                                      <i class="feather icon-check-circle display-3 text-success"></i>
                                      <h5 class="mt-3">@lang('inbox.empty')</h5>
                                      <p>@lang('inbox.nodata')</p>
                                  </form>
                            @endforelse
                        </div>
                        <div id="tab1content" style="display:none;" class="card email-card">
                             
                        </div>
                    </div>
                    <div class="tab-pane" id="b-w-tab2">

                         <form class="text-center">
                              <i class="feather icon-check-circle display-3 text-success"></i>
                              <h5 class="mt-3">@lang('inbox.empty')</h5>
                              <p>@lang('inbox.nodata')</p>
                          </form>
                    </div>
                    <div class="tab-pane" id="b-w-tab3">

                         <form class="text-center">
                              <i class="feather icon-check-circle display-3 text-success"></i>
                              <h5 class="mt-3">@lang('inbox.empty')</h5>
                              <p>@lang('inbox.nodata')</p>
                          </form>
                    </div>
                    <div class="tab-pane" id="b-w-tab4">
                        <div id="tab4data">
                          @forelse($datacp500 as $key => $value)
                              <?php 
                                  if($value->Unread == 'false')
                                  {
                                      $read = '';

                                  }else{
                                      $read = '<span class="badge badge-info" style="float:right">unread</span>';
                                  }
                              ?>

                                <div class="shadow-sm p-2 bg-white rounded" onclick="javascript:loaddata(4,'{{$value->id}}');" style="cursor: pointer;"><?php echo $read ?>{{$value->Subjek}}<br><small class="text-muted">{{date('d/m/Y, h:i a', strtotime($value->TarikhNotis))}}</cite></small></div>
                                <br>
                              
                          @empty
                               <form class="text-center">
                                  <i class="feather icon-check-circle display-3 text-success"></i>
                                  <h5 class="mt-3">@lang('inbox.empty')</h5>
                                  <p>@lang('inbox.nodata')</p>
                              </form>
                          @endforelse
                          </div>
                        <div id="tab4content" style="display:none;" class="card email-card">
                             
                        </div>
                    </div>
                   
                </div>




@endsection
@push('script')
<script type="text/javascript">
  
  function loaddata(type,id) {

    $.ajax({

            type: "GET", 
            url: "{{ URL::to('mail/mobileread')}}"+"/"+id+"/"+type,
                   
            beforeSend: function () 
            {
                
            },
            success: function(data)
            {       
                var x = document.getElementById("tab"+type+"data");
                var y = document.getElementById("tab"+type+"content");
              
                $('#tab'+type+'content').html(data);
                x.style.display = "none";
                y.style.display = "block";

                if(type == 4)
                {
                    $.ajax({

                      type: "GET", 
                      url: "{{ URL::to('mail/cptable')}}"+"/"+id,
                             
                      beforeSend: function () 
                      {
                            
                      },
                      success: function(data)
                      {       
                         
                            $('#home').html(data);
                          
                      }


                  });

                }
            }


        });

   

    
  }
    
</script>

<script type="text/javascript">
  
  function close(type) {

   
        var x = document.getElementById("tab"+type+"data");
        var y = document.getElementById("tab"+type+"content");

        x.style.display = "block";
        y.style.display = "none";
        $('#tab'+type+'content').html('');
    
  }
    
</script>

@endpush