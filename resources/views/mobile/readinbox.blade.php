<div class="card email-card" style="height:70vh;overflow-y:scroll;">
    <?php 
        $date = new DateTime($data->TarikhNotis);
    ?>
    <div class="card-header" >
        <h6 class="d-inline-block m-0" style="font-size: 12px !important;">{{$data->Subjek}}</h6>
        <br>
        <small class="text-muted">{{$date->format('d-m-Y, h:i a')}}</small><a href="javascript:close({{$types}});" style="float:right" class="badge badge-danger"> @lang('inbox.exit')</a>
       
    </div>
    <div class="mail-body">
        <div class="col-md-12 inbox-right" >
            <div class="email-contant">
                <div class="photo-contant">
                    <div>
                        <div class="email-content">
                            <?php echo $body?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

