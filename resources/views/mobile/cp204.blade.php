@extends('ui::ablepro.mobile-content')

@section('content')
<?php

    $row = 0;
    $cp204row = 0;

?>
<style type="text/css">

.nav-pills>li>a:hover {
  background-color: #FABC0B !important
  color:white !important;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:#009EC5;
    color:white;
    font-weight:550;

    }

 </style>
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left;">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/logo2.jpg')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile/taxstatus' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">CP204</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard fixed-top text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>CP204 Information</b></p>
</div>
<div class="tab-content text-left " style="font-size: 12px !important;height:62vh; overflow-x: hidden; overflow-y: scroll;width:100%;">
    <div class="card" style="background-color: transparent;box-shadow: unset">
        <div class="card-body">
            @foreach($cp204data as $keys => $data)
                                               
                @foreach($data as $key =>$datas)
                    <?php 
                        if($datas->asm_yr == date('Y'))
                        {  
                            $dateskim = new DateTime($datas->PAY_ST_DT);
                            $startdate = new DateTime($datas->PAY_ST_DT);
                            $enddate = new DateTime($datas->PAY_END_DT);
                            $cp204row = 1;
                    ?>

                    <div class="card user-card-full">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-sm-12">
                                <div class="card-body">
                                    <div class="row">
                                        
                                        <div class="col-sm-12">
                                             <p class="mb-0"><h5>TAHUN TAKSIRAN : {{$datas->asm_yr}}</h5></p>
                                             <p class="mb-0"><h5>JUMLAH : RM {{number_format($datas->TOTL_AMT,2,'.',',')}}</h5></p>
                                        </div>
                                        
                                         
                                        <div class="card-body table-border-style" style="padding-right: unset; font-size: 12px !important">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-xs text-center">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="9" style="vertical-align: middle;text-align:left;border: unset;text-transform:unset;color:grey"><h6>JADUAL BAYARAN ANSURAN CP204 {{$datas->C_NAME}}</h6></th>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <th style="vertical-align: middle;text-transform:unset;background: darkmagenta;color:white">Bil. Ansuran</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: darkmagenta;color:white">Tarikh Perlu Bayar</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: darkmagenta;color:white">Amaun Ansuran (RM)</th>

                                                            
                                                        </tr>


                                                    </thead>
                                                    <tbody>
                                                        <?php 

                                                            if($datas->bil_ans > 1)
                                                            {
                                                                for ($x = 1; $x <= $datas->bil_ans-1; $x++) 
                                                                {

                                                        ?>


                                                                        <tr>
                                                                            <td>{{$x}}</td>
                                                                            <td>{{$startdate->format('d/m/Y')}}</td>
                                                                            <td>RM {{number_format($datas->MTHLY_INSTAL_AMT,2,'.',',')}}</td>
                                                                        </tr>

                                                        <?php   
                                                                    $startdate->modify('+2 months');
                                                                }  
                                                        ?>

                                                                <tr>
                                                                    <td>{{$datas->bil_ans}}</td>
                                                                    <td>{{$enddate->format('d/m/Y')}}</td>
                                                                    <td>RM {{number_format($datas->LAST_INSTAL_AMT,2,'.',',')}}</td>
                                                                </tr>

                                                        <?php
                                                            }else
                                                            {


                                                            }

                                                        ?>
                                                       
                                                    
                                                       
                                                    </tbody>
                                                
                                                </table>

                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>
                @endforeach
                        
            
            @endforeach

            @if($cp204row == 0)
                
                <form class="text-center">
                    <i class="feather icon-check-circle display-3 text-success"></i>
                    <h5 class="mt-3">@lang('inbox.empty')</h5>
                    <p>@lang('inbox.nodata')</p>
                </form>
                            
            @endif
        </div>
    </div>
</div>




@endsection
@push('script')


@endpush