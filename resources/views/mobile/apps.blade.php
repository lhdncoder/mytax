@extends('ui::ablepro.mobile-content')

@section('content')
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/service.jpg')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">@lang('homepage.service')</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard fixed-top text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>@lang('homepage.apps')</b></p>
</div>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;height:70vh;overflow-y:scroll;overflow-x: hidden;   width:100%;">
        <div id="tabdata">
            <div class="row" style="padding-left:20px;padding-right:20px">
                <?php 
                    
                    $bgcolor1 = 'green';
                    $bgcolor2 = 'blue';
                    $bgcolor3 = 'red';
                    $bgcolor4 = 'yellow';

                    $count = 0;


                ?>

                @foreach($list as $key=> $app)
                    <?php 

                        $bgc = '';
                        $count = $count+1;
                        if($count == 12)
                        {
                            $count = 1;
                        }

                        if($count == 1){$bgc =  $bgcolor1;}
                        if($count == 2){$bgc =  $bgcolor2;}
                        if($count == 3){$bgc =  $bgcolor3;}
                        if($count == 4){$bgc =  $bgcolor4;}
                        if($count == 5){$bgc =  $bgcolor4;}
                        if($count == 6){$bgc =  $bgcolor3;}
                        if($count == 7){$bgc =  $bgcolor2;}
                        if($count == 8){$bgc =  $bgcolor1;}
                        if($count == 9){$bgc =  $bgcolor2;}
                        if($count == 11){$bgc =  $bgcolor3;}
                        if($count == 10){$bgc =  $bgcolor4;}

                    ?>

                    <div class="col-md-4 col-xl-3 col-12" style="padding:5px">
                        <div class="card order-card h-100 " data-animate="" style="color:unset;cursor:pointer;" onclick="javascript:loaddata('{{$app->id}}');" >
                            <div class="card-body " style="padding:10px 10px 0px 10px;">
                               

                                <h6 class="" style="font-size:12px;color:#377c97 !important">
                                    
                                    @if($user->language == 'en')
                                        <td>{{$app->service_en}}</td>
                                    @else
                                        <td>{{$app->service_bm}}</td>
                                    @endif
                                    
                                </h6>
                                <p style="font-size:10px;">

                                    @if($user->language == 'en')
                                        <td>{{$app->description_en}}</td>
                                    @else
                                        <td>{{$app->description_bm}}</td>
                                    @endif

                                </p>
                                 <div class="progress {{$bgc}}">
                                        <div class="progress-bar bg-c-{{$bgc}}" style="width:100%"></div>
                                    </div>

                            </div>
                        </div>
                    </div>



                    

                @endforeach
            </div>
        </div>
        <div id="tabcontent" style="display:none;">
             
        </div>
    </div>
</div>




@endsection
@push('script')
<script type="text/javascript">

$(document).ready(function() {

    var id = {{$ids}};

    if(id > 0)
    {
        loaddata(id);
    }


});
  
  function loaddata(id) {

    $.ajax({

            type: "GET", 
            url: "{{ URL::to('app/mobileview')}}"+"/"+id,
                   
            beforeSend: function () 
            {
                
            },
            success: function(data)
            {       
                var x = document.getElementById("tabdata");
                var y = document.getElementById("tabcontent");
              
                $('#tabcontent').html(data);
                x.style.display = "none";
                y.style.display = "block";

            }


        });

   

    
  }
    
</script>

<script type="text/javascript">
  
  function close() {

   
        var x = document.getElementById("tabdata");
        var y = document.getElementById("tabcontent");

        x.style.display = "block";
        y.style.display = "none";
        $('#tabcontent').html('');
    
  }
    
</script>

@endpush