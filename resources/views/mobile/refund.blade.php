@extends('ui::ablepro.mobile-content')

@section('content')
<?php

   
    $row = 0;
    $refdata = 0;


?>
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/refund.png')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;background-color: white">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile/taxstatus' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">@lang('mytax.refund')</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard fixed-top text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>@lang('mytax.refund')</b></p>
</div>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;height:70vh;overflow-y:scroll;   width:100%;">
        @foreach($apidata as $keys => $data)
            @if($data)
            <div class="col-sm-12  task-card card " style="margin-top:15px">
                <div class="card-body">
                @if($user->access == '2' OR $user->access == '3')
                    <span><b><p>{{$keys}}</p></b></span> <br> 
                @else
                     <span><b><p> INDIVIDU</p></b></span> <br> 
                @endif
                    <ul class="list-unstyled task-list">
                        @foreach($data as $key =>$refund)
                            <?php $daterefund = new DateTime($refund->tkh); $refdata = 1;?>
                            <li style="padding-left: 60px !important">
                               <i class="task-icon feather icon-check bg-c-green linebaricon"></i>
                                <span style="color: grey !important">
                                <p class="m-b-5 linebarparaph"><span style="font-size:10px">{{$daterefund->format('d/m/Y')}}</p>
                                <span class="linebar"></span>
                                <b class="linebarparaph">RM {{$refund->amt}} | 
                                    @if($user->language == 'en')
                                    {{$refund->Keteranganbi}}
                                    @else
                                    {{$refund->Keteranganbm}}
                                    @endif

                                </span></b>
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
        @endforeach
        @if($refdata == 0)
            <div class="card user-card-full">
                <div class="row m-l-0 m-r-0">
                    <div class="col-sm-12">
                        <div class="card-body">
                            <form class="text-center">
                                <i class="feather icon-check-circle display-3 text-success"></i>
                                <h5 class="mt-3">@lang('inbox.empty')</h5>
                                <p>@lang('inbox.nodata')</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>




@endsection
@push('script')


@endpush