        <div class="row">
            <div class="col-md-6" style="padding:unset">
                <div class="card client-map" style="background-color: unset;box-shadow: unset">
                    <div class="card-body">
                        <div class="client-detail">
                            <div class="">
                                <h6>@lang('mobile.label-lejarsalary') :</h6>
                            </div>
                        </div>
                        @foreach($lejar as $key => $data)
                            @if($data->income_type == 'SALARY')

                                <div class="row text-left">
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->BakiCukai,2,'.',',')}}</h6>
                                            <span class="">@lang('mobile.label-lejarbaki')<sup>1</sup></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</h6>
                                            <span class="">@lang('mobile.label-lejarguna')<sup>2</sup></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-blue" role="alert" style="background-color: #2f4f4f36;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;text-decoration: underline;"><a href="javascript:loadpenutup('1','SALARY');">RM {{number_format($data->BakiLejar,2,'.',',')}}</a></h6>
                                            <span class="">@lang('mobile.label-lejarbalance')<sup>3</sup></span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            
                        @endforeach
                       
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="padding:unset">
                <div class="card client-map" style="background-color: unset;box-shadow: unset">
                    <div class="card-body">
                        <div class="client-detail">
                            <div class="">
                                <h6>@lang('mobile.label-lejarckht') :</h6>
                            </div>
                        </div>
                        @foreach($lejar as $key => $data)
                            @if($data->income_type == 'PROPERTIES')

                                @if(($data->BakiCukai == '0') && ($data->ByrnBelumBolehGuna == '0') && ($data->BakiLejar == '0'))
                                   <form class="text-center shadow p-10">
                                      <i class="feather icon-check-circle display-3 text-success"></i>
                                      <h5 class="mt-3">@lang('inbox.empty')</h5>
                                      <p>@lang('inbox.nodata')</p>
                                  </form>
                                @else
                                <div class="row text-left">
                                        <div class="col-lg-12 col-xs-12 col-md-12">
                                            <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                                <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->BakiCukai,2,'.',',')}}</h6>
                                                <span class="">@lang('mobile.label-lejarbaki')<sup>1</sup></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-xs-12 col-md-12">
                                            <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                                <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</h6>
                                                <span class="">@lang('mobile.label-lejarguna')<sup>2</sup></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-xs-12 col-md-12">
                                            <div class="alert alert-info mb-0 shadow text-blue" role="alert" style="background-color: #2f4f4f36;">
                                                <h6 class="alert-heading" style="float:right;margin-left: 5px;text-decoration: underline;"><a href="javascript:loadpenutup('1','PROPERTIES');">RM {{number_format($data->BakiLejar,2,'.',',')}}</a></h6>
                                               <span class="">@lang('mobile.label-lejarbalance')<sup>3</sup></span>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                
                            @endif
                            
                        @endforeach
                       
                    </div>
                </div>
            </div>
        </div>
        <div style="font-size:9px;text-align: justify;">
            <b>Nota:</b><br>
            <b>1.  Baki Cukai</b> = Jumlah Tunggakan cukai/-Lebihan Bayaran cukai, selepas mengambilkira <b>Taksiran & Lain-Lain</b> dan <b>Bayaran & Lain-Lain</b> bagi tahun taksiran yang sama. Amaun ini belum mengambilkira bayaran, kenaikan yang layak dikenakan atau taksiran yang dibangkitkan selepas tarikh kemaskini lejar, jika ada.<br>
            <b>2.  Bayaran Belum Boleh Guna</b> = Bayaran cukai seperti bayaran PCB/CP204. Bayaran ini akan ditolak dengan Taksiran Cukai apabila taksiran dibangkitkan/disifatkan.<br>
            <b>3.  Baki Lejar</b> = Baki di lejar pembayar cukai yang merangkumi jumlah <b>Baki Cukai<sup>1</sup></b> dan <b>Bayaran Belum Boleh Guna<sup>2</sup></b>.
        </div>