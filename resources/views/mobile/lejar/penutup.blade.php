<?php 

if($typel == 'INDIVIDU')
{
    $color = 'darkmagenta';
}else{
    $color = 'orange';
}

?>
   <div class="row">

   <div class="col-md-12"><h6 style="font-size: 12px">
   Jenis Lejar : {{$typelejar->description}} ({{$typel}})<br>

   @if($typel == 'INDIVIDU')
   PAPARAN MENGIKUT TAHUN KALENDAR : <br><br>
   @forelse($calendar as $key => $year)

         <button type="button" style="font-size: 11px" onclick="javascript:loadlejarcalendar({{$year->Tahun}},'{{$typelejar->income_type}}');" class="btn btn-info has-ripple btn-sm"><i class="feather mr-2 icon-calendar"></i>{{$year->Tahun}}<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
   @empty
      Tiada Rekod
   @endforelse
   @endif
   <button type="button" class="btn btn-success has-ripple btn-sm" onclick="javascript:loadlejar();" style="float:right;font-size: 11px"><i class="feather mr-2 icon-info"></i>Kembali<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>

   </h6>
   </div>
   </div>
   <br>

        <div class="table-responsive">
            <table class="table table-bordered table-xs text-center table-striped">
                <thead>
                    <tr>
                        <th colspan="4" style="vertical-align: middle;text-align:left;border: unset;text-transform:unset;color:grey"><h6 style="font-size: 11px">RINGKASAN BAKI MENGIKUT TAHUN TAKSIRAN (KEDUDUKAN SEHINGGA {{date('d/m/Y')}})</h6></th>
                        <th colspan="2" style="vertical-align: middle;text-transform:unset;background: #2066FF;color:white">Ringkasan Baki<sup>4</sup></th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;font-size: 11px">TAKSIRAN</th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;font-size: 11px">TAKSIRAN & <p style="font-size: 11px">LAIN-LAIN<sup>1</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;font-size: 11px">BAYARAN & <p style="font-size: 11px">LAIN-LAIN<sup>2</sup> (RM)</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: {{$color}};color:white;font-size: 11px">BAKI<sup>3</sup> (RM)</th>
                         <th style="vertical-align: middle;text-transform:unset;background: #2066FF;color:white;font-size: 11px">BAYARAN BELUM <p style="font-size: 11px">BOLEH GUNA<sup>5</sup> (RM)</p></th>
                          <th style="vertical-align: middle;text-transform:unset;background: #2066FF;color:white;font-size: 11px">LEBIHAN BAYARAN <p style="font-size: 11px">BAKI CUKAI<sup>6</sup></p></th>
                    </tr>


                </thead>
                <tbody>
                    @forelse($lejar as $dat =>$list)
                        <?php $data = 0;$data1 =0; ?>
                         <tr>
                            <td>
                            @forelse($calendar as $key => $year)
                                    @if($year->Tahun == $list->ASSESSMENT_YEAR)
                                        <a href="javascript:loadlejarcurrent({{$ltype}},{{$year->Tahun}},'{{$typelejar->income_type}}');">{{$list->ASSESSMENT_YEAR}}</a>
                                        <?php $data1 = 1; ?>
                                    @else
                                        <?php $data = 1; ?>
                                    @endif
                            @empty
                               <?php $data = 1; ?>
                            @endforelse
                            @if(($data == 1) && ($data1 == 0)) {{$list->ASSESSMENT_YEAR}} @endif
                             </td>  
                             <td style="text-align: right">{{number_format($list->JumTggnCukai,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->JumBayaranCukai,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->JumBersih,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->ByrnBelumBolehGuna,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($list->BakiCukaiSemasa,2,'.',',')}}</td>

                         </tr>
                       
                    @empty
                        <tr>
                            <td colspan="6">Tiada Rekod</td>
                        <tr>

                    @endforelse
                        <tr>
                            <td>Jumlah</td>  
                             <td style="text-align: right"></td>
                             <td style="text-align: right"></td>
                             <td style="text-align: right">{{number_format($typelejar->BakiLejar,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($typelejar->ByrnBelumBolehGuna,2,'.',',')}}</td>
                             <td style="text-align: right">{{number_format($typelejar->BakiCukai,2,'.',',')}}</td>
                        <tr>

                
                   
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <div style="font-size:10px;text-align: justify;">
          <b>Nota:</b><br>
          <b>1.  Taksiran & Lain-Lain</b> = Taksiran cukai/kenaikan cukai/bayaran balik/pelarasan dan lain-lain.<br>
          <b>2.  Bayaran & Lain-Lain</b> = Bayaran cukai/pengurangan cukai/pelarasan dan lain-lain.<br>
          <b>3.  Baki</b> = Perbezaan antara <b>Taksiran & Lain-Lain<sup>1</sup></b> dan <b>Bayaran & Lain-Lain<sup>2</sup></b>.<br>
          <b>4.  Ringkasan Baki</b> = Butiran terperinci kedudukan <b>Baki<sup>3</sup></b> yang terdiri daripada <b>Bayaran Belum Boleh Guna<sup>5</sup></b> dan/atau <b>Lebihan Bayaran/Baki Cukai<sup>6</sup></b>.<br>
          <b>5.  Bayaran Belum Boleh Guna</b> = Bayaran cukai seperti bayaran PCB/CP204. Bayaran ini akan ditolak dengan Taksiran Cukai apabila taksiran dibangkitkan/disifatkan.<br>
          <b>6.  Lebihan Bayaran/Baki Cukai</b> = Jumlah Tunggakan cukai/-Lebihan Bayaran cukai, selepas mengambilkira <b>Taksiran & Lain-Lain<sup>1</sup></b> dan <b>Bayaran & Lain-Lain<sup>2</sup></b> bagi tahun <br>taksiran yang sama. Amaun ini belum mengambilkira bayaran, kenaikan yang layak dikenakan atau taksiran yang dibangkitkan selepas tarikh kemaskini lejar, jika ada.
        </div>
