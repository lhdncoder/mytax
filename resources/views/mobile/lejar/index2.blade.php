@extends('ui::ablepro.mobile-content')

@section('content')
<style type="text/css">

.nav-pills>li>a:hover {
  background-color: #FABC0B !important
  color:white !important;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:#009EC5;
    color:white;
    font-weight:550;

    }

 </style>
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:right;">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/lejar.png')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;background-color: white">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='{{URL::previous()}}' style="float:left !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">Lejar</h6><i class="feather icon-arrow-left"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard  fixed-top" style="position: sticky;">
    <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">
        @if(sizeof($lejarindividu) > 0)
            <li class="nav-item">
                <a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab1" class="nav-link active" data-toggle="tab">
                    @lang('mobile.label-lejarindividu')
                </a>
            </li>
        @endif
        @if(sizeof($lejarsyarikat) > 0)
            @if(sizeof($lejarindividu) > 0)
                <li class="nav-item">
                    <a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab2" class="nav-link" data-toggle="tab">
                        @lang('mobile.label-lejarsyarikat')
                    </a>
                </li>
            @else 
                <li class="nav-item">
                    <a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab2" class="nav-link active" data-toggle="tab">
                        @lang('mobile.label-lejarsyarikat')
                    </a>
                </li>
            @endif
        @endif
    </ul>
</div>
<div class="tab-content text-left " style="font-size: 12px !important;height:62vh; overflow-x: hidden; overflow-y: scroll;width:100%;">
    <div class="tab-pane @if(sizeof($lejarindividu) > 0) active show @endif" id="b-w-tab1" >
    <div class="row">
        <div class="col-md-6" style="padding:unset">
            <div class="card client-map" style="background-color: unset;box-shadow: unset">
                <div class="card-body">
                    <div class="client-detail">
                        <div class="">
                            <h6>@lang('mobile.label-lejarsalary') :</h6>
                        </div>
                    </div>
                    @foreach($lejarindividu as $key => $data)
                        @if($data->income_type == 'SALARY')

                            <div class="row text-left">
                                <div class="col-lg-12 col-xs-12 col-md-12">
                                    <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                        <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->BakiCukai,2,'.',',')}}</h6>
                                        <span class="">@lang('mobile.label-lejarbaki')</span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12 col-md-12">
                                    <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                        <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</h6>
                                        <span class="">@lang('mobile.label-lejarguna')</span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12 col-md-12">
                                    <div class="alert alert-info mb-0 shadow text-blue" role="alert" style="background-color: #2f4f4f36;">
                                        <h6 class="alert-heading" style="float:right;margin-left: 5px;text-decoration: underline;"><a href="javascript:loadpenutup('SALARY');">RM {{number_format($data->BakiLejar,2,'.',',')}}</a></h6>
                                        <span class="">@lang('mobile.label-lejarbalance')</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                        
                    @endforeach
                   
                </div>
            </div>
        </div>
        <div class="col-md-6" style="padding:unset">
            <div class="card client-map" style="background-color: unset;box-shadow: unset">
                <div class="card-body">
                    <div class="client-detail">
                        <div class="">
                            <h6>@lang('mobile.label-lejarckht') :</h6>
                        </div>
                    </div>
                    @foreach($lejarindividu as $key => $data)
                        @if($data->income_type == 'PROPERTIES')

                            @if(($data->BakiCukai == '0') && ($data->ByrnBelumBolehGuna == '0') && ($data->BakiLejar == '0'))
                               <form class="text-center shadow p-10">
                                  <i class="feather icon-check-circle display-3 text-success"></i>
                                  <h5 class="mt-3">@lang('inbox.empty')</h5>
                                  <p>@lang('inbox.nodata')</p>
                              </form>
                            @else
                            <div class="row text-left">
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->BakiCukai,2,'.',',')}}</h6>
                                            <span class="">@lang('mobile.label-lejarbaki')</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #019E89AD;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</h6>
                                            <span class="">@lang('mobile.label-lejarguna')</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-blue" role="alert" style="background-color: #2f4f4f36;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;text-decoration: underline;"><a href="javascript:loadpenutup('PROPERTIES');">RM {{number_format($data->BakiLejar,2,'.',',')}}</a></h6>
                                           <span class="">@lang('mobile.label-lejarbalance')</span>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            
                        @endif
                        
                    @endforeach
                   
                </div>
            </div>
        </div>
    </div>


    </div>

    <div class="tab-pane @if(sizeof($lejarindividu) > 0)  @else active show @endif" id="b-w-tab2">
        <div class="row">
        <div class="col-md-6" style="padding:unset">
            <div class="card client-map" style="background-color: unset;box-shadow: unset">
                <div class="card-body">
                    <div class="client-detail">
                        <div class="">
                            <h6>@lang('mobile.label-lejarsalary') :</h6>
                        </div>
                    </div>
                    @foreach($lejarsyarikat as $key => $data)
                        @if($data->income_type == 'SALARY')

                            <div class="row text-left">
                                <div class="col-lg-12 col-xs-12 col-md-12">
                                    <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #FE474C;">
                                        <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->BakiCukai,2,'.',',')}}</h6>
                                        <span class="">@lang('mobile.label-lejarbaki')</span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12 col-md-12">
                                    <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #FE474C;">
                                        <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</h6>
                                        <span class="">@lang('mobile.label-lejarguna')</span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12 col-md-12">
                                    <div class="alert alert-info mb-0 shadow text-blue" role="alert" style="background-color: #2f4f4f36;">
                                        <h6 class="alert-heading" style="float:right;margin-left: 5px;text-decoration: underline;"><a href="javascript:loadpenutup('SALARY');">RM {{number_format($data->BakiLejar,2,'.',',')}}</a></h6>
                                        <span class="">@lang('mobile.label-lejarbalance')</span>
                                    </div>
                                </div>
                            </div>
                        @else
                            <form class="text-center shadow p-10">
                              <i class="feather icon-check-circle display-3 text-success"></i>
                              <h5 class="mt-3">@lang('inbox.empty')</h5>
                              <p>@lang('inbox.nodata')</p>
                          </form>
                        @endif
                        
                    @endforeach
                   
                </div>
            </div>
        </div>
        <div class="col-md-6" style="padding:unset">
            <div class="card client-map" style="background-color: unset;box-shadow: unset">
                <div class="card-body">
                    <div class="client-detail">
                        <div class="">
                            <h6>@lang('mobile.label-lejarckht') :</h6>
                        </div>
                    </div>
                    @forelse($lejarsyarikat as $key => $data)
                        @if($data->income_type == 'PROPERTIES')

                            @if(($data->BakiCukai == '0') && ($data->ByrnBelumBolehGuna == '0') && ($data->BakiLejar == '0'))
                               <form class="text-center shadow p-10">
                                  <i class="feather icon-check-circle display-3 text-success"></i>
                                  <h5 class="mt-3">@lang('inbox.empty')</h5>
                                  <p>@lang('inbox.nodata')</p>
                              </form>
                            @else
                            <div class="row text-left">
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #FE474C;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->BakiCukai,2,'.',',')}}</h6>
                                            <span class="">@lang('mobile.label-lejarbaki')</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-white" role="alert" style="background-color: #FE474C;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;">RM {{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</h6>
                                            <span class="">@lang('mobile.label-lejarguna')</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xs-12 col-md-12">
                                        <div class="alert alert-info mb-0 shadow text-blue" role="alert" style="background-color: #2f4f4f36;">
                                            <h6 class="alert-heading" style="float:right;margin-left: 5px;text-decoration: underline;"><a href="javascript:loadpenutup('PROPERTIES');">RM {{number_format($data->BakiLejar,2,'.',',')}}</a></h6>
                                           <span class="">@lang('mobile.label-lejarbalance')</span>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            
                        @else
                            <form class="text-center shadow p-10">
                              <i class="feather icon-check-circle display-3 text-success"></i>
                              <h5 class="mt-3">@lang('inbox.empty')</h5>
                              <p>@lang('inbox.nodata')</p>
                          </form>
                        @endif
                        
                    @endforeach
                   
                </div>
            </div>
        </div>
    </div>
    </div>
   
   
</div>




@endsection
@push('script')


@endpush