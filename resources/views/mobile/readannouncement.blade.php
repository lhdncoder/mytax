<div class="card email-card" style="height:70vh;overflow-y:scroll;">
    <?php 
        $date = new DateTime($data->start_date);
    ?>
    <div class="card-header" >
        <h6 class="d-inline-block m-0" style="font-size: 12px !important;">
        @if($user->language == 'en')
            {{$data->announcement_en}}
        @else
            {{$data->announcement_bm}}
        @endif
        </h6>
        <br>
        <small class="text-muted">{{$date->format('d-m-Y, h:i a')}}</small><a href="javascript:close();" style="float:right" class="badge badge-danger"> @lang('inbox.exit')</a>
       
    </div>
    <div class="mail-body">
        <div class="col-md-12 inbox-right" >
            <div class="email-contant">
                <div class="photo-contant">
                    <div>
                        <div class="email-content" style="font-size:12px">
                            @if($user->language == 'en')
                                <?php echo $data->body_en ?>
                             @else
                                <?php echo $data->body_bm ?>
                             @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

