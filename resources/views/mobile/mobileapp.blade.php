@extends('ui::ablepro.mobile-content')

@section('content')
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/logo2.jpg')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">@lang('homepage.mobile-app')</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard fixed-top text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>Platform : {{$device}}</b></p>
</div>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;height:70vh;overflow-y:scroll;   width:100%;">
        <div class="row" style="margin:1px">
            @foreach($mobile as $key => $value)
              <?php if($device == 'IOS')
                    {
                        $url = $value->ios_url;
                    }else{
                        $url = $value->and_url;
                    }

              ?>
                <div class="col-sm-3 col-4 col-md-2 text-center" style="cursor:pointer;padding-top: 10px;" onclick="location.href='{{$url}}'">
                    @if($value->icon_url) <img src="/storage/appicon/{{$value->icon_url}}" alt="user image" class="latest-posts-img" height="80px" width="80px"> @else <img src="{{asset('themes/ablepro/assets/images/logo2.jpg')}}" alt="user image" class="latest-posts-img" height="80px" width="80px"> @endif
                    <br>
                    @if($user->language == 'en')
                       {{$value->mobile_module_name_en}}                
                    @else
                       {{$value->mobile_module_name_bm}}                
                    @endif
                </div>

                


            @endforeach
        </div>
    </div>
</div>




@endsection
@push('script')


@endpush