@extends('ui::ablepro.mobile-content')

@section('content')
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/ann.png')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;background-color: white">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px;"><h6 class="mb-1 mt-3 text-center">@lang('homepage.announcement')</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard fixed-top text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>@lang('homepage.announcement')</b></p>
</div>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;height:70vh;overflow-y:scroll;overflow-x: hidden;   width:100%;">
        <div id="tabdata">
            <div class="row" style="padding-left:20px;padding-right:20px">
                @forelse($announcement as $key => $data)

                    <div onclick="javascript:loaddata('{{$data->id}}');" class="widget-statstic-card pt-0 pb-1 pl-3 pr-3 bg-white rounded col-12 m-b-10" style="box-shadow: 2px 2px 10px -5px rgb(0, 84, 151);"> 
                        <span style="padding:unset;font-size: 12px;color:#00867b"><strong>{{date("d/m/Y", strtotime($data->start_date))}}</strong></span>
                        <p style="font-size: 11px;color:#00867b">
                            @if($user->language == 'en')
                                {{$data->announcement_en}}
                                <br>read..
                            @else
                                {{$data->announcement_bm}}
                                <br>baca..
                            @endif
                        </p>
                        <i style="font-size:12px;background-color:#0687b9;padding: 35px 35px 12px 12px;" class="fas fa-bell st-icon"></i>
                    </div>
                
                @empty
                <form class="text-center widget-statstic-card pt-2 pb-1 pl-3 pr-3 bg-white rounded col-12">
                    <i class="feather icon-check-circle display-4 text-success"></i>
                    <h6 class="mt-3">@lang('inbox.empty')</h6>
                    <p>@lang('inbox.nodata')</p>
                    <i style="font-size:12px;background-color:#0687b9;padding: 35px 35px 12px 12px;" class="fas fa-bell st-icon"></i>
                </form>
                @endforelse
            </div>
        </div>
        <div id="tabcontent" style="display:none;">
             
        </div>
    </div>
</div>




@endsection
@push('script')
<script type="text/javascript">

$(document).ready(function() {

    var id = {{$ids}};

    if(id > 0)
    {
        loaddata(id);
    }


});
  
  function loaddata(id) {

    $.ajax({

            type: "GET", 
            url: "{{ URL::to('announcement/mobileview')}}"+"/"+id,
                   
            beforeSend: function () 
            {
                
            },
            success: function(data)
            {       
                var x = document.getElementById("tabdata");
                var y = document.getElementById("tabcontent");
              
                $('#tabcontent').html(data);
                x.style.display = "none";
                y.style.display = "block";

            }


        });

   

    
  }
    
</script>

<script type="text/javascript">
  
  function close() {

   
        var x = document.getElementById("tabdata");
        var y = document.getElementById("tabcontent");

        x.style.display = "block";
        y.style.display = "none";
        $('#tabcontent').html('');
    
  }
    
</script>

@endpush