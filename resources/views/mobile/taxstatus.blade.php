@extends('ui::ablepro.mobile-content')

@section('content')
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/logo2.jpg')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">@lang('homepage.taxstatus')</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard fixed-top text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>@lang('homepage.mobile-tax-line')</b></p>
</div>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;height:70vh;overflow-y:scroll;   width:100%;">
        <div class="row" style="margin:1px">
          <div class="col-sm-4 col-6 col-md-3" style="padding: 0px 10px 5px 10px;cursor:pointer;" onclick="location.href='/mobile/lejar'">
              <div class="card bg-c-yellow text-white widget-visitor-card">
                  <div class="card-body text-center" style="padding: 10px 10px;">
                      <h6 class="text-white">LEJAR</h6>
                      <i style="font-size:55px" class="feather icon-file-text"></i>
                  </div>
              </div>
          </div>
          <div class="col-sm-4 col-6 col-md-3" style="padding: 0px 10px 5px 10px;cursor:pointer;" onclick="location.href='/mobile/refund'">
              <div class="card bg-c-green text-white widget-visitor-card">
                  <div class="card-body text-center" style="padding: 10px 10px;">
                      <h6 class="text-white">Bayaran Balik</h6>
                      <i style="font-size:55px" class="feather icon-file-text"></i>
                  </div>
              </div>
          </div>
          <div class="col-sm-4 col-6 col-md-3" style="padding: 0px 10px 5px 10px; cursor:pointer;" onclick="location.href='/mobile/pcb'">
              <div class="card bg-c-blue text-white widget-visitor-card">
                  <div class="card-body text-center" style="padding: 10px 10px;">
                      <h6 class="text-white">PCB</h6>
                      <i style="font-size:55px" class="feather icon-file-text"></i>
                  </div>
              </div>
          </div>
          <div class="col-sm-4 col-6 col-md-3" style="padding: 0px 10px 5px 10px;cursor:pointer;" onclick="location.href='/mobile/cp204'">
              <div class="card bg-c-purple text-white widget-visitor-card" style="background-color: indianred;">
                  <div class="card-body text-center" style="padding: 10px 10px;">
                      <h6 class="text-white">CP204</h6>
                      <i style="font-size:55px" class="feather icon-file-text"></i>
                  </div>
              </div>
          </div>
          <div class="col-sm-4 col-6 col-md-6" style="padding: 0px 10px 5px 10px;cursor:pointer;" onclick="location.href='/mobile/cp500'">
              <div class="card bg-c-red text-white widget-visitor-card">
                  <div class="card-body text-center" style="padding: 10px 10px;">
                      <h6 class="text-white">CP500</h6>
                      <i style="font-size:55px" class="feather icon-file-text"></i>
                  </div>
              </div>
          </div>
          <div class="col-sm-4 col-6 col-md-6" style="padding: 0px 10px 5px 10px;cursor:pointer;" onclick="location.href='/mobile/spc'">
              <div class="card bg-c-red text-white widget-visitor-card" style="background-color: darkcyan">
                  <div class="card-body text-center" style="padding: 10px 10px;">
                      <h6 class="text-white">SPC</h6>
                      <i style="font-size:55px" class="feather icon-file-text"></i>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>




@endsection
@push('script')


@endpush