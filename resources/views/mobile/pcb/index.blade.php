@extends('ui::ablepro.mobile-content')

@section('content')
<style type="text/css">

.nav-pills>li>a:hover {
  background-color: #FABC0B !important
  color:white !important;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:#009EC5;
    color:white;
    font-weight:550;

    }

 </style>
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left;">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/lejar.png')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;background-color: white">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile/taxstatus' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">PCB</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard  fixed-top" style="position: sticky;">
    <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">
        
            <li class="nav-item">
                <a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab1" class="nav-link active" data-toggle="tab">
                    Tahun Taksiran
                </a>
            </li>
           
            <li class="nav-item">
                <a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab2" class="nav-link" data-toggle="tab">
                    Tahun Kalendar
                </a>
            </li>
            
    </ul>
    <span style="font-size:12px;text-align: center" >Nota: <br>Penyata ini hanya memaparkan bayaran PCB sahaja. Penyata kedudukan akaun cukai terperinci perlu semak melalui e-Lejer</span>
</div>
<br>
<div class="tab-content text-left " style="font-size: 12px !important;height:62vh; overflow-x: hidden; overflow-y: scroll;width:100%;">
    <div class="tab-pane active show" id="b-w-tab1" >
        <div class="card-body" style="padding:unset">
            <ul class="nav nav-tabs mb-3 nav-fill" id="myTabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active text-uppercase" id="yeary-tab" data-toggle="tab" href="#yeary" role="tab" aria-controls="yeary" aria-selected="true">THN TAKSIRAN {{$year}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" id="yearl-tab" data-toggle="tab" href="#yearl" role="tab" aria-controls="yearl" aria-selected="false">THN TAKSIRAN {{$lastyear}}</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="yeary" role="tabpanel" aria-labelledby="yeary-tab">
                    
                    <div class="card task-card">
                        <div class="card-header text-center">
                            <h5 style="font-size:12px">PENYATA PCB - CUKAI PENDAPATAN BAGI TAHUN TAKSIRAN {{$year}}</h5>
                            <!-- 'year','lastyear','yearly','totalyearlycurrent','totalyearlylast','calendar','totalcalcurrent','totalcallast' 

                            -->
                        </div>
                        <div class="card-body">
                        @if(!$yearlycurrent->isEmpty())
                            <div class="table-responsive">
                                <table class="table table-bordered table-xs text-center" style="font-size: 11px !important">
                                    <thead>
                                        <tr>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<P style="font-size: 11px !important">KEMASKINI</P></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<p style="font-size: 11px !important">TRANSAKSI</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">KETERANGAN<p style="font-size: 11px !important">TRANSAKSI</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">RUJUKAN/<p style="font-size: 11px !important">NO. RESIT</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAHUN<p style="font-size: 11px !important">TAKSIRAN</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BULAN/<p style="font-size: 11px !important">BIL ANSURAN</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAKSIRAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>1</sup></p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BAYARAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>2</sup></p></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ('year','lastyear','yearlycurrent','yearlylast','totalyearlycurrent','totalyearlylast','calendarcurrent','calendarlast','totalcalcurrent','totalcallast') -->
                                        @forelse($yearlycurrent as $dat =>$list)
                                        <?php 

                                             $datep = new DateTime($list->POSTED_DATE);
                                             $datet = new DateTime($list->TRANSACTION_DATE);
                                             // $total += number_format($list->BayaranCukai, 2);

                                             if($list->RECEIPT_NO == '0')
                                             {
                                                $resit = '';
                                             }else{
                                                $resit = $list->BRANCH_CODE.$list->RECEIPT_NO;
                                             }

                                            
                                        ?>
                                             <tr>
                                                 <td>{{$datep->format('d/m/Y')}}</td>
                                                 <td>{{$datet->format('d/m/Y')}}</td>
                                                 <td>{{$list->Keterangan}}</td>
                                                 <td>{{$resit}}</td>
                                                 @if($list->Keterangan == 'Baki Permulaan')
                                                 <td></td>
                                                 <td></td>
                                                 @else
                                                 <td>{{$list->ASSESSMENT_YEAR}}</td>
                                                 <td>{{$list->ASSESSMENT_NO}}</td>
                                                 @endif
                                                 <td style="text-align: right">{{$list->TggnCukai}}</td>
                                                 <td style="text-align: right">{{$list->BayaranCukai}}</td>
                                             </tr>
                                           
                                         @empty
                                        <tr>    
                                            <td colspan="8"><h6>Tiada Rekod</h6></td>
                                        </tr>
                                        @endforelse
                                         <tr>    
                                            <td colspan="5"><h6></h6></td>
                                            <td><h6>JUMLAH</h6></td>
                                            <td><h6></h6></td>
                                            <td style="text-align: right"><h6>@if($totalyearlycurrent) {{number_format($totalyearlycurrent->BakiCukai,2,'.',',')}} @endif</h6></td>
                                        </tr>
                                    
                                       
                                    </tbody>
                                
                                </table>

                            </div>
                            <!-- <br>
                            <br>
                            <div class="text-center">
                                <a href="#!" class="b-b-primary text-primary">Print</a>
                            </div> -->
                        @else
                            <form class="text-center">
                                <i class="feather icon-check-circle display-3 text-success"></i>
                                <h5 class="mt-3">@lang('inbox.empty')</h5>
                                <p>@lang('inbox.nodata')</p>
                            </form>
                        @endif
                        </div>
                    </div>


                </div>
                <div class="tab-pane fade" id="yearl" role="tabpanel" aria-labelledby="yearl-tab">
                    
                    <div class="card task-card">
                        <div class="card-header text-center">
                            <h5 style="font-size:12px">PENYATA PCB - CUKAI PENDAPATAN BAGI TAHUN TAKSIRAN {{$lastyear}}</h5>
                            
                        </div>
                        <div class="card-body">
                        @if(!$yearlylast->isEmpty())
                            <div class="table-responsive">
                                <table class="table table-bordered table-xs text-center" style="font-size: 11px !important">
                                    <thead>
                                        <tr>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<P style="font-size: 11px !important">KEMASKINI</P></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<p style="font-size: 11px !important">TRANSAKSI</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">KETERANGAN<p style="font-size: 11px !important">TRANSAKSI</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">RUJUKAN/<p style="font-size: 11px !important">NO. RESIT</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAHUN<p style="font-size: 11px !important">TAKSIRAN</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BULAN/<p style="font-size: 11px !important">BIL ANSURAN</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAKSIRAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>1</sup></p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BAYARAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>2</sup></p></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ('year','lastyear','yearlycurrent','yearlylast','totalyearlycurrent','totalyearlylast','calendarcurrent','calendarlast','totalcalcurrent','totalcallast') -->
                                        @forelse($yearlylast as $dat =>$list)
                                        <?php 

                                             $datep = new DateTime($list->POSTED_DATE);
                                             $datet = new DateTime($list->TRANSACTION_DATE);
                                             // $total += number_format($list->BayaranCukai, 2);

                                             if($list->RECEIPT_NO == '0')
                                             {
                                                $resit = '';
                                             }else{
                                                $resit = $list->BRANCH_CODE.$list->RECEIPT_NO;
                                             }

                                            
                                        ?>
                                             <tr>
                                                 <td>{{$datep->format('d/m/Y')}}</td>
                                                 <td>{{$datet->format('d/m/Y')}}</td>
                                                 <td>{{$list->Keterangan}}</td>
                                                 <td>{{$resit}}</td>
                                                 @if($list->Keterangan == 'Baki Permulaan')
                                                 <td></td>
                                                 <td></td>
                                                 @else
                                                 <td>{{$list->ASSESSMENT_YEAR}}</td>
                                                 <td>{{$list->ASSESSMENT_NO}}</td>
                                                 @endif
                                                 <td style="text-align: right">{{$list->TggnCukai}}</td>
                                                 <td style="text-align: right">{{$list->BayaranCukai}}</td>
                                             </tr>
                                           
                                         @empty
                                        <tr>    
                                            <td colspan="8"><h6>Tiada Rekod</h6></td>
                                        </tr>
                                        @endforelse
                                         <tr>    
                                            <td colspan="5"><h6></h6></td>
                                            <td><h6>JUMLAH</h6></td>
                                            <td><h6></h6></td>
                                            <td style="text-align: right"><h6>@if($totalyearlylast) {{number_format($totalyearlylast->BakiCukai,2,'.',',')}} @endif</h6></td>
                                        </tr>
                                    
                                       
                                    </tbody>
                                
                                </table>

                            </div>
                            <!-- <br>
                            <br>
                            <div class="text-center">
                                <a href="#!" class="b-b-primary text-primary">Print</a>
                            </div> -->
                        @else
                            <form class="text-center">
                                <i class="feather icon-check-circle display-3 text-success"></i>
                                <h5 class="mt-3">@lang('inbox.empty')</h5>
                                <p>@lang('inbox.nodata')</p>
                            </form>
                        @endif   
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="b-w-tab2">
        <div class="card-body" style="padding:unset">
            <ul class="nav nav-tabs mb-3 nav-fill" id="myTabsl" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active text-uppercase" id="caly-tab" data-toggle="tab" href="#caly" role="tab" aria-controls="caly" aria-selected="true">THN KALENDAR {{$year}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" id="call-tab" data-toggle="tab" href="#call" role="tab" aria-controls="call" aria-selected="false">THN KALENDAR {{$lastyear}}</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabslContent">
                <div class="tab-pane fade show active" id="caly" role="tabpanel" aria-labelledby="caly-tab">
                    
                    <div class="card task-card">
                        <div class="card-header text-center">
                            <h5 style="font-size:12px">PENYATA PCB - CUKAI PENDAPATAN BAGI TAHUN KALENDAR {{$year}}</h5>
                            <!-- 'year','lastyear','yearly','totalyearlycurrent','totalyearlylast','calendar','totalcalcurrent','totalcallast' 

                            -->
                        </div>
                        <div class="card-body">
                        @if(!$calendarcurrent->isEmpty())
                            <div class="table-responsive">
                                <table class="table table-bordered table-xs text-center" style="font-size: 11px !important">
                                    <thead>
                                        <tr>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<P style="font-size: 11px !important">KEMASKINI</P></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<p style="font-size: 11px !important">TRANSAKSI</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">KETERANGAN<p style="font-size: 11px !important">TRANSAKSI</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">RUJUKAN/<p style="font-size: 11px !important">NO. RESIT</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAHUN<p style="font-size: 11px !important">TAKSIRAN</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BULAN/<p style="font-size: 11px !important">BIL ANSURAN</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAKSIRAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>1</sup></p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BAYARAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>2</sup></p></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ('year','lastyear','yearlycurrent','yearlylast','totalyearlycurrent','totalyearlylast','calendarcurrent','calendarlast','totalcalcurrent','totalcallast') -->
                                        @forelse($calendarcurrent as $dat =>$list)
                                        <?php 

                                             $datep = new DateTime($list->POSTED_DATE);
                                             $datet = new DateTime($list->TRANSACTION_DATE);
                                             // $total += number_format($list->BayaranCukai, 2);

                                             if($list->RECEIPT_NO == '0')
                                             {
                                                $resit = '';
                                             }else{
                                                $resit = $list->BRANCH_CODE.$list->RECEIPT_NO;
                                             }

                                            
                                        ?>
                                             <tr>
                                                 <td>{{$datep->format('d/m/Y')}}</td>
                                                 <td>{{$datet->format('d/m/Y')}}</td>
                                                 <td>{{$list->Keterangan}}</td>
                                                 <td>{{$resit}}</td>
                                                 @if($list->Keterangan == 'Baki Permulaan')
                                                 <td></td>
                                                 <td></td>
                                                 @else
                                                 <td>{{$list->ASSESSMENT_YEAR}}</td>
                                                 <td>{{$list->ASSESSMENT_NO}}</td>
                                                 @endif
                                                 <td style="text-align: right">{{$list->TggnCukai}}</td>
                                                 <td style="text-align: right">{{$list->BayaranCukai}}</td>
                                             </tr>
                                           
                                         @empty
                                        <tr>    
                                            <td colspan="8"><h6>Tiada Rekod</h6></td>
                                        </tr>
                                        @endforelse
                                         <tr>    
                                            <td colspan="5"><h6></h6></td>
                                            <td><h6>JUMLAH</h6></td>
                                            <td><h6></h6></td>
                                            <td style="text-align: right"><h6>@if($totalcalcurrent) {{number_format($totalcalcurrent->BakiCukai,2,'.',',')}} @endif</h6></td>
                                        </tr>
                                    
                                       
                                    </tbody>
                                
                                </table>

                            </div>
                            <!-- <br>
                            <br>
                            <div class="text-center">
                                <a href="#!" class="b-b-primary text-primary">Print</a>
                            </div> -->
                        @else
                            <form class="text-center">
                                <i class="feather icon-check-circle display-3 text-success"></i>
                                <h5 class="mt-3">@lang('inbox.empty')</h5>
                                <p>@lang('inbox.nodata')</p>
                            </form>
                        @endif
                        </div>
                    </div>


                </div>
                <div class="tab-pane fade" id="call" role="tabpanel" aria-labelledby="cal-tab">
                    
                    <div class="card task-card">
                        <div class="card-header text-center">
                            <h5 style="font-size:12px">PENYATA PCB - CUKAI PENDAPATAN BAGI TAHUN KALENDAR {{$lastyear}}</h5>
                            
                        </div>
                        <div class="card-body">
                        @if(!$calendarlast->isEmpty())
                            <div class="table-responsive">
                                <table class="table table-bordered table-xs text-center" style="font-size: 11px !important">
                                    <thead>
                                        <tr>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<P style="font-size: 11px !important">KEMASKINI</P></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TARIKH<p style="font-size: 11px !important">TRANSAKSI</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">KETERANGAN<p style="font-size: 11px !important">TRANSAKSI</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">RUJUKAN/<p style="font-size: 11px !important">NO. RESIT</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAHUN<p style="font-size: 11px !important">TAKSIRAN</p></th>

                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BULAN/<p style="font-size: 11px !important">BIL ANSURAN</p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">TAKSIRAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>1</sup></p></th>
                                            <th style="font-size: 11px !important;vertical-align: middle;text-transform:unset;background: #D2691E;color:white">BAYARAN & <p style="font-size: 11px !important">LAIN-LAIN<sup>2</sup></p></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ('year','lastyear','yearlycurrent','yearlylast','totalyearlycurrent','totalyearlylast','calendarcurrent','calendarlast','totalcalcurrent','totalcallast') -->
                                        @forelse($calendarlast as $dat =>$list)
                                        <?php 

                                             $datep = new DateTime($list->POSTED_DATE);
                                             $datet = new DateTime($list->TRANSACTION_DATE);
                                             // $total += number_format($list->BayaranCukai, 2);

                                             if($list->RECEIPT_NO == '0')
                                             {
                                                $resit = '';
                                             }else{
                                                $resit = $list->BRANCH_CODE.$list->RECEIPT_NO;
                                             }

                                            
                                        ?>
                                             <tr>
                                                 <td>{{$datep->format('d/m/Y')}}</td>
                                                 <td>{{$datet->format('d/m/Y')}}</td>
                                                 <td>{{$list->Keterangan}}</td>
                                                 <td>{{$resit}}</td>
                                                 @if($list->Keterangan == 'Baki Permulaan')
                                                 <td></td>
                                                 <td></td>
                                                 @else
                                                 <td>{{$list->ASSESSMENT_YEAR}}</td>
                                                 <td>{{$list->ASSESSMENT_NO}}</td>
                                                 @endif
                                                 <td style="text-align: right">{{$list->TggnCukai}}</td>
                                                 <td style="text-align: right">{{$list->BayaranCukai}}</td>
                                             </tr>
                                           
                                         @empty
                                        <tr>    
                                            <td colspan="8"><h6>Tiada Rekod</h6></td>
                                        </tr>
                                        @endforelse
                                         <tr>    
                                            <td colspan="5"><h6></h6></td>
                                            <td><h6>JUMLAH</h6></td>
                                            <td><h6></h6></td>
                                            <td style="text-align: right"><h6>@if($totalcallast) {{number_format($totalcallast->BakiCukai,2,'.',',')}} @endif</h6></td>
                                        </tr>
                                    
                                       
                                    </tbody>
                                
                                </table>

                            </div>
                            <!-- <br>
                            <br>
                            <div class="text-center">
                                <a href="#!" class="b-b-primary text-primary">Print</a>
                            </div> -->
                        @else
                            <form class="text-center">
                                <i class="feather icon-check-circle display-3 text-success"></i>
                                <h5 class="mt-3">@lang('inbox.empty')</h5>
                                <p>@lang('inbox.nodata')</p>
                            </form>
                        @endif   
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
   
   
</div>




@endsection
@push('script')


@endpush