@extends('ui::ablepro.mobile-content')

@section('content')
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">

        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/pages/interview.svg')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;background-color: white">
                    <br>
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='{{URL::previous()}}' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px">
                       <h6 class="mb-1 mt-3 text-center">@lang('homepage.mobile-profile')</h6><i class="feather icon-arrow-right" style="float:right"></i></a> 
                    </h5>
                </div>
            </div>
        </div>
        <a href="/mobile/logout" style="float:right;color:#00867b;font-size:11px;font-weight: 600"><span class="feather icon-lock"> </span>@lang('homepage.logout')</a>
    </div>
</div>
<br>
<div class="bt-wizard  fixed-top" style="position: sticky;">

    <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">
        <li class="nav-item">
            <a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab1" class="nav-link active" data-toggle="tab">
                @lang('homepage.mobile-profile-line')
            </a>
        </li>
        <li class="nav-item">
            <a style="font-size:11px !important;margin-top: 10px;margin-bottom: 10px;" href="#b-w-tab2" class="nav-link" data-toggle="tab">
                @lang('homepage.mobile-cert')
            </a>
        </li>

    </ul>
</div>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;height:70vh;overflow-y:scroll;   width:100%;">
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/name.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5">
            <div class="media-body">
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">@lang('mobile.label-name')</h6>
                <p class="m-b-0" style="font-size: 12px !important">{{$user->name}}</p>
            </div>
        </div>
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/ic.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5">
            <div class="media-body">
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">@lang('mobile.label-ic')</h6>
                <p style="font-size: 12px !important" class="m-b-0">{{$user->reference_id}}</p>
            </div>
        </div>
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/taxno.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5">
            <div class="media-body">
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">@lang('mobile.label-taxno')</h6>
                <p style="font-size: 12px !important" class="m-b-0">{{$user->doc_type}} {{$user->tax_no}}</p>
            </div>
        </div>
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/homep.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5 m-t-5">
            <div class="media-body">
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">@lang('mobile.label-phone')</h6>
                <p style="font-size: 12px !important" class="m-b-0">{{$profile->homephone_no}}</p>
            </div>
        </div>
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/phone.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5">
            <div class="media-body">
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">@lang('mobile.label-mphone')</h6>
                <p style="font-size: 12px !important" class="m-b-0">{{$profile->handphone_no}}</p>
            </div>
        </div>
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/mail.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5">
            <div class="media-body">
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">@lang('mobile.label-email')</h6>
                <p style="font-size: 12px !important" class="m-b-0">{{$user->email}}</p>
            </div>
        </div>
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/add.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5">
            <div class="media-body">
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">@lang('mobile.label-address')</h6>
                <p style="font-size: 12px !important" class="m-b-0"> {{$profile->address}}</p>
            </div>
        </div>
        <div class="media mb-3 shadow-sm pt-3 pb-3" >
            <img src="{{asset('themes/ablepro/assets/images/thumb.png')}}" alt="user image" class="img-radius wid-25 align-top m-r-15 m-l-5">
            @if($webau)
            <a href="/mobile/del/{{$webau->id}}" class="media-body" >
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">Setting Up Device Authentication</h6>
                <p style="font-size: 12px !important" class="m-b-0"> Disable WebAuth</p>
            </a>
            @else
            <a href="/webauthn/register" class="media-body" >
                <h6 style="font-size: 12px !important" class="mb-0 text-h-primary">Setting Up Device Authentication</h6>
                <p style="font-size: 12px !important" class="m-b-0"> WebAuth</p>
            </a>
            @endif
        </div>

    </div>

    <div class="tab-pane" id="b-w-tab2" style="font-size: 12px !important;height:70vh;overflow-y:scroll;   width:100%;">
          <div class="card user-card2 shadow-lg" >
            <div class="card">
                            <div class="card-body d-flex align-items-center justify-content-between" style="background-color: cornflowerblue;">
                                <h5 class="mb-0" style="color:white">@lang('profile.title-certs')</h5>
                              
                            </div>
                            <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
                                <div class="bt-wizard" style="position: sticky;">
                                    <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">


                                    @if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 5) OR ($user->access == 7))
                                        <li class="nav-item">
                                            <a class="nav-link active" id="ind-tab" data-toggle="tab" href="#ind" role="tab" aria-controls="ind">
                                                @lang('profile.cert2')
                                            </a>
                                        </li>  
                                    @endif  
                                    @if(($user->access == 2) OR ($user->access == 3) OR ($user->access == 6) OR ($user->access == 7))
                                        <li class="nav-item">
                                            <a class="nav-link @if(($user->access == 2) OR ($user->access == 6))active @endif" id="oef-tab" data-toggle="tab" href="#oef" role="tab" aria-controls="oef">
                                                @lang('profile.cert1')
                                            </a>
                                        </li>  
                                    @endif
                                    @if(($user->access == 4) OR ($user->access == 5) OR ($user->access == 6) OR ($user->access == 7))  
                                        <li class="nav-item ">
                                            <a class="nav-link @if($user->access == 4) active @endif" id="pef-tab" data-toggle="tab" href="#pef" role="tab" aria-controls="pef">
                                                @lang('profile.cert3')
                                            </a>
                                        </li> 
                                    @endif   
                                    </ul>
                                </div>
                                 <div class="tab-content" id="myTabContent">
                                 @if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 5) OR ($user->access == 7))
                                <div class="tab-pane fade active show" id="ind" role="tabpanel" aria-labelledby="ind-tab">
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            @if(isset($dataprofile->CertStatus))

                                                @if($dataprofile->CertStatus == 'True')
                                                <div class="card-body">
                                                    <div class="media-body">
                                                        <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                                        <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$profile->tax_cert_status) }}</b></p>
                                                    </div>
                                                </div>
                                                @else
                                                    <div class="card-body">
                                                        <form class="text-center">
                                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                                            <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                            <!-- <p>@lang('inbox.nodata')</p> -->
                                                        </form>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="card-body">
                                                    <form class="text-center">
                                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if(($user->access == 2) OR ($user->access == 3) OR ($user->access == 6) OR ($user->access == 7))
                                <div class="tab-pane fade @if(($user->access == 2) OR ($user->access == 6))active show @endif" id="oef" role="tabpanel" aria-labelledby="oef-tab">
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            @if($dataprofile->CertStatusOeF == 'True')
                                            <div class="card-body">
                                                <div class="media-body">
                                                    <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                                    <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$dataprofile->CertValidityOeF) }}</b></p>
                                                </div>
                                            </div>
                                

                                            @else
                                                <div class="card-body">
                                                    <form class="text-center">
                                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                                    </form>
                                                </div>
                                            @endif



                                        </div>


                                    </div>
                                    @if(count($comlist) > 0 )
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            <div class="dt-responsive table-responsive" style="background-color: white">
                                                <table class="table table-striped table-bordered nowrap" id="com">
                                                    <thead>
                                                        <tr>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('profile.cert-status')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col5')</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($comlist as $keycom => $com)
                                                            <tr>
                                                                <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                                <td>{{$com->Jenis_File}}</td>
                                                                <td>{{$com->No_Rujukan}}</td>
                                                                <td>{{$com->No_Roc}}</td>
                                                                <td>{{$com->Status_OeF}}</td>
                                                                <td>{{$com->Tarikh_Daftar}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                @endif
                                @if(($user->access == 4) OR ($user->access == 5) OR ($user->access == 6) OR ($user->access == 7))  
                                <div class="tab-pane fade  @if($user->access == 4) active show @endif" id="pef" role="tabpanel" aria-labelledby="pef-tab">
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            @if($dataprofile->CertStatusPeF == 'True')
                                            <div class="card-body">
                                                <div class="media-body">
                                                    <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                                    <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$dataprofile->CertValidityPeF) }}</b></p>
                                                </div>
                                            </div>
                                            @else
                                                <div class="card-body">
                                                    <form class="text-center">
                                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                </div>
                            </div>
                        </div>
        </div>
    </div>
</div>




@endsection
@push('script')


@endpush