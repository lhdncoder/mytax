@extends('ui::ablepro.mobile-content')

@section('content')
<style type="text/css">

.nav-pills>li>a:hover {
  background-color: #FABC0B !important
  color:white !important;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:#009EC5;
    color:white;
    font-weight:550;

    }

 </style>
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: -110px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block" style="margin-top: 0px;float:left;">
                    <img class="img-radius img-fluid wid-70" src="{{asset('themes/ablepro/assets/images/logo2.jpg')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;">
                    
                </div>
                <div class="page-header-title">
                    <h5 class="m-b-10 " style="font-size: 14px;color:#00867b;font-weight: 200">
                       <a href='/mobile/taxstatus' style="float:right !important;margin:unset;cursor:pointer;color:white;font-size: 22px"><h6 class="mb-1 mt-3 text-center">SPC</h6><i class="feather icon-arrow-right" style="float:right"></i></a>    
                    </h5>

                    

                </div>

               

            </div>
        </div>
    </div>
</div>
<br>
<div class="bt-wizard fixed-top text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>SPC Information</b></p>
</div>
<div class="tab-content text-left " style="font-size: 12px !important;height:67vh; overflow-x: hidden; overflow-y: scroll;width:100%;">
    <div class="card" style="background-color: transparent;box-shadow: unset">
        <div class="card-body">
            <div class="row">
                                            @if($spc)
                                            <?php $keyactive = 0;?>

                                            <div class="col-md-9 col-sm-12">
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <?php $keyactive = 0;?>
                                                    @foreach($spc['emp'] as $keys =>$dataspc)
                                                        <div class="tab-pane fade @if($keyactive == 0) show active @endif" id="v-pills-{{str_replace(' ', '', $keys)}}" role="tabpanel" aria-labelledby="v-pills-{{str_replace(' ', '', $keys)}}-tab">
                                                            <div class="accordion" id="accordionExample">
                                                                @foreach($dataspc as $key =>$values)
                                                                    @foreach($values as $keyv =>$value)
                                                                    <div class="card mb-0">
                                                                        <div class="card-header" id="heading{{str_replace(' ', '', $key)}}">
                                                                            <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapse{{str_replace(' ', '', $key)}}" aria-expanded="true" aria-controls="collapse{{str_replace(' ', '', $key)}}">{{$keyv}}<br><span style="font-size: 12px;color:grey">No Cukai: {{$key}}</span></a></h5>
                                                                        </div>
                                                                        <div id="collapse{{str_replace(' ', '', $key)}}" class="collapse" aria-labelledby="heading{{str_replace(' ', '', $key)}}" data-parent="#accordionExample">
                                                                            <div class="col-sm-12  task-card card " style="">
                                                                                <div class="card-body ">
                                                                                   <ul class="list-unstyled task-list">
                                                                                        @foreach($value as $key2 =>$data)
                                                                                            <li style="padding-left: 60px !important">
                                                                                               <i class="task-icon feather icon-check bg-c-green linebaricon"></i>
                                                                                                <span style="color: grey !important">
                                                                                                <p class="m-b-5 linebarparaph"><span style="font-size:10px">{{date('d/m/Y', strtotime($data['date']))}}</p>
                                                                                                <span class="linebar"></span>
                                                                                                <b class="linebarparaph">{{$data['stat']}}</span></b>
                                                                                                </span>
                                                                                            </li>
                                                                                        @endforeach   
                                                                                        </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                @endforeach
                                                            </div>  
                                                        </div>
                                                    <?php $keyactive = 1;?>
                                                    @endforeach 
                                                </div>
                                            </div>

                                                
                                            @else
                                                <div class="col-sm-12">
                                                    <div class="card-body">
                                                        <form class="text-center">
                                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                                            <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                            <p>@lang('inbox.nodata')</p>
                                                        </form>
                                                    </div>
                                                </div>
                                            @endif

                                               
                                        </div>
        </div>
    </div>
</div>




@endsection
@push('script')


@endpush