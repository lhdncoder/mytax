@extends('ui::ablepro.mobile')

@section('content')
<br>
 <div class="card" style="background-color:  #e8eaec;box-shadow: unset;margin-right: -2px;margin-left: -2px;">
    <div class="card-header fixed-top shadow" style="background-color: white;border-bottom:unset !important;border-radius: calc(0.25rem - 0px) calc(0.25rem - 0px) calc(0.25rem - 0px) calc(0.25rem - 0px);padding: 7px !important;position: sticky;">
        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist" style="font-size: 10px;border:unset !important;color:#00867b;">
            <li class="nav-item " ><i class="feather icon-layers f-28" href="/mobile/taxstatus" role="tab" aria-selected="true"></i>
                <a class="nav-link" href="/mobile/taxstatus" role="tab" aria-selected="true" style="color:#00867b;background-size: unset !important;background-image: unset !important">MyTax</a>
            </li>
            <li class="nav-item"><i class="feather icon-mail f-28 social-icon" href="/mobile/inbox" role="tab"></i>
                <a class="nav-link " href="/mobile/inbox" style="background-size: unset !important;color:#00867b;background-image: unset !important">Inbox</a>
            </li>
            <li class="nav-item"><i class="feather icon-user f-28 social-icon" href="/mobile/profile"></i>
                <a class="nav-link "  href="/mobile/profile" style="background-size: unset !important;color:#00867b;background-image: unset !important">Profile</a>
            </li>
            <li class="nav-item"><i class="feather icon-package f-28" href="/mobile/app"></i>
                <a class="nav-link "  href="/mobile/app" style="background-size: unset !important;color:#00867b;background-image: unset !important"">Apps</a>
            </li>

        </ul>
    </div>
    <div class="card-body" style="padding-right: 2px !important;padding-left: 2px !important;">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" data-animate="rubberBand" id="home" role="tabpanel" aria-labelledby="home-tab">



            <div class="card widget-statstic-card" style="">
                <div class="card-body">
                    <div class="card-header-left mb-3">
                        <h5 class="mb-0 text-uppercase" style="font-size: 13px;color:#00867b;">@lang('homepage.restrain')</h5>
                    </div>
                    <i style="background-color:orange;" class="fas fa-plane st-icon"></i>
                    <div class="text-center">
                    <br>
                        @if($profile->tax_restrain == '0')
                      
                            
                               <button type="button" class="btn btn-icon btn-success has-ripple" style="width:70px;height:70px;font-size: 2.28rem"><i class="feather icon-check-circle"></i><span class="ripple ripple-animate"></span></button> <br><br><p style="color:#00867b;"><b>@lang('homepage.restrain-message-false')</b></p>
                                
                            
                        @else
                         
                               <button type="button" class="btn btn-icon btn-danger has-ripple" style="width:70px;height:70px;font-size: 2.28rem"><i class="feather icon-x-circle"></i><span class="ripple ripple-animate"></span></button> <br><br><p style="color:#00867b;">@lang('homepage.restrain-message-true')</p>
                                
                           
                        @endif

                      
                    </div>
                </div>
            </div>

             <div class="row">
                <div class="col-md-6 col-6"  style="">
                    <div class="card bg-c-green order-card h-100" style="margin-right:-10px">
                        <div class="card-body">

                           <?php 
                                $amount = (float)$profile->tax_balance;
                                if($amount < 0)
                                {
                                    $amount = $amount * -1;
                                    $lang = 1;

                                }else if($amount == 0)
                                {
                                    $lang = 2;
                                }else{

                                    $lang = 3;
                                }
                            ?>
                                @if($lang == 1)
                                    <h6 class="text-white" style="font-size: 13px;">@lang('homepage.balance1')</h6>
                                @elseif($lang == 2)
                                    <h6 class="text-white" style="font-size: 13px;">@lang('homepage.balance2')</h6>
                                @else
                                    <h6 class="text-white" style="font-size: 13px;">@lang('homepage.balance3')</h6>
                                @endif

                                <h5 class="text-white">RM {{number_format($amount,2,'.',',')}}</h5>
                                <i style="font-size:35px" class="card-icon fas fa-money-check-alt"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-6"  style="">
                    <div class="card bg-c-yellow order-card h-100" style="margin-left:-10px">
                        <div class="card-body">
                            <h6 class="text-white" style="font-size: 13px;">@lang('homepage.refund')</h6>
                            <h5 class="text-white">RM {{number_format($refunds,2,'.',',')}}</h5>
                            <i style="font-size:35px" class="card-icon fas fa-money-check-alt"></i>
                        </div>
                    </div>
                </div>
            </div>


            @if($user->access !== 2)
            <br>
                <h5 class="text-uppercase" style="font-size: 13px;color:#00867b;">@lang('homepage.beform')</h5>
                
            <div class="row">
                <div class="col-md-6 col-6"  style="">
                    <div class="card order-card h-100" style="margin-right:-10px;background-color: #007Aff">
                        <h6 class="text-right m-1" style="color:white">{{date('Y')-2}}</h6>
                               <div class="card-body">
                                     @if(isset($dataebe2[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                        @if($user->language == 'ms')
                                            Borang {{$dataebe2[0]->JENIS_BORANG}} {{date('Y')-2}} <br>telah dihantar pada {{date('d/m/Y', strtotime($dataebe2[0]->SUBMITDT)),}}
                                        @else

                                            {{$dataebe2[0]->JENIS_BORANG}} {{date('Y')-2}} form <br>was submitted on {{date('d/m/Y', strtotime($dataebe2[0]->SUBMITDT)),}}

                                        @endif

                                        </h6>
                                     @elseif(isset($dataebelogin2[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                            @if($user->language == 'ms')
                                               Borang {{$dataebelogin2[0]->JENIS_BORANG}} belum dihantar.<br> Borang diisi pada {{date('d/m/Y', strtotime($dataebelogin2[0]->tarikh_log)),}}
                                            @else

                                                {{$dataebelogin2[0]->JENIS_BORANG}} {{date('Y')-2}} form <br>not submitted yet. <br>Form was filled out on {{date('d/m/Y', strtotime($dataebelogin2[0]->tarikh_log)),}}
                                            @endif



                                        </h6>
                                     @else
                                        <h6 class="text-white" style="font-size: 14px">@lang('inbox.empty')</h6>
                                     @endif 
                                </div>
                    </div>
                </div>
                <div class="col-md-6 col-6"  style="">
                    <div class="card order-card h-100" style="margin-left:-10px;background-color:#88B258 ;">
                        <h6 class="text-right m-1" style="color:white">{{date('Y')-1}}</h6>

                                <div class="card-body">
                                   
                                     @if(isset($dataebe[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                            @if($user->language == 'ms')
                                                Borang {{$dataebe[0]->JENIS_BORANG}} {{date('Y')-1}} <br>telah dihantar pada {{date('d/m/Y', strtotime($dataebe[0]->SUBMITDT)),}}
                                            @else

                                                {{$dataebe[0]->JENIS_BORANG}} {{date('Y')-1}} form <br>was submitted on {{date('d/m/Y', strtotime($dataebe[0]->SUBMITDT)),}}

                                            @endif

                                        </h6>
                                     @elseif(isset($dataebelogin[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                            @if($user->language == 'ms')
                                               Borang {{$dataebelogin[0]->JENIS_BORANG}} belum dihantar.<br> Borang diisi pada {{date('d/m/Y', strtotime($dataebelogin[0]->tarikh_log)),}}
                                            @else

                                                {{$dataebelogin[0]->JENIS_BORANG}} form not <br>submitted yet. Form <br>was filled out on {{date('d/m/Y', strtotime($dataebelogin[0]->tarikh_log)),}}
                                            @endif

                                         

                                        </h6>
                                     @else
                                        <h6 class="text-white" style="font-size: 14px">@lang('inbox.empty')</h6>
                                     @endif 
                                </div>

                    </div>
                </div>
            </div>
            @endif
                <br>
                <h5 class="text-uppercase" style="font-size: 13px;color:#00867b;">@lang('homepage.service-section') <a href='/mobile/apps/0' style="text-transform:lowercase;font-size:12px;float:right !important;margin:unset;cursor:pointer;color:#00867b;">@lang('homepage.all') <i class="feather icon-chevrons-right"></i></a></h5>
                
                <div class="row swiper-container">
                    

                    <div class="swiper-wrapper" style="padding-bottom:10px !important;">
                    @foreach($list as $key =>$app)
                        <div class="swiper-slide h-100" ">
                            <div class="card bg-info text-white widget-visitor-card h-100" style="cursor:pointer;min-height:80px;background-image: url({{asset('themes/ablepro/assets/images/box.jpg')}});background-size: cover" onclick="location.href='/mobile/apps/{{$app->id}}'">
                                <div class="card-body pl-2 pr-2 pt-0 pb-0 h-100">
                                    @if($user->language == 'en')
                                        <h5 class="" style="font-size: 12px !important;color:grey;">{{$app->service_en}}</h5>
                                        <h6 class="" style="font-size: 11px !important;color:grey">{!! Str::words($app->description_en, 4, ' ...') !!}</h6>
                                    @else
                                        <h5 class="" style="font-size: 12px !important;color:grey">{{$app->service_bm}}</h5>
                                        <h6 class="" style="font-size: 11px !important;color:grey">{!! Str::words($app->description_bm, 4, ' ...') !!}</h6>
                                    @endif
                                </div>
                            </div>
                        </div>

                    @endforeach
                    </div>
                    <div class="swiper-pagination" style="bottom: 0px !important"></div>
                </div>
                <br>
                <h5 class="text-uppercase" style="font-size: 13px;color:#00867b;">@lang('homepage.announcement') <a href='/mobile/ann/0' style="text-transform:lowercase;font-size:12px;float:right !important;margin:unset;cursor:pointer;color:#00867b;">@lang('homepage.all') <i class="feather icon-chevrons-right"></i></a></h5>
   
                @forelse($announcement as $key => $data)

                    <div onclick="location.href='/mobile/ann/{{$data->id}}'" class="widget-statstic-card pt-0 pb-1 pl-3 pr-3 bg-white rounded" style="cursor:pointer;box-shadow: 2px 2px 10px -5px rgb(0, 84, 151);"> 
                        <span style="padding:unset;font-size: 12px;color:#00867b"><strong>{{date("d/m/Y", strtotime($data->start_date))}}</strong></span>
                        <p style="font-size: 11px;color:#00867b">
                            @if($user->language == 'en')
                                {{$data->announcement_en}} - read..
                            @else
                                {{$data->announcement_bm}} - baca..
                            @endif
                        </p>
                        <i style="font-size:12px;background-color:#0687b9;padding: 35px 35px 12px 12px;" class="fas fa-bell st-icon"></i>
                    </div>
                
                @empty
                <form class="text-center widget-statstic-card pt-2 pb-1 pl-3 pr-3 bg-white rounded">
                    <i class="feather icon-check-circle display-4 text-success"></i>
                    <h6 class="mt-3">@lang('inbox.empty')</h6>
                    <p>@lang('inbox.nodata')</p>
                    <i style="font-size:12px;background-color:#0687b9;padding: 35px 35px 12px 12px;" class="fas fa-bell st-icon"></i>
                </form>
                @endforelse
            </div>
            <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
               app list
            </div>
        </div>
    </div>
</div>



@endsection
@push('script')
<script type="text/javascript">
$(document).ready(function() {

$('a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
        var $old_tab = $($(e.target).attr("href"));
        var $new_tab = $($(e.relatedTarget).attr("href"));

        if($new_tab.index() < $old_tab.index()){
            $old_tab.css('position', 'relative').css("right", "0").show();
            $old_tab.animate({"right":"-100%"}, 200, function () {
                $old_tab.css("right", 0).removeAttr("style");
            });
        }
        else {
            $old_tab.css('position', 'relative').css("left", "0").show();
            $old_tab.animate({"left":"-100%"}, 200, function () {
                $old_tab.css("left", 0).removeAttr("style");
            });
        }
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $new_tab = $($(e.target).attr("href"));
        var $old_tab = $($(e.relatedTarget).attr("href"));

        if($new_tab.index() > $old_tab.index()){
            $new_tab.css('position', 'relative').css("right", "-2500px");
            $new_tab.animate({"right":"0"}, 200);
        }
        else {
            $new_tab.css('position', 'relative').css("left", "-2500px");
            $new_tab.animate({"left":"0"}, 200);
        }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // your code on active tab shown
    });

});

    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 10,
      loop: true, 
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        360: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
      }
    });

</script>

@endpush