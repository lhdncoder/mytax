
        <div class="card" style="min-height: 67vh;">
            <div class="card-body ">
            <a type="button" class="fc-month-button fc-button fc-state-default" href="javascript:close();" style="float:right !important;margin:unset;cursor:pointer;">@lang('homepage.service-section-all')</a>
                <h6 class="" style="font-size:14px;color:#377c97 !important">
                    @if($user->language == 'en')
                        <td>{{$app->service_en}}</td>
                    @else
                        <td>{{$app->service_bm}}</td>
                    @endif
                </h6>
                <p style="font-size:12px;">
                    @if($user->language == 'en')
                        <td>{{$app->description_en}}</td>
                    @else
                        <td>{{$app->description_bm}}</td>
                    @endif
                </p>
                
                <div class="progress blue">
                    <div class="progress-bar bg-c-blue" style="width:100%"></div>
                </div>
                <br>
                <hr>
                <br>
                <div class="email-content">
                   @if($user->language == 'en')
                        <?php echo $app->content_en ;?>
                    @else
                        <?php echo $app->content_bm ;?>
                    @endif
                </div>
                
            </div>
        </div>