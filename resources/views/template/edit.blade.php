@extends('ui::ablepro.dashboard')

@section('content')
      <div class="row">
            <!-- liveline-section start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                     <h5 class="mb-0">@lang('homepage.template-manage-edit')</h5>
                     <br>
                       {!! SemanticForm::post(route('mail.updatetemplate'))->attribute('id', 'addtemplate') !!}
                       <input type="hidden" name="id" value="{{$data->id}}">

                       <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab" role="tablist">
                            <li class="nav-item" style="border: 1px solid blue;">
                                <a class="nav-link has-ripple active" id="pills-bm-tab" data-toggle="pill" href="#pills-bm" role="tab" aria-controls="pills-bm" aria-selected="true">Bahasa Melayu<span class="ripple ripple-animate" style="height: 71.65px; width: 71.65px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -13.725px; left: -7.825px;"></span></a>
                            </li>
                            <li class="nav-item" style="border: 1px solid blue;">
                                <a class="nav-link has-ripple" id="pills-en-tab" data-toggle="pill" href="#pills-en" role="tab" aria-controls="pills-en" aria-selected="false">English<span class="ripple ripple-animate" style="height: 74.2667px; width: 74.2667px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -19.0333px; left: 3.21666px;"></span></a>
                            </li>
                            <li class="nav-item" style="border: 1px solid blue;">
                                <a class="nav-link has-ripple" id="pills-set-tab" data-toggle="pill" href="#pills-set" role="tab" aria-controls="pills-set" aria-selected="false">Setting<span class="ripple ripple-animate" style="height: 74.2667px; width: 74.2667px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -19.0333px; left: 3.21666px;"></span></a>
                            </li>
                            
                        </ul>

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade active show" id="pills-bm" role="tabpanel" aria-labelledby="pills-bm-tab">
                                 <div class="form-group">
                                    <label class="label">Isi Kandungan</label><button type="button" class="fc-month-button fc-button fc-state-default" data-toggle="modal" data-target=".bd-example-modal-lg" style="float:right !important;margin:unset;cursor:pointer;">Petunjuk Data Lookup</button>
                                    <textarea id="Description-editor" name="detail">
                                       <?php echo $data->detail?>
                                    </textarea>
                                </div>               
                            </div>
                            <div class="tab-pane fade" id="pills-en" role="tabpanel" aria-labelledby="pills-en-tab">
                                <div class="form-group">
                                    <label class="label">Description</label><button type="button" class="fc-month-button fc-button fc-state-default" data-toggle="modal" data-target=".bd-example-modal-lg" style="float:right !important;margin:unset;cursor:pointer;">Data Lookup Reference</button>
                                    <textarea id="Description-editor-en" name="detail_en">
                                       <?php echo $data->detail_en ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-set" role="tabpanel" aria-labelledby="pills-set-tab">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label> Jenis Template</label>
                                            <select class="form-control" id="lookup" name="lookup">
                                                <option value="">Sila Pilih</option>
                                                @foreach($lkptemplate as $key=>$lookup)
                                                    @if($lookup->id == $data->fk_lkp_template)
                                                        <option value="{{$lookup->id}}" selected="selected">{{$lookup->type}}</option>
                                                    @else
                                                         <option value="{{$lookup->id}}">{{$lookup->type}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('form.queststatus')</label>
                                            <select class="form-control" id="status" name="status">
                                                <option value="">@lang('form.choose')</option>
                                                    @if($data->status == 1)
                                                        <option value="1" selected="selected">Aktif</option>
                                                        <option value="0">Tidak Aktif</option>
                                                    @else
                                                        <option value="0" selected="selected">Tidak Aktif</option>
                                                        <option value="1" >Aktif</option>
                                                    @endif

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit"> @lang('form.saves') </button>
                        <a class="btn btn-danger" href="/mail/template"> @lang('form.back') </a>
                         {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title h4" id="myLargeModalLabel">Data Lookup Table</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                       <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-xs">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Label</th>
                                            <th>Code</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $j=1;?>
                                    @foreach($datalookup as $key => $code)
                                        <tr>
                                            <td width="1%"><?php echo $j ?></td>
                                            <td>{{$code->label}}</td>
                                            <td>{{$code->code}}</td>
                                        </tr>
                                    <?php $j++;?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!-- liveline-section end -->
        </div>
@endsection
@push('script')
<script>
    // DataTable start
    $('#announce-table').DataTable();
    // DataTable end
</script>

<script>
    $('#Description-editor').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}"
    });
</script>
<script>
    $('#Description-editor-en').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}"
    });
</script>

@endpush