@extends('ui::ablepro.dashboard')

@section('content')
        <div class="row">
            <!-- customar project  start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                         <div class="card-body d-flex align-items-center justify-content-between">
                     <h5 class="mb-0">@lang('homepage.template-lookup')</h5>
                    </div>
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i><a  style="color:white" href="{{URL::to('mail/addtemplatetype')}}">
                                @lang('form.addtemplatetype')</a></button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="announce-table" class="table table-bordered table-striped mb-0">
                                  <thead>
                                    <tr>
                                        <th>@lang('form.no')</th>
                                        <th>@lang('form.type')</th>
                                        <th>@lang('form.status')</th>
                                        <th>@lang('form.action')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                   <?php $i=1;?>
                                   @forelse($data as $key=>$value)
                                    <tr>
                                        <td width="1%"><?php echo $i ?></td>
                                        <td>{{$value->type}}</td>
                                        @if($value->status==0)
                                        <td>@lang('form.notactive')</td>
                                        @else
                                        <td>@lang('form.active')</td>
                                        @endif
                                        <td>
                                         <a href='/mail/edittemplatetype/{{$value->id}}' class="btn btn-info btn-sm"><i class="feather icon-edit"></i>&nbsp;@lang('form.edit') </a>    
                                        </td> 
                                    </tr>
                                    <?php $i++;?>
                                     @empty
                                     <tr><td colspan='4'>Tiada Data</td></tr>
                                     @endforelse
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- customar project  end -->
        </div>
        <!-- [ Main Content ] end -->

@endsection
@push('script')
<script>
    // DataTable start
    $('#announce-table').DataTable();
    // DataTable end
</script>
@endpush