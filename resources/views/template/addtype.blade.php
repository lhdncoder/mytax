@extends('ui::ablepro.dashboard')

@section('content')
      <div class="row">
            <!-- liveline-section start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                     <h5 class="mb-0">@lang('homepage.template-lookup')</h5>
                     <br>
                       {!! SemanticForm::post(route('mail.savetemplatetype'))->attribute('id', 'addtemplatetype') !!}
                       
                        <div class="form-group">
                            <label class="label">Nama Jenis</label>
                            <input type="text" class="form-control" id="type" name="type" placeholder="@lang('form.nametype')">
                        </div>
                        <button class="btn btn-primary" type="submit"> @lang('form.saves') </button>
                        <a class="btn btn-danger" href="/mail/templatetype"> @lang('form.back') </a>
                         {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- liveline-section end -->
        </div>
@endsection
@push('script')
<!-- <script>
    // DataTable start
    $('#announce-table').DataTable();
    // DataTable end
</script>

<script>
    $('#Description-editor').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}"
    });
</script> -->

@endpush