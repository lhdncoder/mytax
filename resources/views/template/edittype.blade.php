@extends('ui::ablepro.dashboard')

@section('content')
      <div class="row">
            <!-- liveline-section start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                     <h5 class="mb-0">@lang('homepage.template-lookup-edit')</h5>
                     <br>
                       {!! SemanticForm::post(route('mail.updatetemplatetype'))->attribute('id', 'addtemplatetype') !!}
                       <input type="hidden" name="id" value="{{$lkptemplate->id}}">
                        <div class="form-group">
                            <label class="label">Nama Jenis</label>
                            <input type="text" class="form-control" id="type" name="type" placeholder="@lang('form.nametype')" value="{{$lkptemplate->type}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>@lang('form.queststatus')</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="">@lang('form.choose')</option>
                                            @if($lkptemplate->status == 1)
                                                <option value="1" selected="selected">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            @else
                                                <option value="0" selected="selected">Tidak Aktif</option>
                                                <option value="1" >Aktif</option>
                                            @endif

                                    </select>
                                </div>
                            </div>

                        </div>
                        <button class="btn btn-primary" type="submit"> @lang('form.saves') </button>
                        <a class="btn btn-danger" href="/mail/template"> @lang('form.back') </a>
                         {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- liveline-section end -->
        </div>
@endsection
@push('script')


@endpush