@extends('ui::ablepro.dashboard')

@section('content')
        <div class="tab-pane active" id="shipping-information">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="mb-2">@lang('homepage.search-form')</h5>
                                <p class="text-muted mb-0">@lang('homepage.search-title')</p>
                            </div>
                            <div class="card-body">
                                <div class="card-deck">
                                
                                    <div class="card">
                                         {!! SemanticForm::post()->attribute('id', 'gene')  !!}
                                        <div class="card-body h-100">
                                            <h6 class="mb-0">@lang('homepage.search-title1')</h6>
                                            <br>
                                            
                                                <div class="form-group text-left">
                                                    <div class="custom-controls-stacked">
                                                        <label class="custom-control custom-radio">
                                                            <input name="idtype" type="radio" class="custom-control-input" required="" value="1">
                                                            <span class="custom-control-label">@lang('homepage.loginsinput1')</span>
                                                        </label>
                                                        <label class="custom-control custom-radio">
                                                            <input name="idtype" type="radio" class="custom-control-input" required="" value="2">
                                                            <span class="custom-control-label">@lang('homepage.loginsinput2')</span>
                                                        </label>
                                                        <label class="custom-control custom-radio">
                                                            <input name="idtype" type="radio" class="custom-control-input" required="" value="3">
                                                            <span class="custom-control-label">@lang('homepage.loginsinput3')</span>
                                                        </label>
                                                        <label class="custom-control custom-radio">
                                                            <input name="idtype" type="radio" class="custom-control-input" required="" value="4">
                                                            <span class="custom-control-label">@lang('homepage.loginsinput4')</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <br>
                                                 <div class="form-group mb-3">
                                                    <input type="text" class="form-control" id="idenno" name="idenno" placeholder="No Pengenalan" required>
                                                </div>
                                                <button type="submit" class="btn btn-success btn-block has-ripple">@lang('homepage.search-button')<span class="ripple ripple-animate" style="height: 244.75px; width: 244.75px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -105.375px; left: -5.875px;"></span></button>
                                        </div>
                                         
                                        
                                        {!! Form::close() !!}
                                    </div>

                        
                               
                                    <div class="card">
                                        {!! SemanticForm::post()->attribute('id', 'gene2')  !!}
                                        <div class="card-body h-100">
                                            <h6 class="mb-0">@lang('homepage.search-title2')</h6>
                                            <br>
                                          
                                                
                                              <div class="form-row">
                                                 
                                                <div class="form-group col-md-12 fill">
                                                  <label for="inputNo">@lang('mobile.label-taxno')</label>
                                                  <input name="refno" type="text" class="form-control" id="inputNo" placeholder="@lang('mobile.label-taxno')" required="">
                                                </div>
                                              </div>
                                              <br>
                                              <br>
                                              <br>
                                              <br>
                                              <br><br>
                                              <button type="submit" class="btn btn-success btn-block has-ripple">@lang('homepage.search-button')<span class="ripple ripple-animate" style="height: 244.75px; width: 244.75px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -105.375px; left: -5.875px;"></span></button>
                                        </div>

                                         
                                        {!! Form::close() !!}
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h5 class="mb-2">@lang('homepage.search-result')</h5>
                               
                            </div>
                            <div class="card-body" id="home">
                               
                            </div>
                            <div id="exampleModalCenter" class="modal" aria-labelledby="exampleModalCenterTitle">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content" style="margin:20px">
                                    <br>
                                        <div class="d-flex justify-content-center" style="margin:20px !important"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>
                                        <div class="swal-title">Please Wait..</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

@endsection
@push('script')
<script>
    $(document).ready(function () {

               
        $("#gene").submit(function (e) {
                        

            e.preventDefault();

            var values = $(this).serialize();
            $.ajax({
                    url: "{{ URL::to('search/iden')}}",
                    type: "post",
                    data: values ,
                           
                    beforeSend: function () 
                    {
                         $("#exampleModalCenter").modal('show');
                          // $('#home').html('');
                          
                    },
                    success: function(data)
                    {       
                          $("#exampleModalCenter").modal('hide');
                          $('#home').html(data);
                         
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                      $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
                        
                      }


                });
                                 

        });

        $("#gene2").submit(function (e) {
                        

            e.preventDefault();

            var values = $(this).serialize();
            $.ajax({
                    url: "{{ URL::to('search/iden2')}}",
                    type: "post",
                    data: values ,
                           
                    beforeSend: function () 
                    {
                         $("#exampleModalCenter").modal('show');
                          // $('#home').html('');
                          
                    },
                    success: function(data)
                    {       
                          $("#exampleModalCenter").modal('hide');
                          $('#home').html(data);
                         
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                      $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
                        
                      }


                });
                                 

        });
    });
</script>

<script type="text/javascript">


$(document).ready(function () {

    $('#comle').DataTable();
    $('#comck').DataTable();
    $('#com').DataTable();


});

</script>

<script type="text/javascript">
  
  function loadpenutup(lejar,types) {

    $("#exampleModalCenter").modal('hide');
    $('.swal-title').html('Please wait...');
    $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/penutup')}}"+"/"+types+"/"+lejar,
                   
            beforeSend: function () 
            {
                 $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
                  
            },
            success: function(data)
            {       
                  $("#exampleModalCenter").modal('hide');
                  $('#lejars').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadcom(id,types) {

    $("#exampleModalCenter").modal('hide');
    $('.swal-title').html('Please wait...');
    $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/comdata')}}"+"/"+id+"/"+types,
                   
            beforeSend: function () 
            {
                  $("#exampleModalCenter").modal();
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#lejars').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadlejar() {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
    
    $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/indexlejar')}}",
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal();
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#lejars').html(data);
                                    
                  $('#comle').DataTable();
                  $('#comck').DataTable();
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadlejarcalendar(year,types) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/calendar')}}"+"/"+year+"/"+types,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal();
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#lejars').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadlejarcurrent(ltype,year,types) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/current')}}"+"/"+year+"/"+types+"/"+ltype,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#lejars').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadresit(type,ref,rno,rtype,ltype) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/resit')}}"+"/"+type+"/"+ref+"/"+rno+"/"+rtype+"/"+ltype,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  $('#div_print_resit').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#div_print_resit').html(data);

                  if(data !== '0'){

                        printresit('div_print_resit');
                  }else{

                    alert('no reciept data');
                  }

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  

  


</script>

<script type="text/javascript">
  $("#exampleModalCenter").modal('hide');
  $('.swal-title').html('Please wait...');
  function loadlejarcurrentcom(ltype,year,types,lid) {

     $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/currentcom')}}"+"/"+year+"/"+types+"/"+ltype+"/"+lid,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
            },
            success: function(data)
            {       
                
                  $("#exampleModalCenter").modal('hide');
                  $('#lejars').html(data);

                  
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
               $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  


</script>

<script type="text/javascript">
  $("#exampleModalCenter").modal('hide');
  $('.swal-title').html('Please wait...');
  function loadlejarcalendarcom(ltype,year,types,lid) {

     $.ajax({

            type: "GET", 
            url: "{{ URL::to('search/calendarcom')}}"+"/"+year+"/"+types+"/"+ltype+"/"+lid,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
            },
            success: function(data)
            {       
                
                  $("#exampleModalCenter").modal('hide');
                  $('#lejars').html(data);

                  
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#exampleModalCenter").modal('hide');
                alert('Opps, Please Try Again \n'+thrownError)
              }


        });




  };  


</script>


<script type="text/javascript">
function printdiv(printpage)
{


// var oldstr = document.body.innerHTML;
// var el = document.getElementById("body1");
// el.classList.remove("on-the-fly-behavior");
// el.classList.add("print-fly");
// el.style.fontSize = "10px !important;background-color:white !important";

document.getElementById(printpage).style.width = "100%";
document.getElementById("tableprint").style.width = "100%";
document.getElementById("tableprint").style.fontSize = "10px";
var headstr = "<html><head><title></title></head><body style='background-color: white;font-size:10px;margin-left:50px;margin-right:50px'>";
var footstr = "</body>";
var newstr = document.all.item(printpage).innerHTML;

var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
winPrint.document.write(headstr+newstr+footstr);
winPrint.document.close();
winPrint.focus();
setTimeout(function()
{
  winPrint.print();
  winPrint.close(); 

}, 1000);

}


</script>

<script type="text/javascript">
function printresit(printpage)
  {

      

        document.getElementById("tableprint").style.width = "100%";
        document.getElementById("tableprint2").style.width = "100%";

        var headstr = "<html><head><title></title></head><body style='background-color:white;margin-left:20px;margin-right:20px'>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;

        var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
        winPrint.document.write(headstr+newstr+footstr);
        winPrint.document.close();
        winPrint.focus();
        setTimeout(function()
        {
          winPrint.print();
          winPrint.close(); 

        }, 1000);


        // document.body.innerHTML = headstr+newstr+footstr;
        // window.print();
        // el.classList.remove("print-flys");
        // el.classList.add("on-the-fly-behavior");
        // document.body.innerHTML = oldstr;
        // return false;
  }
</script>


<script type="text/javascript">
function printsurat(printpage)
  {

        var oldstr = document.body.innerHTML;
        var el = document.getElementById("body1");
        el.classList.remove("on-the-fly-behavior");
        el.classList.add("print-flys");

        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;


        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        el.classList.remove("print-flys");
        el.classList.add("on-the-fly-behavior");
        document.body.innerHTML = oldstr;
        $('#cpsurat').html();
        return false;
  }
</script>


@endpush