 <div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card">
                            <div class="card-body d-flex align-items-center justify-content-between" style="background-color: cornflowerblue;">
                                <h5 class="mb-0" style="color:white">@lang('profile.title-infos')</h5>
                                <!-- <a target="_blank" type="button" class="btn btn-primary btn-sm rounded m-0 float-right" href="https://ekls.hasil.gov.my/ssoprod/ujikemaskini/makkemaskini.php">
                                    <i class="feather icon-edit"></i>

                                </a> -->
                              
                            </div>
                            <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
                                <form>
                                    <div class=" row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-name')</label>
                                        <label class="col-sm-9">
                                            {{$user->name}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset"> @lang('mobile.label-ic')</label>
                                        <label class="col-sm-9">
                                            {{$user->reference_id}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-taxno')</label>
                                        <label class="col-sm-9">
                                            {{$user->tax_no}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-phone')</label>
                                        <label class="col-sm-9">
                                            {{$dataprofile->HomePhoneNo}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-mphone')</label>
                                        <label class="col-sm-9">
                                            {{$dataprofile->HandPhoneNo}}  
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-email')</label>
                                        <label class="col-sm-9">
                                            {{$user->email}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-address')</label>
                                        <label class="col-sm-4">
                                            {{$dataprofile->Address}}
                                        </label>
                                    </div>
                                </form>
                            </div>
                            
                            
                            <div class="card-body border-top pro-det-edit collapse " id="pro-det-edit-2">
                                dalam penyelengaraan
                            </div>
                        </div>
</div>

<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between" style="background-color: cornflowerblue;">
            <h5 class="mb-0" style="color:white">@lang('profile.title-certs')</h5>
          
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="bt-wizard" style="position: sticky;">
                <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">


                @if($dataprofile->CertStatus == 'True')
                    <li class="nav-item">
                        <a class="nav-link active" id="ind-tab" data-toggle="tab" href="#ind" role="tab" aria-controls="ind">
                            @lang('profile.cert2')
                        </a>
                    </li>  
                @endif  
                @if($dataprofile->CertStatusOeF == 'True')
                    <li class="nav-item">
                        <a class="nav-link @if(($user->access == 2) OR ($user->access == 6))active @endif" id="oef-tab" data-toggle="tab" href="#oef" role="tab" aria-controls="oef">
                            @lang('profile.cert1')
                        </a>
                    </li>  
                @endif
                @if($dataprofile->CertStatusPeF == 'True')  
                    <li class="nav-item ">
                        <a class="nav-link @if($user->access == 4) active @endif" id="pef-tab" data-toggle="tab" href="#pef" role="tab" aria-controls="pef">
                            @lang('profile.cert3')
                        </a>
                    </li> 
                @endif   
                </ul>
            </div>
             <div class="tab-content" id="myTabContent">
            @if($dataprofile->CertStatus == 'True')
            <div class="tab-pane fade active show" id="ind" role="tabpanel" aria-labelledby="ind-tab">
                <div class="card" style="background-color: transparent">
                    <div class="card-body">
                        @if(isset($dataprofile->CertStatus))

                            @if($dataprofile->CertStatus == 'True')
                            <div class="card-body">
                                <div class="media-body">
                                    <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                    <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$dataprofile->CertValidity) }}</b></p>

                                </div>
                            </div>
                            @else
                                <div class="card-body">
                                    <form class="text-center">
                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                    </form>
                                </div>
                            @endif
                        @else
                            <div class="card-body">
                                <form class="text-center">
                                    <i class="feather icon-check-circle display-3 text-success"></i>
                                    <h5 class="mt-3">@lang('inbox.empty')</h5>
                                    <!-- <p>@lang('inbox.nodata')</p> -->
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
           @if($dataprofile->CertStatusOeF == 'True')
            <div class="tab-pane fade @if(($user->access == 2) OR ($user->access == 6))active show @endif" id="oef" role="tabpanel" aria-labelledby="oef-tab">
                <div class="card" style="background-color: transparent">
                    <div class="card-body">
                        @if($dataprofile->CertStatusOeF == 'True')
                        <div class="card-body">
                            <div class="media-body">
                                <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$dataprofile->CertValidityOeF) }}</b></p>
                            </div>
                        </div>


                        @else
                            <div class="card-body">
                                <form class="text-center">
                                    <i class="feather icon-check-circle display-3 text-success"></i>
                                    <h5 class="mt-3">@lang('inbox.empty')</h5>
                                    <!-- <p>@lang('inbox.nodata')</p> -->
                                </form>
                            </div>
                        @endif



                    </div>


                </div>
                @if(count($comlist) > 0 )
                <div class="card" style="background-color: transparent">
                    <div class="card-body">
                        <div class="dt-responsive table-responsive" style="background-color: white">
                            <table class="table table-striped table-bordered nowrap" id="com">
                                <thead>
                                    <tr>
                                        <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                        <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                        <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                        <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                        <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('profile.cert-status')</th>
                                        <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col5')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($comlist as $keycom => $com)
                                        <tr>
                                            <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                            <td>{{$com->Jenis_File}}</td>
                                            <td>{{$com->No_Rujukan}}</td>
                                            <td>{{$com->No_Roc}}</td>
                                            <td>{{$com->Status_OeF}}</td>
                                            <td>{{$com->Tarikh_Daftar}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endif
           @if($dataprofile->CertStatusPeF == 'True')  
            <div class="tab-pane fade  @if($user->access == 4) active show @endif" id="pef" role="tabpanel" aria-labelledby="pef-tab">
                <div class="card" style="background-color: transparent">
                    <div class="card-body">
                        @if($dataprofile->CertStatusPeF == 'True')
                        <div class="card-body">
                            <div class="media-body">
                                <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$dataprofile->CertValidityPeF) }}</b></p>
                            </div>
                        </div>
                        @else
                            <div class="card-body">
                                <form class="text-center">
                                    <i class="feather icon-check-circle display-3 text-success"></i>
                                    <h5 class="mt-3">@lang('inbox.empty')</h5>
                                    <!-- <p>@lang('inbox.nodata')</p> -->
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            </div>
        </div>
    </div>
</div>

<br>
<div id="lejars">
<p class="mb-0"><h5>@lang('lejar.title-index')</h5></p>
<br>
<?php $row=0; ?>
@if(!$lejar->isEmpty())

@if($dataprofile->CertStatus == 'True')
 <div class="card-body table-border-style">
    <div class="table-responsive shadow" style="border-radius: 8px">
        <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
            <thead>
                <tr>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bil')</th>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-type') (@if($user->language == 'ms') Individu @else Individual @endif)</th>
                    <th colspan="3" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-sum') (RM) </th>
                </tr>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bal')<sup>1</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-unused')<sup>2</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-lej')<sup>3</sup></th>
                <tr>
            </thead>

            <tbody>
            @foreach($lejar as $key => $data)
                @if($data->income_type == 'SALARY')
                    @if($checksal > 0)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>@lang('lejar.table-income')</td>
                             <td colspan="3" style="color:red">@lang('homepage.lejarerror')</td>
                        </tr>
                    @else
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>@lang('lejar.table-income')</td>
                        <td style="text-align: right">{{number_format($data->BakiCukai,2,'.',',')}}</td>
                        <td style="text-align: right">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</td>
                        <td style="text-align: right"><a href="javascript:loadpenutup('1','SALARY');">{{number_format($data->BakiLejar,2,'.',',')}}</a></td>
                    </tr>
                    @endif

                @else

                    @if(($data->BakiCukai == '0') && ($data->ByrnBelumBolehGuna == '0') && ($data->BakiLejar == '0'))
                     <tr>
                        <td>{{$key+1}}</td>
                        <td>@lang('lejar.table-ckht')</td>
                        <td colspan="3">@lang('lejar.table-record')</td>
                    </tr>
                    @else
                        @if($checkpro > 0)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>@lang('lejar.table-ckht')</td>
                                <td colspan="3" style="color:red">@lang('homepage.lejarerror')</td>
                            </tr>
                        @else
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>@lang('lejar.table-ckht')</td>
                            <td style="text-align: right">{{number_format($data->BakiCukai,2,'.',',')}}</td>
                            <td style="text-align: right">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</td>
                             <td style="text-align: right"><a href="javascript:loadpenutup('1','PROPERTIES');">{{number_format($data->BakiLejar,2,'.',',')}}</a></td>
                        </tr>
                        @endif
                    @endif


                @endif
                
            @endforeach
               
            </tbody>
        </table>
    </div>
</div>
@endif
@endif
<br>
@if($dataprofile->CertStatusOeF == 'True')
<div class="card-body table-border-style">
    <div class="table-responsive shadow" style="border-radius: 8px">
        <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
            <thead>
                
                <tr>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bil')</th>
                    <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-type') (@if($user->language == 'ms') Syarikat @else Company @endif)</th>
                    <th colspan="3" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-sum') (RM) </th>
                </tr>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bal')<sup>1</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-unused')<sup>2</sup></th>
                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-lej')<sup>3</sup></th>
                <tr>
            </thead>
            <tbody>
                @if(count($comlist) > 0 )
                    <tr>
                        <td>1</td>
                        <td>@lang('lejar.table-income')</td>
                        <td colspan="3">
                            <span style="cursor:pointer;text-decoration: underline" data-toggle="collapse" data-target=".pro-wrk-edit" aria-expanded="false" aria-controls="pro-wrk-edit-1 pro-wrk-edit-2"><b>@lang('lejar.table-com')</b></span>
                        </td>
                    </tr>
                    <tr class="pro-wrk-edit collapse" id="pro-wrk-edit-2">
                        <td colspan="5" style="padding:20px !important">
                        <button type="button" class="btn btn-primary btn-sm rounded m-0 float-right" data-toggle="collapse" data-target=".pro-wrk-edit" aria-expanded="false" aria-controls="pro-wrk-edit-1 pro-wrk-edit-2">
                            <i class="feather icon-x-circle"></i>
                        </button>
                            <div class="table-responsive"   style="background-color: white;">
                                 <table id="comle" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($comlist as $keycom => $com)
                                            <tr>
                                                <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                <td>{{$com->Jenis_File}}</td>
                                                <td><a href="javascript:loadcom({{$com->id}},'SALARY');">{{$com->No_Rujukan}}</a> </td>
                                                <td>{{$com->No_Roc}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>@lang('lejar.table-ckht')</td>
                        <td colspan="3">
                            <span style="cursor:pointer;text-decoration: underline" data-toggle="collapse" data-target=".pro-wrk-editc" aria-expanded="false" aria-controls="pro-wrk-edit-1c pro-wrk-edit-2c"><b>@lang('lejar.table-com')</b></span>
                        </td>
                    </tr>
                    <tr class="pro-wrk-editc collapse" id="pro-wrk-edit-2c">
                        <td colspan="5" style="padding:20px !important">
                        <button type="button" class="btn btn-primary btn-sm rounded m-0 float-right" data-toggle="collapse" data-target=".pro-wrk-editc" aria-expanded="false" aria-controls="pro-wrk-edit-1c pro-wrk-edit-2c">
                            <i class="feather icon-x-circle"></i>
                        </button>
                            <div class="table-responsive"   style="background-color: white;">
                                 <table id="comck" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($comlist as $keycom => $com)
                                            <tr>
                                                <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                <td>{{$com->Jenis_File}}</td>
                                                <td><a href="javascript:loadcom({{$com->id}},'PROPERTIES');">{{$com->No_Rujukan}}</a> </td>
                                                <td>{{$com->No_Roc}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    


                @else
                <tr>
                    <td>1</td>
                    <td>@lang('lejar.table-income')</td>
                    <td colspan="3">@lang('lejar.table-nocom')</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>@lang('lejar.table-ckht')</td>
                    <td colspan="3">@lang('lejar.table-nocom')</td>
                </tr>
                @endif
               
            </tbody>
        </table>
    </div>
</div>
@endif

<div style="font-size:12px;margin-left:20px">
    <b>@lang('lejar.note'):</b><br>
    @lang('lejar.note-1')<br>
    @lang('lejar.note-2')<br>
    @lang('lejar.note-3')
</div>

</div>


