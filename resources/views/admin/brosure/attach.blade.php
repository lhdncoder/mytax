@if($attall)
    @foreach($attall as $key =>$att)
        <div class="input-group mb-3">
            <input type="text" class="form-control" value="{{$att->label}}">
            <div class="input-group-append input-group-sm">
                <a href="{{$att->full_path}}{{$att->file_name}}" class="btn btn-primary has-ripple" type="button">View<span class="ripple ripple-animate" style=""></span>
                </a>

                <button onclick="delattach({{$att->id}},{{$att->fk_tax_brosure}});" class="btn btn-primary has-ripple" type="button">Delete<span class="ripple ripple-animate" style=""></span></button>
            </div>
        </div>
    @endforeach
@else
   
@endif