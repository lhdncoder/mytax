@extends('ui::ablepro.dashboard')

@section('content')
        <div class="row">
            <!-- customar project  start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body d-flex align-items-center justify-content-between">
                     <h5 class="mb-0">@lang('form.titlebrosure')</h5>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i><a  style="color:white" href="{{URL::to('admin/addbrosure')}}">
                                @lang('form.addbrosure')</a></button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="announce-table" class="table table-bordered table-striped mb-0">
                                 <thead>
                                   <tr>
                                        <th>@lang('form.no')</th>
                                        <th>@lang('form.brosurename')</th>
                                        <th>@lang('form.files')</th>
                                        <th>@lang('form.servicestatus')</th>
                                        <th>@lang('form.action')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                   <?php $i=1;?>
                                    @forelse($list as $key=>$value)
                                    <?php 
                        
                                            $modalname="#modals".$value->id;
                                            $modalnames="modals".$value->id;
                                            $form="form".$modalnames;
                                            $forms="#form".$modalnames;

                                            if($value->status == 1){

                                                $status = 'Aktif';
                                                $rad1 = 'checked';
                                                $rad2 = '';

                                            }else{

                                                $status = 'Tidak Aktif';
                                                $rad1 = '';
                                                $rad2 = 'checked';
                                            }

                                            $files = $value->files;

                                    ?>
                                    <tr>
                                        <td width="1%"><?php echo $i ?></td>
                                        @if($user->language == 'en')
                                              <td>{{$value->name_en}}</td>
                                        @else
                                            <td>{{$value->name_bm}}</td>
                                        @endif
                                      
                                        <td> 
                                        @if($value->files)
                                            {{ $value->files->count() }}
                                        @else
                                            0
                                        @endif
                                        </td>
                                        @if($value->status==0)
                                        <td>@lang('form.notactive')</td>
                                        @else
                                        <td>@lang('form.active')</td>
                                        @endif
                                        <td>     
                                             <button type="button" rel="tooltip" class="btn btn-info btn-sm" data-toggle="modal" data-target=".{{$modalnames}}"><i class="feather icon-edit"></i>@lang('form.edit')
                                            </button>
             
                                            <div class="modal fade bd-example-modal-lg {{$modalnames}}" tabindex="-1" role="dialog" aria-labelledby="{{$modalnames}}" aria-hidden="true">
                                              <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Kemaskini {{$value->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    {!! SemanticForm::post(route('admin.updatebrosure'))->attribute('class', 'ui form')->attribute('id', 'gene')->multipart()   !!}
                                                    <div class="panel-body">
                                                        <input type="hidden" class="form-control" placeholder="" value="{{$value->id}}" name="id">
                                                        
                                                        
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group row">
                                                                    <label for="name_bm" class="col-sm-3 col-form-label">Nama Risalah (BM)</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" name="name_bm" id="name" value="{{$value->name_bm}}" class="form-control" required="" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="name_en" class="col-sm-3 col-form-label">Brochure Name (EN)</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" name="name_en" id="name" value="{{$value->name_en}}" class="form-control" required="" />
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label>@lang('form.queststatus')</label>
                                                                    <select class="form-control js-example-data-array" id="status" name="status">
                                                                        <option value="" {{ data_get($value,'status')==""  ? "selected" : ''}}>@lang('form.choose')</option>
                                                                        <option value="1" {{ data_get($value,'status')=="1" ? "selected" : ''}}>@lang('form.active')</option>
                                                                        <option value="0" {{ data_get($value,'status')=="0" ? "selected" : ''}}>@lang('form.notactive')</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <br>
                                                        <span class="text-left" style="float:left">@lang('form.addattach')</span>
                                                        <br>
                                                        <br>
                                                            <div class="thu text-left" id="thu{{$value->id}}" >
                                                            @if($files)
                                                                @foreach($files as $key =>$att)
                                                                    <div class="input-group mb-3">
                                                                        @if($user->language == 'en')
                                                                             <input type="text" class="form-control" value="{{$att->label_en}}">
                                                                        @else
                                                                            <input type="text" class="form-control" value="{{$att->label_bm}}">
                                                                        @endif
                                                                        
                                                                        <div class="input-group-append input-group-sm">
                                                                            <a href="{{$att->full_path}}{{$att->file_name}}" class="btn btn-primary has-ripple" type="button">View<span class="ripple ripple-animate" style=""></span>
                                                                            </a>

                                                                            <button onclick="delattach({{$att->id}},{{$att->fk_tax_brosure}});" class="btn btn-primary has-ripple" type="button">Delete<span class="ripple ripple-animate" style=""></span></button>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                               
                                                            @endif
                                                            </div>
                                                        <br>
                                                         <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <button type="button" class="btn btn-success btn-sm btn-round has-ripple" id="add_lampiran()_1" onclick="addlampiran({{$value->id}})"><i class="feather icon-plus"></i>@lang('form.addattach')</button>
                                                                    <br>
                                                                    <br>
                                                                    <div class="field" id="lampiran{{$value->id}}">
                                                             
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>

                                                                                          
                                                    </div>
                                                        
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                  </div>
                                                  {!! Form::close() !!}
                                                </div>
                                              </div>
                                            </div>
                                        </td> 
                                    </tr>
                                    <?php $i++;?>
                                     @empty
                                     <tr><td colspan='6'>Tiada Data</td></tr>
                                     @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
@push('script')
<script>
    // DataTable start
    $('#announce-table').DataTable();
    // DataTable end
</script>

<script type="text/javascript">

    
    var index = [];
    index.push(2);
    index.push(3);

    var name1 = "lampiran[]";
    var name2 = "lampiran_name_bm[]";
    var name3 = "lampiran_name_en[]";

    function addlampiran(ids) 
    {
  
        var div = document.createElement('div');
        var id = getID(); 
        div.setAttribute("id","Divs_"+id);
        
        div.innerHTML = '<div class="form-group row"><label for="namebm" class="col-sm-3 col-form-label">Nama Fail (BM)</label><div class="col-sm-9"><input type="text" class="form-control" name="' + name2  + ' id="namebm" placeholder="Nama Fail (BM)"></div></div><div class="form-group row"><label for="nameen" class="col-sm-3 col-form-label">File Name (EN)</label><div class="col-sm-9"><input type="text" class="form-control" name="' + name3  + ' id="nameen" placeholder="File Name (EN)"></div></div><div class="form-group row"><label for="files" class="col-sm-3 col-form-label">File</label><div class="col-sm-9"><input type="file" required="" placeholder="Browse File" name="' + name1 + '"/>' + ' <input id="files" type="button" id="rem_lampiran()_' + id + '" onclick="remlampiran('+id+')" value="-" /></div></div><hr>';


        document.getElementById('lampiran'+ids).appendChild(div);
    };
   
    function remlampiran(id) 
    {
        try
        {
            var element = document.getElementById("Divs_"+id)
            element.parentNode.removeChild(element);
            index[id] = -1;

        }catch(err)
        {
            alert("id: Divs_"+id)
            alert(err)
        }
    } ;

    function getID()
    {
        var emptyIndex = index.indexOf(-1);
        if (emptyIndex != -1)
        {
            index[emptyIndex] = emptyIndex
            return emptyIndex
        }else
        {
           emptyIndex = index.length
           index.push(emptyIndex)
           return emptyIndex
        }
   };
   


    
</script>

<script type="text/javascript">
    
    function delattach(aid,pid)
    {


        $.ajax({ 
            
            type: "GET", 
            url: "{{ URL::to('admin/delattach')}}"+"/"+aid+"/"+pid,
           success: function(html)
            { 
                $('#thu'+pid).html( html );
            }
    });
    }
</script>

@endpush