@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.addbrosure')</h5> 
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
               <div class="card-body">
                {!! SemanticForm::post(route('admin.savebrosure'))->attribute('id', 'savebrosure')->multipart() !!}
                    
                     <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="name_bm" class="col-sm-2 col-form-label">Nama Risalah (BM)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name_bm" id="name" value="" class="form-control" required="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name_en" class="col-sm-2 col-form-label">Brochure Name (EN)</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name_en" id="name" value="" class="form-control" required="" />
                                </div>
                            </div>

                        </div>
                        
                    </div>
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.queststatus')</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="">@lang('form.choose')</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <br>
                    <hr>
                    <br>
                     <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="button" class="btn btn-success btn-sm btn-round has-ripple" id="add_lampiran()_1" onclick="addlampiran()"><i class="feather icon-plus"></i>@lang('form.addattach')</button>
                                <br>
                                <br>
                                <div class="field" id="lampiran">
                         
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <button type="button" class='btn btn-primary has-ripple' onclick="history.back();">@lang('form.back')</button>
                    <button class="btn btn-primary has-ripple" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
               {!! Form::close() !!}
               </div>
            </div>
           </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script type="text/javascript">

    
    var index = [];
    index.push(2);
    index.push(3);

    var name1 = "lampiran[]";
    var name2 = "lampiran_name_bm[]";
    var name3 = "lampiran_name_en[]";

    function addlampiran() 
    {
  
        var div = document.createElement('div');
        var id = getID(); 
        div.setAttribute("id","Divs_"+id);
        div.innerHTML = '<div class="form-group row"><label for="namebm" class="col-sm-3 col-form-label">Nama Fail (BM)</label><div class="col-sm-9"><input type="text" class="form-control" name="' + name2  + ' id="namebm" placeholder="Nama Fail (BM)"></div></div><div class="form-group row"><label for="nameen" class="col-sm-3 col-form-label">File Name (EN)</label><div class="col-sm-9"><input type="text" class="form-control" name="' + name3  + ' id="nameen" placeholder="File Name (EN)"></div></div><div class="form-group row"><label for="files" class="col-sm-3 col-form-label">File</label><div class="col-sm-9"><input type="file" required="" placeholder="Browse File" name="' + name1 + '"/>' + ' <input id="files" type="button" id="rem_lampiran()_' + id + '" onclick="remlampiran('+id+')" value="-" /></div></div><hr>';
        document.getElementById('lampiran').appendChild(div);
    };
   
    function remlampiran(id) 
    {
        try
        {
            var element = document.getElementById("Divs_"+id)
            element.parentNode.removeChild(element);
            index[id] = -1;

        }catch(err)
        {
            alert("id: Divs_"+id)
            alert(err)
        }
    } ;

    function getID()
    {
        var emptyIndex = index.indexOf(-1);
        if (emptyIndex != -1)
        {
            index[emptyIndex] = emptyIndex
            return emptyIndex
        }else
        {
           emptyIndex = index.length
           index.push(emptyIndex)
           return emptyIndex
        }
   };
   


    
</script>
@endpush