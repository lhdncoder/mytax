@extends('ui::ablepro.dashboard')

@section('content')
<style type="text/css">
    /*
*
* ==========================================
* CUSTOM UTIL CLASSES
* ==========================================
*
*/
#upload {
    opacity: 0;
}

#upload-label {
    position: absolute;
    top: 50%;
    left: 1rem;
    transform: translateY(-50%);
}

.image-area {
    border: 2px dashed rgba(255, 255, 255, 0.7);
    padding: 1rem;
    position: relative;
}

.image-area::before {
    content: 'Uploaded image result';
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 0.8rem;
    z-index: 1;
}

.image-area img {
    z-index: 2;
    position: relative;
}
</style>
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.addbanner')</h5> 
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
               <div class="card-body">
                 {!! SemanticForm::post(route('admin.savebanner'))->attribute('id', 'addbanner')->multipart() !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.bannername') (BM)</label>
                                <input type="text" class="form-control" id="title_bm" name="title_bm" placeholder="@lang('form.bannername') (BM)" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.bannername') (EN)</label>
                                <input type="text" class="form-control" id="title_en" name="title_en" placeholder="@lang('form.bannername') (EN)" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('form.start_date')*</label>
                                <input type="text" name="start_date" id="start_date" value="" class="form-control" />
                            </div>
                        </div>
                         <div class="col-sm-6">
                            <div class="form-group">
                                <label>@lang('form.end_date')*</label>
                                <input type="text" name="end_date" id="end_date" value="" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.order')</label>
                                <input type="number" class="form-control" id="order" name="order" value="1" placeholder="@lang('form.order')">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.queststatus')</label>
                                <select class="form-control" id="status" name="status" required="">
                                    <option value="">@lang('form.choose')</option>
                                    <option value="1">@lang('form.active')</option>
                                    <option value="0">@lang('form.notactive')</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <br><hr><br>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Banner Image (1,030px × 300px)</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="upload" name="img" required >
                            <label class="custom-file-label" id="upload-label" for="upload">Choose file</label>
                        </div>
                    </div>
                    Preview : 
                     <div class="image-area mt-4 mb-4 shadow"><img id="imageResult" src="#" alt="" class="" width="100%"></div>
                    
                   
                    <button type="button" class='btn btn-primary has-ripple' onclick="history.back();">@lang('form.back')</button>
                    <button class="btn btn-primary has-ripple" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
                {!! Form::close() !!}
               </div>
            </div>
           </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function(e){

/* Handle any form's submit event. */
    $("addbanner").submit(function(e){

        e.preventDefault();                 /* Stop the form from submitting immediately. */
        var continueInvoke = true;          /* Variable used to avoid $(this) scope confusion with .each() function. */

        /* Loop through each form element that has the required="" attribute. */
        $("form input[required]").each(function(){

            /* If the element has no value. */
            if($(this).val() == ""){
                continueInvoke = false;     /* Set the variable to false, to indicate that the form should not be submited. */
            }

        });

        /* Read the variable. Detect any items with no value. */
        if(continueInvoke == true){
            $(this).submit();               /* Submit the form. */
        }

    });

});
</script>
<script type="text/javascript">
    
     $(function() {
      $('input[name="start_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        locale: {
        format: 'DD-MM-YYYY'
        }
       
      });
    });

      $(function() {
      $('input[name="end_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        locale: {
            format: 'DD-MM-YYYY'
        }
       
      });
    });
</script>

<script type="text/javascript">
    /*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
});

/*  ==========================================
    SHOW UPLOADED IMAGE NAME
* ========================================== */
var input = document.getElementById( 'upload' );
var infoArea = document.getElementById( 'upload-label' );

input.addEventListener( 'change', showFileName );
function showFileName( event ) {
  var input = event.srcElement;
  var fileName = input.files[0].name;
  infoArea.textContent = 'File name: ' + fileName;
}
</script>


@endpush