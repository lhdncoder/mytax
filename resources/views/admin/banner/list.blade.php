@extends('ui::ablepro.dashboard')

@section('content')
        <div class="row">
        @if(!$banner->isEmpty())

        <?php $count = 1;?>
            <div class="col-xl-12">
                <div class="card h-100">
                    <div class="card-body home " id="home">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            
                            <div class="carousel-inner text-center" style="height: 300px">
                             @foreach($banner as $key => $value)
                                <div class="carousel-item @if($count == 1) active @endif">
                                    <img class="shadow" src="/storage/banner/{{$value->banner_img}}" alt="First slide" style="width: 100%;height:300px;object-fit: contain;">
                                </div>
                             <?php $count = $count+1 ;?>
                             @endforeach
                               
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" class="shadow" style="width:5%"><span class="" aria-hidden="true" style="color:grey;font-size: 30px;font-weight: 800"> <i class="fas fa-chevron-circle-left"></i> </span><span class="sr-only">Previous</span></a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="width:5%"><span class="" aria-hidden="true" style="color:grey;font-size: 30px;font-weight: 800"> <i class="fas fa-chevron-circle-right "></i> </span><span class="sr-only">Next</span></a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body d-flex align-items-center justify-content-between">
                     <h5 class="mb-0">@lang('form.titlebanner')</h5>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i><a  style="color:white" href="{{URL::to('admin/addbanner')}}">
                                @lang('form.addbanner')</a></button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="announce-table" class="table table-bordered table-striped mb-0">
                                 <thead>
                                   <tr>
                                        <th>@lang('form.no')</th>
                                        <th>@lang('form.bannername')</th>
                                        <th>@lang('form.start_date')</th>
                                        <th>@lang('form.end_date')</th>
                                        <th>@lang('form.order')</th>
                                        <th>@lang('form.announcestatus')</th>
                                        <th>@lang('form.action')</th>
                                    </tr>
                                </thead>
                               <tbody>
                                     <?php $i=1;?>
                                     @forelse($listbanner as $key => $value)
                                      <tr>
                                         <td><?php echo $i ?></td>
                                         @if($user->language == 'en')
                                              <td>{{$value->title_en}}</td>
                                        @else
                                        <td>{{$value->title_bm}}</td>
                                        @endif
                                         
                                         <td>{{date('d-m-Y', strtotime($value->start_date))}}</td>
                                         <td>{{date('d-m-Y', strtotime($value->end_date))}}</td>
                                          <td>{{$value->order}}</td>
                                            @if($value->status==0)
                                            <td>@lang('form.notactive')</td>
                                            @else
                                            <td>@lang('form.active')</td>
                                            @endif
                                         <td>   
                                        <a href='/admin/banner/{{$value->id}}' class="btn btn-info btn-sm"><i class="feather icon-edit"></i>&nbsp;@lang('form.edit') </a>
                                        </td> 
                                     </tr>
                                     <?php $i++;?>
                                     @empty
                                     <tr><td colspan='6'>Tiada Data</td></tr>
                                     @endforelse
                                 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- customar project  end -->
        </div>
        <!-- [ Main Content ] end -->

@endsection
@push('script')
<script>
    // DataTable start
    $('#announce-table').DataTable();
    // DataTable end
</script>
@endpush