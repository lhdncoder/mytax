@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.addannounce')</h5> 
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
               <div class="card-body">
                {!! SemanticForm::post(route('admin.svannounce'))->attribute('id', 'addannounce') !!}
                 <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab" role="tablist">
                    <li class="nav-item" style="border: 1px solid blue;">
                        <a class="nav-link has-ripple active" id="pills-bm-tab" data-toggle="pill" href="#pills-bm" role="tab" aria-controls="pills-bm" aria-selected="true">Bahasa Melayu<span class="ripple ripple-animate" style="height: 71.65px; width: 71.65px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -13.725px; left: -7.825px;"></span></a>
                    </li>
                    <li class="nav-item" style="border: 1px solid blue;">
                        <a class="nav-link has-ripple" id="pills-en-tab" data-toggle="pill" href="#pills-en" role="tab" aria-controls="pills-en" aria-selected="false">English<span class="ripple ripple-animate" style="height: 74.2667px; width: 74.2667px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -19.0333px; left: 3.21666px;"></span></a>
                    </li>
                    <li class="nav-item" style="border: 1px solid blue;">
                        <a class="nav-link has-ripple" id="pills-set-tab" data-toggle="pill" href="#pills-set" role="tab" aria-controls="pills-set" aria-selected="false">Setting<span class="ripple ripple-animate" style="height: 74.2667px; width: 74.2667px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -19.0333px; left: 3.21666px;"></span></a>
                    </li>
                    
                </ul>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="pills-bm" role="tabpanel" aria-labelledby="pills-bm-tab">
                    <br>
                        <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Tajuk Pengumuman *</label>
                                <input type="text" class="form-control" id="announce_bm" name="announce_bm" placeholder="Tajuk Pengumuman" required="required">
                            </div>
                            <div class="form-group">
                            <label class="label">Isi Kandungan</label>
                            <textarea id="Konten-editor" name="body_bm">
                               
                            </textarea>
                        </div>
                        </div>
                        
                    </div>
                    </div>
                    <div class="tab-pane fade" id="pills-en" role="tabpanel" aria-labelledby="pills-en-tab">
                    <br>
                        <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Announcement Title*</label>
                                <input type="text" class="form-control" id="announce_en" name="announce_en" placeholder="Announcement Title">
                            </div>
                            <div class="form-group">
                            <label class="label">Content</label>
                            <textarea id="content-editor" name="body_en">
                               
                            </textarea>
                        </div>
                        </div>
                        
                    </div>
                    </div>
                    <div class="tab-pane fade" id="pills-set" role="tabpanel" aria-labelledby="pills-set-tab">
                    <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>@lang('form.start_date')*</label>
                                    <input type="text" name="start_date" id="start_date" value="" class="form-control" />
                                </div>
                            </div>
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <label>@lang('form.end_date')*</label>
                                    <input type="text" name="end_date" id="end_date" value="" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>@lang('form.queststatus')*</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="">@lang('form.choose')</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <button type="button" class='btn btn-primary has-ripple' onclick="history.back();">@lang('form.back')</button>
                    <button class="btn btn-primary has-ripple" id="hantar" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
               {!! Form::close() !!}
               </div>
            </div>
           </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
$(document).ready(function() {

     $('#hantar').hide();

    $(document).on('input', "#announce_bm", function () {
    var dInput = this.value;
    var announce_en=document.getElementById('announce_en').value;
    var status=document.getElementById('status').value;

    //console.log(announce_en);
    
    if(dInput=='' || announce_en=='' || status==''){
        $('#hantar').hide(); 
    }else{
        $('#hantar').show(); 

    }
   
    });

    $(document).on('input', "#announce_en", function () {
    var dInput = this.value;
    var announce_bm=document.getElementById('announce_bm').value;
    var status=document.getElementById('status').value;

    //console.log(announce_en);
    
    if(dInput=='' || announce_bm=='' || status==''){
        $('#hantar').hide(); 
    }else{
        $('#hantar').show(); 

    }
   
    });

    $('#status').change(function() {
    var dInput = this.value;
    var announce_bm=document.getElementById('announce_bm').value;
    var announce_en=document.getElementById('announce_en').value;
    // $(this).val() will work here

     if(dInput=='' || announce_en=='' || announce_bm==''){
        $('#hantar').hide(); 
    }else{
        $('#hantar').show(); 

    }
   
});

    $(function() {
      $('input[name="start_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        locale: {
        format: 'DD-MM-YYYY'
        }
       
      });
    });

      $(function() {
      $('input[name="end_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        locale: {
            format: 'DD-MM-YYYY'
        }
       
      });
    });

           $('#addannounce').validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'announce_bm': {
                    required: true,
                },
                 'announce_en': {
                    required: true,
                },
                  'status': {
                    required: true,
                },

            },

            // Errors //

            errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function(element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });

});
</script>
<script>
    $('#content-editor').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }

    });
</script>
<script>
    $('#Konten-editor').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }
    });
</script>

@endpush