@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">Api Report</h5> 
        </div>
        
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
           
            <br>
            <div class="table-responsive">
            <table class="table">
            <thead>
               <tr>
                <th>Api Url</th>
                <th>Error Log</th>
                <th>Date</th>
                </tr>
            </thead>
             <tbody>                        
                 @forelse($data as $key =>$list)
                 <tr>
                    <td>{{$list->api_url}}</td>
                    <td>{{$list->api_error}}</td>
                    <td>{{$list->date}}</td>
                 </tr>
                 @empty
                 @endforelse

            </tbody>
            </table>

        </div>

        </div>
    </div>
</div>

@endsection
@push('script')

@endpush