@extends('ui::ablepro.dashboard')

@section('content')
<?php use App\Models\Mngfeedback;?>
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.reportapp')</h5> 
        </div>
        
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            {!! SemanticForm::post(route('admin.reportfeedback'))->attribute('id', 'editannounce') !!}
                <div class="row">
                    <div class="col-sm-12">
                       <div class="card-body">
                         <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('form.start_date')</label>
                                        <input type="text" name="start_date" id="start_date" value="{{$datefrom}}" class="form-control" />
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('form.end_date')</label>
                                        <input type="text" name="end_date" id="end_date" value="{{$dateto}}" class="form-control" />
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    <button class="btn btn-success btn-sm btn-round has-ripple" id="gen" name="jana" value="2" type="submit">Cari</button>
                </div>
                <br>
                <br>
                  <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-xs-right"> 
                     <div class="form-group">
                       <div class="col-sm-offset-12 col-sm-12 text-right">
                         <a href="{!! URL::to('admin/exportfeedback/1/'.$datefrom.'/'.$dateto) !!}" class="btn btn-sm btn-round has-ripple btn-info" target="
                        _blank">Eksport ke Pdf</a>
                         <a href="{!! URL::to('admin/exportfeedback/2/'.$datefrom.'/'.$dateto) !!}" class="btn btn-sm btn-round has-ripples btn-warning" target="_blank">Eksport ke Excel</a>                      
                      </div>
                    </div>     
                  </div>
              </div> 
            {!! Form::close() !!}
            <br>
            <div class="table-responsive">
              <table class="table">
            <thead>
               <tr>
                <th>@lang('form.bil')</th>
                <th>@lang('form.soalan')</th>
                <th>@lang('form.jawapan')</th>
                <th>@lang('form.jumlah')</th>
                <th>@lang('form.precent')</th>
                </tr>
            </thead>
             <tbody>   
                <?php $i=1; ?>                     
                 @forelse($arr as $key=>$value)
                 <tr>
                  <td><?php echo $i ?></td>
                  @if($user->language == 'en')
                  <td>{{$value['quest_en']}}</td>
                  @else
                   <td>{{$value['quest_bm']}}</td>
                  @endif
                  <td>
                      <table>
                        @foreach($value['detail_answ'] as $keys => $list_answ)
                        <tr>
                          @if($user->language == 'en')
                              <td>
                                 {{$list_answ->answer_en}} 
                              </td>
                          @else
                              <td>
                                  {{$list_answ->answer_bm}}
                              </td>
                          @endif
                            
                          </tr>
                          @endforeach 
                      </table>
                  </td>
                  <td>
                      <table>
                        @foreach($value['detail_answ'] as $keys => $list_answ)

                        <?php 

                          $countanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS bil_answer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                    ->where('fk_lkp_answer_feedback',$list_answ->id)
                                                     ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                     ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();

                       
                        ?>

                        
                        <tr>
                         <td>
                           {{$countanswer->bil_answer}}
                         </td>
                            
                          </tr>
                          @endforeach 
                      </table>
                  </td>
                  <td>
                      <table>
                        @foreach($value['detail_answ'] as $keys => $list_answ)

                          <?php 

                          $countanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS bil_answer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                    ->where('fk_lkp_answer_feedback',$list_answ->id)
                                                    ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                    ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();


                           $allanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS allanswer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                     ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                     ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();

                            if(data_get($allanswer,'allanswer')==0){
                              $peratus=0;

                            }else{
                              $peratus=(data_get($countanswer,'bil_answer')/data_get($allanswer,'allanswer'))*100;

                            }
                            
                       
                        ?>
                        <tr>
                         <td>{{$peratus}}</td>
                            
                          </tr>
                          @endforeach 
                      </table>
                  </td>
                 </tr>
                 <?php $i++;?>
                 @empty
                   <tr><td colspan='12'>Tiada Data</td></tr>
                 @endforelse

            </tbody>
              </table>
           </div>

        </div>
    </div>
</div>

@endsection
@push('script')
<script>
$(document).ready(function() {

    $(function() {
      $('input[name="start_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
        format: 'DD-MM-YYYY'
        }
       
      });
    });

      $(function() {
      $('input[name="end_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
       
      });
    });



});
</script>
@endpush