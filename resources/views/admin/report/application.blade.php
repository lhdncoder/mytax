@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.reportapp')</h5> 
        </div>
        
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            {!! SemanticForm::post(route('admin.reportapp'))->attribute('id', 'editannounce') !!}
                <div class="row">
                    <div class="col-sm-12">
                       <div class="card-body">
                         <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('form.start_date')</label>
                                        <input type="text" name="start_date" id="start_date" value="{{$starts}}" class="form-control" />
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('form.end_date')</label>
                                        <input type="text" name="end_date" id="end_date" value="{{$ends}}" class="form-control" />
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    <button class="btn btn-success btn-sm btn-round has-ripple" id="gen" name="jana" value="2" type="submit">Cari</button>
                </div>
            {!! Form::close() !!}
            <br>
            <div class="table-responsive">
            <table class="table">
            <thead>
               <tr>
                <th>@lang('form.reportmodule')</th>
                <th>@lang('form.reportbil')</th>
                </tr>
            </thead>
             <tbody>                        
                 @forelse($data as $key =>$list)
                 <tr>
                    @if($user->language == 'en')
                        <td>{{$list->names->service_en}}</td>
                    @else
                        <td>{{$list->names->service_bm}}</td>
                    @endif
                    <td>{{$list->sum}}</td>
                 </tr>
                 @empty
                 @endforelse

            </tbody>
            </table>

        </div>

        </div>
    </div>
</div>

@endsection
@push('script')
<script>
$(document).ready(function() {

    $(function() {
      $('input[name="start_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
        format: 'DD-MM-YYYY'
        }
       
      });
    });

      $(function() {
      $('input[name="end_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
       
      });
    });



});
</script>
@endpush