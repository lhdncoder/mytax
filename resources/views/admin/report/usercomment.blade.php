@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">Senarai Komen Maklumbalas</h5> 
        </div>
        
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            {!! SemanticForm::post(route('admin.reportcomment'))->attribute('id', 'editannounce') !!}
                <div class="row">
                    <div class="col-sm-12">
                       <div class="card-body">
                         <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('form.start_date')</label>
                                        <input type="text" name="start_date" id="start_date" value="{{$datefrom}}" class="form-control" />
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>@lang('form.end_date')</label>
                                        <input type="text" name="end_date" id="end_date" value="{{$dateto}}" class="form-control" />
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                <div class="col-sm-12 text-right">
                    <button class="btn btn-success btn-sm btn-round has-ripple" id="gen" name="jana" value="2" type="submit">Cari</button>
                </div>
                 <br>
                <br>
                  <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-xs-right"> 
                     <div class="form-group">
                       <div class="col-sm-offset-12 col-sm-12 text-right">
                         <a href="{!! URL::to('admin/exportkomen/1/'.$datefrom.'/'.$dateto) !!}" class="btn btn-sm btn-round has-ripple btn-info" target="
                        _blank">Eksport ke Pdf</a>
                         <a href="{!! URL::to('admin/exportkomen/2/'.$datefrom.'/'.$dateto) !!}" class="btn btn-sm btn-round has-ripples btn-warning" target="_blank">Eksport ke Excel</a>                      
                      </div>
                    </div>     
                  </div>
              </div> 
            {!! Form::close() !!}
            <br>
            <div class="table-responsive">
            <table class="table">
            <thead>
               <tr>
                <th>@lang('form.bil')</th>
                <th>@lang('form.tarikhcomment')</th>
                <th>@lang('form.comment')</th>
                </tr>
            </thead>
             <tbody>
             <?php $i=1; ?>                         
                 @forelse($data as $key =>$value)
                 <tr>
                  <td><?php echo $i ?></td>
                  <td>{{date("d-m-Y H:i:s", strtotime($value->date_feedback))}}</td>
                   <td>{{$value->comment}}</td>
                 </tr>
                 <?php $i++;?>
                @empty
                   <tr><td colspan='12'>Tiada Data</td></tr>
                 @endforelse

            </tbody>
            </table>

        </div>

        </div>
    </div>
</div>

@endsection
@push('script')
<script>
$(document).ready(function() {

    $(function() {
      $('input[name="start_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
        format: 'DD-MM-YYYY'
        }
       
      });
    });

      $(function() {
      $('input[name="end_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
       
      });
    });



});
</script>
@endpush