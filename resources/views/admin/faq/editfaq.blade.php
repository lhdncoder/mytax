@extends('ui::ablepro.dashboard')

@section('content')


<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.editfaq')</h5> 
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
               <div class="card-body">
                 {!! SemanticForm::post(route('admin.svefaq'))->attribute('id', 'editfaq') !!}
                 <input type="hidden" name="id" value="{{data_get($dataques,'id')}}"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.quest_bm')</label>
                                <textarea class="form-control" id="quest_bm" name="quest_bm" rows="3" placeholder="@lang('form.quest_bm')">{{data_get($dataques,'question_name_bm')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.quest_en')</label>
                                <textarea class="form-control" id="quest_en" name="quest_en" rows="3" placeholder="@lang('form.quest_en')">{{data_get($dataques,'question_name_en')}}</textarea>
                            </div>
                        </div>
                    </div>
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.queststatus')</label>
                                <select class="form-control js-example-data-array" id="status" name="status">
                                    <option value="" {{ data_get($dataques,'status')==""  ? "selected" : ''}}>@lang('form.choose')</option>
                                    <option value="1" {{ data_get($dataques,'status')=="1" ? "selected" : ''}}>@lang('form.active')</option>
                                    <option value="0" {{ data_get($dataques,'status')=="0" ? "selected" : ''}}>@lang('form.notactive')</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <button type="button" class='btn btn-primary btn-sm has-ripple'><a  style="color:white" href="{{URL::to('admin/listfaq')}}">@lang('form.back')</a></button>
                    <button class="btn btn-primary btn-sm has-ripple" type="submit">@lang('form.editquest')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
                   <!--  <button type="button" class="btn btn-primary btn-sm has-ripple" data-toggle="modal" data-target="#exampleModalCenter">@lang('form.addanswer')<span class="ripple ripple-animate" style="height: 174.05px; width: 174.05px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -55.725px; left: -48.3417px;"></span></button> -->
                
                {!! Form::close() !!}

       <br>
       <br>
        <div class="row">
            <!-- customar project  start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6 text-right">
                                <button  type="button" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#exampleModalCenter"><i class="feather icon-plus"></i>
                                @lang('form.addanswer')</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="answer-table" class="table table-bordered table-striped mb-0">
                                   <thead>
                                     <tr>
                                        <th>@lang('form.no')</th>
                                        <th>@lang('form.answ')</th>
                                        <th>@lang('form.answstatus')</th>
                                        <th>@lang('form.action')</th>
                                    </tr>
                                </thead>
               
                                 <tbody>
                                     <?php $i=1;?>
                                     @forelse($dataanswer as $key => $value)
                                      <tr>
                                         <td><?php echo $i ?></td>
                                          @if($user->language == 'en')
                                          <td><?php echo data_get($value,'answer_name_en') ?></td>
                                          @else
                                          <td>{!! data_get($value,'answer_name_bm') !!}</td>
                                          @endif
                                         
                                        @if($value->status==0)
                                        <td>@lang('form.notactive')</td>
                                        @else
                                        <td>@lang('form.active')</td>
                                        @endif
                                         <td >
                                        <a data-target="#editanswmodal" data-toggle="modal" class="btn btn-info btn-sm editanswer" href="#editanswmodal"
                                         data_id="{{$value->id}}"
                                         data_answer_bm="{{ base64_encode( $value->answer_name_bm ) }}"
                                         data_answer_en="{{ base64_encode( $value->answer_name_en ) }}"
                                         data_status="{{$value->status}}"
                                         data_idquest="{{$value->fk_question}}"><i class="feather icon-edit"></i>&nbsp;@lang('form.edit')</a>
                                        </td> 
                                     </tr>
                                     <?php $i++;?>
                                     @empty
                                     <tr><td colspan='4'>Tiada Data</td></tr>
                                     @endforelse
                                 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- customar project  end -->
        </div>
        <!-- [ Main Content ] end -->
          
                    <!---  start modal jawapan-->
                  <div class="modal fade bd-example-modal-lg" id= "exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title h4" id="myLargeModalLabel">@lang('form.addanswer')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                     {!! SemanticForm::post(route('admin.svansw'))->attribute('id', 'addansw') !!}
                     <input type="hidden" name="idquest" value="{{data_get($dataques,'id')}}"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.answ_bm')</label>
                                <textarea class="form-control" id="answ_bm" name="answ_bm" rows="3" placeholder="@lang('form.answ_bm')"></textarea>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.answ_en')</label>
                                <textarea class="form-control" id="answ_en" name="answ_en" rows="3" placeholder="@lang('form.answ_en')"></textarea>
                            </div>
                        </div>
                    </div>
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.answstatus')</label>
                                <select class="form-control" id="statusansw" name="statusansw">
                                    <option value="">@lang('form.choose')</option>
                                    <option value="1">@lang('form.active')</option>
                                    <option value="2">@lang('form.notactive')</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-secondary has-ripple" data-dismiss="modal">@lang('form.close')<span class="ripple ripple-animate" style="height: 75.35px; width: 75.35px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -20.175px; left: -8.50831px;"></span></button>
                    <button class="btn btn-primary has-ripple" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
                    {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                 <!---  end modal jawapan-->

                   <!---  start editanswmodal-->
                  <div class="modal fade bd-example-modal-lg" id= "editanswmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title h4" id="myLargeModalLabel">@lang('form.editanswer')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                    <div class="modal-body">
                     {!! SemanticForm::post(route('admin.sveansw'))->attribute('id', 'editansw') !!}
                     <input type="hidden" name="idansweredit" id="idansweredit" value=""/>
                     <input type="hidden" name="idquestedit" id="idquestedit" value=""/>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.answ_bm')</label>
                                <textarea class="form-control" id="answeredit_bm" name="answeredit_bm" rows="3" placeholder="@lang('form.answ_bm')"></textarea>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.answ_en')</label>
                                <textarea class="form-control" id="answeredit_en" name="answeredit_en" rows="3" placeholder="@lang('form.answ_en')"></textarea>
                            </div>
                        </div>
                    </div>
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.answstatus')</label>
                                <select class="form-control" id="statusedit" name="statusedit">
                                    <option value="">@lang('form.choose')</option>
                                    <option value="1">@lang('form.active')</option>
                                    <option value="0">@lang('form.notactive')</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-secondary has-ripple" data-dismiss="modal">@lang('form.close')<span class="ripple ripple-animate" style="height: 75.35px; width: 75.35px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -20.175px; left: -8.50831px;"></span></button>
                    <button class="btn btn-primary has-ripple" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
                    {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                 <!---  end editanswmodal-->
        
               </div>
            </div>
           </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
    // DataTable start
    $('#answer-table').DataTable();
    // DataTable end
</script>
<script>

$(document).ready(function(){

        //start modal edit answer
       $('.editanswer').click(function() {
       $("#idansweredit").val($(this).attr('data_id'));
       // $("#answeredit_bm").val($(this).attr('data_answer_bm'));
       // $("#answeredit_en").val($(this).attr('data_answer_en'));
       $("#statusedit").val($(this).attr('data_status'));
       $("#idquestedit").val($(this).attr('data_idquest'));

          $('#answeredit_bm').trumbowyg('html', atob($(this).attr('data_answer_bm')));
          $('#answeredit_en').trumbowyg('html', atob($(this).attr('data_answer_en')));

       });
        //end modal edit kenderaan

          $('#editfaq').validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'quest_bm': {
                    required: true,
                },
                 'quest_en': {
                    required: true,
                },
                'status': {
                    required: true,
                },

            },

            // Errors //

            errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function(element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });


      $('#addansw').validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'answ_bm': {
                    required: true,
                },
                'answ_en': {
                    required: true,
                },
                'statusansw': {
                    required: true,
                },

            },

            // Errors //

            errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function(element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });


 $('#editansw').validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'answeredit_bm': {
                    required: true,
                },
                'answeredit_en': {
                    required: true,
                },
                'statusedit': {
                    required: true,
                },

            },

            // Errors //

            errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function(element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });

});
</script>
<script>
    $('#answ_en').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }

    });
</script>
<script>
    $('#answ_bm').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }
    });
</script>
<script>
    $('#answeredit_bm').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }

    });
</script>
<script>
    $('#answeredit_en').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }
    });
</script>


@endpush