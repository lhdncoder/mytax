@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.editservice')</h5> 
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
               <div class="card-body">
                 {!! SemanticForm::post(route('admin.sveservice'))->attribute('id', 'editservice') !!}
                     <input type="hidden" name="id" value="{{data_get($dataservice,'id')}}"/>

                     <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab" role="tablist">
                        <li class="nav-item" style="border: 1px solid blue;">
                            <a class="nav-link has-ripple active" id="pills-bm-tab" data-toggle="pill" href="#pills-bm" role="tab" aria-controls="pills-bm" aria-selected="true">Bahasa Melayu<span class="ripple ripple-animate" style="height: 71.65px; width: 71.65px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -13.725px; left: -7.825px;"></span></a>
                        </li>
                        <li class="nav-item" style="border: 1px solid blue;">
                            <a class="nav-link has-ripple" id="pills-en-tab" data-toggle="pill" href="#pills-en" role="tab" aria-controls="pills-en" aria-selected="false">English<span class="ripple ripple-animate" style="height: 74.2667px; width: 74.2667px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -19.0333px; left: 3.21666px;"></span></a>
                        </li>
                        <li class="nav-item" style="border: 1px solid blue;">
                            <a class="nav-link has-ripple" id="pills-set-tab" data-toggle="pill" href="#pills-set" role="tab" aria-controls="pills-set" aria-selected="false">Setting<span class="ripple ripple-animate" style="height: 74.2667px; width: 74.2667px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -19.0333px; left: 3.21666px;"></span></a>
                        </li>
                        
                    </ul>
                    <br>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade active show" id="pills-bm" role="tabpanel" aria-labelledby="pills-bm-tab">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama Perkhidmatan *</label>
                                        <input type="text" class="form-control" id="nameservice_bm" name="nameservice_bm" placeholder="Nama Perkhidmatan" value="{{data_get($dataservice,'service_bm')}}" required="required">
                                    </div>
                                </div>
                            </div>
                           <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Deskripsi</label>
                                        <input type="text" class="form-control" id="desc_bm" name="desc_bm" placeholder="Deskripsi" value="{{data_get($dataservice,'description_bm')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label">Konten</label>
                                <textarea id="Konten-editor" name="content_bm">
                                    <?php echo data_get($dataservice,'content_bm')?>
                                </textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-en" role="tabpanel" aria-labelledby="pills-en-tab">
                             <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Service Name</label>
                                        <input type="text" class="form-control" id="nameservice_en" name="nameservice_en" placeholder="Service Name" value="{{data_get($dataservice,'service_en')}}">
                                    </div>
                                </div>
                            </div>
                           <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input type="text" class="form-control" id="desc_en" name="desc_en" placeholder="Description" value="{{data_get($dataservice,'description_en')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label">Content</label>
                                <textarea id="content-editor" name="content_en">
                                   <?php echo data_get($dataservice,'content_en')?>
                                </textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-set" role="tabpanel" aria-labelledby="pills-set-tab">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>@lang('form.queststatus')</label>
                                        <select class="form-control js-example-data-array" id="status" name="status">
                                            <option value="" {{ data_get($dataservice,'status')==""  ? "selected" : ''}}>@lang('form.choose')</option>
                                            <option value="1" {{ data_get($dataservice,'status')=="1" ? "selected" : ''}}>@lang('form.active')</option>
                                            <option value="0" {{ data_get($dataservice,'status')=="0" ? "selected" : ''}}>@lang('form.notactive')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>@lang('form.order')</label>
                                        <input type="number" class="form-control" id="order" name="order" placeholder="@lang('form.order')" value="{{data_get($dataservice,'order')}}">
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                             <div class="col-sm-12">
                                <h5 class="mt-3">Access</h5>
                                <hr>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="access[]" class="custom-control-input" id="customCheck1" value="1" @if(in_array("1", $check)) checked @endif>
                                    <label class="custom-control-label" for="customCheck1">IND</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="access[]" class="custom-control-input" id="customCheck2" value="2" @if(in_array("2", $check)) checked @endif>
                                    <label class="custom-control-label" for="customCheck2">EOF</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="access[]" class="custom-control-input" id="customCheck3" value="3" @if(in_array("3", $check)) checked @endif>
                                    <label class="custom-control-label" for="customCheck3">IND & OEF</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="access[]" class="custom-control-input" id="customCheck4" value="4" @if(in_array("4", $check)) checked @endif>
                                    <label class="custom-control-label" for="customCheck4">PEF</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="access[]" class="custom-control-input" id="customCheck5" value="5" @if(in_array("5", $check)) checked @endif>
                                    <label class="custom-control-label" for="customCheck5">IND & PEF</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="access[]" class="custom-control-input" id="customCheck6" value="6" @if(in_array("6", $check)) checked @endif>
                                    <label class="custom-control-label" for="customCheck6">EOF & PEF</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="access[]" class="custom-control-input" id="customCheck7" value="7" @if(in_array("7", $check)) checked @endif>
                                    <label class="custom-control-label" for="customCheck7">IND & EOF & PEF</label>
                                </div>
                            </div>
                            </div>

                        </div>
                       
                            <br>
                            <br>
                        
                    </div>
                    <button type="button" class='btn btn-primary has-ripple' onclick="history.back();">@lang('form.back')</button>
                    <button class="btn btn-primary has-ripple" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
                {!! Form::close() !!}
               </div>
            </div>
           </div>
        </div>
    </div>
</div>

@endsection
@push('script')


<script>
    $('#content-editor').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }
    });
</script>
<script>
    $('#Konten-editor').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }
    });
</script>
@endpush