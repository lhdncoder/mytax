<?php use App\Models\Mngfeedback;?>

<table>
	<tr>
		<td colspan="4"><b>Senarai Komen Maklumbalas Pengguna</b></td>

	</tr>
	<tr>
		<td>&nbsp;</td>
		
	</tr>
	<tr>
		<td><b>Tarikh Mula:</b></td><td>{{$datefrom}}</td>
		<td><b>Tarikh Hingga:</b></td><td>{{$dateto}}</td>
		
	</tr>
</table>
<table border="1">
            <thead>
               <tr>
                <th style="background-color:#E1DFDE"><b>@lang('form.bil')</b></th>
                <th style="background-color:#E1DFDE"><b>@lang('form.tarikhcomment')</b></th>
                <th style="background-color:#E1DFDE"><b>@lang('form.comment')</b></th>
                </tr>
            </thead>
             <tbody>
             	<?php $i=1;?>
             	 @forelse($data as $key =>$value)

             	 <tr>
                  <td align="left"><?php echo $i ?></td>
                  <td>{{date("d-m-Y H:i:s", strtotime($value->date_feedback))}}</td>
                   <td>{{$value->comment}}</td>
               </tr>
             	<?php $i++;?>
                @empty
                   <tr><td>Tiada Data</td></tr>
                 @endforelse

   
             </tbody>
         </table>
