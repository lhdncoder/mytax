<?php use App\Models\Mngfeedback;?>

<style>
    #title{
      font-size:12px;
      font-family: Arial, Helvetica, sans-serif;
    }
      #uuk{
      font-size:12px;
      font-family: Arial, Helvetica, sans-serif;
    }
</style>
<table id="title">
	<tr>
		<td colspan="2"><b>Laporan Maklumbalas Pengguna</b></td>
		

	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td><b>Tarikh Mula:</b></td><td>{{$datefrom}}</td>
		<td><b>Tarikh Hingga:</b></td><td>{{$dateto}}</td>

	</tr>
</table>

<table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
            <thead>
               <tr>
                <th ><b>@lang('form.bil')</b></th>
                <th ><b>@lang('form.soalan')</b></th>
                <th ><b>@lang('form.jawapan')</b></th>
                <th ><b>@lang('form.jumlah')</b></th>
                <th ><b>@lang('form.precent')</b></th>
                </tr>
            </thead>
           <tbody>
           	<?php $i=1;?>
           	 @forelse($arr as $key=>$value)

           	  <tr>
                    <td align="left">{{$i}}</td>
                     @if($user->language == 'en')
                    <td>{{$value['quest_en']}}</td>
	                 @else
	                <td>{{$value['quest_bm']}}</td>
	                 @endif
	                  <td>
                      <table  width="100%" border="1" style="border-collapse: collapse;">

                        @foreach($value['detail_answ'] as $keys => $list_answ)

                        <tr>
                          @if($user->language == 'en')
                              <td>
                                 {{$list_answ->answer_en}} 
                              </td>
                          @else
                              <td>
                                  {{$list_answ->answer_bm}}
                              </td>
                          @endif
                          
                          </tr>
                          @endforeach 
                      </table>
                  </td>
                   <td>
                      <table  width="100%" border="1" style="border-collapse: collapse;">

                        @foreach($value['detail_answ'] as $keys => $list_answ)
                         <?php 

                          $countanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS bil_answer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                    ->where('fk_lkp_answer_feedback',$list_answ->id)
                                                     ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                     ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();

                       
                        ?>

                        <tr>
                    
                              <td align="center" >
                                  {{$countanswer->bil_answer}}
                              </td>
                       
                          
                          </tr>
                          @endforeach 
                      </table>
                  </td>
                    <td>
                      <table  width="100%" border="1"  style="border-collapse: collapse;">

                        @foreach($value['detail_answ'] as $keys => $list_answ)

                           <?php 

                          $countanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS bil_answer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                    ->where('fk_lkp_answer_feedback',$list_answ->id)
                                                    ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                    ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();


                           $allanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS allanswer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                     ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                     ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();

                            if(data_get($allanswer,'allanswer')==0){
                              $peratus="0.00";

                            }else{
                              $peratus=(data_get($countanswer,'bil_answer')/data_get($allanswer,'allanswer'))*100;

                            }
                            ?>

                        <tr>
                         <td align="center">{{$peratus}}</td>
                          
                          </tr>
                          @endforeach 
                      </table>
                  </td>
	               
	         </tr>
	         <?php $i++;?>
                @empty
                   <tr><td>Tiada Data</td></tr>
                 @endforelse
           </tbody>
         </table>