<?php use App\Models\Mngfeedback;?>

<table>
	<tr>
		<td colspan="4"><b>Laporan Maklumbalas Pengguna</b></td>

	</tr>
	<tr>
		<td>&nbsp;</td>
		
	</tr>
	<tr>
		<td><b>Tarikh Mula:</b></td><td>{{$datefrom}}</td>
		<td><b>Tarikh Hingga:</b></td><td>{{$dateto}}</td>
		
	</tr>
</table>
<table border="1">
            <thead>
               <tr>
                <th style="background-color:#E1DFDE"><b>@lang('form.bil')</b></th>
                <th style="background-color:#E1DFDE"><b>@lang('form.soalan')</b></th>
                <th style="background-color:#E1DFDE"><b>@lang('form.jawapan')</b></th>
                <th style="background-color:#E1DFDE"><b>@lang('form.jumlah')</b></th>
                <th style="background-color:#E1DFDE"><b>@lang('form.precent')</b></th>
                </tr>
            </thead>
             <tbody>
             	<?php $i=1;?>
             	 @forelse($arr as $key=>$value)

             	 <tr>
                    <td align="left">{{$i}}</td>
                     @if($user->language == 'en')
                    <td>{{$value['quest_en']}}</td>
	                 @else
	                <td>{{$value['quest_bm']}}</td>
	                 @endif
	                 <td>
	                   <table>
	                   	 <thead>
			               <tr>
			                <th>&nbsp;</th>
			             
			                </tr>
            			</thead>
            			 @foreach($value['detail_answ'] as $keys => $list_answ)

                        <?php 

                          

                           $countanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS bil_answer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                    ->where('fk_lkp_answer_feedback',$list_answ->id)
                                                    ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                    ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();


                           $allanswer=Mngfeedback::select([DB::raw('COUNT(fk_lkp_answer_feedback) AS allanswer')])
                                                    ->where('fk_lkp_question_feedback',$value['id'])
                                                     ->where('date_feedback','>=', array( date("Y-m-d", strtotime($datefrom))))   
                                                     ->where('date_feedback','<', array( date("Y-m-d", strtotime($dateto))))
                                                    ->first();



                             if(data_get($allanswer,'allanswer')==0){
                              $peratus=0;

                            }else{
                              $peratus=(data_get($countanswer,'bil_answer')/data_get($allanswer,'allanswer'))*100;

                            }

                       
                        ?>
             
                        <tr>
                          @if($user->language == 'en')
                              <td>
                                 {{$list_answ->answer_en}} 
                              </td>
                          @else
                              <td>
                                  {{$list_answ->answer_bm}}
                              </td>
                          @endif

                          	<td>
                           {{$countanswer->bil_answer}}
                        	 </td>

                        	 <td>{{number_format($peratus, 2, '.', ' ')}}</td>
                            
                          </tr>
                          @endforeach 
                      </table>
	                 </td>
	                
                 </tr>
             	<?php $i++;?>
                @empty
                   <tr><td>Tiada Data</td></tr>
                 @endforelse

   
             </tbody>
         </table>
