<?php use App\Models\Mngfeedback;?>

<style>
    #title{
      font-size:12px;
      font-family: Arial, Helvetica, sans-serif;
    }
      #uuk{
      font-size:12px;
      font-family: Arial, Helvetica, sans-serif;
    }
</style>
<table id="title">
	<tr>
		<td colspan="2"><b>Laporan Maklumbalas Pengguna</b></td>
		

	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td><b>Tarikh Mula:</b></td><td>{{$datefrom}}</td>
		<td><b>Tarikh Hingga:</b></td><td>{{$dateto}}</td>

	</tr>
</table>

<table id="title" width="100%" border="1" cellpadding="2" cellspacing="0">
            <thead>
               <tr>
                <th align="left"><b>@lang('form.bil')</b></th>
                <th align="left"><b>@lang('form.tarikhcomment')</b></th>
                <th align="left" ><b>@lang('form.comment')</b></th>
                </tr>
            </thead>
           <tbody>
              <?php $i=1;?>
               @forelse($data as $key =>$value)

               <tr>
                  <td align="left"><?php echo $i ?></td>
                  <td align="left">{{date("d-m-Y H:i:s", strtotime($value->date_feedback))}}</td>
                   <td align="left"> {{$value->comment}}</td>
               </tr>
              <?php $i++;?>
                @empty
                   <tr><td>Tiada Data</td></tr>
                 @endforelse

   
             </tbody>
         </table>