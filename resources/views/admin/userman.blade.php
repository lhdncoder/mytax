@extends('ui::ablepro.dashboard')

@section('content')
        <div class="row">
            <div class="col-md-12 col-xl-12 ">
                <div class="card app-design">
                    <div class="card-body">
                        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal" data-whatever="Add New Admin">Add new</button>
                        
                        <div class="design-description d-inline-block m-r-40">
                            <h3 class="f-w-400">{{$role->count()}}</h3>
                            <p class="text-muted">Assigned Administrator</p>
                        </div>
                        <br>
                        <div class="team-box p-b-20">
                            <p class="d-inline-block m-r-20 f-w-400">list : </p>
                            
                        </div>
                        <div class="table-responsive">
                            <table id="announce-table" class="table table-bordered table-striped mb-0">
                                 <thead>
                                   <tr>
                                        <th>No.</th>
                                        <th>Identification No</th>
                                        <th>@lang('form.action')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                   <?php $i=1;?>
                                   @forelse($role as $key=>$value)
                                    <tr>
                                        <td width="1%"><?php echo $i ?></td>
                                        <td>{{$value->name}}</td>
                                        <td>     
                                        <a href='/admin/deleterole/{{$value->id}}' class="btn btn-info btn-sm"><i class="feather icon-delete"></i>Remove </a>
                                        </td> 
                                    </tr>
                                    <?php $i++;?>
                                     @empty
                                     <tr><td colspan='3'>Tiada Data</td></tr>
                                     @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add new idenfication no for admin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    {!! SemanticForm::post(route('admin.saverole'))->attribute('id', 'editapp') !!}
                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Identification No.</label>
                        <input type="text" class="form-control" id="recipient-name" name="name">
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn  btn-primary">Save</button>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>

@endsection
@push('script')
<script>
    // DataTable start
    $('#announce-table').DataTable();
    // DataTable end
</script>
@endpush