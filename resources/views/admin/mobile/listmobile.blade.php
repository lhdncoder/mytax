@extends('ui::ablepro.dashboard')

@section('content')
        <div class="row">
            <!-- customar project  start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                         <div class="card-body d-flex align-items-center justify-content-between">
                     <h5 class="mb-0">@lang('form.titlemobile')</h5>
                    </div>
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6 text-right">
                                <button class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i><a  style="color:white" href="{{URL::to('admin/addmobile')}}">
                                @lang('form.addmobile')</a></button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="announce-table" class="table table-bordered table-striped mb-0">
                                  <thead>
                                    <tr>
                                        <th>@lang('form.no')</th>
                                        <th>Logo</th>
                                        <th>@lang('form.namemobile')</th>
                                        <th>Android</th>
                                        <th>Ios</th>
                                        <th>@lang('form.status')</th>
                                        <th>@lang('form.action')</th>

                                    </tr>
                                </thead>
                                <tbody>

                                   <?php $i=1;?>
                                   @forelse($listmobile as $key=>$value)
                                    <tr>
                                        <td width="1%"><?php echo $i ?></td>
                                        <td>@if($value->icon_url) <img src="/storage/appicon/{{$value->icon_url}}" alt="user image" class="latest-posts-img" height="50px"></td> @else no icon @endif
                                         @if($user->language == 'en')
                                         <td>{{$value->mobile_module_name_en}}</td>
                                         @else
                                         <td>{{$value->mobile_module_name_bm}}</td>
                                         @endif
                                        
                                        <td>{{ Str::limit($value->and_url, 25, ' ...') }}</td>
                                        <td>{{ Str::limit($value->ios_url, 25, ' ...') }}</td>
                                        @if($value->status==0)
                                        <td>@lang('form.notactive')</td>
                                        @else
                                        <td>@lang('form.active')</td>
                                        @endif
                                        <td>
                                         <a href='/admin/editmobile/{{$value->id}}' class="btn btn-info btn-sm"><i class="feather icon-edit"></i>&nbsp;@lang('form.edit') </a>    
                                        </td> 
                                    </tr>
                                    <?php $i++;?>
                                     @empty
                                     <tr><td colspan='6'>Tiada Data</td></tr>
                                     @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- customar project  end -->
        </div>
        <!-- [ Main Content ] end -->

@endsection
@push('script')
<script>
    // DataTable start
    $('#announce-table').DataTable();
    // DataTable end
</script>
@endpush