@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.addmobile')</h5> 
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
               <div class="card-body">
                 {!! SemanticForm::post(route('admin.svmobile'))->attribute('id', 'addmobile')->multipart() !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.namemobile_bm')</label>
                                <input type="text" class="form-control" id="namemobile_bm" name="namemobile_bm" placeholder="@lang('form.namemobile_bm')">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.namemobile_en')</label>
                                <input type="text" class="form-control" id="namemobile_en" name="namemobile_en" placeholder="@lang('form.namemobile_en')">
                            </div>
                        </div>
                    </div>
                    <br><hr><br>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Icon image</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="iconfile" name="img">
                            <label class="custom-file-label" for="iconfile">Choose file</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Url Android</label>
                                <input type="text" class="form-control" id="api" name="api" placeholder="@lang('form.api')">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Url IOS</label>
                                <input type="text" class="form-control" id="url" name="url" placeholder="@lang('form.url')">
                            </div>
                        </div>
                    </div>
                    <br><hr><br>
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.queststatus')</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="">@lang('form.choose')</option>
                                    <option value="1">@lang('form.active')</option>
                                    <option value="0">@lang('form.notactive')</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <button type="button" class='btn btn-primary has-ripple' onclick="history.back();">@lang('form.back')</button>
                    <button class="btn btn-primary has-ripple" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
                {!! Form::close() !!}
               </div>
            </div>
           </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
    $('#iconfile').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
</script>
<script>
$(document).ready(function() {

           $('#addmobile').validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'namemobile_bm': {
                    required: true,
                },
                 'namemobile_en': {
                    required: true,
                },
                  'api': {
                    required: true,
                },
                'url': {
                    required: true,
                },
                'status': {
                    required: true,
                },

            },

            // Errors //

            errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function(element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });


 });
</script>


@endpush