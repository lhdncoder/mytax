@extends('ui::ablepro.dashboard')

@section('content')
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">@lang('form.edithelp')</h5> 
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
               <div class="card-body">
                 {!! SemanticForm::post(route('admin.svehelp'))->attribute('id', 'edithelp') !!}
                  <input type="hidden" name="id" value="{{data_get($datahelp,'id')}}"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.desc_bm')</label>
                                <textarea class="form-control" id="desc_bm" name="desc_bm" rows="3">
                                    <?php echo data_get($datahelp,'description_bm')?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.desc_en')</label>
                                <textarea class="form-control" id="desc_en" name="desc_en" rows="3">
                                    <?php echo data_get($datahelp,'description_en')?> 
                                </textarea>
                            </div>
                        </div>
                    </div>
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>@lang('form.queststatus')</label>
                                <select class="form-control js-example-data-array" id="status" name="status">
                                    <option value="" {{ data_get($datahelp,'status')==""  ? "selected" : ''}}>@lang('form.choose')</option>
                                    <option value="1" {{ data_get($datahelp,'status')=="1" ? "selected" : ''}}>@lang('form.active')</option>
                                    <option value="0" {{ data_get($datahelp,'status')=="0" ? "selected" : ''}}>@lang('form.notactive')</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <button type="button" class='btn btn-primary has-ripple' onclick="history.back();">@lang('form.back')</button>
                    <button class="btn btn-primary has-ripple" type="submit">@lang('form.send')<span class="ripple ripple-animate" style="height: 121.933px; width: 121.933px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -46.5665px; left: 1.0335px;"></span></button>
                {!! Form::close() !!}
               </div>
            </div>
           </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
$(document).ready(function() {

           $('#edithelp').validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'desc_bm': {
                    required: true,
                },
                'desc_en': {
                    required: true,
                },
                'status': {
                    required: true,
                },

            },

            // Errors //

            errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function(element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });


 });
</script>
<script>
    $('#desc_en').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }

    });
</script>
<script>
    $('#desc_bm').trumbowyg({
        svgPath: "{{asset('themes/ablepro/assets/css/plugins/icons.svg')}}",
        btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen'],
            ['fontsize']
        ],
        plugins: {
            fontsize: {
                sizeList: [
                    '12px',
                    '14px',
                    '16px'
                ]
            }
        }
    });
</script>

@endpush