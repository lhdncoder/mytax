 <?php $rowcount = 0; ?>
 <div class="col-md-12">
   <br>
   <br>
   <br>
      <div class="col-md-12"><h6 style="padding-left:16px">Paparan potongan cukai bulanan (PCB) mengikut tahun kalendar: <span class="m-r-20"></span>
  
         
          <button type="button" onclick="javascript:loadpcbcal({{$lastyear}});" class="btn btn-info has-ripple btn-sm"><i class="feather mr-2 icon-calendar"></i>{{$lastyear}}<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>

          <button type="button" onclick="javascript:loadpcbcal({{$year}});" class="btn btn-info has-ripple btn-sm"><i class="feather mr-2 icon-calendar"></i>{{$year}}<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
   
   </h6>
   </div>
    <div class="card-body table-border-style" style="padding-right: unset; font-size: 12px !important">
     <button type="button" class="btn btn-info has-ripple btn-sm" onClick="printdiv('div_printpcb');" style="float:right;margin-left: 10px;"><i class="mr-2 feather icon-printer"></i>@lang('homepage.printlabel')<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
                            <h6>Penyata PCB - cukai pendapatan bagi tahun kalendar {{$selectedyear}}</h6>
        <br>
       <div class="table-responsive shadow" style="border-radius: 8px;margin-bottom:20px">
            <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                <thead>
                    
                    <tr>
                        <!-- <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tarikh<P>Kemaskini</P></th> -->
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tarikh<p style="margin-bottom:2px">Transaksi</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Keterangan<p style="margin-bottom:2px">Transaksi</p></th>

                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Rujukan/<p style="margin-bottom:2px">No. Resit</p></th>

                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tahun<p style="margin-bottom:2px">Taksiran</p></th>

                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Bulan/<p style="margin-bottom:2px">Bil Ansuran</p></th>
                        <!-- <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Taksiran & <p>Lain-lain<sup>1</sup></p></th> -->
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Bayaran & <p style="margin-bottom:2px">Lain-lain<sup>2</sup></p></th>
                    </tr>


                </thead>
                <tbody>
                    
                    @forelse($calendar as $dat =>$list)
                    <?php 

                         $datep = new DateTime($list->POSTED_DATE);
                         $datet = new DateTime($list->TRANSACTION_DATE);
                         // $total += number_format($list->BayaranCukai, 2);

                         if($list->RECEIPT_NO == '0')
                         {
                            $resit = '';
                         }else{
                            $resit = $list->BRANCH_CODE.$list->RECEIPT_NO;
                         }

                        
                    ?>
                         <tr>
                             <!-- <td>{{$datep->format('d/m/Y')}}</td> -->
                             <td>{{$datet->format('d/m/Y')}}</td>
                             <td>{{$list->Keterangan}}</td>
                             <td>{{$resit}}</td>
                             @if($list->Keterangan == 'Baki Permulaan')
                             <td></td>
                             <td></td>
                             @else
                             <td>{{$list->ASSESSMENT_YEAR}}</td>
                             <td>{{$list->ASSESSMENT_NO}}</td>
                             @endif
                             <!-- <td style="text-align: right">{{$list->TggnCukai}}</td> -->
                             <td style="text-align: right">{{$list->BayaranCukai}}</td>
                         </tr>
                       
                     @empty
                    <tr>    
                        <td colspan="6"><h6>Tiada Rekod</h6></td>
                    </tr>
                    <?php $rowcount = 1; ?>
                    @endforelse
                    @if($rowcount == 0)
                     <tr>    
                        <td colspan="3"><h6></h6></td>
                        <td><h6>JUMLAH</h6></td>
                        <td><h6></h6></td>
                        <td style="text-align: right"><h6>@if($total) {{number_format($total->BakiCukai,2,'.',',')}} @endif</h6></td>
                    </tr>
                    @endif
                
                   
                </tbody>
            
            </table>
           

        </div>
         Nota: Penyata ini hanya memaparkan bayaran PCB sahaja. Penyata kedudukan akaun cukai terperinci perlu semak melalui e-Lejer
    </div>
    </div>














    <div class="card-body table-border-style" id='div_printpcb' style="height:100vh;font-size: 11px !important;background-color: white !important;display:none;width:100%">
    <link rel="icon" href="{{asset('themes/ablepro/assets/images/favicon.ico')}}" type="image/x-icon">
        <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/style.css')}}">
        <br>
        <div class="align-middle m-b-25">
            <img src="{{asset('themes/ablepro/assets/images/logoprint.jpg')}}" style="float:left" alt="user image" class="align-top m-r-10" width="80px">
            <br>
            <br>
             <h6>Penyata potongan cukai bulanan (PCB) tahun kalendar {{$selectedyear}}<h6>
        </div>
        <br>
        <br>
        <div class="table-responsive">
            <table class="table table-bordered table-xs text-center" id="tableprint" style="font-size: 10px !important">
                <thead>
                    <tr>
                        <!-- <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tarikh<P>Kemaskini</P></th> -->
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tarikh<p>Transaksi</p></th>
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Keterangan<p>Transaksi</p></th>

                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Rujukan/<p>No. Resit</p></th>

                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tahun<p>Taksiran</p></th>

                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Bulan/<p>Bil Ansuran</p></th>
                        <!-- <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Taksiran & <p>Lain-lain<sup>1</sup></p></th> -->
                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Bayaran & <p>Lain-lain<sup>2</sup></p></th>
                    </tr>


                </thead>
                <tbody>
                    
                    @forelse($calendar as $dat =>$list)
                    <?php 

                         $datep = new DateTime($list->POSTED_DATE);
                         $datet = new DateTime($list->TRANSACTION_DATE);
                         // $total += number_format($list->BayaranCukai, 2);

                         if($list->RECEIPT_NO == '0')
                         {
                            $resit = '';
                         }else{
                            $resit = $list->BRANCH_CODE.$list->RECEIPT_NO;
                         }

                        
                    ?>
                         <tr>
                             <!-- <td>{{$datep->format('d/m/Y')}}</td> -->
                             <td>{{$datet->format('d/m/Y')}}</td>
                             <td>{{$list->Keterangan}}</td>
                             <td>{{$resit}}</td>
                             @if($list->Keterangan == 'Baki Permulaan')
                             <td></td>
                             <td></td>
                             @else
                             <td>{{$list->ASSESSMENT_YEAR}}</td>
                             <td>{{$list->ASSESSMENT_NO}}</td>
                             @endif
                             <!-- <td style="text-align: right">{{$list->TggnCukai}}</td> -->
                             <td style="text-align: right">{{$list->BayaranCukai}}</td>
                         </tr>
                       
                   @empty
                    <tr>    
                        <td colspan="6"><h6>Tiada Rekod</h6></td>
                    </tr>
                    <?php $rowcount = 1; ?>
                    @endforelse
                    @if($rowcount == 0)
                     <tr>    
                        <td colspan="3"><h6></h6></td>
                        <td><h6>JUMLAH</h6></td>
                        <td><h6></h6></td>
                        <td style="text-align: right"><h6>@if($total) {{number_format($total->BakiCukai,2,'.',',')}} @endif</h6></td>
                    </tr>
                    @endif
                
                   
                </tbody>
            
            </table>

        </div>



        <div class="align-middle m-b-25">
           
            <div class="d-inline-block">
                <b>Nota: </b><br>
                <b>1. Taksiran & lain-lain </b>=  Cukai dibangkitkan/kenaikan cukai/bayaran balik dan lain-lain<br>
                <b>2. Bayaran & Lain-lain </b>=  Transaksi yang mengurangkan <b>Taksiran & Lain-Lain<sup>1</sup> </b>. Contoh bayaran PCB, bayaran ansuran dan pelarasan/pengurangan cukai.<br>
                
            </div>
        </div>


        <div class="bottom-align-text fixed-bottom" style="font-size:8px">
            Penafian: Walaupun segala penelitian telah diambil dalam penyediaan maklumat dalam penyata ini, penyata ini bertujuan untuk rujukan sahaja dan<br>
            maklumat dari penyata tidak akan menjelaskan apa - apa tuntutan LHDNM yang difailkan di Mahkamah.
        </div>
    </div>