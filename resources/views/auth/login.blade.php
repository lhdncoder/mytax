@extends('ui::ablepro.login')

@section('content')
    <?php 

    $locale = App::getLocale();
         $activems = '';
         $activeen = '';

    $errors = '0';

    if(isset($error))
    {
        $errors = '1';
    }


    ?>
    <div class="row">
        <div class="col-md-8 col-lg-8">
            <div class="card h-100">
                <div class="card-body home " id="home">
                    @if(!$banner->isEmpty())
                    <?php $count = 1;?>
            
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="">
                        
                        <div class="carousel-inner text-center" style="height: 300px">
                         @foreach($banner as $key => $value)
                            <div class="carousel-item @if($count == 1) active @endif">
                                <img class="shadow" src="/storage/banner/{{$value->banner_img}}" alt="First slide" style="width: 100%;height:300px;object-fit: contain;">
                            </div>
                         <?php $count = $count+1 ;?>
                         @endforeach
                           
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" class="shadow" style="width:5%"><span class="" aria-hidden="true" style="color:grey;font-size: 30px;font-weight: 800"> <i class="fas fa-chevron-circle-left"></i> </span><span class="sr-only">Previous</span></a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="width:5%"><span class="" aria-hidden="true" style="color:grey;font-size: 30px;font-weight: 800"> <i class="fas fa-chevron-circle-right "></i> </span><span class="sr-only">Next</span></a>
                    </div>
               
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="card h-100 p-10" onclick="location.href='http://edaftar.hasil.gov.my/'" style="cursor: pointer;">
                <img src="{{ asset('assets/images/jkk2.jpeg') }}" width="100%">
               <!--  <a href="http://edaftar.hasil.gov.my/" style="text-shadow: 2px 2px 4px white;font-weight: bolder;background-color: #fabd0d;bottom: 10px;position: absolute;width:93%" class="btn rounded has-ripple" name="slog" value="1">e-Daftar<span class="ripple ripple-animate" style="height: 290px; width: 290px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(55, 58, 60) none repeat scroll 0% 0%; opacity: 0.4; top: -128.267px; left: 14px;"></span></a> -->
            </div>
        </div>

    </div>
    <br>
    <br>
    <h5 class="">@lang('homepage.service-section')</h5>
    <hr>

<!--     <div class="row">
        <div class="row swiper-container">
            <div class="swiper-wrapper" style="padding-bottom:10px !important;">
            <?php 
                
                $bgcolor1 = '#800078de';
                $bgcolor2 = '#0000ff8f';
                $bgcolor3 = '#a68b8b';
                $bgcolor4 = '#b1b111';

                $count = 0;


            ?>

            @foreach($list as $key=> $app)
                <?php 

                    $bgc = '';
                    $count = $count+1;
                    if($count == 12)
                    {
                        $count = 1;
                    }

                    if($count == 1){$bgc =  $bgcolor1;}
                    if($count == 2){$bgc =  $bgcolor2;}
                    if($count == 3){$bgc =  $bgcolor3;}
                    if($count == 4){$bgc =  $bgcolor4;}
                    if($count == 5){$bgc =  $bgcolor4;}
                    if($count == 6){$bgc =  $bgcolor3;}
                    if($count == 7){$bgc =  $bgcolor2;}
                    if($count == 8){$bgc =  $bgcolor1;}
                    if($count == 9){$bgc =  $bgcolor2;}
                    if($count == 11){$bgc =  $bgcolor3;}
                    if($count == 10){$bgc =  $bgcolor4;}

                ?>
                    <div class="swiper-slide">
                            <span class="img-radius shadows img-fluid m-20 p-10 text-center" onclick="javascript:loadapp({{$app->id}})" style="cursor:pointer;display: flex;justify-content: center; align-items: center;box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 8px {{$bgc}} solid;height:100px;width:100px;background-image: url({{asset('themes/ablepro/assets/images/circle.jpg')}});background-size: cover;"><b>
                                {{$app->service_bm}}
                        </b>

                    </span>
                    </div>
                @endforeach
            </div>
        </div>
    </div> -->

        <div class="row">
            
            <?php 
                
                $bgcolor1 = 'blue';
                $bgcolor2 = 'blue';
                $bgcolor3 = 'blue';
                $bgcolor4 = 'blue';

                $count = 0;


            ?>

            @foreach($list as $key=> $app)
                <?php 

                    $bgc = '';
                    $count = $count+1;
                    if($count == 12)
                    {
                        $count = 1;
                    }

                    if($count == 1){$bgc =  $bgcolor1;}
                    if($count == 2){$bgc =  $bgcolor2;}
                    if($count == 3){$bgc =  $bgcolor3;}
                    if($count == 4){$bgc =  $bgcolor4;}
                    if($count == 5){$bgc =  $bgcolor4;}
                    if($count == 6){$bgc =  $bgcolor3;}
                    if($count == 7){$bgc =  $bgcolor2;}
                    if($count == 8){$bgc =  $bgcolor1;}
                    if($count == 9){$bgc =  $bgcolor2;}
                    if($count == 11){$bgc =  $bgcolor3;}
                    if($count == 10){$bgc =  $bgcolor4;}

                ?>
                   

                            <div class="col-md-6 col-xl-3">
                                <div class="card order-card h-80 animation-toggle" data-animate="rubberBand" style="color:unset;cursor:pointer;" onclick="javascript:loadapp({{$app->id}})">
                                    <div class="card-body ">
                                        <h6 class="" style="font-size:14px;color:#377c97 !important">
                                            
                                            @if($locale == 'en')
                                                <td>{{$app->service_en}}</td>
                                            @else
                                                <td>{{$app->service_bm}}</td>
                                            @endif
                                            
                                        </h6>
                                       <!--  <p style="font-size:12px;">

                                            @if($locale == 'en')
                                                <td>{!! Str::words($app->description_en, 6, ' ...') !!}</td>
                                            @else
                                                <td>{!! Str::words($app->description_bm, 6, ' ...') !!}</td>
                                            @endif

                                        </p> -->
                                        <!-- <i class="card-icon feather icon-filter" style="color: #38374024;"></i> -->
                                         <div class="progress {{$bgc}}">
                                            <div class="progress-bar bg-c-{{$bgc}}" style="width:100%"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                @endforeach
                        <div class="modal fade rounded" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="loginmodal">  
                            
                            <div class="modal-dialog" style="width: 350px">
                                <div class="modal-content">
                                   <div id="exampleModalCenter" class="modal" aria-labelledby="exampleModalCenterTitle">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content" style="margin:20px">
                                            <br>
                                                <div class="d-flex justify-content-center" style="margin:20px !important"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>
                                                <div class="swal-title">Please Wait..</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-header" style="background-color: darkorange;color:white">

                                        <h5 class="f-w-400" style="color:white">
                                        <img src="{{asset('themes/ablepro/assets/images/rsz_lhdnlogo.png')}}" style="width: 50px" alt="" class="img-fluid" style="float:left">
                                            @lang('homepage.logintitle') 
                                            @if($idtype == 1) (@lang('homepage.loginsinput1')) @elseif ($idtype == 2) (@lang('homepage.loginsinput2')) @elseif ($idtype == 3) (@lang('homepage.loginsinput3')) @elseif ($idtype == 4) (@lang('homepage.loginsinput4'))@endif
                                        </h5>
                                    </div>
                                    <div class="modal-body" id="logindata" style="font-size: 13px !important">

                                                        {!! SemanticForm::post()->attribute('id', 'gene')  !!}
                                                        
                                                        <input name="slog" type="hidden" class="custom-control-input" value="1">
                                                            <div class=" auth-content">
                                                                
                                                                

                                                                @isset($error)
                                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                                            {{$error}}
                                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                            
                                                                        </div>
                                                                @endisset
                                                                
                                                                @isset($phrase)
                                                                    <input name="idtype" type="hidden" class="custom-control-input" value="{{$idtype}}">
                                                                    <input type="hidden" class="form-control" id="idenno" name="idenno" value="{{$uname}}">
                                                                        <div class="tab-pane active show" id="b-w-tab1" style="font-size: 14px !important;width:100%;">
                                                                            <div class="card" style="color:red;padding:10px;background-color: #fbffffe6;text-align: justify;text-justify: inter-word;"><span>@lang('homepage.phrasetext')</span></div>
                                                                        </div>
                                                                    <div class="text-center">
                                                                        <h3 class="mb-4 f-w-400">{{base64_decode($phrase)}}</h3>
                                                                    </div>

                                                                    <br>
                                                                    <p>@lang('homepage.logincont')</p>
                                                                    <div class="form-group mb-4">
                                                                        <label class="floating-label" for="idenpass">@lang('homepage.loginsinput6')</label>
                                                                        <input type="password" class="form-control" id="idenpass" name="idenpass" placeholder="" required="">
                                                                    </div>
                                                                     <div class="form-group mb-4 btn-block btn-group ">
                                                                    <a type="button" class="btn btn-info m-2 rounded" href="/login">@lang('form.back')</a>
                                                                    <button type="submit" class="btn btn-primary m-2 rounded" name="slog" value="2">@lang('form.send')</button>
                                                                    </div>
                                                                @else
                                                                <div class="form-group text-left">
                                                                    <div class="custom-controls-stacked">
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input" required="" value="1" oninvalid="this.setCustomValidity('@lang('auth.reftype')')" oninput="this.setCustomValidity('')">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput1')</span>
                                                                        </label>
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input"  value="2">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput2')</span>
                                                                        </label>
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input"  value="3">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput3')</span>
                                                                        </label>
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input"  value="4">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput4')</span>
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <br>
                                                                <div class="form-group mb-3">
                                                                    <label class="floating-label" for="idenno">@lang('homepage.loginsinput5')</label>
                                                                    <input type="text" class="form-control" id="idenno" name="idenno" placeholder="" required  oninvalid="this.setCustomValidity('@lang('auth.refid')')" oninput="this.setCustomValidity('')">
                                                                </div>
                                                                <button type="submit" class="btn btn-block btn-primary mb-4 rounded btn-sm" name="slog" value="1">@lang('form.send')</button>
                                                                @endisset
                                                                
                                                                {!! Form::close() !!}
                                                                <div class="text-center">
                                                                    <div class="saprator my-4"><span>@lang('homepage.loginchoice')</span></div>
                                                                  
                                                                    <p class="mb-2 mt-4 text-muted"><a href="{{env('FIRSTTIME_URL')}}" class="f-w-400">@lang('homepage.loginfirst')</a></p>
                                                                    <p class="mb-0 text-muted"><a href="{{env('FORGOT_URL')}}" class="f-w-400">@lang('homepage.loginforgot')</a></p>
                                                                </div>
                                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>

@endsection
@push('script')
<script type="text/javascript">
$(document).ready(function() {


var swiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      spaceBetween: 10,
      loop: true, 
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        360: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        640: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 6,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 8,
          spaceBetween: 10,
        },
      }
    });

});

</script>
<script type="text/javascript">
    
function openlogin()
{
     $("#exampleModalCenter").modal('hide');
     $("#loginmodal").modal('show');
    
     
}

</script>
<script>
    $(document).ready(function () {

        var error = {{$errors}};
        if(error > 0)
        {
            $("#loginmodal").modal('show');
        }

        
        $("#gene").submit(function (e) {

            $("#exampleModalCenter").modal('hide');
                         

            e.preventDefault();

            var values = $(this).serialize();
            $.ajax({
                    url: "{{ URL::to('user/loginmodal')}}",
                    type: "post",
                    data: values ,
                    beforeSend: function () 
                    {
                        $("#exampleModalCenter").modal('show');
                   
                    },
                    success: function(html)
                    {       
                        if(html.success)
                          {  
                              window.location = html.url
                          }
                          // document.getElementById('nhtml2').style.display = 'none';
                          $("#exampleModalCenter").modal('hide');
                          $('#logindata').html(html);
                          // window.location.replace("{{ URL::to('ad/index')}}");
                    }


                });
                                 

        });
    });
</script>

@endpush
