@extends('ui::ablepro.mobile-login')
@include('ui::ablepro.mobilemenu')
@section('content')
 <?php 

    $locale = App::getLocale();
?>
<div class="page-header " style="font-size: 14px;position: sticky;margin-top: 80px;">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12" style="padding: 10px;margin-top: -50px;">
            <div class="position-relative d-inline-block text-center" style="margin-top: 0px;">
                    <img class="img-radius img-fluid wid-100" src="{{asset('themes/ablepro/assets/images/logo2.jpg')}}" alt="User image" style="box-shadow: 0 2px 10px -1px rgba(69, 90, 100, 0.3);border: 3px #7fbff7 solid;background-color: white">

                    
                </div>
                
               

            </div>
        </div>
    </div>
</div>
<br>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;overflow-y:scroll;overflow-x: hidden;   width:100%;">
        <div class="auth-content">
        <div class="auth-bg">
            <span class="r"></span>
            <span class="r s"></span>
            <span class="r s"></span>
            <span class="r"></span>
        </div>
        <div class="card" style="background-color: #fbffffe6">
            <div class="card-body text-center">
                <h5 class="" id="names">@lang('homepage.wellcome')</h5>
                <!-- <img src="{{asset('themes/ablepro/assets/images/logo1.jpg')}}" width="150px" class="img-radius" alt="User-Profile-Image"> -->
                {!! SemanticForm::post(route('usermobile.log')) !!}

                @isset($error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{$error}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                @endisset

                <hr>
                 <div class="" id="divweblink" style="display:none">
                <a id="weblink" class="media-body" >
                     <img src="{{asset('themes/ablepro/assets/images/thumb.png')}}" alt="user image" class="img-radius wid-60 align-top m-r-15 m-l-5">
                    <p style="font-size: 12px !important" class="m-b-0"> Login with device</p>
                </a>
                <br>
                <br>
                </div>
                 @isset($phrase)
                    <input name="idtype" type="hidden" class="custom-control-input" value="{{$idtype}}">
                    <input type="hidden" class="form-control" id="idenno" name="idenno" value="{{$uname}}">
                        <div class="tab-pane active show" id="b-w-tab1" style="font-size: 14px !important;width:100%;">
                            <div class="card" style="color:red;padding:10px;background-color: #fbffffe6;text-align: justify;text-justify: inter-word;"><span>@lang('homepage.phrasetext')</span></div>
                        </div>
                    <div class="text-center">
                        <h3 class="mb-4 f-w-400">{{base64_decode($phrase)}}</h3>
                    </div>

                    <br>
                    <p>@lang('homepage.logincont')</p>
                    <div class="form-group mb-4">
                        <label class="floating-label" for="idenpass">@lang('homepage.loginsinput6')</label>
                        <input type="password" class="form-control" id="idenpass" name="idenpass" placeholder="" required="">
                    </div>
                     <div class="form-group mb-4 btn-block btn-group ">
                    <a type="button" class="btn btn-info m-2 rounded" href="/mobile">@lang('form.back')</a>
                    <button type="submit" style="text-shadow: 2px 2px 4px white;font-weight: bolder;background-color: #fabd0d;" class="btn btn-primary m-2 rounded" name="slog" value="2">@lang('form.send')</button>
                    </div>
                @else
                <div class="form-group text-left">
                    <div class="custom-controls-stacked">
                        <label class="custom-control custom-radio">
                            <input name="idtype" type="radio" class="custom-control-input" required="" value="1">
                            <span class="custom-control-label">@lang('homepage.loginsinput1')</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="idtype" type="radio" class="custom-control-input" required="" value="2">
                            <span class="custom-control-label">@lang('homepage.loginsinput2')</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="idtype" type="radio" class="custom-control-input" required="" value="3">
                            <span class="custom-control-label">@lang('homepage.loginsinput3')</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input name="idtype" type="radio" class="custom-control-input" required="" value="4">
                            <span class="custom-control-label">@lang('homepage.loginsinput4')</span>
                        </label>
                    </div>
                </div>
                <br>
                 <div class="form-group mb-3">
                    <input type="text" class="form-control" id="idenno" name="idenno" placeholder="No Pengenalan" required>
                </div>
                                
                <button type="submit" style="text-shadow: 2px 2px 4px white;font-weight: bolder;background-color: #fabd0d;" class="btn btn-block mb-4 rounded" name="slog" value="1">@lang('form.send')</button>

                @endisset
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
</div>
<div class="bt-wizard text-center" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px;position: sticky;">
    <p class="text-center" style="color:#00867b;padding:10px"><b>@lang('homepage.mobile-app')</b></p>
</div>
<div class="tab-content text-left " >
    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 12px !important;height:70vh;overflow-y:scroll;   width:100%;">
        <div class="row" style="margin:1px">
            @foreach($mobile as $key => $value)
                <?php if($device == 'IOS')
                    {
                        $url = $value->ios_url;
                    }else{
                        $url = $value->and_url;
                    }

              ?>

                <div class="col-sm-3 col-4 col-md-2 text-center" style="cursor:pointer;padding-top: 10px;" onclick="location.href='{{$url}}'">
                    @if($value->icon_url) <img src="/storage/appicon/{{$value->icon_url}}" alt="user image" class="latest-posts-img" height="80px" width="80px"> @else <img src="{{asset('themes/ablepro/assets/images/logo2.jpg')}}" alt="user image" class="latest-posts-img" height="80px" width="80px"> @endif
                    <br>
                   @if($locale == 'en')
                       {{$value->mobile_module_name_en}}                
                    @else
                       {{$value->mobile_module_name_bm}}                
                    @endif
                </div>

                


                
            @endforeach
        </div>
    </div>
</div>



@endsection
@push('script')
<script src="{!! secure_asset('vendor/webauthn/webauthn.js') !!}"></script>
<script type="text/javascript">
    
    $(document).ready(function () {

        if(localStorage.name) 
        {
            document.getElementById("names").innerHTML = localStorage.name;
        }

        if(localStorage.devid) 
        {
            
            var a = document.getElementById('weblink'); 
            a.href = "/mobile/webauth/"+localStorage.devid;

            var webauthn = new WebAuthn((name, message) => {
               error(errorMessage(name, message));
            });

            if (! webauthn.webAuthnSupport()) {
              switch (webauthn.notSupportedMessage()) {
                case 'not_secured':
                  error(errors.not_secured);
                  break;
                case 'not_supported':
                  error(errors.not_supported);
                  break;
              }
            }else 
            {
                document.getElementById("divweblink").style.display = "block";
            }
            

        }

});
</script>



@endpush