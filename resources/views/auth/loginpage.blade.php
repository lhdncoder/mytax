@extends('ui::ablepro.loginpage')

@section('content')
    <?php 

    $locale = App::getLocale();
         $activems = '';
         $activeen = '';


    ?>
    <div class="row">
            <div class="col-md-12 col-lg-8 ">
            <div class="card h-100">
                <div class="card-body home " id="home">
                    @if(!$banner->isEmpty())
                    <?php $count = 1;?>
            
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="margin-top:7%">
                        
                        <div class="carousel-inner text-center" style="height: 300px">
                         @foreach($banner as $key => $value)
                            <div class="carousel-item @if($count == 1) active @endif">
                                <img class="shadow" src="/storage/banner/{{$value->banner_img}}" alt="First slide" style="width: 100%;height:300px;object-fit: contain;">
                            </div>
                         <?php $count = $count+1 ;?>
                         @endforeach
                           
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" class="shadow" style="width:5%"><span class="" aria-hidden="true" style="color:grey;font-size: 30px;font-weight: 800"> <i class="fas fa-chevron-circle-left"></i> </span><span class="sr-only">Previous</span></a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="width:5%"><span class="" aria-hidden="true" style="color:grey;font-size: 30px;font-weight: 800"> <i class="fas fa-chevron-circle-right "></i> </span><span class="sr-only">Next</span></a>
                    </div>
               
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4 ">
            <div class="card h-100">
                <div class="card-body">
                    <div class="auth-side-form h-100">
                    {!! SemanticForm::post(route('user.log')) !!}
                        <div class=" auth-content">
                            <img src="{{asset('themes/ablepro/assets/images/lhdnlogo.png')}}" style="width: 60px" alt="" class="img-fluid mb-4 d-block d-xl-none d-lg-none">
                            <h3 class="mb-4 f-w-400">@lang('homepage.logintitle') 
                            @if($idtype == 1) (@lang('homepage.loginsinput1')) @elseif ($idtype == 2) (@lang('homepage.loginsinput2')) @elseif ($idtype == 3) (@lang('homepage.loginsinput3')) @elseif ($idtype == 4) (@lang('homepage.loginsinput4'))@endif

                           

                            </h3>

                            @isset($error)
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        {{$error}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        
                                    </div>
                            @endisset
                                            <hr>
                            @isset($phrase)
                                <input name="idtype" type="hidden" class="custom-control-input" value="{{$idtype}}">
                                <input type="hidden" class="form-control" id="idenno" name="idenno" value="{{$uname}}">
                                    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 14px !important;width:100%;">
                                        <div class="card" style="color:red;padding:10px;background-color: #fbffffe6;text-align: justify;text-justify: inter-word;"><span>@lang('homepage.phrasetext')</span></div>
                                    </div>
                                <div class="text-center">
                                    <h3 class="mb-4 f-w-400">{{base64_decode($phrase)}}</h3>
                                </div>

                                <br>
                                <p>@lang('homepage.logincont')</p>
                                <div class="form-group mb-4">
                                    <label class="floating-label" for="idenpass">@lang('homepage.loginsinput6')</label>
                                    <input type="password" class="form-control" id="idenpass" name="idenpass" placeholder="" required="">
                                </div>
                                 <div class="form-group mb-4 btn-block btn-group ">
                                <a type="button" class="btn btn-info m-2 rounded" href="/login">@lang('form.back')</a>
                                <button type="submit" class="btn btn-primary m-2 rounded" name="slog" value="2">@lang('form.send')</button>
                                </div>
                            @else
                            <input name="idtype" type="hidden" class="custom-control-input" value="{{$idtype}}">
                            <br>

                            <div class="form-group mb-3">
                                <label class="floating-label" for="idenno">@lang('homepage.loginsinput5')</label>
                                <input type="text" class="form-control" id="idenno" name="idenno" placeholder="" required>
                            </div>
                            <button type="submit" class="btn btn-block btn-primary mb-4 rounded" name="slog" value="1">@lang('form.send')</button>
                            @endisset
                            
                            {!! Form::close() !!}
                            <div class="text-center">
                                <div class="saprator my-4"><span>@lang('homepage.loginchoice')</span></div>
                              
                                <p class="mb-2 mt-4 text-muted"><a href="{{env('FIRSTTIME_URL')}}" class="f-w-400">@lang('homepage.loginfirst')</a></p>
                                <p class="mb-0 text-muted"><a href="{{env('FORGOT_URL')}}" class="f-w-400">@lang('homepage.loginforgot')</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@endsection