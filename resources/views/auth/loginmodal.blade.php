    <?php 

    $locale = App::getLocale();
         $activems = '';
         $activeen = '';


    ?>


    {!! SemanticForm::post(route('user.loginmodal')) !!}
        <div class=" auth-content">
            @isset($error)
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{$error}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        
                    </div>
            @endisset
            @isset($phrase)
                <input name="idtype" type="hidden" class="custom-control-input" value="{{$idtype}}">
                <input type="hidden" class="form-control" id="idenno" name="idenno" value="{{$uname}}">
                    <div class="tab-pane active show" id="b-w-tab1" style="font-size: 14px !important;width:100%;">
                        <div class="card" style="font-size:12px;color:red;padding:10px;background-color: #fbffffe6;text-align: justify;text-justify: inter-word;"><span>@lang('homepage.phrasetext')</span></div>
                    </div>
                <div class="text-center">
                    <h3 class="f-w-400">{{base64_decode($phrase)}}</h3>
                </div>

                <br>
                <p style="margin-bottom: unset">@lang('homepage.logincont')</p>
                <div class="form-group">
                    <!-- <label class="floating-label" for="idenpass">@lang('homepage.loginsinput6')</label> -->
                    <input type="password" class="form-control" id="idenpass" name="idenpass" placeholder="" required="">
                </div>
                 <div class="form-group mb-4 btn-block btn-group ">
                <a type="button" class="btn btn-info m-2 rounded btn-sm" href="/">@lang('form.back')</a>
                <button type="submit" class="btn btn-primary m-2 rounded btn-sm" name="slog" value="2">@lang('form.send')</button>
                </div>
            @else
            <div class="form-group text-left">
                <div class="custom-controls-stacked">
                    <label class="custom-control custom-radio">
                        <input name="idtype" type="radio" class="custom-control-input" required="" value="1" oninvalid="this.setCustomValidity('@lang('auth.reftype')')" oninput="this.setCustomValidity('')">
                        <span class="custom-control-label">@lang('homepage.loginsinput1')</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input name="idtype" type="radio" class="custom-control-input"  value="2">
                        <span class="custom-control-label">@lang('homepage.loginsinput2')</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input name="idtype" type="radio" class="custom-control-input"  value="3">
                        <span class="custom-control-label">@lang('homepage.loginsinput3')</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input name="idtype" type="radio" class="custom-control-input"  value="4">
                        <span class="custom-control-label">@lang('homepage.loginsinput4')</span>
                    </label>
                </div>
            </div>

            <br>
            <div class="form-group mb-3">
                <label class="floating-label" for="idenno">@lang('homepage.loginsinput5')</label>
                <input type="text" class="form-control" id="idenno" name="idenno" placeholder="" required  oninvalid="this.setCustomValidity('@lang('auth.refid')')" oninput="this.setCustomValidity('')">
            </div>
            <button type="submit" class="btn btn-block btn-primary mb-4 rounded" name="slog" value="1">@lang('form.send')</button>
            @endisset
            
            {!! Form::close() !!}
            <div class="text-center">
                <div class="saprator"><span>@lang('homepage.loginchoice')</span></div>
              
                <p class="mb-2 text-muted"><a href="{{env('FIRSTTIME_URL')}}" class="f-w-400">@lang('homepage.loginfirst')</a></p>
                <p class="mb-0 text-muted"><a href="{{env('FORGOT_URL')}}" class="f-w-400">@lang('homepage.loginforgot')</a></p>
            </div>
        </div>

