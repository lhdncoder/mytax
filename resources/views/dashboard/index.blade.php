@extends('layout.dash')
@section('title', 'Site-1')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/charts-c3/plugin.css')}}" />
<link rel="stylesheet" href="{{asset('assets/plugins/morrisjs/morris.min.css')}}" />
@stop

@section('content')


<div class="row clearfix">
    <div class="col-md-12">

            <div class="card mcard_2" style="margin-bottom: -10px;">

                <div class="img">
                 

                </div>
                 <span class="text-muted" style="padding-left: 120px"></span>

                <div class="body shadow" style="margin-top: 30px;">
                    
                    <div class="user" style="margin-bottom: unset !important">
                        <img src="{{asset('assets/images/index.png')}}" class="rounded-circle img-raised" alt="profile-image">

                        <div class="details">
                            <br>
                            <span class="text-muted">Selamat datang ke MyTax</span>
                        </div>
                    </div>
                    <ul class="nav nav-tabs p-0 mb-3 nav-tabs-warning shadow" style="float:right;border-radius: inherit;background-color: #b7c6b54f">
                        <li class="nav-item" data-toggle="tooltip" data-placement="top" title="Kembali Ke Halaman Utama"><a class="nav-link" data-toggle="tab" href="#home_only_icon_title"><i class="zmdi zmdi-home"></i></a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile_only_icon_title"><i class="zmdi zmdi-account"></i></a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages_only_icon_title"><i class="zmdi zmdi-email"></i></a></li>
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#settings_only_icon_title"><i class="zmdi zmdi-settings"></i></a></li>
                    </ul>
                    
                   
                   
                </div>
            </div>
            <div class="row clearfix">
               

                <div class="col-lg-6 ">
                    <div class="card">

                        <div class="body shadow">
                            <div id="chart-area-spline-sracked" class="c3_chart d_sales"></div>
                        </div>
                    </div>

                </div>
                 <div class="col-lg-3 " >
                    <div class="card mcard_1 shadow">
                        <div class="img">
                            <img src="{{asset('assets/images/image-gallery/2.jpg')}}" class="img-fluid" alt="">
                        </div>
                        <div class="body">
                            <div class="user">
                                <img src="{{asset('assets/images/index.png')}}" class="rounded-circle img-raised" alt="profile-image">
                                <h5 class="mt-3 mb-1">Muhamad Fezrul Fizree Hashim</h5>
                                <span>INDIVIDU</span>                                
                            </div>
                           
                            <div class="d-flex bd-highlight text-center mt-4">
                               
                                <div class="flex-fill bd-highlight">
                                    <h5 class="mb-0">RM 0.00</h5>
                                    <small>Baki Cukai (e-Lejar)</small>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                 </div>
                
                 <div class="col-lg-3 " >
                    <div class="card shadow">
                        <div class="alert alert-success">
                    <strong>Tahniah,</strong> Tiada Sekatan Untuk Keluar Negara.
                </div>
                        <div class="body todo_list shadow">
                            <div class="input-group mb-4">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Pengumuman</button>
                                </div>
                            </div>                            
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Tarikh Akhir bagi cukai taksiran 
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Ralat : Penngunaan sistem MyTax
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Borang Pembayaran Cukai : Adala...
                                    <span class="badge badge-primary badge-pill">x</span>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
    </div>
     <div class="col-md-12">
     <div class="card" style="margin-bottom: unset;">
            <div class="header">
                <h2><strong>Senarai Aplikasi</strong> Dalam Talian</h2>
               
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">

        <div class="card widget_2 big_icon traffic shadow">
            <div class="body">
                <h6>e-Filing</h6>
                <h2></h2>
                <small>Kaedah mengisi dan menghantar Borang Nyata Cukai Pendapatan (BNCP) secara elektronik</small>
                <div class="progress">
                    <div class="progress-bar l-amber" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card widget_2 big_icon sales shadow">
            <div class="body">
                <h6>ByrHASiL</h6>
                <h2></h2>
                <small>ByrHASiL adalah aplikasi elektronik untuk pembayaran cukai pendapatan melalui bank-bank yang dilantik</small>
                <div class="progress">
                    <div class="progress-bar l-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card widget_2 big_icon email shadow">
            <div class="body">
                <h6>e-Daftar</h6>
                <h2></h2>
                <small>e-Daftar fail cukai untuk pembayar cukai baharu mendapatkan nombor cukai pendapatan </small>
                <div class="progress">
                    <div class="progress-bar l-purple" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card widget_2 big_icon domains shadow">
            <div class="body">
                <h6>e-Lejar </h6>
                <h2></h2>
                <small>e-Lejar ialah satu kemudahan yang disediakan untuk pembayar cukai menyemak...</small>
                <div class="progress">
                    <div class="progress-bar l-green" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row clearfix">
   
</div>

@stop
@section('page-script')
<script src="{{asset('assets/bundles/jvectormap.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/c3.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/index.js')}}"></script>
@stop