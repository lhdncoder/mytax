@extends('ui::ablepro.dashboard')

@section('content')

 <div class="col-lg-12 col-md-12">
    <div class="card table-card review-card">
        <div class="card-header borderless ">
            <h5>@lang('homepage.announcement')</h5>
            <div class="card-header-right">
                <div class="btn-group card-option">
                   <!--  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="feather icon-more-horizontal"></i>
                    </button> -->
                   <!--  <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                    </ul> -->
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="review-block contents" id="contents">
            @forelse($list as $key => $data)
                <div class="row">
                   
                    <div class="col" style="cursor:pointer;" onclick="javascript:loadann({{$data->id}})">
                         @if($user->language == 'en')
                         <h6 class="m-b-15">{{$data->announcement_en}} <span class="float-right f-13 text-muted"> {{date("d/m/Y", strtotime($data->start_date))}}</span></h6>
                         Read..

                         @else
                         <h6 class="m-b-15">{{$data->announcement_bm}} <span class="float-right f-13 text-muted"> {{date("d/m/Y", strtotime($data->start_date))}}</span></h6>
                         Baca..
                         @endif
                        
                       
                    </div>
                </div>
            @empty
            <form class="text-center">
                <i class="feather icon-check-circle display-3 text-success"></i>
                <h5 class="mt-3">@lang('inbox.empty')</h5>
                <p>@lang('inbox.nodata')</p>
            </form>
            @endforelse
               
            </div>
             <div class="review-block home" id="home">
             </div>
          
        </div>
    </div>
</div>

@endsection
@push('script')
<script type="text/javascript">

$(document).ready(function() {

    var id = {{$ids}};

    if(id > 0)
    {
        loadann(id);
    }


});
  
  function loadann(id) {

    $.ajax({

            type: "GET", 
            url: "{{ URL::to('dashboard/announcementread')}}"+"/"+id,
                   
            beforeSend: function () 
            {
                
            },
            success: function(data)
            {       
                var x = document.getElementById("home");
                var y = document.getElementById("contents");
              
              
                x.style.display = "block";
                y.style.display = "none";
              
               $('#home').html(data);
            }


        });

   

    
  }
    
</script>

<script type="text/javascript">
  
  function loadall() {

    var x = document.getElementById("home");
    var y = document.getElementById("contents");
  
  
    x.style.display = "none";
    y.style.display = "block";
  
   $('#home').html('');

    
  }
    
</script>

@endpush