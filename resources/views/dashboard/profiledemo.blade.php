@extends('ui::ablepro.dashboarddemo')

@section('content')
<div class="card new-cust-card">
    <div class="card-header">
        <h5>Sijil Digital Individu</h5>
    </div>
    <div class="ps " style="">
        <div class="card-body p-b-0">
            <div class="row">
                <div class="col-md-4 text-center">
                    <div class="change-profile text-center">
                        <div class="w-auto ">
                                <div class="profile-dp">
                                    <button type="button" class="btn btn-icon btn-success has-ripple"><i class="feather icon-check-circle"></i><span class="ripple ripple-animate" style="top: 24.3167px; left: 28.3833px;"></span></button>
                                </div>
                               <br>
                           
                        </div>
                    </div>
                    <h5 class="mb-1">@if($profile->tax_restrain == 0) SAH @else TAMAT @endif</h5>
                    <p class="mb-2 text-muted">STATUS SIJIL</p>
                </div>
                <div class="col-md-8 mt-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="media">
                            <br>
                                <!-- <i class="fas fa-certificate text-c-blue bg-icon mr-2 mt-1 f-18"></i> -->
                                <div class="media-body">
                                
                                    <p class="mb-0 text-muted"><h4>Tempoh Sah Sijil : </h4></p>
                                    <p class="mb-0 text-muted"><b>{{  str_replace("-"," Hingga ",$profile->tax_cert_status) }}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                <div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card">
                            <div class="card-body d-flex align-items-center justify-content-between">
                                <h5 class="mb-0">Maklumat Asas</h5>
                                <button type="button" class="btn btn-primary btn-sm rounded m-0 float-right" data-toggle="collapse" data-target=".pro-det-edit" aria-expanded="false" aria-controls="pro-det-edit-1 pro-det-edit-2">
                                    <i class="feather icon-edit"></i>
                                </button>
                            </div>
                            <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
                                <form>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label font-weight-bolder">Nama</label>
                                        <div class="col-sm-9">
                                            {{$user->name}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label font-weight-bolder">No. Rujukan</label>
                                        <div class="col-sm-9">
                                            {{$user->reference_id}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label font-weight-bolder">No. Cukai Pendapatan</label>
                                        <div class="col-sm-9">
                                            {{$user->tax_no}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label font-weight-bolder">No. Telefon</label>
                                        <div class="col-sm-9">
                                            {{$profile->homephone_no}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label font-weight-bolder">No. Telefon Bimbit</label>
                                        <div class="col-sm-9">
                                            {{$profile->handphone_no}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label font-weight-bolder">Alamat Email</label>
                                        <div class="col-sm-9">
                                            {{$profile->email}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label font-weight-bolder">Alamat</label>
                                        <div class="col-sm-4">
                                            {{$profile->address}}
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-body border-top pro-det-edit collapse " id="pro-det-edit-2">
                                dalam penyelengaraan
                            </div>
                        </div>
                    </div>

@endsection
