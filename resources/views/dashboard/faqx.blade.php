@extends('ui::ablepro.login')

@section('content')
    <?php 

    $locale = App::getLocale();
         $activems = '';
         $activeen = '';

    $errors = '0';

    if(isset($error))
    {
        $errors = '1';
    }


    ?>
    <style type="text/css">
        div.editable {
    width: 300px;
    height: 200px;
    border: 1px solid #ccc;
    padding: 5px;
    
}

strong {
  font-weight: bold;
}
    </style>
    <div class="row">
        <div class="col-md-12 col-lg-12">
                <h5 class="mb-3"> @lang('homepage.faq')</h5>
    <hr>
     <div class="accordion" id="accordionExample">

    @forelse($listfaq as $key =>$faq)

        <div class="card mb-0">
            <div class="card-header" id="heading{{$faq->id}}" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                @if($locale == 'en')
                <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">{{$faq->question_name_en}}</a></h5>
                @else
                <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">{{$faq->question_name_bm}}</a></h5>
                @endif
             </div>
            <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                @foreach($faq->answer as $key => $answers)
                <div class="card-body">
                 @if($locale == 'ms')
                     
                     <div contenteditable="false" style="background-color: #E9ECEF;padding:20px;"><?php echo $answers->answer_name_bm ?></div>

                 @else
                     <div contenteditable="false" style="background-color: #E9ECEF;padding:20px;"><?php echo $answers->answer_name_en ?></div>
                 @endif
                </div>
                @endforeach
            </div>
        </div>


            @empty
    @endforelse
        </div>

    </div>
 <div class="modal fade rounded" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="loginmodal">  
                            
                            <div class="modal-dialog" style="width: 350px">
                                <div class="modal-content">
                                   <div id="exampleModalCenter" class="modal" aria-labelledby="exampleModalCenterTitle">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content" style="margin:20px">
                                            <br>
                                                <div class="d-flex justify-content-center" style="margin:20px !important"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>
                                                <div class="swal-title">Please Wait..</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-header" style="background-color: darkorange;color:white">

                                        <h5 class="f-w-400" style="color:white">
                                        <img src="{{asset('themes/ablepro/assets/images/rsz_lhdnlogo.png')}}" style="width: 50px" alt="" class="img-fluid" style="float:left">
                                            @lang('homepage.logintitle') 
                                            @if($idtype == 1) (@lang('homepage.loginsinput1')) @elseif ($idtype == 2) (@lang('homepage.loginsinput2')) @elseif ($idtype == 3) (@lang('homepage.loginsinput3')) @elseif ($idtype == 4) (@lang('homepage.loginsinput4'))@endif
                                        </h5>
                                    </div>
                                    <div class="modal-body" id="logindata" style="font-size: 13px !important">

                                                        {!! SemanticForm::post()->attribute('id', 'gene')  !!}
                                                        
                                                        <input name="slog" type="hidden" class="custom-control-input" value="1">
                                                            <div class=" auth-content">
                                                                
                                                                

                                                                @isset($error)
                                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                                            {{$error}}
                                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                            
                                                                        </div>
                                                                @endisset
                                                                
                                                                @isset($phrase)
                                                                    <input name="idtype" type="hidden" class="custom-control-input" value="{{$idtype}}">
                                                                    <input type="hidden" class="form-control" id="idenno" name="idenno" value="{{$uname}}">
                                                                        <div class="tab-pane active show" id="b-w-tab1" style="font-size: 14px !important;width:100%;">
                                                                            <div class="card" style="color:red;padding:10px;background-color: #fbffffe6;text-align: justify;text-justify: inter-word;"><span>@lang('homepage.phrasetext')</span></div>
                                                                        </div>
                                                                    <div class="text-center">
                                                                        <h3 class="mb-4 f-w-400">{{base64_decode($phrase)}}</h3>
                                                                    </div>

                                                                    <br>
                                                                    <p>@lang('homepage.logincont')</p>
                                                                    <div class="form-group mb-4">
                                                                        <label class="floating-label" for="idenpass">@lang('homepage.loginsinput6')</label>
                                                                        <input type="password" class="form-control" id="idenpass" name="idenpass" placeholder="" required="">
                                                                    </div>
                                                                     <div class="form-group mb-4 btn-block btn-group ">
                                                                    <a type="button" class="btn btn-info m-2 rounded" href="/login">@lang('form.back')</a>
                                                                    <button type="submit" class="btn btn-primary m-2 rounded" name="slog" value="2">@lang('form.send')</button>
                                                                    </div>
                                                                @else
                                                                <div class="form-group text-left">
                                                                    <div class="custom-controls-stacked">
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input" required="" value="1" oninvalid="this.setCustomValidity('@lang('auth.reftype')')" oninput="this.setCustomValidity('')">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput1')</span>
                                                                        </label>
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input"  value="2">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput2')</span>
                                                                        </label>
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input"  value="3">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput3')</span>
                                                                        </label>
                                                                        <label class="custom-control custom-radio">
                                                                            <input name="idtype" type="radio" class="custom-control-input"  value="4">
                                                                            <span class="custom-control-label">@lang('homepage.loginsinput4')</span>
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <br>
                                                                <div class="form-group mb-3">
                                                                    <label class="floating-label" for="idenno">@lang('homepage.loginsinput5')</label>
                                                                    <input type="text" class="form-control" id="idenno" name="idenno" placeholder="" required  oninvalid="this.setCustomValidity('@lang('auth.refid')')" oninput="this.setCustomValidity('')">
                                                                </div>
                                                                <button type="submit" class="btn btn-block btn-primary mb-4 rounded btn-sm" name="slog" value="1">@lang('form.send')</button>
                                                                @endisset
                                                                
                                                                {!! Form::close() !!}
                                                                <div class="text-center">
                                                                    <div class="saprator my-4"><span>@lang('homepage.loginchoice')</span></div>
                                                                  
                                                                    <p class="mb-2 mt-4 text-muted"><a href="{{env('FIRSTTIME_URL')}}" class="f-w-400">@lang('homepage.loginfirst')</a></p>
                                                                    <p class="mb-0 text-muted"><a href="{{env('FORGOT_URL')}}" class="f-w-400">@lang('homepage.loginforgot')</a></p>
                                                                </div>
                                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>

@endsection
@push('script')
<script type="text/javascript">
$(document).ready(function() {


var swiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      spaceBetween: 10,
      loop: true, 
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        360: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        640: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 6,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 8,
          spaceBetween: 10,
        },
      }
    });

});

</script>
<script type="text/javascript">
    
function openlogin()
{
     $("#exampleModalCenter").modal('hide');
     $("#loginmodal").modal('show');
    
     
}

</script>
<script>
    $(document).ready(function () {

        var error = {{$errors}};
        if(error > 0)
        {
            $("#loginmodal").modal('show');
        }

        
        $("#gene").submit(function (e) {

            $("#exampleModalCenter").modal('hide');
                         

            e.preventDefault();

            var values = $(this).serialize();
            $.ajax({
                    url: "{{ URL::to('user/loginmodal')}}",
                    type: "post",
                    data: values ,
                    beforeSend: function () 
                    {
                        $("#exampleModalCenter").modal('show');
                   
                    },
                    success: function(html)
                    {       
                          // document.getElementById('nhtml2').style.display = 'none';
                          $("#exampleModalCenter").modal('hide');
                          $('#logindata').html(html);
                          // window.location.replace("{{ URL::to('ad/index')}}");
                    }


                });
                                 

        });
    });
</script>

@endpush
