<!DOCTYPE html>
<html lang="en">

<head>
    <title>Site-2</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
   
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('themes/ablepro/assets/images/favicon.ico')}}" type="image/x-icon">
    
    <!-- vendor css -->
    <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/plugins/swiper.min.css')}}">
           

</head>
<body class="" style="background-color: #2132b0">
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar menu-light ">
        <div class="navbar-wrapper  ">
            <div class="navbar-content scroll-div " >
                
                <div class="">
                    <div class="main-menu-header" style="height: unset">
                        <img class="" src="{{asset('themes/ablepro/assets/images/lhdnlogo.png')}}" alt="User-Profile-Image" style="width:60px">

                    </div>
                    
                </div>
                
                <ul class="nav pcoded-inner-navbar ">
                    <li class="nav-item pcoded-menu-caption">
                        <label>Menu Utama</label>
                    </li>
                    <li class="nav-item"><a href="index.html" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a></li>
                    
                    <li class="nav-item pcoded-hasmenu">
                        <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layout"></i></span><span class="pcoded-mtext">Perkhidmatan</span></a>
                        <ul class="pcoded-submenu">
                            
                            <li><a href="layout-horizontal.html" target="_blank">Horizontal</a></li>
                            <li><a href="layout-horizontal-2.html" target="_blank">Horizontal v2</a></li>
                            <li><a href="layout-horizontal-rtl.html" target="_blank">Horizontal RTL</a></li>
                            <li><a href="layout-box.html" target="_blank">Box layout</a></li>
                            <li><a href="layout-light.html" target="_blank">Navbar dark</a></li>
                            <li><a href="layout-dark.html" target="_blank">Dark layout <span class="pcoded-badge badge badge-danger">Hot</span></a></li>
                        </ul>
                    </li>
                    <li class="nav-item"><a href="" class="nav-link "><span class="pcoded-micon"><i class="feather icon-activity"></i></span><span class="pcoded-mtext">Bantuan</span></a></li>
                    <li class="nav-item">
                        <a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layers"></i></span><span class="pcoded-mtext">Soalan Lazim</span></a>
                        
                    </li>
                    

                </ul>
                
                <div class="card text-center">
                    <div class="card-block">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="feather icon-sunset f-40"></i>
                        <h6 class="mt-3">Bantuan Lanjut</h6>
                        <p>Terdapat Masalah dalam talian atau kemusykilan ? </p>
                        <a href="#!" target="_blank" class="btn btn-primary btn-sm text-white m-0">Sila Hubungi Kami</a>
                    </div>
                </div>
                
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->
    <!-- [ Header ] start -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
        
            
                <div class="m-header">
                    <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
                    <a href="#!" class="b-brand">
                        <!-- ========   change your logo hear   ============ -->
                        <img src="{{asset('themes/ablepro/assets/images/logo.png')}}" alt="" class="logo">
                        <img src="{{asset('themes/ablepro/assets/images/logo-icon.png')}}" alt="" class="logo-thumb">
                    </a>
                    <a href="#!" class="mob-toggler">
                        <i class="feather icon-more-vertical"></i>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a href="#!" class="pop-search"><i class="feather icon-search"></i></a>
                            <div class="search-bar">
                                <input type="text" class="form-control border-0 shadow-none" placeholder="Search hear">
                                <button type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li>
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
                                <div class="dropdown-menu dropdown-menu-right notification">
                                    <div class="noti-head">
                                        <h6 class="d-inline-block m-b-0">Notifications</h6>
                                        <div class="float-right">
                                            <a href="#!" class="m-r-10">mark as read</a>
                                            <a href="#!">clear all</a>
                                        </div>
                                    </div>
                                    <ul class="noti-body">
                                        <li class="n-title">
                                            <p class="m-b-0">NEW</p>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="{{asset('themes/ablepro/assets/images/user/avatar-1.jpg')}}" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>John Doe</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>5 min</span></p>
                                                    <p>New ticket Added</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="n-title">
                                            <p class="m-b-0">EARLIER</p>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="{{asset('themes/ablepro/assets/images/user/avatar-2.jpg')}}" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>10 min</span></p>
                                                    <p>Prchace New Theme and make payment</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="{{asset('themes/ablepro/assets/images/user/avatar-1.jpg')}}" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>Sara Soudein</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>12 min</span></p>
                                                    <p>currently login</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="{{asset('themes/ablepro/assets/images/user/avatar-2.jpg')}}" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                                    <p>Prchace New Theme and make payment</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="noti-footer">
                                        <a href="#!">show all</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown drp-user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-user"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-notification">
                                    <div class="pro-head">
                                        <img src="{{asset('themes/ablepro/assets/images/user/avatar-1.jpg')}}" class="img-radius" alt="User-Profile-Image">
                                        <span>John Doe</span>
                                        <a href="auth-signin.html" class="dud-logout" title="Logout">
                                            <i class="feather icon-log-out"></i>
                                        </a>
                                    </div>
                                    <ul class="pro-body">
                                        <li><a href="user-profile.html" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
                                        <li><a href="email_inbox.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>
                                        <li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Lock Screen</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                
            
    </header>
    <!-- [ Header ] end -->
    
    

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Selamat Datang, Fezrul</h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">

            <div class="col-xl-6 col-md-12">
                <div class="card user-card-full">
                    <div class="row m-l-0 m-r-0">
                        <div class="col-sm-4 user-profile-side" style="background-image: url('{{asset('themes/ablepro/assets/images/bg.png')}}')">
                            <div class="card-body text-center text-white">
                                <div class="m-b-25">
                                    <img src="{{asset('themes/ablepro/assets/images/lhdnlogo.png')}}" class="img-radius" alt="User-Profile-Image" width="70px">
                                </div>
                                <h6 class="f-w-600 text-white">MUHAMAD FEZRUL FIZREE HASHIM</h6>
                                <p>Jenis Akaun : INDIVIDU</p>
                                <!-- <a href="#!" class="text-white"><i class="feather icon-edit m-t-10 f-16"></i></a> -->
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="card-body">
                                <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Maklumat Akaun</h6>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600">Email</p>
                                        <h6 class="text-muted f-w-400"><a href="mailto:someone@example.com?Subject=Hello%20again" target="_top">jeny@gmail.com</a> </h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600">No Cukai</p>
                                        <h6 class="text-muted f-w-400">SG 9393388282</h6>
                                    </div>
                                     <div class="card bg-c-blue text-white text-center" style="margin-top:15px">
                                        <div class="card-body">
                                            <blockquote class="blockquote mb-0">
                                                <p>Tahniah, Anda tiada sekatan ke luar negara</p>
                                                <footer class="blockquote-footer text-white">
                                                    <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                                                </footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
                <div class="card latest-update-card">
                    <div class="card-header">
                        <h5>Pembayaran Lebihan</h5>
                        <div class="card-header-right">
                            <div class="btn-group card-option">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-more-horizontal"></i>
                                </button>
                                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                    <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                    <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="latest-update-box">
                            <div class="row p-t-30 p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline-flex">23-04-2020</p>
                                    <i class="feather icon-check bg-twitter update-icon"></i>
                                </div>
                                <div class="col">
                                    <a href="#!">
                                        <h6>Pembayaran Selesai</h6>
                                    </a>
                                    <p class="text-muted m-b-0">Pembayaran ke akaun anda telah selesai</p>
                                </div>
                            </div>
                            <div class="row p-b-30">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline-flex">17-04-2020</p>
                                    <i class="feather icon-check bg-c-red update-icon"></i>
                                </div>
                                <div class="col">
                                    <a href="#!">
                                        <h6>Dalam Proses</h6>
                                    </a>
                                    <p class="text-muted m-b-0">Pengagihan baki sedang di proses</p>
                                </div>
                            </div>
                            <div class="row p-b-0">
                                <div class="col-auto text-right update-meta">
                                    <p class="text-muted m-b-0 d-inline-flex">15-04-2020</p>
                                    <i class="feather icon-check bg-facebook update-icon"></i>
                                </div>
                                <div class="col">
                                    <a href="#!">
                                        <h6>Borang Di terima</h6>
                                    </a>
                                    <p class="text-muted m-b-10">Terima Kasih kerana menjalankan Tanggungjawab</p>

                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>


           
            <!-- Latest Customers end -->
        </div>

            <h5 class="">Menu</h5>
            <hr>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card social-card bg-c-red">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-mail f-34 text-c-red social-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-0 text-white">My Inbox</h6>
                                <p>0 Pesanan</p>
                                <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                            </div>
                        </div>
                    </div>
                    <a href="#!" class="download-icon"><i class="feather icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card social-card bg-c-blue">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-user f-34 text-c-blue social-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-0 text-white">My Profile</h6>
                                <p>.</p>
                                <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                            </div>
                        </div>
                    </div>
                    <a href="#!" class="download-icon"><i class="feather icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="col-xl-4 col-md-12">
                <div class="card social-card bg-c-green">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-book f-34 text-c-green social-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-0 text-white">My Tax Status</h6>
                                <p>0 Pending</p>
                                <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                            </div>
                        </div>
                    </div>
                    <a href="#!" class="download-icon"><i class="feather icon-arrow-down"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
             
        </div>

        <h5 class="">Senarai Perkhidmatan</h5>
            <hr>
        <div class="row swiper-container">
            

            <div class="swiper-wrapper" style="padding-bottom:10px !important">
                <div class="swiper-slide">
                    <div class="card bg-info text-white widget-visitor-card h-100">
                        <div class="card-body text-center">
                            <h5 class="text-white">e-Filing</h5>
                            <!-- <h6 class="text-white" style="font-size: 12px !important">Kaedah mengisi dan menghantar Borang Nyata Cukai Pendapatan (BNCP) secara elektronik</h6> -->
                            <i class="feather icon-package"></i>
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="card bg-info text-white widget-visitor-card h-100">
                        <div class="card-body text-center">
                            <h5 class="text-white">ByrHASiL</h5>
                           <!--  <h6 class="text-white" style="font-size: 12px !important">ByrHASiL adalah aplikasi elektronik untuk pembayaran cukai pendapatan melalui bank-bank yang dilantik</h6> -->
                            <i class="feather icon-package"></i>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card bg-info text-white widget-visitor-card h-100">
                        <div class="card-body text-center">
                            <h5 class="text-white">e-Daftar</h5>
                           <!--  <h6 class="text-white" style="font-size: 12px !important">e-Daftar adalah aplikasi permohonan pendaftaran fail cukai pendapatan untuk pembayar cukai baharu mendapatkan nombor cukai pendapatan </h6> -->
                            <i class="feather icon-package"></i>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card bg-info text-white widget-visitor-card h-100">
                        <div class="card-body text-center">
                            <h5 class="text-white">e-Data PCB</h5>
                            <!-- <h6 class="text-white">Daily user</h6> -->
                            <i class="feather icon-package"></i>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card bg-info text-white widget-visitor-card h-100">
                        <div class="card-body text-center">
                            <h5 class="text-white">e-Data Praisi </h5>
                            <!-- <h6 class="text-white">Daily user</h6> -->
                            <i class="feather icon-package"></i>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="card bg-info text-white widget-visitor-card h-100">
                        <div class="card-body text-center">
                            <h5 class="text-white">e-Filling CKHT / WHT</h5>
                            <!-- <h6 class="text-white">Daily user</h6> -->
                            <i class="feather icon-package"></i>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
<!--            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div> -->
        </div>


        <!-- [ Main Content ] end -->
    </div>
</div>

<!-- Button trigger modal -->


    <!-- Warning Section Ends -->

    <!-- Required Js -->
    <script src="{{asset('themes/ablepro/assets/js/vendor-all.min.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/plugins/bootstrap.min.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/ripple.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/pcoded.min.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/plugins/swiper.min.js')}}"></script>


<!-- Apex Chart -->
<script src="{{asset('themes/ablepro/assets/js/plugins/apexcharts.min.js')}}"></script>
<!-- custom-chart js -->
<script src="{{asset('themes/ablepro/assets/js/pages/dashboard-main.js')}}"></script>

<script type="text/javascript">


$(document).ready(function () {


});



    

</script>

<script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 10,
      loop: true, 
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
      }
    });
  </script>


</body>

</html>
