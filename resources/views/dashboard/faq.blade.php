@extends('ui::ablepro.dashboard')

@section('content')
  <div class="col-sm-12">
    <h5 class="mb-3"> @lang('homepage.faq')</h5>
    <hr>
     <div class="accordion" id="accordionExample">

    @forelse($listfaq as $key =>$faq)

        <div class="card mb-0">
            <div class="card-header" id="heading{{$faq->id}}" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                @if($user->language == 'en')
                <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">{{$faq->question_name_en}}</a></h5>
                @else
                <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">{{$faq->question_name_bm}}</a></h5>
                @endif
             </div>
            <div id="collapse{{$faq->id}}" class="collapse" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                @foreach($faq->answer as $key => $answers)
                <div class="card-body">
                    @if($user->language == 'en')
                    <div contenteditable="false" style="background-color: #E9ECEF;padding:20px;"><?php echo $answers->answer_name_en ?></div>
                    @else
                    <div contenteditable="false" style="background-color: #E9ECEF;padding:20px;"><?php echo $answers->answer_name_bm ?></div>
                    @endif
                </div>
                @endforeach
            </div>
        </div>




    @empty
    @endforelse

    </div>
</div>
        
@endsection
