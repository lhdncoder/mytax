@extends('ui::ablepro.dashboard')

@section('content')
<style type="text/css">

@media (max-width: 767px) {
    .cardss-size {
        margin-top: unset !important;
        margin-bottom: 130px;
    }
}

@media (min-width: 767px) {
    .cheight {
        height:217px;
    }
}

.tooltip {
  /*font-family: Georgia;*/
  font-size: 12px;
}
/*.tooltip .tooltip-inner {
  background-color: #ffc;
  color: #c00;
  min-width: 250px;
}*/


</style>
    <div class="row">

    <?php 
        $amount = (float)$profile->tax_balance;
        if($amount < 0)
        {
            $amount = $amount * -1;
            $lang = 1;

        }else if($amount == 0)
        {
            $lang = 2;
        }else{

            $lang = 3;
        }
    ?>
    <?php $sizz = '321';$mrgin = 'margin-top: 68px;' ?>
        <div class="col-xl-4 col-md-4 h-100">

            <div class="col-md-12"  style="padding:unset !important">
                <div class="card text-white widget-statstic-card" @if($lang == 13) style="cursor:pointer;padding:unset;" 
                    onclick="window.open('https://byrhasil.hasil.gov.my/one.php?ID={{$user->tax_no}}&amaun={{$amount}}&jenis={{$user->doc_type}}&ansuran=99&tahun=', '_blank');" @else style="padding:unset;" @endif>

                    <div class="card-header text-center" style="background-color:#4680ff;font-size: 13px"><b><i class="feather icon-info st-icon bg-c-yellow" data-toggle="tooltip" data-placement="top" title="@if($lang == 1) @lang('homepage.balance12') @elseif($lang == 2) @lang('homepage.balance22') @else @lang('homepage.balance32') @endif"></i>@if($lang == 1)
                            @lang('homepage.balance1')
                        @elseif($lang == 2)
                            @lang('homepage.balance2')
                        @else
                            @lang('homepage.balance3')
                        @endif</b></div>
                    
                    
                    <div class="card-body text-center" style="padding-top:11px;padding-bottom:10px;background-color: #f8f9fa;color:black">                  
                                            
                         <h2 style="@if($lang == 3) color:red; @endif">RM {{number_format($amount,2,'.',',')}}</h2>
                    </div>
                </div>
            </div>
            @if(isset($dataebe[0]->NOPENGENALAN))
                <?php 
                    if($user->language == 'ms')
                    {
                        $sizz = '300'; 
                        $mrgin = 'margin-top: 30px;' ;
                    }else
                    {
                        $sizz = '320'; 
                        $mrgin = 'margin-top: 50px;' ;
                    }
                     
                ?>
            @elseif(isset($dataebelogin[0]->NOPENGENALAN))
               
                <?php 
                    if($user->language == 'ms')
                    {
                        $sizz = '300'; 
                        $mrgin = 'margin-top: 30px;' ;
                    }else
                    {
                        $sizz = '320'; 
                        $mrgin = 'margin-top: 50px;' ;
                    }
                     
                ?>
            @else
                <?php 
                    if($user->language == 'ms')
                    {
                        $sizz = '300'; 
                        $mrgin = 'margin-top: 30px;' ;
                    }else
                    {
                        $sizz = '320'; 
                        $mrgin = 'margin-top: 50px;' ;
                    }
                     
                ?>
            @endif 
            <div class="col-md-12"  style="padding:unset !important;margin-top:50px;height:20px">
                <div class="card bg-c-blue text-white widget-statstic-card">
                   
                    <div class="card-body text-center" style="padding:unset !important;height:20px">
                   
                    </div>
                </div>
            </div>
            <div class="col-md-12 cardss-size"  style="height:15px;padding:unset !important;{{$mrgin}} ">
                <div class="card text-white widget-statstic-card" style="cursor:pointer;padding:unset;background-color:#f8f9fa; " onclick="location.href='/dashboard/taxstatus'">
                <div class="card-header text-center" style="background-color:#4680ff;font-size: 13px"><b><i class="feather icon-info st-icon bg-c-yellow" data-toggle="tooltip" data-placement="top" title="@if($user->language == 'ms') Jumlah bayaran balik @else Approved refund amount @endif"></i> @lang('homepage.refund')</b></div>
                
                    <div class="card-body text-center" style="padding-top:11px;padding-bottom:10px;">
                        <h2 class="text-black">RM {{number_format($refunds,2,'.',',')}}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-4 h-100">

<!--             <div class="card widget-statstic-card" >
                <div class="card-body">
                    <div class="card-header-left mb-3">
                        <h5 class="mb-0" style="font-size: 13px">@lang('homepage.restrain')</h5>
                    </div>

                    <i class="feather icon-info st-icon bg-c-yellow" data-toggle="tooltip" data-placement="top" title=""></i>
                    <div class="text-center">
                    <br>
                        @if($profile->tax_restrain == '0')
                      
                            <blockquote class="blockquote mb-0 card-body">
                               <button type="button" class="btn btn-icon btn-success has-ripple" style="cursor:none;width:30px;height:30px;font-size: 2.28rem"><i class="feather icon-check-circle"></i><span class="ripple ripple-animate"></span></button> <p><h5 style="font-size: 14px">@lang('homepage.restrain-message-false')</h5></p>
                                
                            </blockquote>
                        @else
                            <blockquote class="blockquote mb-0 card-body">
                               <button type="button" class="btn btn-icon btn-danger has-ripple" style="cursor:none;width:30px;height:30px;font-size: 2.28rem"><i class="feather icon-x-circle"></i><span class="ripple ripple-animate"></span></button> <p style="font-size: 14px">@lang('homepage.restrain-message-true')</p>
                                
                            </blockquote>
                        @endif

                      
                    </div>
                </div>
            </div> -->
            <div class="col-md-12"  style="padding:unset !important;">
                <div class="card text-white widget-statstic-card" style="padding:unset;background-color: #f8f9fa;color:black;min-height:  @if($user->access !== 2) 110px @else 375px @endif">
                 <div class="card-header text-center" style="background-color:#4680ff;font-size: 13px"><b><i class="feather icon-award st-icon w-120"></i> @lang('homepage.restrain')</b></div>
                @if($profile->tax_restrain == '0')
                    <i class="feather icon-check-circle st-icon bg-c-yellow" data-toggle="tooltip" data-placement="top" title=""></i>
                @else
                     <i class="feather icon-x-circle st-icon bg-c-yellow" data-toggle="tooltip" data-placement="top" title=""></i>
                @endif
                    <div class="card-body text-center">
                        @if($profile->tax_restrain == '0')
                            <h5 class="">@lang('homepage.restrain-message-false')</h5>
                        @else
                             <h5 class="">@lang('homepage.restrain-message-true')</h5>
                        @endif
                    </div>

                </div>
            </div>

            @if($user->access !== 2)
            <div class="col-md-12"  style="padding:unset !important;">
                <div class="card  text-white widget-statstic-card cheight" style="padding:unset;min-height: 110px;background-color: #F8F9FA">
                    <div class="card-header text-center" style="background-color:#4680ff;font-size: 13px"><b><i class="feather icon-info st-icon bg-c-yellow" data-toggle="tooltip" data-placement="top" title=""></i> @lang('homepage.beform')</b></div>
                    <div class="card-body text-center" style="margin-top: -10px;">
                        <div class="row">
                            <div class="col-md-6" style="padding:2px !important">
                            <h5>{{date('Y')-2}}</h5>
                               <div class="card-body text-left" style="background-color: #007Aff;border-radius: 5px;padding: 10px;margin: 0px;margin-bottom: 5px;height:95px">
                                     @if(isset($dataebe2[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                        @if($user->language == 'ms')
                                            Borang {{$dataebe2[0]->JENIS_BORANG}} {{date('Y')-2}} <br>telah dihantar pada {{date('d/m/Y', strtotime($dataebe2[0]->SUBMITDT)),}}
                                        @else

                                            {{$dataebe2[0]->JENIS_BORANG}} {{date('Y')-2}} form <br>was submitted on {{date('d/m/Y', strtotime($dataebe2[0]->SUBMITDT)),}}

                                        @endif

                                        </h6>
                                     @elseif(isset($dataebelogin2[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                            @if($user->language == 'ms')
                                               Borang {{$dataebelogin2[0]->JENIS_BORANG}} belum dihantar.<br> Borang diisi pada {{date('d/m/Y', strtotime($dataebelogin2[0]->tarikh_log)),}}
                                            @else

                                                {{$dataebelogin2[0]->JENIS_BORANG}} {{date('Y')-2}} form <br>not submitted yet. <br>Form was filled out on {{date('d/m/Y', strtotime($dataebelogin2[0]->tarikh_log)),}}
                                            @endif



                                        </h6>
                                     @else
                                        <h6 class="text-white" style="font-size: 14px">@lang('inbox.empty')</h6>
                                     @endif 
                                </div>
                            </div>
                            <div class="col-md-6" style="padding:2px !important">
                            <h5>{{date('Y')-1}}</h5>

                                 <div class="card-body text-left" style="background-color: #88B258;border-radius: 5px;padding: 10px;margin: 0px;margin-bottom: 5px;height:95px">
                                   
                                     @if(isset($dataebe[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                            @if($user->language == 'ms')
                                                Borang {{$dataebe[0]->JENIS_BORANG}} {{date('Y')-1}} <br>telah dihantar pada {{date('d/m/Y', strtotime($dataebe[0]->SUBMITDT)),}}
                                            @else

                                                {{$dataebe[0]->JENIS_BORANG}} {{date('Y')-1}} form <br>was submitted on {{date('d/m/Y', strtotime($dataebe[0]->SUBMITDT)),}}

                                            @endif

                                        </h6>
                                     @elseif(isset($dataebelogin[0]->NOPENGENALAN))
                                        <h6 class="text-white" style="font-size: 14px">

                                            @if($user->language == 'ms')
                                               Borang {{$dataebelogin[0]->JENIS_BORANG}} belum dihantar.<br> Borang diisi pada {{date('d/m/Y', strtotime($dataebelogin[0]->tarikh_log)),}}
                                            @else

                                                {{$dataebelogin[0]->JENIS_BORANG}} form not <br>submitted yet. Form <br>was filled out on {{date('d/m/Y', strtotime($dataebelogin[0]->tarikh_log)),}}
                                            @endif

                                         

                                        </h6>
                                     @else
                                        <h6 class="text-white" style="font-size: 14px">@lang('inbox.empty')</h6>
                                     @endif 
                                </div>

                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            @endif

            
        </div>
        <div class="col-md-4">

            <div class="card text-white bg-light">
                <div class="card-header" style="background-color:#4680ff;font-size: 13px"><b><i class="feather icon-award st-icon w-120"></i> @lang('homepage.graph-label')</b></div>
                 
                <div class="card-body">
                @if(isset($graph[0]))    
                    @if(count($graph[0]) == 3)

                        @if($browser == 'IE')

                            @push('script')
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                       
                                        var options = {
                                            chart: {
                                                height: <?php echo $sizz ?>,
                                                type: 'donut',
                                            },
                                            series: [{{$graph[0][0]->AMT}}, {{$graph[0][1]->AMT}}, {{$graph[0][2]->AMT}}],
                                            labels: ["{{$graph[0][0]->ASM_YEAR}}", "{{$graph[0][1]->ASM_YEAR}}", "{{$graph[0][2]->ASM_YEAR}}"],
                                            colors: ["#9CCC65", "#4680FF", "#FCD842"],
                                            legend: {
                                                show: true,
                                                position: 'bottom',
                                            },

                                            plotOptions: {
                                                pie: {
                                                    donut: {
                                                        labels: {
                                                            show: true,
                                                            name: {
                                                                show: true
                                                            },
                                                            value: {
                                                                show: true
                                                            }
                                                        },
                                                         size: '60%'
                                                    }
                                                }
                                            },
                                            tooltip: {
                                                    custom: function(value,labels) {

                                                     var totalg = ({{ $graph[0][0]->AMT + $graph[0][1]->AMT + $graph[0][2]->AMT }} / 100);
                                                     var tall =  format.format(value*totalg);

                                                    return ('<div class="arrow_box" style="text-align:center">' +
                                                      '<span>' + '@lang("homepage.graph")' + labels + ':  ' + tall  + '</span>' +
                                                      '</div>'
                                                      );
                                                  }
                                                },
                                            dataLabels: {
                                                    enabled: true,
                                                   

                                                       formatter: function (value) {
                                                            var totalg = ({{ $graph[0][0]->AMT + $graph[0][1]->AMT + $graph[0][2]->AMT }} / 100);
                                                            var tall =  format.format(value*totalg);
                                                            return tall
                                                      },


                                                    dropShadow: {
                                                        enabled: false,
                                                    },
                                                    style: {

                                                           fontSize: "12px",
                                                            fontFamily: "Helvetica, Arial, sans-serif",
                                                            fontWeight: "bold",
                                                            colors: ['#333'],
                                                      },
                                                },
                                               
                                            responsive: [{
                                                breakpoint: 100,
                                                options: {          
                                                    legend: {
                                                        position: 'bottom'
                                                    }
                                                }
                                            }]
                                        }

                                        var chart = new ApexCharts(
                                            document.querySelector("#donut"),
                                            options
                                        );
                                        chart.render();
                                    });
                                 </script>
                            @endpush


                        @else

                            @push('script')
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                           
                                        var options = {
                                            chart: {
                                                height: <?php echo $sizz ?>,
                                                type: 'donut',
                                            },
                                            series: [{{$graph[0][0]->AMT}}, {{$graph[0][1]->AMT}}, {{$graph[0][2]->AMT}}],
                                            labels: ["{{$graph[0][0]->ASM_YEAR}}", "{{$graph[0][1]->ASM_YEAR}}", "{{$graph[0][2]->ASM_YEAR}}"],
                                            colors: ["#9CCC65", "#4680FF", "#FCD842"],
                                            legend: {
                                                show: true,
                                                position: 'bottom',
                                            },

                                            plotOptions: {
                                                pie: {
                                                    donut: {
                                                        labels: {
                                                            show: true,
                                                            name: {
                                                                show: true
                                                            },
                                                            value: {
                                                                show: true
                                                            }
                                                        },
                                                         size: '60%'
                                                    }
                                                }
                                            },
                                                tooltip: {
                                                    custom: function({series, seriesIndex, dataPointIndex, w}) {
                                                    return ('<div class="arrow_box" style="text-align:center">' +
                                                      '<span>' + '@lang("homepage.graph")' + w.config.labels[seriesIndex] + ': RM ' + w.config.series[seriesIndex].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")  + '</span>' +
                                                      '</div>'
                                                      );
                                                  }
                                                },
                                                dataLabels: {
                                                    enabled: true,
                                                   

                                                       formatter: function(value, { seriesIndex, dataPointIndex, w }) {
                                                        return "RM " + w.config.series[seriesIndex].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
                                                      },


                                                    dropShadow: {
                                                        enabled: false,
                                                    },
                                                    style: {

                                                           fontSize: "12px",
                                                            fontFamily: "Helvetica, Arial, sans-serif",
                                                            fontWeight: "bold",
                                                            colors: ['#333'],
                                                      },
                                                },

                                            responsive: [{
                                                breakpoint: 100,
                                                options: {          
                                                    legend: {
                                                        position: 'bottom'
                                                    }
                                                }
                                            }]
                                        }

                                        var chart = new ApexCharts(
                                            document.querySelector("#donut"),
                                            options
                                        );
                                        chart.render();
                                    });

                                </script>
                            @endpush

                        @endif
                               


                       
                        <div id="donut" style="width:100%"></div>
                    @else

                        @if($browser == 'IE')
                            @push('script')
                                <script type="text/javascript">
                                    $(document).ready(function() {

                                        var options = {
                                            chart: {
                                                height: <?php echo $sizz ?>,
                                                type: 'donut',
                                            },
                                            series: [1, 1, 1],
                                            labels: ["{{date('Y') - 3}}", "{{date('Y') - 2}}", "{{date('Y') - 1}}"],
                                            colors: ["#9CCC65", "#4680FF", "#FCD842"],
                                            legend: {
                                                show: true,
                                                position: 'bottom',
                                            },

                                            plotOptions: {
                                                pie: {
                                                    donut: {
                                                        labels: {
                                                            show: false,
                                                            name: {
                                                                show: true
                                                            },
                                                            value: {
                                                                show: true
                                                            }
                                                        },
                                                         size: '60%'
                                                    }
                                                }
                                            },
                                             tooltip: {
                                                    custom: function(value,labels) {

                                                     return ('<div class="arrow_box" style="text-align:center">' +
                                                      '<span>' + '@lang("homepage.graph")' + labels + ': RM 0.00 </span>' +
                                                      '</div>'
                                                      );
                                                  }
                                                },
                                            dataLabels: {
                                                    enabled: true,
                                                   

                                                       formatter: function (value) {
                                                           return "RM 0.00"
                                                      },


                                                    dropShadow: {
                                                        enabled: false,
                                                    },
                                                    style: {

                                                           fontSize: "12px",
                                                            fontFamily: "Helvetica, Arial, sans-serif",
                                                            fontWeight: "bold",
                                                            colors: ['#333'],
                                                      },
                                                },
                                            
                                            responsive: [{
                                                breakpoint: 100,
                                                options: {          
                                                    legend: {
                                                        position: 'bottom'
                                                    }
                                                }
                                            }]
                                        }
                                        var chart = new ApexCharts(
                                            document.querySelector("#donut"),
                                            options
                                        );
                                        chart.render();
                                    });
                                </script>
                            @endpush
                        @else

                            @push('script')
                                <script type="text/javascript">
                                    $(document).ready(function() {

                                        var options = {
                                            chart: {
                                                height: <?php echo $sizz ?>,
                                                type: 'donut',
                                            },
                                            series: [1, 1, 1],
                                            labels: ["{{date('Y') - 3}}", "{{date('Y') - 2}}", "{{date('Y') - 1}}"],
                                            colors: ["#9CCC65", "#4680FF", "#FCD842"],
                                            legend: {
                                                show: true,
                                                position: 'bottom',
                                            },

                                            plotOptions: {
                                                pie: {
                                                    donut: {
                                                        labels: {
                                                            show: false,
                                                            name: {
                                                                show: true
                                                            },
                                                            value: {
                                                                show: true
                                                            }
                                                        },
                                                         size: '60%'
                                                    }
                                                }
                                            },
                                            tooltip: {
                                      custom: function({series, seriesIndex, dataPointIndex, w}) {
                                        return '<div class="arrow_box" style="text-align:center">' +
                                          '<span>' + '@lang("homepage.graph")' + w.config.labels[seriesIndex] + ': RM 0.00 </span>' +
                                          '</div>'
                                      }
                                    },
                                            dataLabels: {
                                                enabled: true,
                                               formatter: function(value, { seriesIndex, dataPointIndex, w }) {
                                                    return "RM 0.00"
                                                  },
                                                dropShadow: {
                                                    enabled: false,
                                                },
                                                style: {
                                                    
                                                    fontSize: "12px",
                                                    fontFamily: "Helvetica, Arial, sans-serif",
                                                    fontWeight: "bold",
                                                    colors: ['#333'],
                                                }
                                            },
                                            responsive: [{
                                                breakpoint: 100,
                                                options: {          
                                                    legend: {
                                                        position: 'bottom'
                                                    }
                                                }
                                            }]
                                        }
                                        var chart = new ApexCharts(
                                            document.querySelector("#donut"),
                                            options
                                        );
                                        chart.render();
                                    });
                                </script>
                            @endpush

                        @endif

                        <div id="donut" style="width:100%"></div>
                    @endif
                @else
                    
                    @if($browser == 'IE')
                            @push('script')
                                <script type="text/javascript">
                                    $(document).ready(function() {

                                        var options = {
                                            chart: {
                                                height: <?php echo $sizz ?>,
                                                type: 'donut',
                                            },
                                            series: [1, 1, 1],
                                            labels: ["{{date('Y') - 3}}", "{{date('Y') - 2}}", "{{date('Y') - 1}}"],
                                            colors: ["#9CCC65", "#4680FF", "#FCD842"],
                                            legend: {
                                                show: true,
                                                position: 'bottom',
                                            },

                                            plotOptions: {
                                                pie: {
                                                    donut: {
                                                        labels: {
                                                            show: false,
                                                            name: {
                                                                show: true
                                                            },
                                                            value: {
                                                                show: true
                                                            }
                                                        },
                                                         size: '60%'
                                                    }
                                                }
                                            },

                                            tooltip: {
                                                    custom: function(value,labels) {

                                                     return ('<div class="arrow_box" style="text-align:center">' +
                                                      '<span>' + '@lang("homepage.graph")' + labels + ': RM 0.00 </span>' +
                                                      '</div>'
                                                      );
                                                  }
                                                },
                                            dataLabels: {
                                                    enabled: true,
                                                   

                                                       formatter: function (value) {
                                                           return "RM 0.00"
                                                      },


                                                    dropShadow: {
                                                        enabled: false,
                                                    },
                                                    style: {

                                                           fontSize: "12px",
                                                            fontFamily: "Helvetica, Arial, sans-serif",
                                                            fontWeight: "bold",
                                                            colors: ['#333'],
                                                      },
                                                },
                                            
                                            responsive: [{
                                                breakpoint: 100,
                                                options: {          
                                                    legend: {
                                                        position: 'bottom'
                                                    }
                                                }
                                            }]
                                        }
                                        var chart = new ApexCharts(
                                            document.querySelector("#donut"),
                                            options
                                        );
                                        chart.render();
                                    });
                                </script>
                            @endpush
                        @else

                            @push('script')
                                <script type="text/javascript">
                                    $(document).ready(function() {

                                        var options = {
                                            chart: {
                                                height: <?php echo $sizz ?>,
                                                type: 'donut',
                                            },
                                            series: [1, 1, 1],
                                            labels: ["{{date('Y') - 3}}", "{{date('Y') - 2}}", "{{date('Y') - 1}}"],
                                            colors: ["#9CCC65", "#4680FF", "#FCD842"],
                                            legend: {
                                                show: true,
                                                position: 'bottom',
                                            },

                                            plotOptions: {
                                                pie: {
                                                    donut: {
                                                        labels: {
                                                            show: false,
                                                            name: {
                                                                show: true
                                                            },
                                                            value: {
                                                                show: true
                                                            }
                                                        },
                                                         size: '60%'
                                                    }
                                                }
                                            },
                                            tooltip: {
                                      custom: function({series, seriesIndex, dataPointIndex, w}) {
                                        return '<div class="arrow_box" style="text-align:center">' +
                                          '<span>' + '@lang("homepage.graph")' + w.config.labels[seriesIndex] + ': RM 0.00 </span>' +
                                          '</div>'
                                      }
                                    },
                                            dataLabels: {
                                                enabled: true,
                                               formatter: function(value, { seriesIndex, dataPointIndex, w }) {
                                                    return "RM 0.00"
                                                  },
                                                dropShadow: {
                                                    enabled: false,
                                                },
                                                style: {
                                                    
                                                    fontSize: "12px",
                                                    fontFamily: "Helvetica, Arial, sans-serif",
                                                    fontWeight: "bold",
                                                    colors: ['#333'],
                                                }
                                            },
                                            responsive: [{
                                                breakpoint: 100,
                                                options: {          
                                                    legend: {
                                                        position: 'bottom'
                                                    }
                                                }
                                            }]
                                        }
                                        var chart = new ApexCharts(
                                            document.querySelector("#donut"),
                                            options
                                        );
                                        chart.render();
                                    });
                                </script>
                            @endpush

                        @endif
                    <div id="donut" style="width:100%"></div>
                @endif
                </div>
            </div>
           
        </div>
    </div>
    <br>
    <br>
     <img src="{{$url}}" style="display:none">
    
   
    
    <div class="row shadow rounded" style="background-color: white;padding-top: 30px;">
         <div class="col-md-12"  style="">
           
            <button type="button" class="fc-month-button fc-button fc-state-default" onclick="location.href='/dashboard/apps'" style="float:right !important;margin:unset;cursor:pointer;">@lang('homepage.service-section-all')</button>
             <h5 class="">@lang('homepage.service-section')</h5>
            <hr>
         </div>
       
     
            
            <?php 
                
                $bgcolor1 = 'blue';
                $bgcolor2 = 'blue';
                $bgcolor3 = 'blue';
                $bgcolor4 = 'blue';

                $count = 0;


            ?>

            @foreach($list as $key=> $app)
                <?php 

                    $bgc = '';
                    $count = $count+1;
                    if($count == 12)
                    {
                        $count = 1;
                    }

                    if($count == 1){$bgc =  $bgcolor1;}
                    if($count == 2){$bgc =  $bgcolor2;}
                    if($count == 3){$bgc =  $bgcolor3;}
                    if($count == 4){$bgc =  $bgcolor4;}
                    if($count == 5){$bgc =  $bgcolor4;}
                    if($count == 6){$bgc =  $bgcolor3;}
                    if($count == 7){$bgc =  $bgcolor2;}
                    if($count == 8){$bgc =  $bgcolor1;}
                    if($count == 9){$bgc =  $bgcolor2;}
                    if($count == 11){$bgc =  $bgcolor3;}
                    if($count == 10){$bgc =  $bgcolor4;}

                ?>
                   

                            <div class="col-md-6 col-xl-3">
                                <div class="card order-card h-80 animation-toggle" data-animate="rubberBand" style="color:unset;cursor:pointer;" onclick="location.href='/app/view/{{$app->id}}'">
                                    <div class="card-body ">
                                        <h6 class="" style="font-size:14px;color:#377c97 !important">
                                            
                                            @if($user->language == 'en')
                                                {{$app->service_en}}
                                            @else
                                                {{$app->service_bm}}
                                            @endif
                                            
                                        </h6>
                                        <!-- <p style="font-size:12px;">

                                            @if($user->language == 'en')
                                                <td>{!! Str::words($app->description_en, 6, ' ...') !!}</td>
                                            @else
                                                <td>{!! Str::words($app->description_bm, 6, ' ...') !!}</td>
                                            @endif

                                        </p> -->
                                        <!-- <i class="card-icon feather icon-filter" style="color: #38374024;"></i> -->
                                         <div class="progress {{$bgc}}">
                                            <div class="progress-bar bg-c-{{$bgc}}" style="width:100%"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                @endforeach
    </div>


@endsection

