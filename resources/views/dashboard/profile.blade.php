@extends('ui::ablepro.dashboard')

@section('content')
 <style>
.linebaricon{

    width: 30px;height: 30px;margin-left: -7px;font-size: 25px;margin-top: 7px;
}

.linebar{

    border-bottom: solid 2px #e2e5e8;margin-left: -40px;margin-top: -3px;color:transparent;width: 43px;position: absolute;
}
.linebarparaph
{
    border-left: solid #e2e5e8; padding-left: 5px;
}

.nav-pills>li>a:hover {
  background-color: #FABC0B;
  color:white !important;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:deepskyblue;
    color:white;
    font-weight:550;

    }

 </style>
<!-- <div class="card new-cust-card">
    <div class="card-header">
        <h5>Sijil Digital @if($user->access== 1) Individu @elseif($user->access== 3) Syarikat @else Pentadbir @endif </h5>
    </div>
    <div class="ps " style="">
        <div class="card-body p-b-0">
            <div class="row">
                <div class="col-md-4 text-center">
                    <div class="change-profile text-center">
                        <div class="w-auto ">
                                <div class="profile-dp">
                                    <button type="button" class="btn btn-icon btn-success has-ripple"><i class="feather icon-check-circle"></i><span class="ripple ripple-animate" style="top: 24.3167px; left: 28.3833px;"></span></button>
                                </div>
                               <br>
                           
                        </div>
                    </div>
                    <h5 class="mb-1">@if($profile->tax_restrain == 0) SAH @else TAMAT @endif</h5>
                    <p class="mb-2 text-muted">STATUS SIJIL</p>
                </div>
                <div class="col-md-8 mt-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="media">
                            <br>
                                <div class="media-body">
                                
                                    <p class="mb-0 text-muted"><h4>Tempoh Sah Sijil : </h4></p>
                                    <p class="mb-0 text-muted"><b>{{  str_replace("-"," Hingga ",$profile->tax_cert_status) }}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

                <div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card">
                            <div class="card-body d-flex align-items-center justify-content-between" style="background-color: cornflowerblue;">
                                <h5 class="mb-0" style="color:white">@lang('profile.title-infos')</h5>
                                <!-- <a target="_blank" type="button" class="btn btn-primary btn-sm rounded m-0 float-right" href="https://ekls.hasil.gov.my/ssoprod/ujikemaskini/makkemaskini.php">
                                    <i class="feather icon-edit"></i>

                                </a> -->
                              
                            </div>
                            <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
                                <form>
                                    <div class=" row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-name')</label>
                                        <label class="col-sm-9">
                                            {{$user->name}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset"> @lang('mobile.label-ic')</label>
                                        <label class="col-sm-9">
                                            {{$user->reference_id}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-taxno')</label>
                                        <label class="col-sm-9">
                                            {{$user->tax_no}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-phone')</label>
                                        <label class="col-sm-9">
                                            {{$profile->homephone_no}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-mphone')</label>
                                        <label class="col-sm-9">
                                            {{$profile->handphone_no}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-email')</label>
                                        <label class="col-sm-9">
                                            {{$user->email}}
                                        </label>
                                    </div>
                                    <div class="row" style="margin-bottom:unset">
                                        <label class="col-sm-3  font-weight-bolder" style="padding-top:unset">@lang('mobile.label-address')</label>
                                        <label class="col-sm-4">
                                            {{$profile->address}}
                                        </label>
                                    </div>
                                </form>
                            </div>
                            
                            
                            <div class="card-body border-top pro-det-edit collapse " id="pro-det-edit-2">
                                dalam penyelengaraan
                            </div>
                        </div>
                    </div>

                     <div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card">
                            <div class="card-body d-flex align-items-center justify-content-between" style="background-color: cornflowerblue;">
                                <h5 class="mb-0" style="color:white">@lang('profile.title-certs')</h5>
                              
                            </div>
                            <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
                                <div class="bt-wizard" style="position: sticky;">
                                    <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">


                                    @if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 5) OR ($user->access == 7))
                                        <li class="nav-item">
                                            <a class="nav-link active" id="ind-tab" data-toggle="tab" href="#ind" role="tab" aria-controls="ind">
                                                @lang('profile.cert2')
                                            </a>
                                        </li>  
                                    @endif  
                                    @if(($user->access == 2) OR ($user->access == 3) OR ($user->access == 6) OR ($user->access == 7))
                                        <li class="nav-item">
                                            <a class="nav-link @if(($user->access == 2) OR ($user->access == 6))active @endif" id="oef-tab" data-toggle="tab" href="#oef" role="tab" aria-controls="oef">
                                                @lang('profile.cert1')
                                            </a>
                                        </li>  
                                    @endif
                                    @if(($user->access == 4) OR ($user->access == 5) OR ($user->access == 6) OR ($user->access == 7))  
                                        <li class="nav-item ">
                                            <a class="nav-link @if($user->access == 4) active @endif" id="pef-tab" data-toggle="tab" href="#pef" role="tab" aria-controls="pef">
                                                @lang('profile.cert3')
                                            </a>
                                        </li> 
                                    @endif   
                                    </ul>
                                </div>
                                 <div class="tab-content" id="myTabContent">
                                 @if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 5) OR ($user->access == 7))
                                <div class="tab-pane fade active show" id="ind" role="tabpanel" aria-labelledby="ind-tab">
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            @if(isset($dataprofile->CertStatus))

                                                @if($dataprofile->CertStatus == 'True')
                                                <div class="card-body">
                                                    <div class="media-body">
                                                        <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                                        <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$profile->tax_cert_status) }}</b></p>
                                                    </div>
                                                </div>
                                                @else
                                                    <div class="card-body">
                                                        <form class="text-center">
                                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                                            <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                            <!-- <p>@lang('inbox.nodata')</p> -->
                                                        </form>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="card-body">
                                                    <form class="text-center">
                                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if(($user->access == 2) OR ($user->access == 3) OR ($user->access == 6) OR ($user->access == 7))
                                <div class="tab-pane fade @if(($user->access == 2) OR ($user->access == 6))active show @endif" id="oef" role="tabpanel" aria-labelledby="oef-tab">
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            @if($dataprofile->CertStatusOeF == 'True')
                                            <div class="card-body">
                                                <div class="media-body">
                                                    <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                                    <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$dataprofile->CertValidityOeF) }}</b></p>
                                                </div>
                                            </div>
                                

                                            @else
                                                <div class="card-body">
                                                    <form class="text-center">
                                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                                    </form>
                                                </div>
                                            @endif



                                        </div>


                                    </div>
                                    @if(count($comlist) > 0 )
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            <div class="dt-responsive table-responsive" style="background-color: white">
                                                <table class="table table-striped table-bordered nowrap" id="com">
                                                    <thead>
                                                        <tr>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('profile.cert-status')</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col5')</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($comlist as $keycom => $com)
                                                            <tr>
                                                                <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                                <td>{{$com->Jenis_File}}</td>
                                                                <td>{{$com->No_Rujukan}}</td>
                                                                <td>{{$com->No_Roc}}</td>
                                                                <td>{{$com->Status_OeF}}</td>
                                                                <td>{{$com->Tarikh_Daftar}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                @endif
                                @if(($user->access == 4) OR ($user->access == 5) OR ($user->access == 6) OR ($user->access == 7))  
                                <div class="tab-pane fade  @if($user->access == 4) active show @endif" id="pef" role="tabpanel" aria-labelledby="pef-tab">
                                    <div class="card" style="background-color: transparent">
                                        <div class="card-body">
                                            @if($dataprofile->CertStatusPeF == 'True')
                                            <div class="card-body">
                                                <div class="media-body">
                                                    <p class="mb-0 text-muted"><h4>@lang('profile.cert-status') : </h4></p>
                                                    <p class="mb-0 text-muted"><b>{{  str_replace("-"," - ",$dataprofile->CertValidityPeF) }}</b></p>
                                                </div>
                                            </div>
                                            @else
                                                <div class="card-body">
                                                    <form class="text-center">
                                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                                    </form>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>


@endsection

@push('script')

<script type="text/javascript">


$(document).ready(function () {

    $('#com').DataTable();


});

</script>

@endpush