    <?php 

    $locale = \App::getLocale();
    $activems = '';
    $activeen = '';


    ?>
<div class="row">
    <!-- liveline-section start -->
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body ">
           
                <h6 class="" style="font-size:14px;color:#377c97 !important">
                    @if($locale == 'en')
                        <td>{{$app->service_en}}</td>
                    @else
                        <td>{{$app->service_bm}}</td>
                    @endif
                </h6>
                <p style="font-size:12px;">
                    @if($locale == 'en')
                        <td>{{$app->description_en}}</td>
                    @else
                        <td>{{$app->description_bm}}</td>
                    @endif
                </p>
                
                <div class="progress blue">
                    <div class="progress-bar bg-c-blue" style="width:100%"></div>
                </div>
                <br>
                <hr>
                <br>
                <div class="email-content">
                   @if($locale == 'en')
                        <?php echo $app->content_en ;?>
                    @else
                        <?php echo $app->content_bm ;?>
                    @endif
                </div>
                
            </div>
        </div>
    </div>
    <!-- liveline-section end -->
</div>