    <?php 

    $locale = App::getLocale();
         $activems = '';
         $activeen = '';


    ?>
  <div class="col-sm-12">
    <h5 class="mb-3"> @lang('form.titlebrosure')</h5>
    <hr>
         <div class="row">
    <?php 
        
        $bgcolor1 = 'green';
        $bgcolor2 = 'blue';
        $bgcolor3 = 'red';
        $bgcolor4 = 'yellow';

        $count = 0;


    ?>

        
        <div class="col-md-12 col-xl-12">
            <div class="card new-cust-card">

                    <div class="" >
                        <div class="card-body p-b-0">
                            <div class="card-body">
                           
                                    <div class="row m-b-25">
                                        <div class="col-auto p-r-0">
                                            <i class="feather icon-file-text text-c-blue d-block f-60 w-50"></i>
                                        </div>
                                        <div class="col">
                                            
                                                  
                                                    <a source="{{ asset('usermanual.pdf') }}" >
                                                        
                                                        @if($locale == 'en')
                                                            <h6 class="m-b-5" style="font-size: 13px"> MyTax User Manual</h6>
                                                        @else
                                                           <h6 class="m-b-5" style="font-size: 13px">Panduan Pengguna Sistem MyTax</h6>
                                                        @endif
                                                    </a>
                                                    <a style="color:white;margin-top: 12px;font-size: 13px" source="{{ asset('usermanual.pdf') }}" class="_df_button btn btn-success btn-sm">@lang('homepage.flip')</a>
                                            
                                            
                                        </div>
                                    </div>
                            </div>
                        </div>
                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 415px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 321px;"></div></div></div>
                </div>
        </div>


        
</div>
</div>