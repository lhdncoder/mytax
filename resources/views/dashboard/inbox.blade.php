@extends('ui::ablepro.dashboard')

@section('content')
<style type="text/css">
    .nav-pills>li>a:hover {
  background-color: #008abf !important;
  color:white !important;
  border-radius: 10px;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:deepskyblue;
    color:white;
    font-weight:550;

    }

.email-card .tab-content .nav-pills > li .nav-link.active {

    background-color: #008abf;
    color: white;
}
</style>
<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card">
        <div class="card-body d-flex align-items-center justify-content-between">
            <h5 class="mb-0">My Inbox ({{$inboxcount}})</h5>
            
        </div>
        <div class="card-body border-top pro-det-edit collapse show" id="pro-det-edit-1">
            <div class="row">
            <div class="col-sm-12">
                <div class="card email-card">
                   
                    <div class="card-body">
                        <div class="mail-body">
                            <div class="row">
                                <!-- [ inbox-left section ] start -->
<!--                                 <div class="col-xl-2 col-md-3">
                                    <ul class="mb-2 nav nav-tab flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="false">
                                                <span><i class="feather icon-inbox"></i> Inbox</span>
                                                <span class="float-right">{{$inboxcount}}</span>
                                            </a>
                                        </li>
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-starred-tab" data-toggle="pill" href="#v-pills-starred" role="tab">
                                                <span><i class="feather icon-star-on"></i> Starred</span>
                                            </a>
                                        </li>
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-draft" role="tab">
                                                <span><i class="feather icon-file-text"></i> Drafts</span>
                                            </a>
                                        </li>
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-mail" role="tab">
                                                <span><i class="feather icon-navigation"></i> Sent Mail</span>
                                            </a>
                                        </li>
                                        <li class="nav-item mail-section">
                                            <a class="nav-link text-left" id="v-pills-Trash-tab" data-toggle="pill" href="#v-pills-Trash" role="tab">
                                                <span><i class="feather icon-trash-2"></i> Trash</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div> -->
                                <div class="col-xl-12 col-md-12 inbox-right">

                                    <div class="tab-content p-0" id="v-pills-tabContent">
                                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab" >

                                            <ul class="nav nav-pills nav-fill mb-0" id="pills-tab" role="tablist" style="padding: unset !important;border: unset !important;">
                                               
                                                <li class="nav-item">
                                                    <a style="border-radius: 10px;margin-right: 5px;padding-bottom:15px;" class="nav-link active" id="pills-social-tab" data-toggle="pill" href="#pills-social" role="tab" aria-controls="pills-social" aria-selected="false"><span><i class="feather icon-mail"></i>
                                                            @lang('inbox.info')</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a style="border-radius: 10px;margin-right: 5px;padding-bottom:15px" class="nav-link" id="pills-update-tab" data-toggle="pill" href="#pills-update" role="tab" aria-controls="pills-update" aria-selected="false"><span><i class="feather icon-mail"></i>
                                                            @lang('inbox.letter')</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a style="border-radius: 10px;margin-right: 5px;padding-bottom:15px" class="nav-link" id="pills-forum-tab" data-toggle="pill" href="#pills-forum" role="tab" aria-controls="pills-forum" aria-selected="false"><span><i class="feather icon-mail"></i>
                                                            @lang('inbox.noti')</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a style="border-radius: 10px;margin-right: 5px;padding-bottom:15px" class="nav-link" id="pills-Promotion-tab" data-toggle="pill" href="#pills-Promotion" role="tab" aria-controls="pills-Promotion" aria-selected="false"><span><i class="feather icon-mail"></i>
                                                            @lang('inbox.memo')</span></a>
                                                </li>
                                            </ul>
                                            <br>
                                            <div class="tab-content" id="pills-tabContent">
                                                
                                                <div class="tab-pane fade show active" id="pills-social" role="tabpanel" aria-labelledby="pills-social-tab">
                                                    <div class="mail-body-content table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                            @forelse($data as $key => $value)
                                                            <?php 
                                                                if($value->Unread == 'false')
                                                                {
                                                                    $read = 'read';

                                                                }else{
                                                                    $read = 'unread';
                                                                }
                                                            ?>
                                                                 <tr class="{{$read}}">
                                                                   
                                                                    <!-- <td><a href="#" class="email-name waves-effect">{{$read}}</a></td> -->
                                                                    <td><a href="/mail/read/{{$value->id}}" class="email-name waves-effect">{{$value->Subjek}}</a></td>
                                                                    
                                                                    <td class="email-time">{{date('d/m/Y, h:i a', strtotime($value->TarikhNotis))}}</td>
                                                                </tr>
                                                            @empty
                                                                 <tr class="read">
                                                                    <td colspan="3">@lang('inbox.empty')</td>
                                                                </tr>
                                                            @endforelse
                                                              

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="pills-Promotion" role="tabpanel" aria-labelledby="pills-Promotion-tab">
                                                    <div class="mail-body-content table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                 <tr class="read">
                                                                    <td colspan="3">@lang('inbox.empty')</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="pills-update" role="tabpanel" aria-labelledby="pills-update-tab">
                                                    <div class="mail-body-content table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr class="read">
                                                                    <td colspan="3">@lang('inbox.empty')</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="pills-forum" role="tabpanel" aria-labelledby="pills-forum-tab">
                                                    <div class="mail-body-content table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                            @forelse($datacp500 as $key => $value)
                                                            <?php 
                                                                if($value->Unread == 'false')
                                                                {
                                                                    $read = 'read';

                                                                }else{
                                                                    $read = 'unread';
                                                                }
                                                            ?>
                                                                 <tr class="{{$read}}">
                                                                   
                                                                    <!-- <td><a href="#" class="email-name waves-effect">{{$read}}</a></td> -->
                                                                    <td><a href="/mail/read/{{$value->id}}" class="email-name waves-effect">{{$value->Subjek}}</a></td>
                                                                    
                                                                    <td class="email-time">{{date('d/m/Y, h:i a', strtotime($value->TarikhNotis))}}</td>
                                                                </tr>
                                                            @empty
                                                                 <tr class="read">
                                                                    <td colspan="3">@lang('inbox.empty')</td>
                                                                </tr>
                                                            @endforelse
                                                              

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="card-body border-top pro-det-edit collapse " id="pro-det-edit-2">
           
        </div>
    </div>
</div>

@endsection
