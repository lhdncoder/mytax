@extends('ui::ablepro.dashboarddemo')

@section('content')

    <div class="row">
        <div class="col-xl-3 col-md-3">
            <div class="card user-card user-card-1 h-100">
                <div class="card-header border-0 p-2 pb-0">
                    <div class="cover-img-block">
                        <img src="{{asset('themes/ablepro/assets/images/widget/slider7.jpg')}}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="user-about-block text-center">
                        <div class="row align-items-end">
                            <div class="col"></div>
                            <div class="col"><img class="img-radius img-fluid wid-80" src="{{asset('themes/ablepro/assets/images/user/avatar-1.jpg')}}" alt="User image"></div>
                            <div class="col"></div>
                        </div>
                    </div>
                    <div class="text-center">
                        <h6 class="mb-1 mt-3">{{$user->name}}</h6>
                        <p class="mb-3 text-muted">SG {{$user->tax_no}}</p>
                        <br>
                        <div class="row text-center">
                            <div class="col">
                                <h6 class="mb-1">Baki Cukai</h6>
                                <p class="mb-0">RM {{$profile->tax_balance}}</p>
                            </div>
                            <div class="col">
                                <h6 class="mb-1">Bayaran Balik</h6>
                                <p class="mb-0">RM {{$profile->tax_refund}}</p>
                            </div>
                        </div>
                        <br>
                       
                    </div>                        
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-3">
            
            <div class="card user-card user-card-1 h-100">
                <div class="card-header border-0 p-2 pb-0">
                    <div class="cover-img-block">
                        <img src="{{asset('themes/ablepro/assets/images/widget/slider5.jpg')}}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="card-body pt-0">
                    
                    <div class="text-center">
                        <h6 class="mb-1 mt-3">SEMAKAN SEKATAN PERJALANAN</h6>
                        <br>
                        @if($profile->tax_restrain == '0')
                        <div class="card">
                            <blockquote class="blockquote mb-0 card-body">
                               <button type="button" class="btn btn-icon btn-success has-ripple"><i class="feather icon-check-circle"></i><span class="ripple ripple-animate"></span></button> <br><br><p>Tahniah!!, Anda Tiada Sekatan Perjalanan</p>
                                
                            </blockquote>
                        </div>
                        @else
                        <div class="card">
                            <blockquote class="blockquote mb-0 card-body">
                               <button type="button" class="btn btn-icon btn-danger has-ripple"><i class="feather icon-x-circle"></i><span class="ripple ripple-animate"></span></button> <br><br><p>Harap Maaf, Anda telah disekat dari keluar negara</p>
                                
                            </blockquote>
                        </div>
                        @endif

                      
                    </div>                        
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card h-100">
               
                <div class="card-body">
                    <div id="line-chart-1"></div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <h5 class="">Menu</h5>
            <hr>
        <div class="row">
            <div class="col-xl-4 col-md-6" >
                <div class="card social-card bg-c-red animation-toggle" data-animate="bounceIn" style="cursor:pointer; " onclick="location.href='user/inbox/{{$user->id}}'">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-mail f-34 text-c-red social-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-0 text-white">My Inbox</h6>
                                <p>0 Pesanan</p>
                                <p class="m-b-0">Lihat Pesanan Anda</p>
                            </div>
                        </div>
                    </div>
                    <a href="user/inbox/{{$user->id}}" class="download-icon"><i class="feather icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card social-card bg-c-blue animation-toggle" data-animate="bounceIn" style="cursor:pointer; " onclick="location.href='user/profile/{{$user->id}}'">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-user f-34 text-c-blue social-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-0 text-white">My Profile</h6>
                                <p>Nama, Alamat dll</p>
                                <p class="m-b-0">Kemaskini Maklumat Anda</p>
                            </div>
                        </div>
                    </div>
                    <a href="user/profile/{{$user->id}}" class="download-icon"><i class="feather icon-arrow-down"></i></a>
                </div>
            </div>
            <div class="col-xl-4 col-md-12">
                <div class="card social-card bg-c-green animation-toggle" data-animate="bounceIn">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <i class="feather icon-book f-34 text-c-green social-icon"></i>
                            </div>
                            <div class="col">
                                <h6 class="m-b-0 text-white">My Tax Status</h6>
                                <p>Borang</p>
                                <p class="m-b-0">Pending</p>
                            </div>
                        </div>
                    </div>
                    <a href="#!" class="download-icon"><i class="feather icon-arrow-down"></i></a>
                </div>
            </div>
        </div>
    <br>
    <button type="button" class="fc-month-button fc-button fc-state-default" style="float:right !important;margin:unset">Semua Applikasi</button>
    <h5 class="">Senarai Aplikasi Pilihan</h5>
    <hr>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="card order-card h-80 animation-toggle" data-animate="rubberBand" style="color:unset">
                <div class="card-body ">
                    <h6 class="" style="font-size:14px;color:#377c97 !important">e-Filing</h6>
                    <p style="font-size:12px;">Kaedah mengisi dan menghantar Borang Nyata Cukai Pendapatan (BNCP) secara elektronik</p>
                    <i class="card-icon feather icon-filter" style="color: #38374024;"></i>
                     <div class="progress green">
                            <div class="progress-bar bg-c-green" style="width:100%"></div>
                        </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card order-card h-80 animation-toggle" data-animate="rubberBand" style="color:unset">
                <div class="card-body">
                    <h6 class="" style="font-size:14px;color:#377c97 !important">ByrHasil</h6>
                    <p style="font-size:12px;">ByrHASiL adalah aplikasi elektronik untuk pembayaran cukai pendapatan</p>
                    <i class="card-icon feather icon-credit-card" style="color: #38374024;"></i>
                     <div class="progress blue">
                            <div class="progress-bar bg-c-blue" style="width:100%"></div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card order-card h-80 animation-toggle" data-animate="rubberBand" style="color:unset">
                <div class="card-body">
                    <h6 class="" style="font-size:14px;color:#377c97 !important">e-Daftar</h6>
                    <p style="font-size:12px;">e-Daftar fail cukai untuk pembayar cukai baharu mendapatkan nombor cukai pendapatan</p>
                    <i class="card-icon feather icon-edit" style="color: #38374024;"></i>
                     <div class="progress red">
                            <div class="progress-bar bg-c-red" style="width:100%"></div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3">
            <div class="card order-card h-80 animation-toggle animated" data-animate="rubberBand" style="color:unset">
                <div class="card-body">
                    <h6 class="" style="font-size:14px;color:#377c97 !important">e-Lejar</h6>
                    <p style="font-size:12px;">e-Lejar ialah satu kemudahan yang disediakan untuk pembayar cukai menyemak...</p>
                    <i class="card-icon feather icon-file-text" style="color: #38374024;"></i>
                     <div class="progress yellow">
                            <div class="progress-bar bg-c-yellow" style="width:100%"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection
