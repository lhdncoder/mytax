    <?php 

    $locale = App::getLocale();
         $activems = '';
         $activeen = '';


    ?>
  <div class="col-sm-12">
    <h5 class="mb-3"> @lang('homepage.help')</h5>
    <hr>
    
    @foreach($list as $key=> $value)
        

        <div class="col-md-12 col-xl-12">
            <div class="card order-card" data-animate="rubberBand" style="color:unset">
                <div class="card-body ">
                                         
                        @if($locale == 'en')
                            <?php echo $value->description_en ?>
                        @else
                            <?php echo $value->description_bm ?>
                        @endif
        
                </div>
            </div>
        </div>
         <hr>
    @endforeach
        
</div>
