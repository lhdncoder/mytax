@extends('ui::ablepro.dashboard')

@section('content')
  <div class="col-sm-12">
    <h5 class="mb-3"> @lang('homepage.apps')
    <button type="button" style="float:right !important;margin-left:5px;cursor:pointer;" class="btn btn-primary btn-sm" onclick="openfav();"><i class="feather icon-tag"></i> @lang('homepage.service-section')</button>
    </h5>

    <hr>
         <div class="row">
    <?php 
        
        $bgcolor1 = 'green';
        $bgcolor2 = 'blue';
        $bgcolor3 = 'red';
        $bgcolor4 = 'yellow';

        $count = 0;


    ?>

    @foreach($list as $key=> $app)
        <?php 

            $bgc = '';
            $count = $count+1;
            if($count == 12)
            {
                $count = 1;
            }

            if($count == 1){$bgc =  $bgcolor1;}
            if($count == 2){$bgc =  $bgcolor2;}
            if($count == 3){$bgc =  $bgcolor3;}
            if($count == 4){$bgc =  $bgcolor4;}
            if($count == 5){$bgc =  $bgcolor4;}
            if($count == 6){$bgc =  $bgcolor3;}
            if($count == 7){$bgc =  $bgcolor2;}
            if($count == 8){$bgc =  $bgcolor1;}
            if($count == 9){$bgc =  $bgcolor2;}
            if($count == 11){$bgc =  $bgcolor3;}
            if($count == 10){$bgc =  $bgcolor4;}

        ?>

        <div class="col-md-6 col-xl-3">
            <div class="card animation-toggle" data-animate="rubberBand" style="color:unset;cursor:pointer;min-height: 126.4px" onclick="location.href='/app/view/{{$app->id}}'">
                <div class="card-body">
                   

                    <h6 class="" style="font-size:14px;color:#377c97 !important">
                        
                        @if($user->language == 'en')
                            {{$app->service_en}}
                        @else
                            {{$app->service_bm}}
                        @endif
                        
                    </h6>
                    <p style="font-size:12px;">

                        @if($user->language == 'en')
                            {!! Str::words($app->description_en, 6, ' ...') !!}
                        @else
                            {!! Str::words($app->description_bm, 6, ' ...') !!}
                        @endif

                    </p>

                   <!--  <i class="card-icon feather icon-filter" style="color: #38374024;"></i> -->
                     <div class="progress {{$bgc}}">
                            <div class="progress-bar bg-c-{{$bgc}}" style="width:100%"></div>
                      </div>

                </div>
            </div>
        </div>
         <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="favmodal">  
                            
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                           <div id="exampleModalCenter" class="modal" aria-labelledby="exampleModalCenterTitle">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content" style="margin:20px">
                                    <br>
                                        <div class="d-flex justify-content-center" style="margin:20px !important"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>
                                        <div class="swal-title">Please Wait..</div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-header">
                                <h5 class="modal-title">@lang('homepage.service-fav') <br><span style="font-size: 11px">@lang('homepage.service-fav-note')</span>
                                </h5>
                                
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

                            </div>
                            <div class="modal-body" id="logindata">
                                <div class="row">
                                   <div class="card-body table-border-style">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>@lang('form.nameservice')</th>
                                                        <th>@lang('form.action')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($appfav as $key => $fav)
                                                    <tr>
                                                        <td>{{$key + 1}}</td>
                                                        <td> @if($user->language == 'en')
                                                                    <?php echo $fav->service_en ;?>
                                                                @else
                                                                    <?php echo $fav->service_bm ;?>
                                                                @endif
                                                        </td>
                                                        <td><a style="cursor:pointer;" class="btn btn-primary btn-sm" href="javascript:removefav('{{$fav->id}}');"<i class="feather icon-delete"></i> @lang('form.remove')</a></td>
                                                    </tr>
                                                    @empty
                                                    
                                                    @endforelse
                                                    @for ($i = $appfavcount; $i < 4; $i++)
                                                        <tr>
                                                            <td>{{$i+1}}</td>
                                                            <td>None</td>
                                                            <td>None</td>
                                                        </tr>
                                                    @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


    @endforeach
        
</div>
        
@endsection

@push('script')





<script type="text/javascript">


function openfav(id)
{
     $("#exampleModalCenter").modal('hide');
     $("#favmodal").modal('show');
    
     
}

</script>

<script type="text/javascript">

function removefav(id)
{
    $.ajax({

            url: "{{ URL::to('app/removefav')}}"+"/"+id,
            type: "get",
            beforeSend: function () 
            {
                $("#exampleModalCenter").modal('show');
           
            },
            success: function(html)
            {       
                  // document.getElementById('nhtml2').style.display = 'none';
                  $("#exampleModalCenter").modal('hide');
                  $('#logindata').html(html);
                  // window.location.replace("{{ URL::to('ad/index')}}");
            }


    });
}
</script>


@endpush
