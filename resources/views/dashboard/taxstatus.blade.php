@extends('ui::ablepro.dashboard')

@section('content')
 <style>
.linebaricon{

    width: 30px;height: 30px;margin-left: -7px;font-size: 25px;margin-top: 7px;
}

.linebar{

    border-bottom: solid 2px #e2e5e8;margin-left: -40px;margin-top: -3px;color:transparent;width: 43px;position: absolute;
}
.linebarparaph
{
    border-left: solid #e2e5e8; padding-left: 5px;
}

.nav-pills>li>a:hover {
  background-color: #FABC0B;
  color:white !important;
}

.nav-pills .nav-link.active, .nav-pills .nav-link.active:hover,.nav-pills .nav-link.active:focus{
    background-color:deepskyblue;
    color:white;
    font-weight:550;

    }

 </style>
<?php

    $cp500 = '';
    $cp204 = '';
    // $spc   = '';
    $row = 0;
    $cp204row = 0;
    $refdata = 0;

    // if($cp204data){}else{ $cp204 = 'display:none'; }
    // if($cp500data){}else{ $cp500 = 'display:none'; }



    // if($user->access == 1)
    // {
    //     $cp204 = 'display:none';
    // }

    // if($user->access == 3)
    // {
    //     $cp500 = 'display:none';
    // }


?>
<div class="col-sm-12">
    <h5 class="mb-3"> @lang('homepage.header-tax')</h5>
    <hr>
    <div class="card-body" style="display: block; background-color: #E3F1FF;">

        <div class="bt-wizard" style="position: sticky;">
            <ul class="nav nav-pills nav-fill mb-3 mt-10" style="background-color: white;border-radius:5px;padding-left:3px;padding-right:3px">
            <!-- @if($user->access == 4)
            @else -->
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home">
                        LEJAR
                    </a>
                </li>    
            <!-- @endif -->
            @if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 5) OR ($user->access == 7))
                 <li class="nav-item">
                    <a class="nav-link" id="pcb-tab" data-toggle="tab" href="#pcb" role="tab" aria-controls="pcb">
                        PCB
                    </a>
                </li>
            @else
               
            @endif
            @if($user->access == 4)
            @else
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile">
                        BAYARAN BALIK
                    </a>
                </li>
            @endif
            @if(($user->access == 1) OR ($user->access == 4) OR ($user->access == 5))
            @else
                <li class="nav-item">
                    <a class="nav-link" id="cp204-tab" data-toggle="tab" href="#cp204" role="tab" aria-controls="cp204">
                        CP204
                    </a>
                </li>
            @endif
            @if(($user->access == 2) OR ($user->access == 4) OR ($user->access == 6))
            @else
                <li class="nav-item">
                    <a class="nav-link" id="cp500-tab" data-toggle="tab" href="#cp500" role="tab" aria-controls="cp500">
                        CP500
                    </a>
                </li>
            @endif
            @if(($user->access == 2) OR ($user->access == 4) OR ($user->access == 6))
            @else
                <li class="nav-item">
                    <a class="nav-link" id="spc-tab" data-toggle="tab" href="#spc" role="tab" aria-controls="spc">
                        SPC
                    </a>
                </li>
            @endif
                
            </ul>
        </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active show homes" id="home" role="tabpanel" aria-labelledby="home-tab">
                   <br>
                   <p class="mb-0"><h6>@lang('lejar.title-index')</h6></p>
                   <br>
                   @if(!$lejar->isEmpty())

                       @if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 4) OR ($user->access == 5) OR ($user->access == 7))
                        <div class="card-body table-border-style">

                            <div class="table-responsive shadow" style="border-radius: 8px">
                                <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bil')</th>
                                            <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-type') (@if($user->language == 'ms') Individu @else Individual @endif)</th>
                                            <th colspan="3" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-sum') (RM) </th>
                                        </tr>
                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bal')<sup>1</sup></th>
                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-unused')<sup>2</sup></th>
                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-lej')<sup>3</sup></th>
                                        <tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lejar as $key => $data)
                                    @if($data->income_type == 'SALARY')
                                        @if($checksal > 0)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>@lang('lejar.table-income')</td>
                                                 <td colspan="3" style="color:red">@lang('homepage.lejarerror')</td>
                                            </tr>
                                        @else
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>@lang('lejar.table-income')</td>
                                            <td style="text-align: right">{{number_format($data->BakiCukai,2,'.',',')}}</td>
                                            <td style="text-align: right">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</td>
                                            <td style="text-align: right"><a href="javascript:loadpenutup('1','SALARY');">{{number_format($data->BakiLejar,2,'.',',')}}</a></td>
                                        </tr>
                                        @endif

                                    @else

                                        @if(($data->BakiCukai == '0') && ($data->ByrnBelumBolehGuna == '0') && ($data->BakiLejar == '0'))
                                         <tr>
                                            <td>{{$key+1}}</td>
                                            <td>@lang('lejar.table-ckht')</td>
                                            <td colspan="3">@lang('lejar.table-record')</td>
                                        </tr>
                                        @else
                                            @if($checkpro > 0)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>@lang('lejar.table-ckht')</td>
                                                    <td colspan="3" style="color:red">@lang('homepage.lejarerror')</td>
                                                </tr>
                                            @else
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>@lang('lejar.table-ckht')</td>
                                                <td style="text-align: right">{{number_format($data->BakiCukai,2,'.',',')}}</td>
                                                <td style="text-align: right">{{number_format($data->ByrnBelumBolehGuna,2,'.',',')}}</td>
                                                 <td style="text-align: right"><a href="javascript:loadpenutup('1','PROPERTIES');">{{number_format($data->BakiLejar,2,'.',',')}}</a></td>
                                            </tr>
                                            @endif
                                        @endif


                                    @endif
                                        
                                    @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                         @endif
                   @else

                         @if(($user->access == 1) OR ($user->access == 3) OR ($user->access == 4) OR ($user->access == 5) OR ($user->access == 7))
                        <div class="card-body table-border-style">
                             <div class="table-responsive shadow" style="border-radius: 8px">
                                <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bil')</th>
                                            <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-type') (@if($user->language == 'ms') Individu @else Individual @endif)</th>
                                            <th colspan="3" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-sum') (RM) </th>
                                        </tr>
                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bal')<sup>1</sup></th>
                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-unused')<sup>2</sup></th>
                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-lej')<sup>3</sup></th>
                                        <tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>@lang('lejar.table-income')</td>
                                            <td colspan="3">@lang('lejar.table-record')</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>@lang('lejar.table-ckht')</td>
                                            <td colspan="3">@lang('lejar.table-record')</td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif

                       
                    @endif
                    <br>
                    
                    @if(($user->access == 2) OR ($user->access == 3) OR ($user->access == 6) OR ($user->access == 7))
                    <div class="card-body table-border-style">
                         <div class="table-responsive shadow" style="border-radius: 8px">
                            <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                                <thead>
                                    <tr>
                                        <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bil')</th>
                                        <th rowspan="2" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-type') (@if($user->language == 'ms') Syarikat @else Company @endif)</th>
                                        <th colspan="3" style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-sum') (RM) </th>
                                    </tr>
                                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-bal')<sup>1</sup></th>
                                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-unused')<sup>2</sup></th>
                                        <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">@lang('lejar.table-header-lej')<sup>3</sup></th>
                                    <tr>
                                </thead>
                                <tbody>
                                    @if(count($comlist) > 0 )
                                        <tr>
                                            <td>1</td>
                                            <td>@lang('lejar.table-income')</td>
                                            <td colspan="3">
                                                <span style="cursor:pointer;text-decoration: underline" data-toggle="collapse" data-target=".pro-wrk-edit" aria-expanded="false" aria-controls="pro-wrk-edit-1 pro-wrk-edit-2"><b>@lang('lejar.table-com')</b></span>
                                            </td>
                                        </tr>
                                        <tr class="pro-wrk-edit collapse" id="pro-wrk-edit-2">
                                            <td colspan="5" style="padding:20px !important">
                                            <button type="button" class="btn btn-primary btn-sm rounded m-0 float-right" data-toggle="collapse" data-target=".pro-wrk-edit" aria-expanded="false" aria-controls="pro-wrk-edit-1 pro-wrk-edit-2">
                                                <i class="feather icon-x-circle"></i>
                                            </button>
                                                <div class="dt-responsive table-responsive" style="background-color: white">
                                                    <table id="comle" class="table table-striped table-bordered nowrap">
                                                        <thead>
                                                            <tr>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($comlist as $keycom => $com)
                                                                <tr>
                                                                    <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                                    <td>{{$com->Jenis_File}}</td>
                                                                    <td><a href="javascript:loadcom({{$com->id}},'SALARY');">{{$com->No_Rujukan}}</a> </td>
                                                                    <td>{{$com->No_Roc}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>2</td>
                                            <td>@lang('lejar.table-ckht')</td>
                                            <td colspan="3">
                                                <span style="cursor:pointer;text-decoration: underline" data-toggle="collapse" data-target=".pro-wrk-editc" aria-expanded="false" aria-controls="pro-wrk-edit-1c pro-wrk-edit-2c"><b>@lang('lejar.table-com')</b></span>
                                            </td>
                                        </tr>
                                        <tr class="pro-wrk-editc collapse" id="pro-wrk-edit-2c">
                                            <td colspan="5" style="padding:20px !important">
                                            <button type="button" class="btn btn-primary btn-sm rounded m-0 float-right" data-toggle="collapse" data-target=".pro-wrk-editc" aria-expanded="false" aria-controls="pro-wrk-edit-1c pro-wrk-edit-2c">
                                                <i class="feather icon-x-circle"></i>
                                            </button>
                                                <div class="dt-responsive table-responsive" style="background-color: white">
                                                    <table id="comck" class="table table-striped table-bordered nowrap">
                                                        <thead>
                                                            <tr>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col1')</th>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col2')</th>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col3')</th>
                                                                <th style="vertical-align: middle;text-transform:unset;background: cadetblue;color:white">@lang('lejar.comtable-col4')</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($comlist as $keycom => $com)
                                                                <tr>
                                                                    <td class="text-left">{{$com->Nama_Syarikat}}</td>
                                                                    <td>{{$com->Jenis_File}}</td>
                                                                    <td><a href="javascript:loadcom({{$com->id}},'PROPERTIES');">{{$com->No_Rujukan}}</a> </td>
                                                                    <td>{{$com->No_Roc}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        


                                    @else
                                    <tr>
                                        <td>1</td>
                                        <td>@lang('lejar.table-income')</td>
                                        <td colspan="3">@lang('lejar.table-nocom')</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>@lang('lejar.table-ckht')</td>
                                        <td colspan="3">@lang('lejar.table-nocom')</td>
                                    </tr>
                                    @endif
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    
                    <div style="font-size:12px;margin-left:20px">
                        <b>@lang('lejar.note'):</b><br>
                        @lang('lejar.note-1')<br>
                        @lang('lejar.note-2')<br>
                        @lang('lejar.note-3')
                    </div>
                </div>

                <div class="tab-pane fade " id="pcb" role="tabpanel" aria-labelledby="pcb-tab">
                    <br>
                   <!-- <p class="mb-0"><h5>PENYATA PCB</h5></p>
                    <div class="col-sm-12 col-md-12">
                        <div class="table-responsive">
                            <table class="table table-borderless table-xs" style="font-weight: bolder;">
                              
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td colspan="3">: {{$user->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>No. Pengenalan</td>
                                        <td>: {{$user->reference_id}}</td>
                                        <td>Caw. Taksiran</td>
                                        <td>: {{$checkprofile->IT_Assm_Branch}}</td>
                                    </tr>
                                    <tr>
                                        <td>No. Cukai Pendapatan</td>
                                        <td>: {{$user->doc_type}} {{$user->tax_no}}</td>
                                        <td>Caw. Pungutan</td>
                                        <td>: {{$checkprofile->IT_Collection_Branch}}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <br> -->
                       <div class="row text-center">
                           <div class="col-md-12"><h6 class="p-l-30">Paparan penyata : <span class="m-r-20"></span>
                                 <button type="button" onclick="javascript:loadpcb('current');" class="btn btn-info has-ripple btn-sm"><i class="feather mr-2 icon-calendar"></i>Tahun Taksiran<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>

                                  <button type="button" onclick="javascript:loadpcb('calendar');" class="btn btn-info has-ripple btn-sm"><i class="feather mr-2 icon-calendar"></i>Tahun Kalendar<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
                           </h6>
                           </div>
                       </div>
                       <br>
                        <div class="row" id="pcbdetail">
                        </div>
                </div>

                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    @foreach($apidata as $keys => $data)
                        @if($data)
                        <div class="col-sm-12  task-card  " style="margin-top:15px">
                            <div class="">
                            @if($user->access == '2' OR $user->access == '3')
                                <span><b><p>{{$keys}}</p></b></span> <br> 
                            @else
                                 <span><b><p> INDIVIDU</p></b></span> <br> 
                            @endif
                                <ul class="list-unstyled task-list">
                                    @foreach($data as $key =>$refund)
                                        <?php $daterefund = new DateTime($refund->tkh); $refdata = 1;?>
                                        <li style="padding-left: 60px !important">
                                           <i class="task-icon feather icon-check bg-c-green linebaricon"></i>
                                            <span style="color: grey !important">
                                            <p class="m-b-5 linebarparaph"><span style="font-size:10px">{{$daterefund->format('d/m/Y')}}</p>
                                            <span class="linebar"></span>
                                            <b class="linebarparaph">RM {{number_format($refund->amt,2,'.',',')}} | 
                                                @if($user->language == 'en')
                                                {{$refund->Keteranganbi}}
                                                @else
                                                {{$refund->Keteranganbm}}
                                                @endif

                                            </span></b>
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    @if($refdata == 0)
                        <div class="">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-sm-12">
                                    <div class="card-body">
                                        <form class="text-center">
                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                            <h5 class="mt-3">@lang('inbox.empty')</h5>
                                            <!-- <p>@lang('inbox.nodata')</p> -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="tab-pane fade" id="cp204" role="tabpanel" aria-labelledby="cp204-tab">
                   
                    @foreach($cp204data as $keys => $data)
                                   
                        @foreach($data as $key =>$datas)
                            <?php 
                                if($datas->asm_yr == date('Y'))
                                {  
                                    $dateskim = new DateTime($datas->PAY_ST_DT);
                                    $startdate = new DateTime($datas->PAY_ST_DT);
                                    $enddate = new DateTime($datas->PAY_END_DT);
                                    $cp204row = 1;
                            ?>

                            <div class="card user-card-full">
                                <div class="row m-l-0 m-r-0">
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <div class="row">
                                                
                                                <div class="col-sm-12">
                                                     <p class="mb-0"><h5>TAHUN TAKSIRAN : {{$datas->asm_yr}}</h5></p>
                                                     <p class="mb-0"><h5>JUMLAH : RM {{number_format($datas->TOTL_AMT,2,'.',',')}}</h5></p>
                                                </div>
                                                
                                                 
                                                <div class="card-body table-border-style" style="padding-right: unset; font-size: 12px !important">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-xs text-center">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="9" style="vertical-align: middle;text-align:left;border: unset;text-transform:unset;color:grey"><h6>JADUAL BAYARAN ANSURAN CP204 {{$datas->C_NAME}}</h6></th>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Bil. Ansuran</th>
                                                                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tarikh Perlu Bayar</th>
                                                                    <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Amaun Ansuran (RM)</th>

                                                                    
                                                                </tr>


                                                            </thead>
                                                            <tbody>
                                                                <?php 

                                                                    if($datas->bil_ans > 1)
                                                                    {
                                                                        for ($x = 1; $x <= $datas->bil_ans-1; $x++) 
                                                                        {

                                                                ?>


                                                                                <tr>
                                                                                    <td>{{$x}}</td>
                                                                                    <td>{{$startdate->format('d/m/Y')}}</td>
                                                                                    <td>RM {{number_format($datas->MTHLY_INSTAL_AMT,2,'.',',')}}</td>
                                                                                </tr>

                                                                <?php   
                                                                            $startdate->modify('+2 months');
                                                                        }  
                                                                ?>

                                                                        <tr>
                                                                            <td>{{$datas->bil_ans}}</td>
                                                                            <td>{{$enddate->format('d/m/Y')}}</td>
                                                                            <td>RM {{number_format($datas->LAST_INSTAL_AMT,2,'.',',')}}</td>
                                                                        </tr>

                                                                <?php
                                                                    }else
                                                                    {


                                                                    }

                                                                ?>
                                                               
                                                            
                                                               
                                                            </tbody>
                                                        
                                                        </table>

                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php } ?>
                        @endforeach
                                
                    
                    @endforeach

                    @if($cp204row == 0)
                        <div class="">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-sm-12">
                                    <div class="card-body">
                                        <form class="text-center">
                                            <i class="feather icon-check-circle display-3 text-success"></i>
                                            <h5 class="mt-3">@lang('inbox.empty')</h5>
                                            <!-- <p>@lang('inbox.nodata')</p> -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="tab-pane fade" id="cp500" role="tabpanel" aria-labelledby="cp500-tab">
                    @if($cp500data)
                    <div class="">
                        <div class="row m-l-0 m-r-0">
                           
                            <div class="col-sm-12">
                                <div class="card-body">

                                     <?php 

                                             $dateskim = new DateTime($cp500data->SKIM_DATE);
                                             $startdate = new DateTime($cp500data->BASIS_START_DATE);
                                             $enddate = new DateTime($cp500data->BASIS_END_DATE);

                                                                   
                                        ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            @if($surat)
                                                <button type="button" class="btn btn-info has-ripple btn-sm" onClick="printdivsurat('cpsurat');" style="float:right;margin-left: 10px;"><i class="mr-2 feather icon-printer"></i>@lang('homepage.printlabel')<span class="ripple ripple-animate" style="height: 87.7px; width: 87.7px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -18.05px; left: 7.19999px;"></span></button>
                                            @endif
                                            <p class="mb-0"><h6>Tarikh skim : {{$dateskim->format('d M Y')}}</h6></p>
                                            <p class="mb-0"><h6>Tahun taksiran : {{$cp500data->ASSESSMENT_YEAR}}</h6></p>
                                            <p class="mb-0"><h6>Jumlah anggaran : RM {{number_format($cp500data->TOTAL_AMOUNT_CP500,2,'.',',')}}</h6></p>

                                        <br>
                                        <br>
                                        <h6>Jadual bayaran ansuran CP500</h6>
                                       
                                         <br>
                                        </div>
                                         <div class="card-body table-border-style" style="padding-right: unset; font-size: 12px !important">
                                            <div class="table-responsive shadow" style="border-radius: 8px;margin-bottom:30px">
                                                <table class="table table-bordered table-xs text-center table-striped" style="margin-bottom:unset">
                                                    <thead>
                                                        <tr>
                                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Bil. Ansuran</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Tarikh Perlu Bayar</th>
                                                            <th style="vertical-align: middle;text-transform:unset;background: #4680FF;color:white">Amaun Ansuran (RM)</th>

                                                            
                                                        </tr>


                                                    </thead>
                                                    <tbody>
                                                        <?php 

                                                            if($cp500data->BIL_PERLU_BYR > 1)
                                                            {
                                                                for ($x = 1; $x <= $cp500data->BIL_PERLU_BYR-1; $x++) 
                                                                {

                                                        ?>


                                                                        <tr>
                                                                            <td>{{$x}}</td>
                                                                            <td>{{$startdate->format('d/m/Y')}}</td>
                                                                            <td>RM {{number_format($cp500data->MIN_DUE_AMOUNT,2,'.',',')}}</td>
                                                                        </tr>

                                                        <?php   
                                                                    $startdate->modify('+2 months');
                                                                }  
                                                        ?>

                                                                <tr>
                                                                    <td>{{$cp500data->BIL_PERLU_BYR}}</td>
                                                                    <td>{{$enddate->format('d/m/Y')}}</td>
                                                                    <td>RM {{number_format($cp500data->MAX_DUE_AMOUNT,2,'.',',')}}</td>
                                                                </tr>

                                                        <?php
                                                            }else
                                                            {


                                                            }

                                                        ?>
                                                       
                                                    
                                                       
                                                    </tbody>
                                                
                                                </table>

                                            </div>
                                            <br>

                                        </div>
                                           
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                     @else
                    <div class="">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-sm-12">
                                <div class="card-body">
                                    <form class="text-center">
                                        <i class="feather icon-check-circle display-3 text-success"></i>
                                        <h5 class="mt-3">@lang('inbox.empty')</h5>
                                        <!-- <p>@lang('inbox.nodata')</p> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif



                    <div id="cpsurat" style="display:none">
                    </div>
                </div>

                <div class="tab-pane fade" id="spc" role="tabpanel" aria-labelledby="spc-tab">
                    <div class="" style="background-color: transparent">
                        <div class="card-body">
                            <div class="row">
                                @if($spc)
                                <?php $keyactive = 0;?>
                                <div class="col-md-4 col-sm-12">
                                    <ul class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    @foreach($spc['emp'] as $keys =>$dataspc)  
                                        @foreach($dataspc as $key =>$values)
                                            @foreach($values as $keyv =>$value)
                                                <li style="margin-bottom: 10px"><a class="nav-link text-left @if($keyactive == 0) active @endif" id="v-pills-{{str_replace(' ', '', $key)}}-tab" data-toggle="pill" href="#v-pills-{{str_replace(' ', '', $key)}}" role="tab" aria-controls="v-pills-{{$key}}" aria-selected="true" style="font-weight: bolder;font-size: 12px;background-color: #00BFFF;color:white">{{$keyv}}<br><span style="font-size: 12px;color:white"><b>@if($user->language == 'en') Tax No.: @else No Cukai: @endif {{$key}}</b></span></a>
                                                </li>
                                                <?php $keyactive = 1;?>
                                            @endforeach 
                                        @endforeach 
                                    @endforeach 
                                    </ul>
                                </div>
                                <div class="col-md-8 col-sm-12">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        <?php $keyactive = 0;?>
                                        @foreach($spc['emp'] as $keys =>$dataspc)
                                            @foreach($dataspc as $key =>$values)
                                            <div class="tab-pane fade @if($keyactive == 0) show active @endif" id="v-pills-{{str_replace(' ', '', $key)}}" role="tabpanel" aria-labelledby="v-pills-{{str_replace(' ', '', $key)}}-tab">
                                                <div class="accordion" id="accordionExample">
                                                    
                                                        @foreach($values as $keyv =>$value)
                                                        <div class="card mb-0">

                                                            <div class="col-sm-12  task-card card rounded" style="">
                                                                    <div class="card-header">
                                                                        <span style="font-size: 14px;color:grey">@if($user->language == 'en') Tax No.: @else No Cukai: @endif </span><b>{{$user->doc_type}} {{$user->tax_no}}</b>
                                                                    </div>
                                                                    <div class="card-body ">
                                                                       <ul class="list-unstyled task-list">
                                                                            @foreach($value as $key2 =>$data)
                                                                                <li style="padding-left: 60px !important">
                                                                                   <i class="task-icon feather icon-check bg-c-green linebaricon"></i>
                                                                                    <span style="color: grey !important">
                                                                                    <p class="m-b-5 linebarparaph"><span style="font-size:10px">{{date('d/m/Y', strtotime($data['date']))}}</p>
                                                                                    <span class="linebar"></span>
                                                                                    <b class="linebarparaph">{{$data['stat']}} @if($data['Amt'] > 0) (RM {{number_format($data['Amt'],2,'.',',')}}) @endif</span></b>
                                                                                    </span>
                                                                                </li>
                                                                            @endforeach   
                                                                            </ul>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        @endforeach
                                                </div>  
                                            </div>
                                            <?php $keyactive = 1;?>
                                            @endforeach
                                        
                                        @endforeach 
                                    </div>
                                </div>

                                    
                                @else
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <form class="text-center">
                                                <i class="feather icon-check-circle display-3 text-success"></i>
                                                <h5 class="mt-3">@lang('inbox.empty')</h5>
                                                <!-- <p>@lang('inbox.nodata')</p> -->
                                            </form>
                                        </div>
                                    </div>
                                @endif

                                   
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div id="exampleModalCenter" class="modal" aria-labelledby="exampleModalCenterTitle">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content" style="margin:20px">
                <br>
                    <div class="d-flex justify-content-center" style="margin:20px !important"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>
                    <div class="swal-title">Please Wait..</div>
                </div>
            </div>
        </div>
    </div>

</div>
        
@endsection
@push('script')



<script type="text/javascript">


$(document).ready(function () {

    $('#comle').DataTable();
    $('#comck').DataTable();


});

</script>

<script type="text/javascript">
  
  function loadpenutup(lejar,types) {

    $("#exampleModalCenter").modal('hide');
    $('.swal-title').html('Please wait...');
    $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/penutup')}}"+"/"+types+"/"+lejar,
                   
            beforeSend: function () 
            {
                 $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
                  
            },
            success: function(data)
            {       
                  $("#exampleModalCenter").modal('hide');
                  $('#home').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadcom(id,types) {

    $("#exampleModalCenter").modal('hide');
    $('.swal-title').html('Please wait...');
    $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/comdata')}}"+"/"+id+"/"+types,
                   
            beforeSend: function () 
            {
                  $("#exampleModalCenter").modal();
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#home').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadlejar() {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
    
    $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/index')}}",
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal();
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#home').html(data);
                                    
                  $('#comle').DataTable();
                  $('#comck').DataTable();
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadlejarcalendar(year,types) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/calendar')}}"+"/"+year+"/"+types,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal();
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#home').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadlejarcurrent(ltype,year,types) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/current')}}"+"/"+year+"/"+types+"/"+ltype,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#home').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
  
  function loadresit(type,ref,rno,rtype,ltype) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/resit')}}"+"/"+type+"/"+ref+"/"+rno+"/"+rtype+"/"+ltype,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  $('#div_print_resit').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#div_print_resit').html(data);

                  if(data !== '0'){

                        printresit('div_print_resit');
                  }else{

                    alert('no reciept data');
                  }

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  

  


</script>

<script type="text/javascript">
  $("#exampleModalCenter").modal('hide');
  $('.swal-title').html('Please wait...');
  function loadlejarcurrentcom(ltype,year,types,lid) {

     $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/currentcom')}}"+"/"+year+"/"+types+"/"+ltype+"/"+lid,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
            },
            success: function(data)
            {       
                
                  $("#exampleModalCenter").modal('hide');
                  $('#home').html(data);

                  
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
  $("#exampleModalCenter").modal('hide');
  $('.swal-title').html('Please wait...');
  function loadlejarcalendarcom(ltype,year,types,lid) {

     $.ajax({

            type: "GET", 
            url: "{{ URL::to('lejar/calendarcom')}}"+"/"+year+"/"+types+"/"+ltype+"/"+lid,
                   
            beforeSend: function () 
            {
                   $("#exampleModalCenter").modal('show');
                  // $('#home').html('');
            },
            success: function(data)
            {       
                
                  $("#exampleModalCenter").modal('hide');
                  $('#home').html(data);

                  
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>



<script type="text/javascript">
  
  function loadpcb(types) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('pcb/detail')}}"+"/"+types,
                   
            beforeSend: function () 
            {
                 $("#exampleModalCenter").modal('show');
                  // $('#pcbdetail').html('');
                  
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#pcbdetail').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>


<script type="text/javascript">
  
  function loadpcbcal(year) {
$("#exampleModalCenter").modal('hide');
$('.swal-title').html('Please wait...');
     $.ajax({

            type: "GET", 
            url: "{{ URL::to('pcb/calendar')}}"+"/"+year,
                   
            beforeSend: function () 
            {
                  $("#exampleModalCenter").modal('show');
                  // $('#pcbdetail').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#pcbdetail').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
  $("#exampleModalCenter").modal('hide');
  $('.swal-title').html('Please wait...');
  function loadpcbyear(year) {

     $.ajax({

            type: "GET", 
            url: "{{ URL::to('pcb/year')}}"+"/"+year,
                   
            beforeSend: function () 
            {
                  $("#exampleModalCenter").modal('show');
                  // $('#pcbdetail').html('');
            },
            success: function(data)
            {       
                // alert(data);
                  // document.getElementById('home').html(data);
                  $("#exampleModalCenter").modal('hide');
                  $('#pcbdetail').html(data);

                  
                  // document.getElementById("nod").value = data;
                 
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "{{ route('home.device')}}";
              }


        });




  };  


</script>

<script type="text/javascript">
function printdiv(printpage)
{


// var oldstr = document.body.innerHTML;
// var el = document.getElementById("body1");
// el.classList.remove("on-the-fly-behavior");
// el.classList.add("print-fly");
// el.style.fontSize = "10px !important;background-color:white !important";

document.getElementById(printpage).style.width = "100%";
document.getElementById("tableprint").style.width = "100%";
document.getElementById("tableprint").style.fontSize = "10px";
var headstr = "<html><head><title></title></head><body style='background-color: white;font-size:10px;margin-left:50px;margin-right:50px'>";
var footstr = "</body>";
var newstr = document.all.item(printpage).innerHTML;

var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
winPrint.document.write(headstr+newstr+footstr);
winPrint.document.close();
winPrint.focus();
setTimeout(function()
{
  winPrint.print();
  winPrint.close(); 

}, 1000);

}


</script>

<script type="text/javascript">
function printresit(printpage)
  {

      

        document.getElementById("tableprint").style.width = "100%";
        document.getElementById("tableprint2").style.width = "100%";

        var headstr = "<html><head><title></title></head><body style='background-color:white;margin-left:20px;margin-right:20px'>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;

        var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
        winPrint.document.write(headstr+newstr+footstr);
        winPrint.document.close();
        winPrint.focus();
        setTimeout(function()
        {
          winPrint.print();
          winPrint.close(); 

        }, 1000);


        // document.body.innerHTML = headstr+newstr+footstr;
        // window.print();
        // el.classList.remove("print-flys");
        // el.classList.add("on-the-fly-behavior");
        // document.body.innerHTML = oldstr;
        // return false;
  }
</script>

<script type="text/javascript">
function printdivsurat(printpage)
{


    $("#exampleModalCenter").modal('hide');
    $.ajax({

        type: "GET", 
        url: "{{ URL::to('cp500/surat')}}",
        beforeSend: function () 
        {
              $("#exampleModalCenter").modal('show');
              $('#cpsurat').html('');
        },
               
        
        success: function(data)
        {   
            $("#exampleModalCenter").modal('hide');
            $('#cpsurat').html(data);
            

            var headstr = "<html><head><title></title></head><body style='background-color:white'>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;

            var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
            winPrint.document.write(headstr+newstr+footstr);
            winPrint.document.close();
            winPrint.focus();
            setTimeout(function()
            {
              winPrint.print();
              winPrint.close(); 

            }, 1000);
            
             
        },
        error: function (xhr, ajaxOptions, thrownError) {
            window.location.href = "{{ route('home.device')}}";
          }

    });

}





</script>

<script type="text/javascript">
function printsurat(printpage)
  {

        var oldstr = document.body.innerHTML;
        var el = document.getElementById("body1");
        el.classList.remove("on-the-fly-behavior");
        el.classList.add("print-flys");

        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;


        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        el.classList.remove("print-flys");
        el.classList.add("on-the-fly-behavior");
        document.body.innerHTML = oldstr;
        $('#cpsurat').html();
        return false;
  }
</script>


@endpush
