@extends('layout.dashm')
@section('title', 'Mobile-1')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/charts-c3/plugin.css')}}" />
<link rel="stylesheet" href="{{asset('assets/plugins/morrisjs/morris.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/plugins/swiper.min.css')}}" />

    

@stop

@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card mcard_2" style="margin-bottom: -10px;">
            <span class="text-muted" style="padding-left: 120px"></span>
                <ul class="nav nav-tabs p-0 nav-tabs-warning shadow fixed-bottom nav-fill" style="background-color:#2132b0;height: 40px;">
                    <li class="nav-item" data-toggle="tooltip" data-placement="top" title="Kembali Ke Halaman Utama">
                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" style="margin-bottom: unset;border: unset !important;color:white"><i class="zmdi zmdi-home" style="font-size: 25px"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" style="margin-bottom: unset;border: unset !important ;color:white"><i class="zmdi zmdi-account" style="font-size: 25px"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#messages_only_icon_title" style="margin-bottom: unset;border: unset !important;color:white"><i class="zmdi zmdi-email" style="font-size: 25px"></i></a></li>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#settings_only_icon_title" style="margin-bottom: unset;border: unset !important;color:white"><i class="zmdi zmdi-settings" style="font-size: 25px"></i></a></li>
                </ul>
            </div>
            <div class="row clearfix" style="height:40px">
            </div>
    </div>
</div>
<div class="card-body shadow" style="padding-right: 4px !important;padding-left: 4px !important;background-color:snow;border-radius: 20px;">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <div class="card user-card user-card-2 shape-center" style="margin-bottom: unset;">
                            <div class="card-header border-0 p-2 pb-0">
                                <div class="cover-img-block">
                                    <h6 class="mb-1 mt-3" style="font-size: 13px">Muhamad Fezrul Fizree Bin Hashim</h6>
                                    <p class="mb-3 text-muted" style="font-size: 13px">INDIVIDU DAN SYARIKAT</p>
                                </div>
                            </div>
                            <div class="card-body pt-0" style="min-height: unset;">
                                <div class="user-about-block text-center">

                                </div>
                                <div class="text-center">
                                    
                                    
                                </div>
                                <hr class="wid-80 b-wid-3 my-4">
                                <div class="row text-center" >
                                    <div class="col text-left">
                                        <h6 class="mb-1" style="font-size: 12px">No Hasil</h6>
                                        <p class="mb-0" style="font-size: 12px">SG 20202020202</p>
                                    </div>
                                    
                                    <div class="col text-right">
                                        <h6 class="mb-1" style="font-size: 12px">Baki Cukai</h6>
                                        <p class="mb-0" style="font-size: 12px">RM 0.00</p>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="alert alert-success" role="alert">
                                <div class="container">
                                    <div class="alert-icon">
                                        <i class="zmdi zmdi-thumb-up"></i>
                                    </div>
                                    <strong>Tahniah!</strong> Tiada Sekatan Untuk Keluar Negara
                                   
                                </div>
                            </div>
                        <br>
                        
                        <h5 class="" style="font-size: 12px">Perkhidmatan Pilihan</h5>
                    
                        <div class="row swiper-container">
                            

                            <div class="swiper-wrapper" style="padding-bottom:10px !important;">
                                <div class="swiper-slide">
                                    <div class="card mcard_3 h-100">
                                        <div class="img" style="background-image: unset !important">
                                            <img src="{{asset('assets/images/edaftar.jpg')}}" class="img-fluid" alt="" style="height: 80px;width: 100%;">
                                        </div>
                                        <div class="body text-left">
                                           
                                            <span class="text-muted" style="font-size: 9px">e-Daftar adalah aplikasi permohonan pendaftaran fail cukai pendapatan untuk pembayar cukai baharu mendapatkan nombor cukai pendapatan</span>
                                            <h6 class="mt-2"><a href="javascript:void(0);" title="">e-Daftar</a></h6>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card mcard_3 h-100">
                                        <div class="img" style="background-image: unset !important">
                                            <img src="{{asset('assets/images/e-filing-lhdn-1.jpg')}}" class="img-fluid" alt="" style="height: 80px;width: 100%;">
                                        </div>
                                        <div class="body text-left">
                                           
                                            <span class="text-muted" style="font-size: 9px">Kaedah mengisi dan menghantar Borang Nyata Cukai Pendapatan (BNCP) secara elektronik</span>
                                            <h6 class="mt-2"><a href="javascript:void(0);" title="">e-Filing</a></h6>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="card mcard_3 h-100">
                                        <div class="img" style="background-image: unset !important">
                                            <img src="{{asset('assets/images/jombyrhasil.jpg')}}" class="img-fluid" alt="" style="height: 80px;width: 100%;">
                                        </div>
                                        <div class="body text-left">
                                           
                                            <span class="text-muted" style="font-size: 9px">ByrHASiL adalah aplikasi elektronik untuk pembayaran cukai pendapatan melalui bank-bank yang dilantik</span>
                                            <h6 class="mt-2"><a href="javascript:void(0);" title="">ByrHASiL</a></h6>
                                            
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Data PCB</h5>
                                            <!-- <h6 class="text-white">Daily user</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Data Praisi </h5>
                                            <!-- <h6 class="text-white">Daily user</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Filling CKHT / WHT</h5>
                                            <!-- <h6 class="text-white">Daily user</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                <!--            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div> -->
                        </div>

                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <p class="mb-0">Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four
                            loko
                            farm-to-table
                            craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. accusamus tattooed echo park.</p>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <p class="mb-0">Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed
                            craft beer,
                            iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Lnyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                    </div>
                </div>
            </div>


<div class="row clearfix">
   
</div>

@stop
@section('page-script')
<script src="{{asset('assets/bundles/jvectormap.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/c3.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/index.js')}}"></script>
<script src="{{asset('assets/js/swiper.min.js')}}"></script>
    
<script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 2,
      spaceBetween: 5,
      loop: true, 
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        640: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
      }
    });
  </script>
@stop