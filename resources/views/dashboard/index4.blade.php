<!DOCTYPE html>
<html lang="en">

<head>
    <title>Mobile-2</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
   
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('themes/ablepro/assets/images/favicon.ico')}}" type="image/x-icon">
    
    <!-- vendor css -->
    <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('themes/ablepro/assets/css/plugins/swiper.min.css')}}">
           

</head>
<body class="" style="background-image: url({{asset('themes/ablepro/assets/images/header2.jpg')}});background-size: contain;">
    <!-- [ Pre-loader ] start  style="background-image: url('assets/images/header2.jpg');background-position: right;" -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar menu-light" style="display:none">

    </nav>
    <!-- [ navigation menu ] end -->
    <!-- [ Header ] start -->

    <!-- [ Header ] end -->

        <header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
        
            
                <div class="m-header">
                
                    <a href="#!" class="b-brand">
                        <!-- ========   change your logo hear   ============ -->
                        <!-- <img src="assets/images/lhdnlogos.png" alt="" class="logo">
                        <img src="assets/images/lhdnlogo-icon.png" alt="" class="logo-thumb"> -->
                    </a>
                    <!-- <a href="#!" class="mob-toggler">
                        <i class="feather icon-more-vertical"></i>
                    </a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a href="#!" class="pop-search"><i class="feather icon-search"></i></a>
                            <div class="search-bar">
                                <input type="text" class="form-control border-0 shadow-none" placeholder="Search hear">
                                <button type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li>
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
                                <div class="dropdown-menu dropdown-menu-right notification">
                                    <div class="noti-head">
                                        <h6 class="d-inline-block m-b-0">Notifications</h6>
                                        <div class="float-right">
                                            <a href="#!" class="m-r-10">mark as read</a>
                                            <a href="#!">clear all</a>
                                        </div>
                                    </div>
                                    <ul class="noti-body">
                                        <li class="n-title">
                                            <p class="m-b-0">NEW</p>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>John Doe</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>5 min</span></p>
                                                    <p>New ticket Added</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="n-title">
                                            <p class="m-b-0">EARLIER</p>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>10 min</span></p>
                                                    <p>Prchace New Theme and make payment</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>Sara Soudein</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>12 min</span></p>
                                                    <p>currently login</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="assets/images/user/avatar-2.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                                    <p>Prchace New Theme and make payment</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="noti-footer">
                                        <a href="#!">show all</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown drp-user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-user"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-notification">
                                    <div class="pro-head">
                                        <img src="assets/images/user/avatar-1.jpg" class="img-radius" alt="User-Profile-Image">
                                        <span>John Doe</span>
                                        <a href="auth-signin.html" class="dud-logout" title="Logout">
                                            <i class="feather icon-log-out"></i>
                                        </a>
                                    </div>
                                    <ul class="pro-body">
                                        <li><a href="user-profile.html" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
                                        <li><a href="email_inbox.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>
                                        <li><a href="auth-signin.html" class="dropdown-item"><i class="feather icon-lock"></i> Lock Screen</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                
            
    </header>
    

<!-- [ Main Content ] start -->
<div class="pcoded-main-container" style="margin-left: unset !important">
    <div class="pcoded-content" style="margin-top: 80px">
        <!-- [ breadcrumb ] start -->
        <div class="page-header " style="font-size: 12px;position: sticky;">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10" style="font-size: 12px">Selamat Datang, Fezrul</h5>
                        </div>
                        <ul class="breadcrumb" >
                            <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item" ><a href="#!" style="font-size: 12px">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->

        <div class="card" style="background-color: transparent !important;margin:10px;box-shadow: unset;margin-right: -5px;margin-left: -5px;">
            <div class="card-header fixed-top shadow" style="background-color: white;border-bottom:unset !important;border-radius: calc(0.25rem - 0px) calc(0.25rem - 0px) calc(0.25rem - 0px) calc(0.25rem - 0px);padding: 7px !important;position: sticky;">
                <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist" style="font-size: 10px;border:unset !important">
                    <li class="nav-item" ><i class="feather icon-home f-28 text-c-blue" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"></i>
                        <a class="nav-link active text-uppercase" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" style="background-size: unset !important;background-image: unset !important">Home</a>
                    </li>
                    <li class="nav-item"><i class="feather icon-mail f-28 text-c-blue social-icon" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"></i>
                        <a class="nav-link text-uppercase" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" style="background-size: unset !important;background-image: unset !important">My Inbox</a>
                    </li>
                    <li class="nav-item"><i class="feather icon-package f-28 text-c-blue" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"></i>
                        <a class="nav-link text-uppercase" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false" style="background-size: unset !important;background-image: unset !important">My Profile</a>
                    </li>
                    <li class="nav-item"><i class="feather icon-package f-28 text-c-blue" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact2" aria-selected="false"></i>
                        <a class="nav-link text-uppercase" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact2" aria-selected="false" style="background-size: unset !important;background-image: unset !important">My Apps</a>
                    </li>

                </ul>
            </div>
            <div class="card-body" style="padding-right: 4px !important;padding-left: 4px !important;">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <div class="card user-card user-card-2 shape-center">
                            <div class="card-header border-0 p-2 pb-0">
                                <div class="cover-img-block">
                                    <img src="{{asset('themes/ablepro/assets/images/lhdn-malaysia.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                <div class="user-about-block text-center">

                                </div>
                                <div class="text-center">
                                    <h6 class="mb-1 mt-3" style="font-size: 13px">Muhamad Fezrul Fizree Bin Hashim</h6>
                                    <p class="mb-3 text-muted" style="font-size: 13px">INDIVIDU DAN SYARIKAT</p>
                                    
                                </div>
                                <hr class="wid-80 b-wid-3 my-4">
                                <div class="row text-center">
                                    <div class="col text-left">
                                        <h6 class="mb-1" style="font-size: 12px">No Hasil</h6>
                                        <p class="mb-0" style="font-size: 12px">SG 20202020202</p>
                                    </div>
                                    
                                    <div class="col text-right">
                                        <h6 class="mb-1" style="font-size: 12px">Baki Cukai</h6>
                                        <p class="mb-0" style="font-size: 12px">RM 0.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h5 class="" style="font-size: 12px">Perkhidmatan Pilihan</h5>
                    
                        <div class="row swiper-container">
                            

                            <div class="swiper-wrapper" style="padding-bottom:10px !important;">
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Filing</h5>
                                            <!-- <h6 class="text-white" style="font-size: 12px !important">Kaedah mengisi dan menghantar Borang Nyata Cukai Pendapatan (BNCP) secara elektronik</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">ByrHASiL</h5>
                                           <!--  <h6 class="text-white" style="font-size: 12px !important">ByrHASiL adalah aplikasi elektronik untuk pembayaran cukai pendapatan melalui bank-bank yang dilantik</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Daftar</h5>
                                           <!--  <h6 class="text-white" style="font-size: 12px !important">e-Daftar adalah aplikasi permohonan pendaftaran fail cukai pendapatan untuk pembayar cukai baharu mendapatkan nombor cukai pendapatan </h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Data PCB</h5>
                                            <!-- <h6 class="text-white">Daily user</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Data Praisi </h5>
                                            <!-- <h6 class="text-white">Daily user</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="card bg-info text-white widget-visitor-card h-100">
                                        <div class="card-body text-center">
                                            <h5 class="text-white" style="font-size: 11px !important">e-Filling CKHT / WHT</h5>
                                            <!-- <h6 class="text-white">Daily user</h6> -->
                                            <i class="feather icon-package"></i>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                <!--            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div> -->
                        </div>

                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                         <p class="mb-0">tab 2</p>
                    </div>
                   
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                     
                        <div class="card-body">
                            <div class="text-center">
                                <img src="{{asset('themes/ablepro/assets/images/pages/interview.svg')}}" alt="image" class="img-fluid wid-100 mb-2">
                                <h5>Maklumat Asas<br></h5>
                                <br>
                            </div>
                            <div class="table-responsive" style="font-size: 12px;">
                                <table class="table table-sm mb-0">
                                    <tbody>
                                        <tr>
                                            <td>Nama</td>
                                            <td class="text-wrap"> : Mumbai</td>
                                        </tr>
                                        <tr>
                                            <td>No. Rujukan</td>
                                            <td> : 60% Minimum</td>
                                        </tr>
                                        <tr>
                                            <td>No. Cukai Pendapatan</td>
                                            <td> : Max 10000</td>
                                        </tr>
                                        <tr>
                                            <td>No. Telefon</td>
                                            <td> : 2.5 Lacs</td>
                                        </tr>
                                        <tr>
                                            <td>No. Telefon Bimbit</td>
                                            <td class="text-wrap"> : 30 Android Developer</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat Email</td>
                                            <td  class="text-wrap"> : IIRC</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td class="text-wrap"> : 06/11/2016 to 06/11/2016</td>
                                        </tr>
                                         <tr>
                                             <td></td>
                                              <td></td>
                                        </tr>
                                        <tr>
                                             <td></td>
                                              <td></td>
                                        </tr>
                                       
                                    </tbody>
                                </table>

                            </div>
                          
                        </div>
                          <div class="card user-card2 overflow-hidden" style="font-size: 12px;">
                            <div class="card-body text-center">
                                <h6 class="m-b-15">Status Sijil</h6>
                                <div class="risk-rate">
                                    <span><b><i class="feather icon-check-circle"></i></b></span>
                                </div>
                                <h3 class="m-b-20 m-t-15">SAH</h3>
                                <div class="row justify-content-center m-t-10 b-t-default m-l-0 m-r-0">
                                    <div class="col m-t-15 b-r-default">
                                        <h6 class="">Tempoh sah sijil</h6>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-success btn-block">23/1/2020 11:14:05 AM Hingga 23/1/2022 11:14:05 AM</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
                        <p class="mb-0">tab 4</p>
                    </div>
                </div>
            </div>
        </div>
           
            <!-- Latest Customers end -->




        <!-- [ Main Content ] end -->
    </div>
</div>
<!-- Button trigger modal -->


    <!-- Warning Section Ends -->

    <!-- Required Js -->
    <script src="{{asset('themes/ablepro/assets/js/vendor-all.min.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/plugins/bootstrap.min.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/ripple.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/pcoded.min.js')}}"></script>
    <script src="{{asset('themes/ablepro/assets/js/plugins/swiper.min.js')}}"></script>


<!-- Apex Chart -->
<script src="{{asset('themes/ablepro/assets/js/plugins/apexcharts.min.js')}}"></script>
<!-- custom-chart js -->
<script src="{{asset('themes/ablepro/assets/js/pages/dashboard-main.js')}}"></script>

<script type="text/javascript">


$(document).ready(function () {


});



    

</script>

<script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 3,
      spaceBetween: 5,
      loop: true, 
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        640: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
      }
    });
  </script>


</body>

</html>
