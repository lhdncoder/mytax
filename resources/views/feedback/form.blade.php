@extends('ui::ablepro.dashboardfeedback')

@section('content')
<style type="text/css">
.input-hidden {
  position: absolute;
  left: -9999px;
}

input[type=radio]:checked + label>img {
  border: 5px solid #fff;
  box-shadow: 0 0 3px 3px #0B6FF7;
}

/* Stuff after this is only to make things more pretty */
input[type=radio] + label>img {
  border: 1px;
  width: 150px;
  height: 150px;
  transition: 500ms all;
}

input[type=radio]:checked + label>img {
  transform: 
    rotateZ(-10deg) 
    rotateX(10deg);
}
 #back{

   width:100%; 
   height:50%; 
   /*background:url('{{ public_path('images/Transparent_Logo.png') }}');*/
   background-image: url('{{asset('packages/threef/entree/img/Transparent_Logo.png')}}');
   background-position: center;
   background-repeat: no-repeat;
  /*background-attachment:fixed;*/
      
  }
label{
   display:inline-block;
}
label img{
 pointer-events: none;
 -moz-user-select: -moz-none; // to remove blue color highlight in which we 
   //get on clicking image in firefox.
}
   
</style>
{!! SemanticForm::post(route('feedback.svfeedback'))->attribute('id', 'addfaq') !!}
<div class="col-md-12" align="center">
<div  class="col-md-10" align="center">
<div class="card">
  <div class="card-header">
    <h5>@lang('form.titlefeedback')</h5>
  </div>
<div class="table-responsive">

<h2>@lang('form.msglogout')</h2>
<h7>@lang('form.msgfeedback')</h7>
<br>
<br>
@foreach($arr as $key=>$value)
<br>
@if($user->language == 'en')
<h5>{{$value['quest_en']}}</h5>
 <input type="text" class="input-hidden" id="quest[]" name="quest[]" value="{{$value['id']}}">
@else
<h5>{{$value['quest_bm']}}</h5>
 <input type="text" class="input-hidden" id="quest[]" name="quest[]" value="{{$value['id']}}">
@endif
<table border="0">
<tr>
  @foreach($value['detail_pic'] as $keys => $list)
  <td align="center">
    <input class="input-hidden" type="radio" name="emotion[{{$list->fk_lkp_question_feedback}}]" id="emotion[{{$list->id}}]" value="{{$list->id}}">
  <label for="emotion[{{$list->id}}]">
    <?php $path=$list->full_path.$list->file_name?>
  <img 
    src="{{asset($path)}}"
    alt="I'm sad" width="100" height="100"/>
  </label>
  </td>
  <td>
    &nbsp;
  </td>
@endforeach
</tr>

<tr>
  @foreach($value['detail_answ'] as $keys => $list_answ)
  @if($user->language == 'en')
  <td align="center" style="width:100px;"><b>{{$list_answ->answer_en}}<b>
  @else
   <td align="center" style="width:100px;"><b>{{$list_answ->answer_bm}}<b>
  @endif
  </td>
  <td>&nbsp;</td>
 @endforeach
</tr>

</table>

@endforeach
<br>

</div>
<h5>@lang('form.feedback')</h5>
<div class="col-md-12">
&nbsp;
</div>
<div class="col-md-12">
<div class="col-md-6">
<textarea placeholder="Ulasan" style="height: calc(1.5em + 1.25rem + 2px);" class="form-control" id="remarks" name="remarks"></textarea>
</div>
<div class="col-md-12">
<h>@lang('form.feedbackthanks')</h5></h>
</div>
<div class="row">       
<div class="col-md-9 text-right">
 <a href="/" class="btn btn-primary btn-sm">@lang('form.out')</a>
 <button class="btn btn-primary btn-sm" type="submit" id="hantar" name="hantar">   
 @lang('form.send')
</button>
<div class="col-md-12">
&nbsp;
</div>
</div>
</div>

 <!---  start editanswmodal-->
<div class="modal fade bd-example-modal-lg" id= "editanswmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
  <div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  <h6 class="modal-title h6" id="myLargeModalLabel"> <b>@lang('form.validremarks')</b></h6>
                  </div>
              </div>
          </div>
      </div>
<!---  end editanswmodal-->
{!! Form::close() !!}
</div>
</div>
</div>
</div>
@stop

@push('script')
<script>
$(document).ready(function() {





 $('#hantar').click(function () {


  var inps = document.getElementsByName('quest[]');
    for (var i = 0; i <inps.length; i++) {
        var inp=inps[i];
        //alert("quest["+i+"].value="+inp.value);

       if (!$("input[name='emotion["+inp.value+"]']:checked").val()) {
        $('#editanswmodal').modal('toggle');
         return false;
    }
    else {
      return true;
    }

       

        // if ($('input[name=emotion['+inp.value+']]:checked').length > 0) {
        // alert('please');
        // break;
        //     }
      }    

     //return false;

  });

           $('#addfaqxx').validate({
            ignore: '.ignore, .select2-input',
            focusInvalid: false,
            rules: {
                'emotion': {
                    required: true,
                },
                //  'remarks': {
                //     required: true,
                // },
               

            },

            // Errors //

            errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function(element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function(element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });


 });
</script>



@endpush