<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title-index' => 'Ringkasan baki mengikut jenis lejar',
    
    'table-header-bil' => 'Bil',
    'table-header-type' => 'Jenis Lejar',
    'table-header-sum' => 'Ringkasan Baki',
    'table-header-bal' => 'Baki Cukai',
    'table-header-unused' => 'Bayaran Belum Boleh Guna',
    'table-header-lej' => 'Baki Lejar',

    'table-income' => 'Cukai Pendapatan',
    'table-ckht' => 'Cukai Keuntungan Harta Tanah',

    'table-nocom' => 'Tiada Maklumat Syarikat',
    'table-com' => 'Ada Maklumat Syarikat',

    'table-record' => 'Tiada Rekod',

    'note' => 'Nota',
    'note-1' => '<b>1.  Baki Cukai</b> = Jumlah Tunggakan cukai/-Lebihan Bayaran cukai, selepas mengambilkira <b>Taksiran & Lain-Lain</b> dan <b>Bayaran & Lain-Lain</b> bagi tahun taksiran yang sama. Amaun ini belum mengambilkira bayaran, kenaikan yang layak dikenakan atau taksiran yang dibangkitkan selepas tarikh kemaskini lejar, jika ada.',
    'note-2' => '<b>2.  Bayaran Belum Boleh Guna</b> = Bayaran cukai seperti bayaran PCB/CP204. Bayaran ini akan ditolak dengan Taksiran Cukai apabila taksiran dibangkitkan/disifatkan.',
    'note-3' => '<b>3.  Baki Lejar</b> = Baki di lejar pembayar cukai yang merangkumi jumlah <b>Baki Cukai<sup>1</sup></b> dan <b>Bayaran Belum Boleh Guna<sup>2</sup></b>',


    'title-penutup' => 'Jenis Lejar',
    'title-calview' => 'Paparan lejar terperinci mengikut tahun kalendar',
    'title-sum-current' => 'Ringkasan baki mengikut tahun taksiran (Kedudukan sehingga',

    'table-penutup-col1' => 'Ringkasan Baki',
    'table-penutup-col2' => 'Taksiran',
    'table-penutup-col3' => 'Taksiran & <p style="margin-bottom:2px">Lain-lain',
    'table-penutup-col4' => 'Bayaran & <p style="margin-bottom:2px">Lain-lain',
    'table-penutup-col5' => 'Baki',
    'table-penutup-col6' => 'Bayaran Belum <p style="margin-bottom:2px">Boleh Guna',
    'table-penutup-col7' => 'Lebihan Bayaran <p style="margin-bottom:2px">Baki Cukai',
    'table-penutup-total' => 'Jumlah',

    'note-penutup-1' => '<b>1.  Taksiran & Lain-Lain</b> = Taksiran cukai/kenaikan cukai/bayaran balik/pelarasan dan lain-lain.<br>',
    'note-penutup-2' => '<b>2.  Bayaran & Lain-Lain</b> = Bayaran cukai/pengurangan cukai/pelarasan dan lain-lain.<br>',
    'note-penutup-3' => '<b>3.  Baki</b> = Perbezaan antara <b>Taksiran & Lain-Lain<sup>1</sup></b> dan <b>Bayaran & Lain-Lain<sup>2</sup></b>.<br>',
    'note-penutup-4' => '<b>4.  Ringkasan Baki</b> = Butiran terperinci kedudukan <b>Baki<sup>3</sup></b> yang terdiri daripada <b>Bayaran Belum Boleh Guna<sup>5</sup></b> dan/atau <b>Lebihan Bayaran/Baki Cukai<sup>6</sup></b>.<br>',
    'note-penutup-5' => '<b>5.  Bayaran Belum Boleh Guna</b> = Bayaran cukai seperti bayaran PCB/CP204. Bayaran ini akan ditolak dengan Taksiran Cukai apabila taksiran dibangkitkan/disifatkan.<br>',
    'note-penutup-6' => '<b>6.  Lebihan Bayaran/Baki Cukai</b> = Jumlah Tunggakan cukai/-Lebihan Bayaran cukai, selepas mengambilkira <b>Taksiran & Lain-Lain<sup>1</sup></b> dan <b>Bayaran & Lain-Lain<sup>2</sup></b> bagi tahun <br>taksiran yang sama. Amaun ini belum mengambilkira bayaran, kenaikan yang layak dikenakan atau taksiran yang dibangkitkan selepas tarikh kemaskini lejar, jika ada.',


    'title-calendar' => 'Jenis Lejar',
    'title-calendar-year-1' => "Lejar",
    'title-calendar-year-2' => "bagi tahun kalendar",
    'title-calendar-current' => '(Kedudukan sehingga',

    'table-calendar-col1' => 'Tarikh<p style="margin-bottom:2px">Transaksi</p>',
    'table-calendar-col2' => 'Kod',
    'table-calendar-col3' => 'Keterangan<p style="margin-bottom:2px">Transaksi</p>',
    'table-calendar-col4' => 'Rujukan/<p style="margin-bottom:2px">No. Resit</p>',
    'table-calendar-col5' => 'Tahun<p style="margin-bottom:2px">Taksiran</p>',
    'table-calendar-col6' => 'Bulan/<p style="margin-bottom:2px">Bil Ansuran</p>',
    'table-calendar-col7' => 'Taksiran & <p style="margin-bottom:2px">Lain-lain<sup>1</sup></p>',
    'table-calendar-col8' => 'Bayaran & <p style="margin-bottom:2px">Lain-lain<sup>2</sup></p>',
    'table-calendar-col9' => 'Baki (RM)<sup>3</sup>',

    'table-calendar-col10' => 'Ringkasan Baki',
    'table-calendar-col11-o' => 'Baki Cukai<sup>5</sup> Tertunggak',
    'table-calendar-col11-r' => 'Baki Cukai<sup>5</sup> Boleh Dibayar Balik',
    'table-calendar-col12' => 'Bayaran Belum Boleh Guna<sup>6</sup>',
    'table-calendar-col13' => 'Baki Lejar',

    'note-calendar-1' => '<b>1. Taksiran & lain-lain </b>=  Cukai dibangkitkan/kenaikan cukai/bayaran balik dan lain-lain<br>',
    'note-calendar-2' => '<b>2. Bayaran & Lain-lain </b>=  Transaksi yang mengurangkan <b>Taksiran & Lain-Lain<sup>1</sup> </b>. Contoh bayaran PCB, bayaran ansuran dan pelarasan/pengurangan cukai.<br>',
    'note-calendar-3' => '<b>3. Baki </b> = Perbezaan antara <b>Taksiran & Lain-Lain<sup>1</sup></b> dan <b>Bayaran & Lain-lain<sup>2</sup>.</b><br>',
    'note-calendar-4' => '<b>4. Ringkasan Baki </b> = Butiran Terperinci kedudukan <b>Baki<sup>3</sup></b> yang terdiri daripada <b>Belum Boleh Guna<sup>5</sup></b> dan/atau <b>Baki Cukai<sup>6</sup></b><br>',
    'note-calendar-5' => '<b>5. Baki cukai </b> = Jumlah Tunggakan Cukai/-Lebihan Bayaran Cukai, selepas mengambilkira <b>Taksiran & lain-Lain<sup>1</sup></b> dan <b>Bayaran Cukai<sup>2</sup></b> bagi tahun taksiran yang sama.<br>',
    'note-calendar-6' => '<b>6. Bayaran Belum Boleh Guna </b> = Bayaran yang telah dibuat oleh pembayar cukai seperti bayaran PCB atau bayaran ansuran.<br>Bayaran ini akan ditolak dengan baki cukai apabila taksiran dibangkitkan/disifatkan.',


    'title-current' => 'Jenis Lejar',
    'title-current-year-1' => "Lejar",
    'title-current-year-2' => "bagi tahun taksiran",
    'title-current-current' => '(Kedudukan sehingga',
    'table-current-col0' => 'Tarikh<p style="margin-bottom:2px">Kemaskini</p>',
    'table-current-col1' => 'Tarikh<p style="margin-bottom:2px">Transaksi</p>',
    'table-current-col2' => 'Kod',
    'table-current-col3' => 'Keterangan<p style="margin-bottom:2px">Transaksi</p>',
    'table-current-col4' => 'Rujukan/<p style="margin-bottom:2px">No. Resit</p>',
    'table-current-col5' => 'Tahun<p style="margin-bottom:2px">Taksiran</p>',
    'table-current-col6' => 'Bulan/<p style="margin-bottom:2px">Bil Ansuran</p>',
    'table-current-col7' => 'Taksiran & <p style="margin-bottom:2px">Lain-lain<sup>1</sup></p>',
    'table-current-col8' => 'Bayaran & <p style="margin-bottom:2px">Lain-lain<sup>2</sup></p>',
    'table-current-col9' => 'Baki (RM)<sup>3</sup>',

    'table-current-col10' => 'Ringkasan Baki',
    'table-current-col11-o' => 'Baki Cukai<sup>5</sup> Tertunggak',
    'table-current-col11-r' => 'Baki Cukai<sup>5</sup> Boleh Dibayar Balik',
    'table-current-col12' => 'Bayaran Belum Boleh Guna<sup>6</sup>',
    'table-current-col13' => 'Baki Lejar',

    'note-current-1' => '<b>1. Taksiran & lain-lain </b>=  Cukai dibangkitkan/kenaikan cukai/bayaran balik dan lain-lain<br>',
    'note-current-2' => '<b>2. Bayaran & Lain-lain </b>=  Transaksi yang mengurangkan <b>Taksiran & Lain-Lain<sup>1</sup> </b>. Contoh bayaran PCB, bayaran ansuran dan pelarasan/pengurangan cukai.<br>',
    'note-current-3' => '<b>3. Baki </b> = Perbezaan antara <b>Taksiran & Lain-Lain<sup>1</sup></b> dan <b>Bayaran & Lain-lain<sup>2</sup>.</b><br>',
    'note-current-4' => '<b>4. Ringkasan Baki </b> = Butiran Terperinci kedudukan <b>Baki<sup>3</sup></b> yang terdiri daripada <b>Belum Boleh Guna<sup>5</sup></b> dan/atau <b>Baki Cukai<sup>6</sup></b><br>',
    'note-current-5' => '<b>5. Baki cukai </b> = Jumlah Tunggakan Cukai/-Lebihan Bayaran Cukai, selepas mengambilkira <b>Taksiran & lain-Lain<sup>1</sup></b> dan <b>Bayaran Cukai<sup>2</sup></b> bagi tahun taksiran yang sama.<br>',
    'note-current-6' => '<b>6. Bayaran Belum Boleh Guna </b> = Bayaran yang telah dibuat oleh pembayar cukai seperti bayaran PCB atau bayaran ansuran.<br>Bayaran ini akan ditolak dengan baki cukai apabila taksiran dibangkitkan/disifatkan.',

    'comtable-col1' => 'Nama Syarikat',
    'comtable-col2' => 'Jenis Fail',
    'comtable-col3' => 'No. Rujukan',
    'comtable-col4' => 'No. ROC',
    'comtable-col5' => 'Tarikh Daftar',
    

    'com-penutup-label1' => 'Nama Syarikat',
    'com-penutup-label2' => 'No. Rujukan',
    'com-penutup-label3' => 'Jenis Lejar',
    'com-penutup-label4' => 'Caw. Taksiran',
    'com-penutup-label5' => 'Caw. Pungutan',
    'com-penutup-label6' => 'Nama Bank',
    'com-penutup-label7' => 'No. Akaun',
    

        
        
        
        
        










];
