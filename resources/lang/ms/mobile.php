<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'label-name' => 'Nama',
    'label-ic' => 'No. Pengenalan',
    'label-taxno' => 'Nombor Cukai Pendapatan',
    'label-phone' => 'Nombor Telefon',
    'label-mphone' => "Nombor Telefon Bimbit",
    'label-email' => 'Alamat Emel',
    'label-address' => 'Alamat',

    'label-certstatus' => 'Status Sijil',
    'label-certperiod' => 'Tempoh sah sijil',
    'label-range' => 'Sehingga',
    'label-certvalid' => 'SAH',
    'label-certINVALID' => 'TAMAT',

    'label-lejarindividu' => 'Individu',
    'label-lejarsyarikat' => 'Syarikat',

    'label-lejarsalary' => 'Cukai Pendapatan',
    'label-lejarckht' => 'Cukai Keuntungan Harta Tanah',

    'label-lejarbaki' => 'Baki Cukai',
    'label-lejarguna' => 'Bayaran Belum Boleh Guna',
    'label-lejarbalance' => 'Baki Lejar',
];
