<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title-infos' => 'Maklumat Asas',
    'title-certs' => 'Status Sijil Digital',


    'cert1' => 'Sijil Digital Organisasi',
    'cert2' => 'Sijil Digital Individu',
    'cert3' => 'Sijil Digital Pentadbir',

    'cert-status' => 'Tempoh Sah Sijil Digital',

];
