<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'label-name' => 'Name',
    'label-ic' => 'Identification No.',
    'label-taxno' => 'Tax Number',
    'label-phone' => 'Phone Number',
    'label-mphone' => "Mobile Phone Number",
    'label-email' => 'Email',
    'label-address' => 'Address',

    'label-certstatus' => 'Certificate Status',
    'label-certperiod' => 'validity of certificate',
    'label-range' => 'Until',
    'label-certvalid' => 'VALID',
    'label-certINVALID' => 'EXPIRED',

    'label-lejarindividu' => 'Personal',
    'label-lejarsyarikat' => 'Company',

    'label-lejarsalary' => 'Income tax',
    'label-lejarckht' => 'Real Property Profit Tax',

    'label-lejarbaki' => 'Tax Balance',
    'label-lejarguna' => 'Pending Payment',
    'label-lejarbalance' => 'Ledger Balance',

];
