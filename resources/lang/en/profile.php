<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title-infos' => 'Basic Information',
    'title-certs' => 'Digital Certificate Status',

    'cert1' => 'Organization Digital Certificate',
    'cert2' => 'Individual Digital Certificate',
    'cert3' => 'Administrator Digital Certificate',

    'cert-status' => 'Digital Certificate Validation Period',


];
