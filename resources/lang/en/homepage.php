<?php

return [

    'wellcome' => 'Welcome To MyTax',
    'maskhead' => 'Individual Tax Dashboard Summary',
    'contacts'  => 'Contact Us',
    'contactstitle'  => 'More Help',
    'contactsbody'  => 'Do you need help or guide ?',
    'main_menu' => 'Main Menu',
    'faq' => 'Frequency Answered Question (FAQ)',
    'faqmenu' => 'FAQ',
    'announcement' => 'Announcements',
    'all' => 'Show All',
    'new' => 'New',
    'management' => 'Management',
    'application' => 'MyTax Module',
    'mobile' => 'Mobile',
    'feedback' => 'Feedback',
    'help' => 'Help',
    'service' => 'Services',
    'logout' => 'Logout',
    'login' => 'Login',
    'apps' => 'List of Online Services',
    'taxstatus' => 'MyTax Status',
    // 'service'=>'Service',
    'help'=>'Help',
    'reportapp'=>'User Frequency Report',
    'service-section' => 'Favourite Services',
    'service-section-all' => 'All Services',
    'service-fav' => 'Manage Your Favourite',
    'service-fav-note' => 'You can add up to 4 favourite',
    'service-section-add' => 'Add favourite service',
    'inbox-count' => 'Mails',
    'balance1' => 'Overpaid Tax Balance',
    'balance2' => 'Tax Balance',
    'balance3' => 'Tax Balance Unpaid',
    'userman' => 'User Management',

    'lastupdate' => 'Last Updated at',

    'balance12' => 'Tax Payment Excess, after consider Assessment & Others and Payment & Others for the same assessment year. This amount not yet consider payment, eligible tax increase or raised assessment after the date of ledger updated, if any',
    'balance22' => 'Total of Tax Outstanding / Tax Payment Excess, after consider Assessment & Others and Payment & Others for the same assessment year. This amount not yet consider payment, eligible tax increase or raised assessment after the date of ledger updated, if any',
    'balance32' => 'Total of Tax Outstanding, after consider Assessment & Others and Payment & Others for the same assessment year. This amount not yet consider payment, eligible tax increase or raised assessment after the date of ledger updated, if any',

    'refund' => 'Approved Tax Refund For Current Year',
    'restrain' => 'Travel Restrictions Check',
    'restrain-message-false' => 'You have no restriction',
    'restrain-message-true' => 'Opps, You have been blaclisted from travel',
    'profile-message' => 'Personal Info, Certificate..',
    'graph-label' => 'Your contribution As a Taxpayer',
    'template-management' => 'Template Management',
    'template-manage' => 'Template Setting',
    'template-lookup' => 'Template Type Setting',
    'template-manage-edit' => 'Edit Template',
    'template-lookup-edit' => 'Edit Template Type',
    'flip' => 'View / Download',
    'feedbackform'=>'Feedback Form',
    'mobile-tax-tag'=>'Fast And Easy',
    'mobile-tax-line'=>'Your tax information just a click away',

    'mobile-profile'=>'Your Profile',
    'mobile-profile-line'=>'General Information',
    'mobile-app'=>'Mobile HASiL Application',


    'header-inbox' => 'Mailbox',
    'header-profile' => 'Profile',
    'header-tax' => 'MyTax Status',
    

    'mobile-inbox'=>'Your Inbox',
    'mobile-inbox-line'=>'We keep you in touch',
    'back-button'=>' Back',
    'logincont'=>'To proceed, please enter your password and click Send.', 
    'logintitle'=>'Log in to Mytax',

    'loginsinput1'=>'New IC No.',
    'loginsinput2'=>'Passport No.',
    'loginsinput3'=>'Army No.',
    'loginsinput4'=>'Police No.',
    'loginsinput5'=>'IDENTIFICATION NO.',
    'loginsinput6'=>'password',
    'loginfirst'=>'First Time Login',
    'loginforgot'=>'Forgot Password',
    'loginchoice'=>'Or',

    'printlabel'=>'Print',
    'backlabel'=>'Back',
    'sumlabel'=>'Summary',
    'beform'=>'e-Filing Status Form',






    'phrasetext'=>'The below <strong>Security Phrase</strong> is a security measure to ensure that you are login to ezHASiL website. Please verify that the displayed <strong>Security Phrase</strong> is correct before proceed. If not, do not enter your password. ',

    'lejarerror' => 'You currently cannot view your e-lejar information,<br>Please contact the nearest LHDNM branch/<br>Contact Hasil Care Line at 1-800-88-5436 (LHDN)<br>to view your lejar information',


    'graph'=>'Total tax has been <br> paid on ',

    'search'=>'Search',
    'search-button'=>'Search',
    'search-form'=>'Search Form',
    'search-title'=>'You can search by identification number or by registered tax number.',
    'search-title1'=>'Search by identification number',
    'search-title2'=>'Search by registred tax number',

    'search-result'=>'Search Result',
    'search-type'=>'File Type',

];


 